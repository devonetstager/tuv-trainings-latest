<?php

return [
    'facebook' => '<li><a class="fbpg" href=":url" class="social-button :class" id=":id"><span class="fa fa-facebook-official"></span></a></li>',
    'twitter' => '<li><a class="twpg" href=":url" class="social-button :class" id=":id"><span class="fa fa-twitter"></span></a></li>',
    'gplus' => '<li><a href=":url" class="social-button :class" id=":id"><span class="fa fa-google-plus"></span></a></li>',
    'linkedin' => '<li><a class="instapg" href=":url" class="social-button :class" id=":id"><span class="fa fa-linkedin"></span></a></li>',
    'whatsapp' => '<li><a class="instapg" target="_blank" href=":url" class="social-button :class" id=":id"><span class="fa fa-whatsapp"></span></a></li>',
    'pinterest' => '<li><a href=":url" class="social-button :class" id=":id"><span class="fa fa-pinterest"></span></a></li>',
];
