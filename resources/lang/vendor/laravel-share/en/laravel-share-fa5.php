<?php

return [
    'facebook' => '<li><a class="fbpg" href=":url" class="social-button :class" id=":id"><span class="fab fa-facebook-square"></span></a></li>',
    'twitter' => '<li><a class="twpg" href=":url" class="social-button :class" id=":id"><span class="fab fa-twitter"></span></a></li>',
    'gplus' => '<li><a href=":url" class="social-button :class" id=":id"><span class="fab fa-google-plus-g"></span></a></li>',
    'linkedin' => '<li><a class="instapg" href=":url" class="social-button :class" id=":id"><span class="fab fa-linkedin"></span></a></li>',
    'whatsapp' => '<li><a class="instapg" target="_blank" href=":url" class="social-button :class" id=":id"><span class="fab fa-whatsapp"></span></a></li>',
    'pinterest' => '<li><a href=":url" class="social-button :class" id=":id"><span class="fab fa-pinterest"></span></a></li>',
];
