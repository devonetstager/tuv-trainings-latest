@include('frontend.includes.header')

<!-- ****Banner Section**** -->
<section class="innerbanner" style="background: url({{ asset('public/frontend/images/regbanner.jpg') }}) no-repeat center top;">
    <div class="breadcrmb">
        <div class="wid">
            <h2>Registration</h2>
            <div class="brdcrmb">
                <a href="{{route('index')}}">Home</a><span>/</span><small>CIPS Course Registration</small>
            </div>
        </div>
    </div>
</section>


<!-- Inner page content section -->

<section class="contactuss regnow">
    <div class="wid">
            <div class="contfomr">
                    <h1>Course Details</h1>
                    <form method="post" action="{{route('cips.registration.complete')}}">
                        @csrf
                        <ul>
                            <li class="selct">
                                <select name="cips_id" id="course">
                               @foreach($cipscourses as $course)
                                    <option class="category" value="{{$course->cips_id}}" @if($course->cips_id == $selectedcourse->cips_id) selected="selected" @endif>{{$course->name}}</option>
                                    @endforeach 
                                </select>
                            </li>
                        </ul>
                        <span>Available Date & Locations</span>
                        <ul class="halfform">
                                <!-- <li class="selct">
                                    <select>
                                 
                                    </select>
                                </li> -->
                                <li class="selct custom-select">
                                    <select>
                                        <option class="category" value="">Month</option>
                                        <option value="{{date('m',strtotime($selectedcourse->start_date))}}" selected>{{date('F',strtotime($selectedcourse->start_date))}}</option>
                                       
                                      
                                    </select>
                                </li>
                                <li class="selct custom-select">
                                    <select>
                                        <option class="category" value="">City</option>
                                        <option value="{{$selectedcourse->location}}" selected>{{$selectedcourse->location}}</option>
                                       
                                      
                                    </select>
                                </li>
                              
                                    <li class="selct custom-select">
                                        <select>
                                        <option value="{{$selectedcourse->city->city_id}}">{{$selectedcourse->city->name}}</option>
                                       
                                        <!-- <option value="2">Doha.Qatar</option> -->
                                        
                                        </select>
                                    </li>
                                    <li class="rgradio">
                                        <ul>
                                            <li class="regtype">
                                                Register Type
                                            </li>
                                            <li>
                                                <input type="radio" name="pay_option">
                                                <label>Company Paid</label>
                                            </li>
                                            <li>
                                                <input type="radio" name="pay_option"> <label>Self Paid</label>
                                            </li>
                                        </ul>
                                    </li>
                            </ul>
                </div>
        <div class="contfomr">
            <h1>Personal Details</h1>
                <ul>
                    <li>
                         
                        <input type="text" name="name" value="{{old('name')}}"  placeholder="Name">
                    {!! $errors->first('name', '<p style="color:red;" class="help-block error">:message</p>') !!}
                    </li>
                    <li>
                        <input type="email" name="email" value="{{old('email')}}"  placeholder="Email">
                    {!! $errors->first('email', '<p style="color:red;" class="help-block error">:message</p>') !!}

                    </li>
                    <li>
                        <input type="tel" name="mobile" value="{{old('mobile')}}"   placeholder="Phone">
                    {!! $errors->first('mobile', '<p style="color:red;" class="help-block error">:message</p>') !!}                        

                    </li>
                    <li>
                        <input type="text" name="company" value="{{old('company')}}"  placeholder="Company">
                    {!! $errors->first('company', '<p style="color:red;" class="help-block error">:message</p>') !!}                        
                    </li>
                    <li>
                        <input type="text" name="address" value="{{old('address')}}"  placeholder="Address">
                   {!! $errors->first('address', '<p style="color:red;" class="help-block error">:message</p>') !!}
                    </li>
                    <li>
                        <input type="text" name="post" placeholder="P.O Box">
                    </li>
                    <li>
                        <input type="text" name="city" placeholder="City">
                    </li>
                    <li>
                     @php $enquiry_url= Request::fullUrl(); @endphp
                <input type="hidden" name="enquiry_url" value="{{$enquiry_url}}">
                <input type="hidden"  required="required" name="recaptcha" value="" id="recaptcha" >
                        <input type="text" name="country" placeholder="Country">
                    </li>
                </ul>
                <div class="regbuttons">
                        <div class="regbtn2">
                         @php $enquiry_url= Request::fullUrl(); @endphp
               <input type="hidden" name="enquiry_url" value="{{$enquiry_url}}">
                                <input type="submit" value="Register and Pay Later">
                            </div>
                        <!-- <div class="paybtn">
                            <input type="submit" value="Pay Now">
                        </div> -->
                </div>
            </form>
        </div>
    </div>
</section>
<script src="https://www.google.com/recaptcha/api.js?render=6Lfjh7oUAAAAAPZJDpn5AcCMoSz2aghEyvBQ-0Ct"></script>
<script>
grecaptcha.ready(function() {
    grecaptcha.execute('6Lfjh7oUAAAAAPZJDpn5AcCMoSz2aghEyvBQ-0Ct', {action: 'contactus'}).then(function(token) {
    document.getElementById("recaptcha").value = token;
    });
});
</script>   


@include('frontend.includes.footer')