@include('frontend.includes.header')

<!-- ****Banner Section**** -->
<section class="innerbanner" style="background: url({{ asset('public/frontend/images/regbanner.jpg') }}) no-repeat center top;">
    <div class="breadcrmb">
        <div class="wid">
            <h2>Registration</h2>
            <div class="brdcrmb">
                <a href="{{route('index')}}">Home</a><span>/</span><a href="{{route('course-detail',[encrypt($selectedcourse->course_id)])}}">Course</a><span>/</span><small>Registration</small>
            </div>
        </div>
    </div>
</section>


<!-- Inner page content section -->

<section class="contactuss regnow">
    <div class="wid">
            <div class="contfomr">
                    <h1>Course Details</h1>
                    <form method="post" action="{{route('registration.complete')}}">
                        @csrf
                        
                        <ul>

                            <li class="selct ">
                                <select name="category" id="category" class="cat-list">
                                <option class="" value="">Select Category</option>
                                @foreach($categories as $category)
                                <option class="category" value="{{$category->category_id}}" @if($category->category_id == $selectedcourse->category_id) selected="selected" @endif>{{$category->name}}</option>
                                    @endforeach
                                </select >
                            </li>
                            <li class="selct">
                                <select name="course" id="course">
                                @foreach($courses as $course)
                                    <option class="category" value="{{$course->course_id}}" @if($course->course_id == $selectedcourse->course_id) selected="selected" @endif>{{$course->name}}</option>
                                    @endforeach 
                                </select>
                            </li>
                        </ul>
                        <span>Available Dates</span>
                        <ul class="halfform">
                                <li class="selct custom-select">
                                    <select>
                                    <option value="">Select Month</option>
                                    @foreach($selectedcourse->coursedetails->pluck('start_date') as $month)
                                        <option value="{{date('M',strtotime($month))}}">{{date('F',strtotime($month))}}</option>
                                        @endforeach
                                    </select>
                                </li>
                                <li class="selct custom-select">
                                    <select>
                                        <option class="category" value="">City</option>
                                        @foreach($selectedcourse->coursedetails->pluck('city_name')->unique() as $city)
                                        <option value="1" selected>{{$city}}</option>                                       
                                        @endforeach
                                        
                                      
                                    </select>
                                </li>
                                <li class="selct custom-select">
                                        <select>
                                        
                                            <option class="category" value="0">Language</option>
                                            <option value="1">English</option>
                                            <option value="2">Arabic</option>
                                        </select>
                                    </li>
                                    <li class="selct custom-select">
                                        <select>
                                        <option value="">Venue</option>
                                        @foreach($selectedcourse->coursedetails->pluck('location') as $location)
                                        <option value="1" selected>{{$location}}</option> 
                                        @endforeach                               
                                        </select>
                                    </li>
                                    <li class="rgradio">
                                        <ul>
                                            <li class="regtype">
                                                Register Type
                                            </li>
                                            <li>
                                                <input type="radio" name="pay_option">
                                                <label>Company Paid</label>
                                            </li>
                                            <li>
                                                <input type="radio" name="pay_option"> <label>Self Paid</label>
                                            </li>
                                        </ul>
                                    </li>
                            </ul>
                </div>
        <div class="contfomr">
            <h1>Personal Details</h1>
                <ul>
                    <li>
                        <input type="hidden" name="course_id" value="{{$course_id}}">    
                        <input type="text" name="name" value="{{old('name')}}"  placeholder="Name">
                    {!! $errors->first('name', '<p style="color:red;" class="help-block error">:message</p>') !!}
                    </li>
                    <li>
                        <input type="email" name="email" value="{{old('email')}}"  placeholder="Email">
                    {!! $errors->first('email', '<p style="color:red;" class="help-block error">:message</p>') !!}

                    </li>
                    <li>
                        <input type="tel" name="mobile" value="{{old('mobile')}}"   placeholder="Phone">
                    {!! $errors->first('mobile', '<p style="color:red;" class="help-block error">:message</p>') !!}                        

                    </li>
                    <li>
                        <input type="text" name="company" value="{{old('company')}}"  placeholder="Company">
                    {!! $errors->first('company', '<p style="color:red;" class="help-block error">:message</p>') !!}                        
                    </li>
                    <li>
                        <input type="text" name="address" value="{{old('address')}}"  placeholder="Address">
                   {!! $errors->first('address', '<p style="color:red;" class="help-block error">:message</p>') !!}
                    </li>
                    <li>
                        <input type="text" name="post" placeholder="P.O Box">
                    </li>
                    <li>
                        <input type="text" name="city" placeholder="City">
                    </li>
                    <li>
                     @php $enquiry_url= Request::fullUrl(); @endphp
                <input type="hidden" name="enquiry_url" value="{{$enquiry_url}}">
                <input type="hidden"  required="required" name="recaptcha" value="" id="recaptcha" >
                        <input type="text" name="country" placeholder="Country">
                    </li>
                </ul>
                <div class="regbuttons">
                    <div class="regbtn2">
                        <input type="submit" value="Register and Pay Later">
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>

<script>
$( "#category" ).change(function() {
    

    var category_id = $('#category').val();
        if(category_id !=''){
        
            $.ajax({
            url : "{{ route('fetch-course') }}",
            type: "get",
            dataType: "JSON",
            data: {category_id:category_id},
            success: function(data)
            {
                $('#course').html(data);
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error getting data');
            }
        });
        }
});


</script>
<script src="https://www.google.com/recaptcha/api.js?render=6Lfjh7oUAAAAAPZJDpn5AcCMoSz2aghEyvBQ-0Ct"></script>
<script>
grecaptcha.ready(function() {
    grecaptcha.execute('6Lfjh7oUAAAAAPZJDpn5AcCMoSz2aghEyvBQ-0Ct', {action: 'contactus'}).then(function(token) {
    document.getElementById("recaptcha").value = token;
    });
});
</script>

@include('frontend.includes.footer')