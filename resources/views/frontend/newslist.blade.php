@include('frontend.includes.header')

<!-- ****Banner Section**** -->
<section class="innerbanner" style="background: url({{ asset('public/frontend/images/newsbanner.jpg') }}) no-repeat center top;">
    <div class="breadcrmb">
        <div class="wid">
            <h2>Corporate News</h2>
            <div class="brdcrmb">
                <a href="{{route('index')}}">Home</a><Span>/ Media / </Span><small>Corporate News</small>
            </div>
        </div>
    </div>
</section>


<!-- Inner page content section -->
<section class="newssec">
        <div class="wid">
            <ul>@foreach($newsdata as $news)

                <li>
                    <a href="{{route('news-detail',[encrypt($news->news_id)])}}">
                        <div class="newsimg">
                            <img src="{{asset('/storage/app/news/imagethumb')}}/{{$news->image_banner}}">
                        </div>
                        <div class="newdate">
                       
                     {{date('d,F, Y', strtotime($news->news_date))}}
                        </div>
                        <p>
                        {{$news->title}}
                        </p>
                    </a>
                </li>
                @endforeach
             </ul>
        </div>
    </section>
    @include('frontend.includes.footer')