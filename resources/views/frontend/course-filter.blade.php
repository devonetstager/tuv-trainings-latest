@include('frontend.includes.header')
<!-- ****Banner Section**** -->
<section class="innerbanner" style="background: url({{ asset('public/frontend/images/coursebanner.jpg') }}) no-repeat center top;">
   <div class="breadcrmb">
      <div class="wid">
         <h2>Courses</h2>
         <div class="brdcrmb">
            <a href="{{route('index')}}">Home</a><Span>/</Span><small>Courses</small>
         </div>
      </div>
   </div>
</section>
<!-- Inner page content section -->
<section class="innerpg coursedtls">
   <div class="wid">
      <div class="coursecat">
         <div class="secname">
            Select Month
         </div>
         <ul class="crcat" id="month_list">
              @php $segment =Request::segment(2); @endphp
             @for ($i = 0; $i <= 5; $i++)

                <li @if($segment == date("m", strtotime( date( 'Y-m-01' )." +$i months"))) class="act_month"  @endif  id="c_current">
                  <a href = "{{route('course.filter',date("m", strtotime( date( 'Y-m-01' )." +$i months")))}}">
                  {{date(" F ", strtotime( date( 'Y-m-01' )." +$i months"))}} 
                  </a>
               </li>
            @endfor
         </ul>
         </ul>
      </div>
      <div class="abtcnt">
         <div class="courses crseinner">
            <div class="wid">
               <ul>
         @foreach($courses as $course)
         <li>
            <div class="courseimg">
               <a href="{{route('course-detail',[encrypt($course->course_id)])}}" class="crsbtnImg">
               <img src="{{asset('storage/app/courses/courseimage/')}}/{{$course->image}}"></a>
            </div>
            <a href="{{route('course-detail',[encrypt($course->course_id)])}}" class="crsbtn">
            {{str_limit(strip_tags($course->name), 40)}}
            </a>
            <div class="courceLoc">
               @foreach($course->coursedetails as $details)
               <a href="#" class="locIcon">{{str_limit(strip_tags($details->location), 10)}}</a>
               <a href="#" class="timeIcon">{{ date('d', strtotime($details->start_date))}}-{{date('d,M, Y', strtotime($details->end_date))}}</a>
               @endforeach
            </div>
            <a href="{{route('course-registration', [encrypt($course->course_id)])}}" class="smlbtn">
            Register Now
            </a>
         </li>
         @endforeach
      </ul>
            </div>
         </div>
      </div>
   </div>
</section>

 {{-- <script>
    function get_course(event,id){
        event.preventDefault();
      var _token = $('input[name="_token"]').val();
        $.ajax({
          url:"{{ route('course.filter') }}",
          method:"post",
          data:{data:id, _token:_token},
          success:function(data){
             
         
          }
      
        });  
   }
</script> --}}
@include('frontend.includes.footer')