<?php

namespace App\Http\Controllers\Frontend;
use App\Models\Admin\Category;
use App\Models\Admin\Course;
use App\Models\Admin\CourseDetail;
use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeSearchController extends Controller
{
   // Frontend Course Search 
   public function course_search(){
        $data = Course::latest();
        if(request('course')){
            $data->where('name', 'like',"%".request('course')."%");
        }
        if(request('category')){
            $data->where('category_id',request('category'));
        }
        if(request('month')){
            $data->whereHas('coursedetails',function($query){
                $query->whereMonth('start_date',request('month'));
            });
        }
        $results = $data->get();  
        $categories = Category::orderBy('category_id', 'DESC')->get();
        $maincategory = Category::orderBy('name', 'ASC')->get();
       return view('frontend.course-search-result',compact('categories','results','maincategory'));

   } 
   public function search_auto_complete(Request $request){
      
        if($request->ajax())
        {
        $query = $request->get('query');
       
        $data = Course::select('name')->where('name','like',"%$query%")->get(); 
         
        $output = '<ul class="dropdown-menu" style="display:block; position:relative">';
        foreach($data as $row)
        {
        $output .= '
        <li><a href="#">'.$row->name.'</a></li>
        ';
        }
        $output .= '</ul>';
        echo $output;
        }
    
      

   }
   
}
