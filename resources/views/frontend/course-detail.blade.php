@include('frontend.includes.header')
<!-- ****Banner Section**** -->
<section class="innerbanner" style="background: @if($datas->banner_image) url({{asset('storage/app/courses/bannerimage/')}}/{{$datas->banner_image}}) @else  url({{ asset('public/frontend/images/behaviourbanner.jpg') }}) @endif no-repeat center top;">
   <div class="breadcrmb">
      <div class="wid">
         <h2>Courses</h2>
         <div class="brdcrmb">
            <a href="{{route('index')}}">Home</a><Span>/</Span><a href="{{route('courselist')}}">Courses</a><Span>/</Span><a href="{{route('courses', [encrypt($datas->category_id)])}}">{{$datas->category->name}}</a><Span>/</Span><small>{{$datas->name}}</small>
         </div>
      </div>
   </div>
   <div class="wid">
      <div class="crsebtns">
         <ul>
            <li>
               <a href="#" onClick="openPopup('div1');">Quick Enquiry</a>
            </li>
            <li>
               <a href="#" onClick="openPopup('div2');">Download Brochure</a>
            </li>
            <li class="rgstrbtn">
               <a href="#" onclick="return views5()">Register</a>
            </li>
         </ul>
      </div>
   </div>
   <!-- Quick Enquiry Form -->
   <div id="div1" class="popupp" style="display:none;">
      <div class="fadeout"></div>
      <div class="frompop">
         <h2>Quick Enquiry </h2>
         <div class="cancel" onclick="closePopup();"></div>
         <form name="quick_enquire" method="post" action="{{route('quick-enquiry')}}">
            @csrf
            <ul>
               <li>
                  <input type="text" name="name" required="required" id="textfield" placeholder="Name">
               </li>
               <li>
                  <input type="text" name="email" required="required" id="textfield" placeholder="Email">
               </li>
               <li>
                  <input type="text" name="mobile" required="required" id="textfield" placeholder="Phone">
               </li>
               <li>
                  <input type="text" name="address"  id="textfield" placeholder="Address">
               </li>
               <li>
                  <textarea name="message" placeholder="Message"></textarea>
               </li>
               <li>
               @php $enquiry_url= Request::fullUrl(); @endphp
               <input type="hidden" name="enquiry_url" value="{{$enquiry_url}}">
                 <!--<input type="hidden"  name="recaptcha" value="" id="recaptcha" >-->
                  <input type="submit" name="button" id="button" value="Submit">
               </li>
            </ul>
         </form>
      </div>
   </div>
   <div id="div2" class="popupp" style="display:none;">
      <div class="fadeout"></div>
      <div class="frompop">
         <h2>Get Brochure </h2>
         <div class="cancel" onclick="closePopup();"></div>
         <form name="quick_enquire" method="post" action="{{route('brochure-download',[encrypt($datas->course_id)])}}">
            @csrf
            <!-- <input type="hidden" name="_token" value="sK8tygRnPASzz5IMjAFnrj7TvG1CeqvGLb9bJE9j"> -->              
            <ul>
               <li>
                  <input type="text" name="name" required="required" id="textfield" placeholder="Name">
               </li>
               <li>
                  <input type="text" name="email" required="required" id="textfield" placeholder="Email">
               </li>
               <li>
                  <input type="text" name="mobile" required="required" id="textfield" placeholder="Phone">
               </li>
               <li>
                @php $enquiry_url= Request::fullUrl(); @endphp
               <input type="hidden" name="enquiry_url" value="{{$enquiry_url}}">
                  <input type="submit" name="button" id="button" value="Submit">
               </li>
            </ul>
         </form>
      </div>
   </div>
</section>
</section>
<!-- Inner page content section -->
<section class="innerpg coursedtls courseinn">
    
<section  class="innerSerchForm">
     <div class="bannerform">
      <div class="wid">
        <form action="{{route('course.search')}}" method="post">
            <div id="courselist">
            </div>
            {{ csrf_field() }}
            <ul>
               <li class="type">
                  <input type="text" id="course" placeholder="Course Name" name="course">
               </li>
               <li class="selct custom-select">
                  <select name="category">
                     <option class="category" value="">Category</option>
                     <option class="category" value="">All Category</option>
                     @foreach($categories as $category)
                     <option class="category" value="{{$category->category_id}}">{{$category->name}}</option>
                     @endforeach
                  </select>
               </li>
               <li class="selct2 custom-select">
                  <select name="month">
                     <option class="category" value="">Month</option>
                     <option class="category" value="">All Months</option>
                     @foreach($months as $num => $name)
                     <option value="{{$num}}">{{$name}}</option>
                     @endforeach
                  </select>
               </li>
               <li class="buttn">
                  <input type="submit" value="Search">
               </li>
            </ul>
         </form>
      </div>
   </div>
</section>
   <div class="wid">
      <div class="coursecat">
         <div class="secname">
            Categories
         </div>
        <ul class="crcat" id="month_list">
                        @foreach($allcategories->where('master_id',0)->all() as $category)
                            <li>
                                <img src="{{ asset('/storage/app/categories/icon/') }}/{{$category->icon}}">
                            <a @if($category->category_id ==  $datas->category_id) class="cat_active" @endif  @if($category->category_id==23) href="{{route('sub-categories',encrypt($category->category_id))}}" @else  href="{{route('courses', [encrypt($category->category_id)])}}" title="{{$category->name}}" @endif>{{$category->name}}</a>
                              <ul class="subItem">
                                 @foreach($allcategories->where('master_id',$category->category_id)->all() as $subCats)
                                       <li><a @if($subCats->category_id ==  $datas->category_id) class="cat_active" @endif href="{{route('courses', [encrypt( $subCats->category_id)])}}#c_area" id="{{$subCats->category_id}}">{{$subCats->name}}</a></li>
                                 @endforeach
                              </ul>
                            </li>
                        @endforeach   
                    </ul>
      </div>
      <div class="abtcnt">
         <h1>{{$datas->name}}</h1>
         <div class="coursedtts">
            <ul>
               <li>
                  <a href="#overview" onclick="return views()">Overview</a>
               </li>
               <li>
                  <a href="#location" onclick="return views2()">Location & Date</a>
               </li>
               <li>
                  <a href="#fees" onclick="return views3()">Fees</a>
               </li>
               <li>
                  <a href="#courseout" onclick="return views4()">Course Outline</a>
               </li>
            </ul>
         </div>
         <div class="crscntn">
            <div id="overview">
               <h2>Overview</h2>
               <div class="mncntnt">
                  {!!$datas->over_view!!}
               </div>
            </div>
            @foreach($datas->coursedetails as $detail)
            <div id="location">
               <h2>Location & Date</h2>
               <ul class="locdate">
                  <li>
                     <div class="year">
                        {{ date('Y', strtotime($detail->start_date))}}
                     </div>
                     <div class="detlss" id="register">
                        <ul>
                           <li>
                              <img src="{{ asset('public/frontend/images/date.png') }}">
                          {{ date('d M', strtotime($detail->start_date))}}-{{date('d M  Y', strtotime($detail->end_date))}}
                           </li>
                           <li>
                              <img src="{{ asset('public/frontend/images/location.png') }}">
                              {{ $detail->location}}
                           </li>
                        </ul>
                        <div class="regbtn">
                           <a href="{{route('course-registration', [encrypt($datas->course_id)])}}" class="odbttn ">Register Now</a>
                        </div>
                     </div>
                  </li>
               </ul>
            </div>
            <div id="fees">
               <h2>Fees</h2>
               <ul class="locdate">
                  <li>
                     <div class="year">
                       {{ date('Y', strtotime($detail->start_date))}}
                     </div>
                     <div class="detlss fee">
                        <ul>
                           <li class="perpart">
                              Per participant
                           </li>
                           <li class="feees">
                              {{ $coursedetails->currency->symbol}} {{ number_format($detail->fee)}}
                           </li>
                        </ul>
                     </div>
                  </li>
               </ul>
               <div class="condtns">
                  Fees + VAT as applicable<br>
                  Tax Registration Number : 100139824100506
               </div>
            </div>
            @endforeach
            <div id="courseout">
               @if($category->category_id==23) <h2>Content</h2> @else <h2>Course Outline</h2> @endif
               {!!$datas->outline!!}
            </div>
         </div>
      </div>
   </div>
</section>
<section>
<section class="blogRelated">
   <div class="abtcnt">
   <div class="courses crseinner">
      <h2>Related Courses</h2>
      <div class="wid">
         <ul>
            @foreach($related_course as $course)
            <li>
               <div class="courseimg">
                  <a href="{{route('course-detail',[encrypt($course->course_id)])}}">
                  <img src="{{ asset('/storage/app/courses/courseimage/') }}/{{$course->image}}">
                  </a>
               </div>
               <a href="{{route('course-detail',[encrypt($course->course_id)])}}" class="crsbtn">
               {{$course->name}}
               </a>
                <div class="courceLoc">
                     @foreach($course->coursedetails as $details)
                     <a href="#" class="locIcon">{{str_limit(strip_tags($details->location), 10)}}</a>
                     <a href="#" class="timeIcon">{{ date('d M', strtotime($details->start_date))}}-{{date('d M  Y', strtotime($details->end_date))}}</a>
                     @endforeach
               </div>
               <a href="{{route('course-registration', [encrypt($course->course_id)])}}" class="smlbtn">
               Register Now
               </a>
            </li>
            @endforeach                             
         </ul>
      </div>
   </div>
</section>
<script type="text/javascript">
   function openPopup(el) {
      $('.popupp').hide();
      $('#' + el).fadeIn(200);   
   }
   
   function closePopup() {
       $('.popupp').fadeOut(300);
   }
</script>
<script>
   function views() {
   $('html, body').animate({
   'scrollTop' : $("#overview").position().top - $('#headerBar').height()
   },1000);
   }
   
   function views2() {
   $('html, body').animate({
   'scrollTop' : $("#location").position().top - $('#headerBar').height()
   },1000);
   }
   function views3() {
   $('html, body').animate({
   'scrollTop' : $("#fees").position().top - $('#headerBar').height()
   },1000);
   }
   function views4() {
   $('html, body').animate({
   'scrollTop' : $("#courseout").position().top - $('#headerBar').height()
   },1000);
   }
    function views5() {
   $('html, body').animate({
   'scrollTop' : $("#register").position().top - $('#headerBar').height()
   },1000);
   }
   
</script>
<script src="https://www.google.com/recaptcha/api.js?render=6Lfjh7oUAAAAAPZJDpn5AcCMoSz2aghEyvBQ-0Ct"></script>
<script>
grecaptcha.ready(function() {
    grecaptcha.execute('6Lfjh7oUAAAAAPZJDpn5AcCMoSz2aghEyvBQ-0Ct', {action: 'coursedetail'}).then(function(token) {
    //   console.log(token);
    document.getElementById("recaptcha").value = token;
    });
});
</script>
@include('frontend.includes.footer')