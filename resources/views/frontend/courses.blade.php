@include('frontend.includes.header')
<!-- ****Banner Section**** -->
<section class="innerbanner" style="background: url({{ asset('public/frontend/images/coursebanner.jpg') }}) no-repeat center top;">
    <div class="breadcrmb">
        <div class="wid">
            <h2>Courses</h2>
            <div class="brdcrmb">
                <a href="{{route('index')}}">Home</a><Span>/</Span><small>Courses</small>
            </div>
        </div>
    </div>
</section>


<!-- Inner page content section -->

    <section class="innerpg coursedtls" id="c_area">
        <section class="innerSerchForm">
     <div class="bannerform">
      <div class="wid">
        <form action="{{route('course.search')}}" method="post">
            <div id="courselist">
            </div>
            {{ csrf_field() }}
            <ul>
               <li class="type">
                  <input type="text" id="course" placeholder="Course Name" name="course">
               </li>
               <li class="selct custom-select">
                  <select name="category">
                     <option class="category" value="">Category</option>
                     <option class="category" value="">All Category</option>
                     @foreach($categories as $category)
                     <option class="category" value="{{$category->category_id}}">{{$category->name}}</option>
                     @endforeach
                  </select>
               </li>
               <li class="selct2 custom-select">
                  <select name="month">
                     <option class="category" value="">Month</option>
                     <option class="category" value="">All Months</option>
                     @foreach($months as $num => $name)
                     <option value="{{$num}}">{{$name}}</option>
                     @endforeach
                  </select>
               </li>
               <li class="buttn">
                  <input type="submit" value="Search">
               </li>
            </ul>
         </form>
      </div>
   </div>
</section>
            <div class="wid">
                <div class="coursecat">
                    <div class="secname">
                        Categories
                    </div>
                    <ul class="crcat" id="month_list">
                        @foreach($allcategories->where('master_id',0)->all() as $category)
                            <li>
                                <img src="{{ asset('/storage/app/categories/icon/') }}/{{$category->icon}}">
                            <a  @if($category->category_id ==  $category_name->category_id) class="cat_active" @endif  @if($category->category_id==23) href="{{route('sub-categories',encrypt($category->category_id))}}" @else  href="{{route('courses', [encrypt($category->category_id)])}}" title="{{$category->name}}" @endif>{{$category->name}}</a>
                                <ul class="subItem">
                                    @foreach($allcategories->where('master_id',$category->category_id)->all() as $subCats)
                                        <li><a @if($subCats->category_id == $category_id) class="cat_active" @endif href="{{route('courses', [encrypt( $subCats->category_id)])}}#c_area" id="{{$subCats->category_id}}">{{$subCats->name}}</a></li>
                                    @endforeach
                                </ul>
                            </li>
                        @endforeach   
                    </ul>
                </div>
                <div class="abtcnt">
                 <h3> {{$category_name->name}}</h3>
                        <div class="courses crseinner">
                                <div class="wid">
                                       <ul>
                                    @foreach($datas as $data)
                                        <li>
                                            <div class="courseimg">
                                            <a href="{{route('course-detail',[encrypt($data->course_id)])}}" class="crsbtnImg">
                                                <img src="{{asset('storage/app/courses/courseimage/')}}/{{$data->image}}">
                                            </a>
                                            </div>
                                            <a href="{{route('course-detail',[encrypt($data->course_id)])}}" class="crsbtn">
                                            {{str_limit(strip_tags($data->name), 40)}}
                                                </a>
                                            <div class="courceLoc">
                                            @foreach($data->coursedetails as $details)
                                            <a href="#" class="locIcon">{{str_limit(strip_tags($details->location), 10)}}</a>
                                            <a href="#" class="timeIcon">{{ date('d M', strtotime($details->start_date))}}-{{date('d M  Y', strtotime($details->end_date))}}</a>
                                            @endforeach
                                            </div>
                                            <a href="{{ route('course-registration',[encrypt($data->course_id)])}}" class="smlbtn">
                                                Register Now
                                            </a>
                                        </li>
                                            @endforeach
                                        </ul>
                                </div>
                            </div>
                </div>
            </div>
        </section>

@include('frontend.includes.footer')
