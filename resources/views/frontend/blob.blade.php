@include('frontend.includes.header')

<!-- ****Banner Section**** -->
<section class="innerbanner" style="background: url({{ asset('public/frontend/images/blog.jpg')}}) no-repeat center top;">
    <div class="breadcrmb">
        <div class="wid">
            <h2>Technology Blog</h2>
            <div class="brdcrmb">
                <a href="{{route('index')}}">Home</a> / Media / <small>Technology Blog</small>
            </div>
        </div>
    </div>
</section>



<!-- Inner page content section -->
<section class="newssecdet">
        <div class="wid">
                <div class="socshare">
                        <!-- <ul>
                            <li><a href="#" class="fbpg"></a>
                            </li>
                            <li><a href="#" class="twpg"></a>
                            </li>
                            <li><a href="#" class="instapg"></a>
                            </li>
                        </ul> -->
                        <h2>Technology Blog</h2>
                        <!--<div class="datte">{{date('Y-m-d')}}</div>-->
                    </div>
                <div class="newsindt">
                        <!-- <div class="newws-img">
                                <img src="./images/gal1.jpg">
                            </div> -->
                    <div class="newws techBlog">
                    <section class="newssec">
        <div class="wid">
            <ul>
                @foreach($technews as $news)

                <li>
                    <a href="{{route('news-detail',[encrypt($news->news_id)])}}">
                        <div class="newsimg">
                            <img src="{{asset('/storage/app/news/imagethumb')}}/{{$news->image_banner}}">
                        </div>
                        <div class="newdate">
                       
                     {{date('d,M, Y', strtotime($news->news_date))}}
                        </div>
                        <div class="blogCntP">
                            
                            <p>
                        {{$news->title}}
                        </p>
                        </div>
                    </a>
                </li>
                @endforeach
             </ul>
        </div>
    </section>  </div>
                </div>
        </div>
    </section>

    @include('frontend.includes.footer')