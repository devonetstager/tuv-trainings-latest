   <!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->

<!-- Mirrored from www.tuvnordtrainings.com/nebosh/ by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 20 Sep 2019 10:16:05 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	
    <title>TÜV NORD - Nebosh - TÜV NORD Training Centre</title>
  	<meta name="description" content="TUV NORD : TUV Middle East is a Member of TÜV NORD Group, the German Inspection, Certification, Training and Consulting Body" />
	<meta name="keywords" content="Industrial Inspection Services, Third Party Inspection, Plant & Equipment Assessment, Management Systems Certification, Health Safety & Environment, Consultancy and Training " />
  
	<link rel="shortcut icon" type="image/x-icon" href="{{asset('public/frontend/nebosh/img/favicon.png')}}">
	<link href="https://fonts.googleapis.com/css?family=Montserrat:200,300,400,500,700" rel="stylesheet">
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet">
  <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
  <link href="{{ asset('public/frontend/nebosh/css/aos.css') }}" rel="stylesheet" type="text/css">
	<link href="{{asset('public/frontend/nebosh/css/animate.css')}}" rel="stylesheet">
	<link href="{{asset('public/frontend/nebosh/css/owl.carousel.min.css')}}" rel="stylesheet">
	<link href="{{asset('public/frontend/nebosh/css/owl.theme.min.css')}}" rel="stylesheet">
	<link href="{{asset('public/frontend/nebosh/css/layout.css')}}" rel="stylesheet">
    <link href="{{asset('public/frontend/nebosh/css/landing_style.css')}}" type="text/css"  rel="stylesheet" />
	<script src="{{asset('public/frontend/nebosh/js/modernizr.js')}}"></script>
	<script src="{{asset('public/frontend/nebosh/js/scripts.js')}}"></script>
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-143023641-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-143023641-1');
</script>
</head>
<body>

<header>
  <div class="wid">
  <div class="logo_land"> 
  <a href="http://www.tuvnordtrainings.com/"><img src="{{asset('public/frontend/nebosh/images/tuv_logo.png')}}"> </div></a>
  <div class="caLLac">
    <p>ABU DHABI: <a href="tel:+971 0586820107"> +971 058-682-0107</a> DUBAI: <a href="tel:+971 0503606297"> +971 050-360-6297</a></p>
  </div>
</header>

<!--========================== BANNER ========================-->
<section class="banner">
  <div class="wid">
    <div class="cpatON">
    	<div class="for_off"><img src="{{asset('public/frontend/nebosh/images/offrt_img.png')}}"></div>
      <div class="CapMin">
        <div class="captioncvr">
          <div class="ram">Special Offer</div>
          <span>ON CERTIFIED COUSES</span> </div>
        <ul>
          <li><a href="#buNDlofrIGC">NEBOSH IGC</a></li>
          <li><a href="#buNDlofrONG">NEBOSH PSM</a></li>
          <!-- <li><a href="#buNDlofrPSM">NEBOSH PSM (Pilot)</a></li> -->
        </ul>
        <!--<a href="public-courses/list-upcoming-courses/">LEARN MORE</a> -->
           <a href="http://www.tuvnordtrainings.com/">LEARN MORE</a>
</div>
    </div>
    <div class="bNForM" id="bNForM">
      
      <form name="bnbform" action="{{route('nebosh.submit')}}" id="bnbform" method="post">
        <p>ENQUIRE NOW</p>
        @csrf
        <input type="text" name="name" id="bnbname"  required="required"  value="{{old('name')}}"  placeholder="Full Name">
        {!! $errors->first('name', '<p style="color:red;" class="help-block error">:message</p>') !!}
        <input type="text" name="mobile" id="bnbphone" required="required"  value="{{old('mobile')}}" placeholder="Phone">
         {!! $errors->first('mobile', '<p style="color:red;" class="help-block error">:message</p>') !!}
        <input type="text" name="email" id="bnbemail"  required="required" value="{{old('email')}}" placeholder="Email">
        {!! $errors->first('email', '<p style="color:red;" class="help-block error">:message</p>') !!}
        <input type="text" name="city" id="bnbecountry" required="required"  value="{{old('city')}}"  placeholder="City">
         {!! $errors->first('city', '<p style="color:red;" class="help-block error">:message</p>') !!}
        <textarea name="message" id="bnbmessage" placeholder="Course interested in?">{{old('message')}}</textarea>
        @php $enquiry_url= Request::fullUrl(); @endphp
               <input type="hidden" name="enquiry_url" value="{{$enquiry_url}}">
                <input type="hidden"  required="required" name="recaptcha" value="" id="recaptcha" >
        <input type="submit" value="Submit"  name="bnbformbtn" id="bnbformbtn">
      </form>
      <div id="errMsg" class="errHide"></div>
    </div>
  </div>
</section>
<section class="blbg">
  <div class="wid">
    <h1>COURSES & OFFERS</h1>
  </div>
</section>

<!--========================== SEC1 ========================-->
<section class="sec1">
  <div class="wid">
    <div class="bundlimg rgtfl">
      <div class="ofrHed">BUNDLE OFFER</div>
      <div class="blucnt">
        <p>NEBOSH IGC + IOSH Managing Safely +Risk Assessment +OSHAD-SF V.3 Internal Auditor Training</p>
        <span>PRICE: AED 3,000</span> <a class="lnbNForM" href="#">book NOW</a> </div>
    </div>
    <div class="buNDlofr" id="buNDlofrIGC">
      <h2>NEBOSH IGC</h2>
      <p>The NEBOSH International General Certificate is established as a leading award in basic health and safety held by more than 70,000 people. This comprehensive NEBOSH Certificate 88 hours course which concludes with a two-part written examination and practical safety assessment, gives an excellent grounding in the fundamentals of occupational health and safety management. Having completed NEBOSH Certificate course, you will have the competence and knowledge to recognise hazards and risks, analyse the significance of human factors such as attitudes and perception, and be able to report to senior management appropriate and cost-effective recommendations.</p>
      <h3>Who is it for</h3>
      <ul>
        <li>Managers.</li>
        <li> Supervisors.</li>
        <li> Worker representatives and others, who require a knowledge and under- standing of health and safety principles, and practices.</li>
        <li>The NEBOSH International General Certificate is also suitable for those embarking on a career in health and safety, providing a valuable foundation for further professional study.</li>
      </ul>
      <h3>What is expected outcome</h3>
      <span>Certificate:</span>
      <p>Upon successfully passing of Exam, the certificate will be issued by NEBOSH, UK after minimum 10 weeks of the course/exam completion.</p>
    </div>
  </div>
</section>

<!--========================== SEC 2 ========================-->
<section class="sec2">
  <div class="wid">
    <div class="bundlimg bundlimg2 leftfl">
      <div class="ofrHed">NEBOSH PSM</div>
      <div class="blucnt">
        <p>NEBOSH HSE Certificate in Process Safety Management</p>
        <span>PRICE: AED 2,500</span> <a class="lnbNForM" href="#">book NOW</a> </div>
    </div>
    <div class="buNDlofr" id="buNDlofrONG">
      <h2>NEBOSH PSM</h2>
      <p>The NEBOSH HSE Certificate in Process Safety Management is designed specifically for those with safety responsibilities in the process industry, such as oil and gas, chemicals, plastics and pharmaceuticals. The course has been developed through a collaboration between the NEBOSH and the Health and Safety Executive (HSE) the UK’s Health & safety Regulator. This NEW specialist qualification aims to provide holders with the knowledge and understanding of Process Safety Management to ensure that they can contribute to the effective management of process safety risks.

</p>
      <h3>Who is it for</h3>
      <ul>

        <li>Managers</li>
        <li>Supervisors</li>
        <li>Employee representatives</li>
        <li>Newly appointed health and safety advisers working within the Oil & Gas Industry.</li>
      </ul>
      <h3>What is expected outcome</h3>
      <span>Certificate:</span>
      <p>Upon successfully passing of Exam, the certificate will be issued by NEBOSH, UK after minimum 14 weeks of the course/exam completion.</p>
    </div>
  </div>
</section>

<!--========================== SEC 3 ========================-->
<!--

-->
<!--========================== SEC 4 ========================-->

<section class="sec4">
  <div class="wid">
    <h4>Book your seats Now</h4>
    <a class="lnbNForM" href="#">book now</a> </div>
</section>

<!--========================== SEC 4 ========================-->

<section class="sec5">
  <div class="wid">
    <div class="firsthlf">
      <!--<div class="minhed">Schedule / Timings</div>-->
      <div class="wholcnt no">
      <!--  <div class="grbg"> 
          <h4>CONTINUOUS BATCH (6:PM-10:PM)</h4>
        </div>
        <p>&nbsp;</p>-->
        <!--<ul>-->
        
        <!--<li>NEBOSH IGC - October 17-19, 22-26, 29-31, November 1-2, 5-6 (15 Days)</li>
        <li>IOSH MS - October 8-11 (4 Days)</li>
        <li>Risk Assessment - October 12 (1 Day)</li>
        <li>AD OHSMS Internal Auditor V.3 Training-<br />
October 15-16 (2 Days)</li>
        <li>NEBOSH Oil & Gas - October 22-28
(7 Days)</li>-->
        
        <!--<li>NEBOSH IGC - August 6-10 & 13-17 & 20-24 (15 Days)</li>
        <li>IOSH MS - July 25-27 & 29 (4 Days)</li>
        <li>Risk Assessment - July 30 (1 Day)</li>
        <li>AD OHSMS Internal Auditor V.3 Training-<br />
July 31, August 1-2 (3 Days)</li>
        <li>NEBOSH Oil & Gas - August 5-10 & 12
(7 Days)</li>-->
        
         <!-- <li>NEBOSH IGC - June 3-8 & 10-15 (12 Days)</li>
          <li>IOSH MS - June 18-20 (3 Days)</li>
          <li>Risk Assessment - June 21 (1 Day)</li>
          <li>AD OHSMS Internal Auditor V.3</li>
          <li>Training- June 25-26 (2 Days)</li>
          <li>NEBOSH Oil & Gas - June 18-22 (5 Days)</li>-->
        <!--</ul>-->
       <!-- <div class="blubg">
          <h4>Friday Batch  (8:AM-5:PM)</h4>
        </div>-->
        <!--<ul>-->
       <!-- <li>NEBOSH IGC - Sep 15, 22, 29, Oct 6, 13, 20,
27, Nov 3, 10, 17 (10 Days)</li>
		<li>NEBOSH IGC - Dec 1, 8, 15, 22, 29, Jan 5, 12,
19, 26, Feb 2 <br>(10 Days)</li>-->
        <!--<li>NEBOSH IGC - Sep 8, 15, 22, 29, Oct 6, 13, 20,
27, Nov 3, 10 (10 Days)</li>-->
         <!-- <li>NEBOSH IGC - June 9, 16, 23,<br>
            July 7, 14, 21, 28, August 4, 11, 18 </li>-->
        <!--</ul>-->
        <img src="{{asset('public/frontend/nebosh/images/offerimg.jpg')}}" class="ofrimgs">
      </div>
    </div>
    <div class="Senhlf">
      <div class="minhed">Terms & Conditions </div>
      <div class="wholcnt">
        <ul>
          <!--<li>You may confirm your attendance by filling the attached
            registration form along with the payment.</li>
          <li>Price for accredited courses includes examination,
            accreditation, certification fees, lunch
            and refreshments.</li>
          <li>Payment can be done as an initial down
            payment with Guarantee cheque or
            full payment on the day of registration.</li>
          <li>Dates are subjected to change depending on the number
            of participants or official holidays during the course.</li>
          <li>No refund shall be made after the official registration.</li>-->
          <li>You may confirm your attendance by filling the attached
registration form along with the payment.</li>
          <li>Price for accredited courses includes examination,
accreditation, certification fees, lunch and refreshments.</li>
          <!--<li>Payment can be done as an initial down payment
with Guarantee cheque or full payment on the day of
registration.</li>-->
          <li>Dates are subjected to change depending on the
number of participants or official holidays during the
course.</li>
          <li>No refund shall be made after the official registration.</li>
        </ul>
      </div>
    </div>
  </div>
</section>
        

<script>
jQuery(document).ready(function($){
$('.responsive-tabs').responsiveTabs({
  accordionOn: ['xs', 'sm']
  });
});
</script>
<script>
//landing Form ends here
function bn_buy_validation()
{
	flag=0;
	
	bnbname	= $("#bnbname").val();
	if(bnbname=="")
	{
	
		$("#errMsg").addClass("errShow");
		
		$("#errMsg").addClass("errCls");
		$("#errMsg").removeClass("errHide");
		
		$("#errMsg").html("Enter required fields...!");
		
		goToByScroll("bnbname");
		$("#bnbname").focus();

		flag=1;//return false;
	}
	bnbecountry	= $("#bnbecountry").val();
	if(bnbecountry=="")
	{
	
		$("#errMsg").addClass("errShow");
		
		$("#errMsg").addClass("errCls");
		$("#errMsg").removeClass("errHide");
		
		$("#errMsg").html("Enter required fields...!");
		
		goToByScroll("bnbecountry");
		$("#bnbecountry").focus();

		flag=1;//return false;
	}	
	bnbphone	= $("#bnbphone").val();
	if(bnbphone=="")
	{
		$("#errMsg").addClass("errShow");
		
		$("#errMsg").addClass("errCls");
		$("#errMsg").removeClass("errHide"); 
		$("#errMsg").html("Enter required fields...!");  
		      
		goToByScroll("bnbphone");
		$("#bnbphone").focus();

		flag=1;//return false;
	}
	
	
	if(bnbphone!="")
	{
		if(!phonenumber(bnbphone))

		{
			$("#errMsg").addClass("errShow");
		
			$("#errMsg").addClass("errCls");
			$("#errMsg").removeClass("errHide");  
			$("#errMsg").html("Enter required fields...!");
			   
			goToByScroll("bnbphone");
			$("#bnbphone").focus();
	
			flag=1;//return false;
				
			
		}
	}
	bnbemail	= $("#bnbemail").val();
	if(bnbemail=="")
	{
		$("#errMsg").addClass("errShow");
		
		$("#errMsg").addClass("errCls");
		$("#errMsg").removeClass("errHide");
		$("#errMsg").html("Enter required fields...!");
		   
		goToByScroll("bnbemail");
		$("#bnbemail").focus();

		flag=1;//return false;
	}
		
	if(bnbemail!="")
	{
		if(!checkEmail(bnbemail))

		{
			$("#errMsg").addClass("errShow");
		
			$("#errMsg").addClass("errCls");
			$("#errMsg").removeClass("errHide");
			$("#errMsg").html("Enter required fields...!");
		         
			goToByScroll("bnbemail");
			$("#bnbemail").focus();
	
			flag=1;//return false;
			
			
		}
	}	
		
	

	
	if(flag==1)
		return false;
	else 
		return true;
		
		
}
$("#bnbformbtn").click(function(){
	
	$("#errMsg").removeClass("succCls");
	$("#errMsg").removeClass("errCls");
	
	
	loader = 'assets/images/loader.gif';
	
	
	if (bn_buy_validation() != false) {
		
		
		$("#errMsg").addClass("errShow");
		$("#errMsg").removeClass("errHide");
		
		$("#errMsg").removeClass("errCls");
		
		
		
			
            //Creating manual form data for Submitting
            var frmFormData = new FormData();
            //        Syntax: frmFormData.append('KEY','VALUE');

            frmFormData.append('bnbname', $("#bnbname").val());
            frmFormData.append('bnbphone', $("#bnbphone").val());
			
			frmFormData.append('bnbemail', $("#bnbemail").val());
	        frmFormData.append('bnbecountry', $("#bnbecountry").val());
			frmFormData.append('bnbmessage', $("#bnbmessage").val());

			
		
           
      
			$("#errMsg").html('<img src="'+loader+'" />');

            $.ajax({
                url: '/nebosh/neboshmail/',
                type: 'POST',
                data: frmFormData,
                cache: false,
                //dataType: 'json',
                processData: false, // Don't process the files
                contentType: false, // Set content type to false as jQuery will tell the server its a query string request
                success: function (data, textStatus, jqXHR) {
                    if (data == "failed") {
                         $("#errMsg").html("Please try later.");
                    }
                    else {
                     	$("#errMsg").addClass("succCls");
					  	$("#errMsg").html("Enquiry submitted successfully.");
						window.location="http://www.tuvnordtrainings.com/thank-you/";
						
						
						$("#bnbname").val("");
						$("#bnbphone").val("");
						$("#bnbemail").val("");
						$("#bnbmessage").val("");
						$("#bnbecountry").val();
						
		
 
                    }

                },
                error: function (jqXHR, textStatus, errorThrown) {
                }
            });
        }
		
         return false;
    

});

//landing Form ends here
// COMMON FUNCTIONS STARTS HERE

$(".erMsg").mouseover(function(){
	$(this).removeClass("error");
	$(this).addClass("hide");
});
function hide_this(span_id)
{
	//document.getElementById(span_id).className="hide";
	$("#"+span_id).removeClass("error");
	$("#"+span_id).addClass("hide");
}
function goToByScroll(id){
    // Remove "link" from the ID
	return false;
    id = id.replace("link", "");
    // Scroll
    $('html,body').animate({
    scrollTop: $("#"+id).offset().top},
    'slow');

}

function checkEmail(email)
{
	
	var d = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;
	if(email.search(d) == -1){
		return false;
	
	}
	else
	{
		return true;
	}
		
}
function phonenumber(phonenumber)
{
	
	var d = /^[\+]?\d([- ]*\d){5,19}$/;
	if(phonenumber.search(d) == -1){
		return false;
	
	}
	else
	{
		return true;
	}
		
}

$(document).ready(function(){ 
  var $link = $(".lnbNForM");
  var $formInput = $("#bnbname");

  $link.on("click", function(){ 
      $formInput.focus();
  });
});

// COMMON FUNCTIONS ENDS HERE
</script>
<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5b15a8bb8859f57bdc7bd66d/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>

<div id="top"><a href="#top"><i class="fa fa-angle-up" aria-hidden="true"></i></a></div>




<!--<script src="assets/js/jquery-1.12.0.min.js"></script> -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<!--<script src="assets/js/aos.js"></script> -->
<!--<script src="assets/js/owl.carousel.min.js"></script> -->
<!--<script src="assets/js/jquery.bootstrap-responsive-tabs.min.js"></script> -->
<!--<script src="assets/js/custom.js"></script> -->
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAJ0OQIdhczYbjCjsbR0veUNzjTBodFMNM&callback=initMap"></script>
<!-- Footer section -->

<footer>
    <div class="wid">
        <div class="footrlst">
           
            <div class="ftrlst ftrlinks">
                <div class="ftrhead">
                    Quick Links
                </div>
                <ul>
                    <li><a href="{{ route('index')}}" title="Home">Home</a></li>
                    <li><a href="{{ route('tuv-middle-east')}}" title="About Us">About Us</a></li>
                    <li><a href="{{route('courselist')}}" title="Courses">Courses</a></li>
                    <li><a href="{{ route('blob')}}" title="Media">Media</a></li>
                    <li><a href="{{ route('public-courses')}}" title="Services">Services</a></li>
                    <li><a href="{{ route('contact-us')}}" title="Contact Us">Contact Us</a></li>
                </ul>
            </div>
            <div class="ftrlst ftradrs">
                <div class="ftrhead">
                    Address
                </div>
                @foreach($contacts as $contact)
             </p>
               
                <div class="addrs">
                <!--{{$contact->name}}    <br>-->
                {{$contact->building}}    <br>
                {{$contact->column1}}<br>
                {{$contact->area}},<br>
                <!-- {{$contact->column2}}<br> -->
                {{$contact->state}},<br>
                {{$contact->country}}
                </div>
            </div>
            <div class="ftrlst ftrcnt">
                <div class="ftrhead">
                    Contact Us
                </div>
                <ul class="cntdtls">
                    <li>Mr. MASOOD HUSAIN - Abu Dhabi</li>
                    <li>
                      Mobile :   <a href="tel:+971 058-682-0107"> +971 058-682-0107</a>
                    </li>
                    <li>
                      Email  <a href="mailto:masoodh@tuv-nord.me"> : masoodh@tuv-nord.me</a>
                    </li>
                    <li>Mr. HARI RAGHUNADHAN - Dubai</li>
                    <li>
                      Mobile :   <a href="tel:+971 050-360-6297"> +971 050-360-6297</a>
                    </li>
                    <li>
                      Email  <a href="mailto:harir@tuv-nord.me"> : harir@tuv-nord.me</a>
                    </li>
                </ul>
                <div class="socialmd">
                    <ul>
                        <li>
                            <a target="_blank" href="{{$contact->facebook_url}}" class="fbpg"></a>
                        </li>
                        <li>
                            <a target="_blank" href="{{$contact->youtube_url}}" class="youtb"></a>
                        </li>
                        <li>
                            <a target="_blank" href="{{$contact->linkedin_url}}" class="instapg"></a>
                        </li>
                          <li>
                            <a target="_blank" href="{{$contact->twitter_url}}" class="twpg"></a>
                        </li>
                    </ul>
                </div>
            </div>
            @endforeach
        </div>
        <div class="copryts">
            Copyright © {{ date('Y') }} TUV MIDDLE EAST. All Rights Reserved.
        </div>
    </div>
    <script src="https://www.google.com/recaptcha/api.js?render=6Lfjh7oUAAAAAPZJDpn5AcCMoSz2aghEyvBQ-0Ct"></script>
<script>
grecaptcha.ready(function() {
    grecaptcha.execute('6Lfjh7oUAAAAAPZJDpn5AcCMoSz2aghEyvBQ-0Ct', {action: 'nebosh'}).then(function(token) {
    //   console.log(token);
    document.getElementById("recaptcha").value = token;
    });
});
</script>
</footer>
<script type="text/javascript">
    $(function() {
      $(".js__p_start, .js__p_another_start").simplePopup();
    });
  </script>

<script type="text/javascript" src="{{ asset('public/frontend/js/script.js') }}"></script>
 <script type="text/javascript">
$(window).scroll(function() {
$("header").toggleClass("aniPos", $(this).scrollTop() > 200);
});
</script>
<!--Slider-->
<script>
$(document).ready(function() {
$('#bannerSlider').lightSlider({
item:1,
auto: true,
loop: true,
slideMove:1,
mode: 'slide',
addClass: 'bannerSlider',
easing: 'cubic-bezier(0.25, 0, 0.25, 1)',
speed:1000,
pause: 3000,
adaptiveHeight:true,
responsive : [
{
breakpoint:800,
settings: {
item:1,
slideMargin:6,
}
},
{
breakpoint:480,
settings: {
item:1,
slideMove:1
}
}
]
});
});
</script>


<script>
        $(document).ready(function() {
        $('#testSlider').lightSlider({
        item:2,
        auto: true,
        loop: true,
        slideMove:1,
        mode: 'slide',
        addClass: 'testSlider',
        easing: 'cubic-bezier(0.25, 0, 0.25, 1)',
        speed:1000,
        pause: 3000,
        adaptiveHeight:true,
        responsive : [
        {
        breakpoint:800,
        settings: {
        item:1,
        slideMargin:6,
        }
        },
        {
        breakpoint:480,
        settings: {
        item:1,
        slideMove:1
        }
        }
        ]
        });
        
        
        
        $('#brandsSlider').lightSlider({
        item:5,
        auto: true,
        loop: true,
        slideMove:1,
        mode: 'slide',
        addClass: 'brandsSlider',
        easing: 'cubic-bezier(0.25, 0, 0.25, 1)',
        speed:1000,
        pause: 3000,
        adaptiveHeight:true,
        responsive : [
        {
        breakpoint:800,
        settings: {
        item:3,
        slideMargin:6,
        }
        },
        {
        breakpoint:480,
        settings: {
        item:1,
        slideMove:1
        }
        }
        ]
        });
        
        
        });
        </script>
<script>
    var x, i, j, selElmnt, a, b, c;
    /*look for any elements with the class "custom-select":*/
    x = document.getElementsByClassName("custom-select");
    for (i = 0; i < x.length; i++) {
      selElmnt = x[i].getElementsByTagName("select")[0];
      /*for each element, create a new DIV that will act as the selected item:*/
      a = document.createElement("DIV");
      a.setAttribute("class", "select-selected");
      a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
      x[i].appendChild(a);
      /*for each element, create a new DIV that will contain the option list:*/
      b = document.createElement("DIV");
      b.setAttribute("class", "select-items select-hide");
      for (j = 1; j < selElmnt.length; j++) {
        /*for each option in the original select element,
        create a new DIV that will act as an option item:*/
        c = document.createElement("DIV");
        c.innerHTML = selElmnt.options[j].innerHTML;
        c.addEventListener("click", function(e) {
            /*when an item is clicked, update the original select box,
            and the selected item:*/
            var y, i, k, s, h;
            s = this.parentNode.parentNode.getElementsByTagName("select")[0];
            h = this.parentNode.previousSibling;
            for (i = 0; i < s.length; i++) {
              if (s.options[i].innerHTML == this.innerHTML) {
                s.selectedIndex = i;
                h.innerHTML = this.innerHTML;
                y = this.parentNode.getElementsByClassName("same-as-selected");
                for (k = 0; k < y.length; k++) {
                  y[k].removeAttribute("class");
                }
                this.setAttribute("class", "same-as-selected");
                break;
              }
            }
            h.click();
        });
        b.appendChild(c);
      }
      x[i].appendChild(b);
      a.addEventListener("click", function(e) {
          /*when the select box is clicked, close any other select boxes,
          and open/close the current select box:*/
          e.stopPropagation();
          closeAllSelect(this);
          this.nextSibling.classList.toggle("select-hide");
          this.classList.toggle("select-arrow-active");
        });
    }
    function closeAllSelect(elmnt) {
      /*a function that will close all select boxes in the document,
      except the current select box:*/
      var x, y, i, arrNo = [];
      x = document.getElementsByClassName("select-items");
      y = document.getElementsByClassName("select-selected");
      for (i = 0; i < y.length; i++) {
        if (elmnt == y[i]) {
          arrNo.push(i)
        } else {
          y[i].classList.remove("select-arrow-active");
        }
      }
      for (i = 0; i < x.length; i++) {
        if (arrNo.indexOf(i)) {
          x[i].classList.add("select-hide");
        }
      }
    }
    /*if the user clicks anywhere outside the select box,
    then close all select boxes:*/
    document.addEventListener("click", closeAllSelect);
    </script>
    <script>
function views() {
$('html, body').animate({
'scrollTop' : $("#overview").position().top - $('#headerBar').height()
});
}

function views2() {
$('html, body').animate({
'scrollTop' : $("#location").position().top - $('#headerBar').height()
});
}
function views3() {
$('html, body').animate({
'scrollTop' : $("#fees").position().top - $('#headerBar').height()
});
}
function views4() {
$('html, body').animate({
'scrollTop' : $("#courseout").position().top - $('#headerBar').height()
});


</script>
<!--    <script>-->
<!--$(document).ready(function(){-->

<!-- $('#course').keyup(function(){ -->
<!--        var query = $(this).val();-->
<!--        if(query != '')-->
<!--        {-->
<!--         var _token = $('input[name="_token"]').val();-->
<!--         $.ajax({-->
<!--          url:"{{ route('search-auto-complete') }}",-->
<!--          method:"POST",-->
<!--          data:{query:query, _token:_token},-->
<!--          success:function(data){-->
             
<!--           $('#courselist').fadeIn();  -->
<!--           $('#courselist').html(data);-->
<!--          }-->
<!--         });-->
<!--        }else{-->
<!--            $('#courselist').fadeOut();-->
<!--        }-->
<!--    });-->
<!--    $(document).on('click', 'a', function(){  -->
<!--        $('#course').val($(this).text());-->
       
<!--        $('#courselist').fadeOut();  -->
<!--    });-->
      
    

 
<!--});-->

<!--</script>-->
<!--<script>!function(e,t,a){var c=e.head||e.getElementsByTagName("head")[0],n=e.createElement("script");n.async=!0,n.defer=!0, n.type="text/javascript",n.src=t+"/static/js/chat_widget.js?config="+JSON.stringify(a),c.appendChild(n)}(document,"https://app.engati.com",{bot_key:"327f4e6719594d01",welcome_msg:true,branding_key:"default",server:"https://app.engati.com",e:"p" });</script>-->
<!--<script>-->
<script>
$("#vatOkBtn").click(function(){
    $(".vatMsg").hide();
});
</script>   
<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5b15a8bb8859f57bdc7bd66d/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--var button = document.getElementById('vatOkBtn'); // Assumes element with id='button'-->

<!--button.onclick = function() {-->
<!--    var div = document.getElementById('coockie_data');-->
<!--    if (div.style.display !== 'none') {-->
<!--        div.style.display = 'none';-->
<!--    }-->
<!--    else {-->
<!--        div.style.display = 'block error';-->
<!--    }-->
<!--};-->
</script>

<script>
 $(document).ready(function() {
var button = document.getElementById('vatOkBtn'); // Assumes element with id='button' 

button.onclick = function() {  
    var div = document.getElementById('coockie_data');
        // ajax call to set cookie
        var _token = $('input[name="_token"]').val();
        $.ajax({
          url:"{{ route('set-vat-cookie') }}",
          method:"post",
          data:{data:1, _token:_token},
          success:function(data){
             
            cookiehide();
          }
      
        });  
}
function cookiehide(){
    var div = document.getElementById('coockie_data');
    div.style.display = 'none   ';
}
});
</script>
<!--Slider Ends-->
</body>
</html>
