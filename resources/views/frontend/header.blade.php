<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>TÜV NORD Training Center</title>
<meta name="format-detection" content="telephone=no"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=1.0, minimum-scale=1.0, maximum-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<link rel="icon" type="image/png" sizes="32x32" href="{{ asset('public/frontend/images/favicon32.png') }}">
<link rel="icon" type="image/png" sizes="96x96" href="{{ asset('public/frontend/images/favicon96.png') }}">
<link rel="icon" type="image/png" sizes="16x16" href="{{ asset('public/frontend/images/favicon16.png') }}">
<link href="{{ asset('public/frontend/css/style.css') }}" rel="stylesheet" type="text/css">

</head>
<style>
#search-query {
    width:300px;
    padding:5px 10px;
    margin-left:10px;
}
/****** jQuery Autocomplete CSS *************/
 .ui-corner-all {
    -moz-border-radius: 0;
    -webkit-border-radius: 0;
    border-radius: 0;
}
.ui-menu {
    border: 1px solid lightgray;
    font-family: Verdana, Arial, Helvetica, sans-serif;
    font-size: 15px;
}
.ui-menu .ui-menu-item a {
    color: #888;
}
.ui-menu .ui-menu-item:hover {
    display: block;
    text-decoration: none;
    color: #3D3D3D;
    cursor: pointer;
    background-color: lightgray;
    background-image: none;
    border: 1px solid lightgray;
}
.ui-widget-content .ui-state-hover, .ui-widget-content .ui-state-focus {
    border: 1px solid lightgray;
    background-image: none;
    background-color: lightgray;
    font-weight: bold;
    color: #3D3D3D;
}
</style>
<body>

<!-- ****Header Section**** -->

<header>
    <div class="wid">
        <div class="tophead">
            <div class="logo">
                <a href="i{{route('index')}}">
                    <img src="{{ asset('public/frontend/images/TUV_logo.png') }}" alt="TUV-logo" title="TUV NORD">
                </a>
            </div>
            <div class="tpcenter">
                <a href="#" class="trbttn">
                    Training Calendar 2020
                </a>
            </div>
            <div class="tpcontct">
                <ul class="cntdtls">
                    <li>
                        <a href="tel:+971 2 4411148" title="+971 2 4411148">
                            Phone:+971 2 4411148
                        </a>
                    </li>
                    <li>
                        Fax:+971 2 4411149
                    </li>
                </ul>
                <div class="offrs">
                    <a href="#" class="mn_b">Offers</a>
                </div>
            </div>
        </div>
    </div>
    <div class="menuD">
    	<a href="#menu" id="nav-toggle" class="menu-link">
  	<span></span>
  </a>
  <nav id="menu" class="menu">
		<ul class="level-01">              
			<li><a href="{{ route('index')}}"title="Home">Home</a></li>
			<li><a href="#" title="About Us">About Us</a><span class="has-subnav"></span>
                <ul class="level-1">
                    <li><a href="{{ route('tuv-middle-east')}}" title="TÜV Middle East">TÜV Middle East</a></li>
                    <li><a href="#" title="TÜV NORD">TÜV NORD</a></li>
                    <li><a href="#" title="TÜV NORD Training Centre">TÜV NORD Training Centre</a></li>
                    <li><a href="#" title="Qualifications">Qualifications</a></li>
                    <li><a href="#" title="Clientele">Clientele</a></li>
                    <li><a href="#" title="Testimonials">Testimonials</a></li>
                    <li><a href="#" title="Appreciation Letters">Appreciation Letters</a></li>
                    <li><a href="#" title="Press Release">Press Release</a></li>
                    <li><a href="#" title="Photo Gallery">Photo Gallery</a></li>
                    <li><a href="#" title="FAQ">FAQ</a></li>
                </ul>
            </li>
			<li><a href="{{route('courselist')}}" title="Courses">Courses</a><span class="has-subnav"></span>
                <ul class="level-1">
                @foreach($categories as $category)
                    <li><a href="{{route('courses', [encrypt($category->category_id)])}}" title="Project & Program Management"> {{$category->name}}</a></li>
                @endforeach    
                  
                </ul>
            </li>
            <li><a href="#" title="Media">Media</a><span class="has-subnav"></span>
                <ul class="level-1">
                    <li><a href="{{ route('corporate-news')}}" title="Corporate News">Corporate News</a></li>
                    <li><a href="training-blog.html" title="Training Blog">Training Blog</a></li>
                </ul>
            </li>
            <li><a href="#" title="Services">Services</a><span class="has-subnav"></span>
                <ul class="level-1">
                    <li><a href="#" title="Public Courses">Public Courses</a></li>
                    <li><a href="#" title="In Houses Courses">In Houses Courses</a></li>
                    <li><a href="#" title="Integrated E-Learning">Integrated E-Learning</a></li>
                    <li><a href="#" title="Conferences">Conferences</a></li>
                </ul>
            </li>
            <li class="has-subnav"><a href="{{ route('contact-us')}}" title="Contact Us">Contact Us</a><span class="has-subnav"></span>
                <ul class="level-1">
                    <li><a href="#" title="Locations">Locations</a></li>
                </ul>
            </li>
		</ul>
	</nav>
    </div>

</header>