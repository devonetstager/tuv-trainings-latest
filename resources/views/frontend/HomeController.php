<?php

namespace App\Http\Controllers\Frontend;
use App\Models\Admin\Category;
use App\Models\Admin\Course;
use App\Models\Admin\News;
use App\Models\Admin\SixSigmaCourse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function index(){
      
        $categories = Category::orderBy('name', 'ASC')->get();
        $courses = Course::all()->where('status',0);
        $newsdata = News::all()->where('status',1);
        $sigmacourses = SixSigmaCourse::all()->where('status',1);
        $months = array(1 => 'January.', 2 => 'February.', 3 => 'March.', 4 => 'April.',
         5 => 'May', 6 => 'June.',7 => 'July.', 8 => 'August.', 9 => 'September.', 10 => 'October.',
          11 => 'November.', 12 => 'December.');       
               
        return view('frontend.index',compact('categories','courses','newsdata','sigmacourses','months'));
    }

    public  function registration(){
        
        $categories = Category::orderBy('name', 'ASC')->get();

        return view('frontend.registration',compact('categories'));
        }
    public  function tuv_middle_east(){
       
        return view('frontend.tuv-middle-east');
        }
    public  function contactus(){
    
        return view('frontend.contactus');
        }
    public  function corporate_news(){
        return view('frontend.corporate-news');
        }
    public  function course_detail($course_id){
      
        $categories = Category::all();
        $datas = Course::with('category')->find(decrypt($course_id));
        
        return view('frontend.course-detail',compact('categories','datas'));
        }
    public  function courses($category_id){
        $categories = Category::all();
        $category_id = decrypt($category_id);
        $datas = Course::all()->where('category_id',$category_id);
        
        return view('frontend.courses',compact('categories','datas','category_id'));
    }
    public function courselist(){
        $categories = Category::all();
        $courses = Course::all();
        return view('frontend.courselist',compact('courses','categories'));

    }
    public  function sigma_course_detail($six_sigma_id){
      
        $categories = Category::all();
        $datas = SixSigmaCourse::find(decrypt($six_sigma_id));
        
        return view('frontend.sigma-details',compact('categories','datas'));
    }
    public  function news_detail($news_id){
      
        $categories = Category::all();
        $datas = News::find(decrypt($news_id));
        
        return view('frontend.news-details',compact('categories','datas'));
    }
    
                  

}
