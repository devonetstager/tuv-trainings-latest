@include('frontend.includes.header')

<!-- ****Banner Section**** -->
<section class="innerbanner" style="background: url({{ asset('public/frontend/images/partners.jpg') }}) no-repeat center top;">
    <div class="breadcrmb">
        <div class="wid">
            <h2>Become a Partner</h2>
            <div class="brdcrmb">
                <a href="{{route('index')}}">Home</a><span>/</span><a href="">Services</a><Span>/</Span><small>Become a Partner</small>
            </div>
        </div>
    </div>
</section>


<!-- Inner page content section -->

<section class="contactuss regnow">
   <div class="wid">
        <div class="contfomr">
            <h1>Personal Details</h1>
           <form method="post" action="{{route('enquiry')}}" enctype="multipart/form-data">
            @csrf
                <ul>
                <li>
                 <input type="text" name="name"  value="{{old('name')}}"  placeholder="Name">
                {!! $errors->first('name', '<p style="color:red;" class="help-block error">:message</p>') !!}

                </li>
                <li>
                  <input type="email" name="email"  value="{{old('email')}}"   placeholder="Email">
                {!! $errors->first('email', '<p style="color:red;" class="help-block error">:message</p>') !!}
                </li>
                <li>
                  <input type="tel" name="mobile" value="{{old('mobile')}}"   placeholder="Phone">
                {!! $errors->first('mobile', '<p style="color:red;" class="help-block error">:message</p>') !!}
                </li>
                <li>
                   <input type="text" name="company"  value="{{old('company')}}" placeholder="Company">
                {!! $errors->first('company', '<p style="color:red;" class="help-block error">:message</p>') !!}
                </li>
                <li>
                   <input type="text" name="address"  value="{{old('address')}}"  placeholder="Address">
                {!! $errors->first('address', '<p style="color:red;" class="help-block error">:message</p>') !!}
                </li>
                <li>
                  <input type="text" name="post" value="{{old('post')}}"  placeholder="P.O Box">
                </li>
                <li>
                 <input type="text" name="city" value="{{old('city')}}"  placeholder="City">
                </li>
                <li>
                  <input type="text" name="country" value="{{old('country')}}"  placeholder="Country">
                </li>
                     <li> 
                     Message
                       <textarea name="message"></textarea>
                    </li>
                    <li>
                        <input type="file" name="document" placeholder="document">
                    </li>
                     <li class="frmbtn">
                      @php $enquiry_url= Request::fullUrl(); @endphp
                    <input type="hidden" name="enquiry_url" value="{{$enquiry_url}}">
                    <input type="hidden"  required="required" name="recaptcha" value="" id="recaptcha" >
                           <input type="submit" value="Submit">
                     </li>
                </ul>
               <!--  <div class="regbuttons">
                        <div class="regbtn2">
                                
                            </div>
                        <div class="paybtn">
                            <input type="submit" value="Pay Now">
                        </div>
                </div> -->
            </form>
        </div>
    </div>
</section>
<script src="https://www.google.com/recaptcha/api.js?render=6Lfjh7oUAAAAAPZJDpn5AcCMoSz2aghEyvBQ-0Ct"></script>
<script>
grecaptcha.ready(function() {
    grecaptcha.execute('6Lfjh7oUAAAAAPZJDpn5AcCMoSz2aghEyvBQ-0Ct', {action: 'hrciregister'}).then(function(token) {
    document.getElementById("recaptcha").value = token;
    });
});
</script>

@include('frontend.includes.footer')