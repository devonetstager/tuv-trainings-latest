@include('frontend.includes.header')
<!-- ****Banner Section**** -->
<section class="innerbanner" style="background: url({{ asset('public/frontend/images/newsbanner.jpg') }}) no-repeat center top;">
    <div class="breadcrmb">
        <div class="wid">
            <h2>Courses</h2>
            <div class="brdcrmb">
                <a href="index.html">Home</a><Span>/</Span><small>News</small>
            </div>
        </div>
    </div>
</section>


<!-- Inner page content section -->
<section class="newssec">
        <div class="wid">
            <ul>
                <li>
                    <a href="#">
                        <div class="newsimg">
                            <img src="{{ asset('public/frontend/images/news1.jpg') }}">
                        </div>
                        <div class="newdate">
                            Jun 26, 2019
                        </div>
                        <p>
                            How to Effectively Manage an IT Crisis?
                        </p>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <div class="newsimg">
                            <img src="{{ asset('public/frontend/images/news2.jpg') }}">
                        </div>
                        <div class="newdate">
                            Jun 11, 2019
                        </div>
                        <p>
                            The Construction Project Delivery Process
                        </p>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <div class="newsimg">
                            <img src="{{ asset('public/frontend/images/news3.jpg') }}">
                        </div>
                        <div class="newdate">
                            May 20, 2019
                        </div>
                        <p>
                            How to Strive for Continual Improvement within IT
                        </p>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <div class="newsimg">
                            <img src="{{ asset('public/frontend/images/news4.jpg') }}">
                        </div>
                        <div class="newdate">
                            May 16, 2019
                        </div>
                        <p>
                            Checklist for Protecting Confidential Information in ..
                        </p>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <div class="newsimg">
                            <img src="{{ asset('public/frontend/images/news5.jpg') }}">
                        </div>
                        <div class="newdate">
                            JMay 13, 2019
                        </div>
                        <p>
                            Sync Your Sales & Marketing Efforts for a Successful Business
                        </p>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <div class="newsimg">
                            <img src="{{ asset('public/frontend/images/news6.jpg') }}">
                        </div>
                        <div class="newdate">
                            May 05, 2019
                        </div>
                        <p>
                            5 Key Factors of a Successful Negotiation
                        </p>
                    </a>
                </li>
    
    
                <li>
                        <a href="#">
                            <div class="newsimg">
                                <img src="{{ asset('public/frontend/images/news7.jpg') }}">
                            </div>
                            <div class="newdate">
                                Apr 29, 2019
                            </div>
                            <p>
                                Creating a Successful IT strategy
                            </p>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="newsimg">
                                <img src="{{ asset('public/frontend/images/news8.jpg') }}">
                            </div>
                            <div class="newdate">
                                Apr 25, 2019
                            </div>
                            <p>
                                Why Do Mergers & Acquisitions Take Place?
                            </p>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="newsimg">
                                <img src="{{ asset('public/frontend/images/news9.jpg') }}">
                            </div>
                            <div class="newdate">
                                Apr 20, 2019
                            </div>
                            <p>
                                What Is Learning and Development?
                            </p>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="newsimg">
                                <img src="{{ asset('public/frontend/images/news10.jpg') }}">
                            </div>
                            <div class="newdate">
                               Apr 18, 2019
                            </div>
                            <p>
                                Addressing Tail Risk within FRTB
                            </p>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="newsimg">
                                <img src="{{ asset('public/frontend/images/news11.jpg') }}">
                            </div>
                            <div class="newdate">
                                Mar 18, 2019
                            </div>
                            <p>
                                    Self-Development
                            </p>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="newsimg">
                                <img src="{{ asset('public/frontend/images/news6.jpg') }}">
                            </div>
                            <div class="newdate">
                                Mar 15, 2019
                            </div>
                            <p>
                                Healthier Financials from the Patient Experience
                            </p>
                        </a>
                    </li>
                    <li>
                            <a href="#">
                                <div class="newsimg">
                                    <img src="{{ asset('public/frontend/images/news1.jpg') }}">
                                </div>
                                <div class="newdate">
                                    Jun 26, 2019
                                </div>
                                <p>
                                    How to Effectively Manage an IT Crisis?
                                </p>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="newsimg">
                                    <img src="{{ asset('public/frontend/images/news2.jpg') }}">
                                </div>
                                <div class="newdate">
                                    Jun 11, 2019
                                </div>
                                <p>
                                    The Construction Project Delivery Process
                                </p>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="newsimg">
                                    <img src="{{ asset('public/frontend/images/news3.jpg') }}">
                                </div>
                                <div class="newdate">
                                    May 20, 2019
                                </div>
                                <p>
                                    How to Strive for Continual Improvement within IT
                                </p>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="newsimg">
                                    <img src="{{ asset('public/frontend/images/news4.jpg') }}">
                                </div>
                                <div class="newdate">
                                    May 16, 2019
                                </div>
                                <p>
                                    Checklist for Protecting Confidential Information in ..
                                </p>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="newsimg">
                                    <img src="{{ asset('public/frontend/images/news5.jpg') }}">
                                </div>
                                <div class="newdate">
                                    JMay 13, 2019
                                </div>
                                <p>
                                    Sync Your Sales & Marketing Efforts for a Successful Business
                                </p>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="newsimg">
                                    <img src="{{ asset('public/frontend/images/news6.jpg') }}">
                                </div>
                                <div class="newdate">
                                    May 05, 2019
                                </div>
                                <p>
                                    5 Key Factors of a Successful Negotiation
                                </p>
                            </a>
                        </li>
            
            
                        <li>
                                <a href="#">
                                    <div class="newsimg">
                                        <img src="{{ asset('public/frontend/images/news7.jpg') }}">
                                    </div>
                                    <div class="newdate">
                                        Apr 29, 2019
                                    </div>
                                    <p>
                                        Creating a Successful IT strategy
                                    </p>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <div class="newsimg">
                                        <img src="{{ asset('public/frontend/images/news8.jpg') }}">
                                    </div>
                                    <div class="newdate">
                                        Apr 25, 2019
                                    </div>
                                    <p>
                                        Why Do Mergers & Acquisitions Take Place?
                                    </p>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <div class="newsimg">
                                        <img src="{{ asset('public/frontend/images/news9.jpg') }}">
                                    </div>
                                    <div class="newdate">
                                        Apr 20, 2019
                                    </div>
                                    <p>
                                        What Is Learning and Development?
                                    </p>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <div class="newsimg">
                                        <img src="{{ asset('public/frontend/images/news10.jpg') }}">
                                    </div>
                                    <div class="newdate">
                                       Apr 18, 2019
                                    </div>
                                    <p>
                                        Addressing Tail Risk within FRTB
                                    </p>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <div class="newsimg">
                                        <img src="{{ asset('public/frontend/images/news11.jpg') }}">
                                    </div>
                                    <div class="newdate">
                                        Mar 18, 2019
                                    </div>
                                    <p>
                                            Self-Development
                                    </p>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <div class="newsimg">
                                        <img src="{{ asset('public/frontend/images/news6.jpg') }}">
                                    </div>
                                    <div class="newdate">
                                        Mar 15, 2019
                                    </div>
                                    <p>
                                        Healthier Financials from the Patient Experience
                                    </p>
                                </a>
                            </li>
            </ul>
        </div>
    </section>

@include('frontend.includes.footer')