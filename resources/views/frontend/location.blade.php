@include('frontend.includes.header')
<!-- ****Banner Section**** -->
<section class="innerbanner" style="background: url({{asset('public/frontend/images/Location.jpg')}}) no-repeat center top;">
   <div class="breadcrmb">
      <div class="wid">
         <h2>Contact Us</h2>
         <div class="brdcrmb">
            <a href="{{route('index')}}">Home</a><span>/</span>Contact Us<span>/</span><small>Locations</small>
         </div>
      </div>
   </div>
</section>
<!-- Inner page content section -->
<section class="locationss">
   <div class="wid">
      <div id="verticalTab">
         <ul class="resp-tabs-list">
            @foreach($locations as $location)
            <li lat="{{$location->latitude}}" long="{{$location->longitude}}">{{$location->name}}</li>
            @endforeach
         </ul>
         <div class="resp-tabs-container">
            <?php $count = 0 ;?>
            @foreach($locations as $location)
            <div>
               <div class="locdet">
                  <h3>Address</h3>
                  <p>
                     <!--TUV NORD Training Center<br>-->
                     {{$location->building}},<br>
                     {{$location->addressline1}},<br>
                     {{$location->addressline2}}<br>
                     {{$location->phone}}<br>
                     {{$location->state}},<br>
                     {{$location->country}}
                  </p>
               </div>
               <div class="locmap">
                  <h2>Map</h2>
                  <div id="map_canvas{{$count}}" style="width:100%; height:250px; margin-left:80px;"></div>
                  <?php ++$count ;?> 
               </div>
            </div>
            @endforeach
         </div>
      </div>
   </div>
</section>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDnUbzrV5PzdJzVHuXrWE715wkq4r0RvjQ"
   async defer></script>
<script type="text/javascript">
   var map1, map2;
   
   function initGoogleMap(latitude,longitude,position) {
       // create the maps
       var geocodes = {lat: parseFloat(latitude), lng:parseFloat(longitude)};
   
       var myOptions = {
           zoom: 14,
           center: new google.maps.LatLng(latitude,longitude),
           mapTypeId: google.maps.MapTypeId.ROADMAP
       }
       
       map1 = new google.maps.Map(document.getElementById("map_canvas"+position), myOptions);
       var marker = new google.maps.Marker({position: geocodes, map: map1});
   }
   
   $(document).ready(function(){
       
       $("#verticalTab .resp-tabs-list li").click(function(){
           var ariaCtrl = $(this).attr('aria-controls');
           var position =  ariaCtrl.substr(ariaCtrl.length - 1);
           initGoogleMap($(this).attr('lat'),$(this).attr('long'),position);
       });
   });
   
   
</script> 
<script type="text/javascript" src="{{ asset('public/frontend/js/easy-responsive-tabs.js') }}"></script>
<script>
   $(document).ready(function () {
        $('#horizontalTab').easyResponsiveTabs({
            type: 'default', //Types: default, vertical, accordion           
            width: 'auto', //auto or any width like 600px
            fit: true,   // 100% fit in a container
            closed: 'accordion', // Start closed if in accordion view
        activate: function(event) { // Callback function if tab is switched
            var $tab = $(this);
            var $info = $('#tabInfo');
            var $name = $('span', $info);
            $name.text($tab.text());
            $info.show();
        }
        });
        $('#verticalTab').easyResponsiveTabs({
                type: 'vertical',
                width: 'auto',
                fit: true
        });

        var lat = $("#verticalTab .resp-tabs-list li:first-child").attr('lat');
        var long = $("#verticalTab .resp-tabs-list li:first-child").attr('long');
        var ariaCtrl = $("#verticalTab .resp-tabs-list li:first-child").attr('aria-controls');
        console.log(ariaCtrl);
        
        var position =  ariaCtrl.substr(ariaCtrl.length - 1);
        initGoogleMap(lat,long,position);
   });
</script>
@include('frontend.includes.footer')