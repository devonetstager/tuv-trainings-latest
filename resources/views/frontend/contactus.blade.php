@include('frontend.includes.header')
<!-- ****Banner Section**** -->
<section class="innerbanner" style="background: url({{ asset('public/frontend/images/contactus.jpg') }}) no-repeat center top;">
    <div class="breadcrmb">
        <div class="wid">
            <h2>Contact Us</h2>
            <div class="brdcrmb">
                <a href="index.html">Home</a><span>/</span><small>Contact Us</small>
            </div>
        </div>
    </div>
</section>

<!-- Inner page content section -->
<section class="contactuss">
    <div class="wid">
        <p>We believe that communication is the key to a beautiful relation, and it all starts here. Let’s chat, then let’s put forward light that will hit the nail on the head. Get in touch today.
        </p>
        <div class="contfomr">
            <h1>Enquire Now</h1>
            <form method="post" action="{{route('contact-enquiry')}}">
                @csrf
                  {!! $errors->first('recaptcha', '<p style="color:red;" class="help-block error">session expired</p>') !!}
                <ul>
                    <li>
                        <input type="text" name="name" value="{{old('name')}}"   placeholder="Name">
                    {!! $errors->first('name', '<p style="color:red;" class="help-block error">:message</p>') !!}
                    </li>
                    <li>
                        <input type="email" name="email" value="{{old('email')}}"   placeholder="Email">
                    {!! $errors->first('email', '<p style="color:red;" class="help-block error">:message</p>') !!}
                    </li>
                    <li>
                        <input type="tel" name="mobile"   placeholder="Phone">
                    </li>
                    <li>
                        <input type="text" name="company"   placeholder="Company">
                    </li>
                    <li class="ttarea">
                        <textarea  name="message"  placeholder="Message"></textarea>
                           <input type="hidden"  required="required" name="recaptcha" value="" id="recaptcha" >
                    </li>
                    <li class="frmbtn">
                     @php $enquiry_url= Request::fullUrl(); @endphp
               <input type="hidden" name="enquiry_url" value="{{$enquiry_url}}">
                        <input type="submit" class="odbttn">
                    </li>
                </ul>
            </form>
        </div>
        
     <!--Map Section -->
    <section class="mapsec">
        <div class="wid">
            <div class="map">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3632.6140067010747!2d54.43398331446904!3d24.429473868412817!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3e5e684bfe0e8f73%3A0x65c4d88e1ea42b0f!2sTUV+NORD+Middle+East!5e0!3m2!1sen!2sin!4v1564423733196!5m2!1sen!2sin" width="100%" height="375" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
        </div>
    </section>
         <div class="contdtlss">
            <ul>
                <li>
                    <a href="{{$contact->phone}}"><span>Phone</span> :{{$contact->phone}}</a>
                </li>
                <li class="fax">
                    <span>Fax</span> : {{$contact->fax  }}
                </li>
                <li>
                    <a href="mailto:{{$contact->email}}"><span>Email</span> : {{$contact->email}}</a>
                </li>
            </ul>
        </div>
    </div>
</section>
<script src="https://www.google.com/recaptcha/api.js?render=6Lfjh7oUAAAAAPZJDpn5AcCMoSz2aghEyvBQ-0Ct"></script>
<script>
grecaptcha.ready(function() {
    grecaptcha.execute('6Lfjh7oUAAAAAPZJDpn5AcCMoSz2aghEyvBQ-0Ct', {action: 'contactus'}).then(function(token) {
    //   console.log(token);
    document.getElementById("recaptcha").value = token;
    });
});
</script>

@include('frontend.includes.footer')