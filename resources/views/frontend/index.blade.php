@include('frontend.includes.header')
<div id="div1" class="popupp" style="display:none;">
   <div class="fadeout"></div>
   <div class="frompop">
      <h2>Download Calender </h2>
      <div class="cancel" onclick="closePopup();"></div>
      <form name="quick_enquire" method="post" action="{{route('calander.download')}}">
         @csrf
         <!-- <input type="hidden" name="_token" value="sK8tygRnPASzz5IMjAFnrj7TvG1CeqvGLb9bJE9j">   -->             
         <ul>
         <ul>
            <li>
               <input type="text" name="name" required="required" id="textfield" placeholder="Name">
            </li>
            <li>
               <input type="text" name="email" required="required" id="textfield1" placeholder="Email">
            </li>
            <li>
               <input type="text" name="mobile" required="required" id="textfield2" placeholder="Phone">
            </li>
            <li>
               <input type="submit" name="button" id="button" value="Submit">
            </li>
         </ul>
      </form>
   </div>
</div>
<section class="bannersec">
   <div class="slideCvr">
      <ul id="bannerSlider" class="bannerSlider">
         @foreach($homebanner as $banner)
         <li>
            <div class="bannerimg" style="background: url({{asset('storage/app/homebanner/')}}/{{$banner->banner_image}}) no-repeat center top;">
               <div class="wid">
                  <div class="bannertext">
                     <h1>{{$banner->title}}</h1>
                  </div>
               </div>
            </div>
         </li>
         @endforeach
      </ul>
   </div>
  <div class="calenderBanner">
      <div class="calenderBann">
         <small>Training Calender</small>
         <div class="calenderImgBanner"></div>
         <a onClick="openPopup('div1');" class="downloadCalnder" title="Download Now">Download Now</a>
      </div>
   </div>
   <div class="bannerform">
      <div class="wid">
        <form action="{{route('course.search')}}" method="post">
            <div id="courselist">
            </div>
            {{ csrf_field() }}
            <ul>
               <li class="type">
                  <input type="text" id="course" placeholder="Course Name" name="course">
               </li>
               <li class="selct custom-select">
                  <select name="category">
                     <option class="category" value="">Category</option>
                     <option class="category" value="">All Category</option>
                     @foreach($categories as $category)
                     <option class="category" value="{{$category->category_id}}">{{$category->name}}</option>
                     @endforeach
                  </select>
               </li>
               <li class="selct2 custom-select">
                  <select name="month">
                     <option class="category" value="">Month</option>
                     <option class="category" value="">All Months</option>
                     @foreach($months as $num => $name)
                     <option value="{{$num}}">{{$name}}</option>
                     @endforeach
                  </select>
               </li>
               <li class="buttn">
                  <input type="submit" value="Search">
               </li>
            </ul>
         </form>
      </div>
   </div>
</section>
<!-- Upcoming Courses -->
<section class="courses">
   <div class="wid">
      <div class="headng">
         Upcoming Courses
      </div>
      <ul>
         @foreach($courses as $course)
         <li>
            <div class="courseimg">
               <a href="{{route('course-detail',[encrypt($course->course_id)])}}" class="crsbtnImg">
               <img src="{{asset('storage/app/courses/courseimage/')}}/{{$course->image}}"></a>
            </div>
            <a href="{{route('course-detail',[encrypt($course->course_id)])}}" class="crsbtn">
            {{str_limit(strip_tags($course->name), 40)}}
            </a>
            <div class="courceLoc">
               @foreach($course->coursedetails as $details)
               <a href="#" class="locIcon">{{str_limit(strip_tags($details->location), 18)}}</a>
               <a href="#" class="timeIcon">{{ date('d M', strtotime($details->start_date))}} - {{date('d M  Y', strtotime($details->end_date))}}</a>
               @endforeach
            </div>
            <a href="{{route('course-registration', [encrypt($course->course_id)])}}" class="smlbtn">
            Register Now
            </a>
         </li>
         @endforeach
      </ul>
      <div class="vlbttn">
         <a href="{{route('course.filter',date('m') )}}" class="odbttn">
         View All
         </a>
      </div>
   </div>
</section>
<!--  Webinar Section -->
<section class="newssec">
   <div class="wid">
      <div class="headng">
         Webinars
      </div>
      <ul>
         @foreach($webinars as $webinar)
         <li>
         <a target="_blank" href="{{$webinar->webinar_link}}">
            <div class="newsimg">
            <img src="{{asset('/storage/app/webinar/image')}}/{{$webinar->webinar_image}}">
            </div>
            <div class="newdate">
              {{date('d M Y', strtotime($webinar->webinar_date))}}
            </div>
            <p>
               {{$webinar->title}}
            </p>
            </a>
         </li>
         @endforeach
      </ul>
     
   </div>
</section>
<!-- ****category Section**** -->
<section class="categry">
   <div class="wid">
      <div class="headng">
         Categories
      </div>
      <ul>
         @foreach($categories as $category)
         <li>
            <!--<a href="{{route('courses', [encrypt($category->category_id)])}}"> -->
<a  @if($category->name == "ISO / Management System Trainings") href="  {{route('sub-categories', [encrypt($category->category_id)])}}" @else href="{{route('courses', [encrypt($category->category_id)])}}" @endif>               <div class="catimg">
                  <img src="{{asset('storage/app/categories/')}}/{{$category->image}}">
               </div>
               <div class="cathead">
                  {{ str_limit(strip_tags($category->name), 40) }}
               </div>
            </a>
         </li>
         @endforeach
      </ul>
   </div>
</section>
<!-- Latest News Section -->
<section class="newssec">
   <div class="wid">
      <div class="headng">
         Technology Blog
      </div>
      <ul>
         @foreach($newsdata as $news)
         <li>
         <a href="{{route('news-detail',[encrypt($news->news_id)])}}">
            <div class="newsimg">
            <img src="{{asset('/storage/app/news/imagethumb')}}/{{$news->image_banner}}">
            </div>
            <div class="newdate">
              {{$news->news_date}}
            </div>
            <p>
               {{$news->title}}
            </p>
            </a>
         </li>
         @endforeach
      </ul>
      <div class="vlbttn">
         <a href="{{route('corporate-news')}}" class="odbttn">
         View All
         </a>
      </div>
   </div>
</section>
<!-- Six Sigma Courses -->
<section class="sixsigma">
   <div class="wid">
      <div class="headng white">
         NEBOSH Courses
      </div>
      <ul>
         @foreach($sigmacourses as $sigma)
         <li>
            <a href="{{route('sigma-detail', [encrypt($sigma->six_sigma_id)])}}">
               <div class="siximg">
                  <img src="{{asset('/storage/app/sigmacourses')}}/{{$sigma->course_image}}">
               </div>
               <div class="sixtext">
                  {{ $sigma->name}}
               </div>
            </a>
         </li>
         @endforeach
      </ul>
   </div>
</section>
<!-- HRCI Section -->
<section class="hrci">
   <div class="wid">
      <div class="headng">
         CQI | IRCA Certifications
      </div>
      <ul>
         @foreach($hrcicourses as $hrci)
         <li>
            <a href="{{route('hrci-detail', [encrypt($hrci->hrci_id)])}}">
               <div class="hrciimg">
                  <img src="{{asset('/storage/app/hrci/hrciimage')}}/{{$hrci->hrci_image}}">
               </div>
               <p>
                  {{$hrci->name}}
               </p>
            </a>
         </li>
         @endforeach
      </ul>
      </div>
</section>
<!-- Other Ceritifications Section -->
<section class="sixsigma cipsScn">
   <div class="wid">
      <div class="headng white">
        Other Certifications
      </div>
      <ul>        
         <li>
             <a href="https://tuvnordtrainings.com/courses/eyJpdiI6Ilp1V216OHQrU1M2bWlRXC9xbDV0TEdnPT0iLCJ2YWx1ZSI6Ik5YXC9BbnZXSjRQR1pvOXFxbzhUT2hBPT0iLCJtYWMiOiIzMTdkYjM2Y2RjMWE5ZmJiZTAwNTE1OTY4NDZlYjFjMjk2YWY2Mjg4Y2RhMzBlZGI2N2U5MGFhOTFiNjlmMDE4In0=">
               <div class="siximg">
                   <img src="{{asset('public/frontend/images/other/cips.jpeg')}}">
               </div>
               <div class="sixtext">
               CIPS Courses - Certificate, Advanced Certificate, Diploma, Advanced Diploma and Professional Diploma
               </div>
            </a>
         </li>
         <li>
             <a href="https://tuvnordtrainings.com/courses/eyJpdiI6IjZiZlIwR3VmRnk1UE1CRElDNVZ4SlE9PSIsInZhbHVlIjoiek9ZV0tFa3JwWXBuc3dUblVNb09ydz09IiwibWFjIjoiNmZkYjlmNjJhMDdjNGYzYTQ4NjgzZDg3N2ZhNzc5YWZhNDZjNzIyOGZmNGEzZmM3ODViM2Y3ODA5ZmZhZGYxMSJ9">
               <div class="siximg">
                   <img src="{{asset('public/frontend/images/other/habc.jpeg')}}">
               </div>
               <div class="sixtext">
             HABC Courses - Basic Emergency First Aid at work, Paediatric First Aid, Advance Emergency First Aid at work
               </div>
            </a>
         </li>
         <li>
             <a href="https://tuvnordtrainings.com/courses/eyJpdiI6Ik5JMk80c2xhUlgrSktEZWxEOGhEUFE9PSIsInZhbHVlIjoiM3dlVjJycE52WjlUYVwvWFhLMERSYlE9PSIsIm1hYyI6ImViNzk3ZWVmMzIwZjNhZDQzMThiY2FiNWNlNzZlNTA4YjFiZmVkMDZkMzk4ZDU4MThjZGNmMTllMzdmOTNmZTcifQ==">
               <div class="siximg">
                   <img src="{{asset('public/frontend/images/other/hrci.jpeg')}}">
               </div>
               <div class="sixtext">
             HRCi Courses - Associate Professional, Professional, Senior Professional and Global Professional in HR
               </div>
            </a>
         </li>
           <li>
             <a href="https://tuvnordtrainings.com/courses/eyJpdiI6IjZiZlIwR3VmRnk1UE1CRElDNVZ4SlE9PSIsInZhbHVlIjoiek9ZV0tFa3JwWXBuc3dUblVNb09ydz09IiwibWFjIjoiNmZkYjlmNjJhMDdjNGYzYTQ4NjgzZDg3N2ZhNzc5YWZhNDZjNzIyOGZmNGEzZmM3ODViM2Y3ODA5ZmZhZGYxMSJ9">
               <div class="siximg">
                   <img src="{{asset('public/frontend/images/other/iosh.jpeg')}}">
               </div>
               <div class="sixtext">
            IOSH Courses available - IOSH Managing Safely, IOSH Working Safely and IOSH Managing Safely in Construction
               </div>
            </a>
         </li>
           <li>
             <a href="https://tuvnordtrainings.com/courses/eyJpdiI6InVGKzk0MUcwUnZaS1FuWEdMd0Fnd1E9PSIsInZhbHVlIjoiM01OT1ZWQUdPdUk1WlZqQlwvR0pjUEE9PSIsIm1hYyI6ImQ0Y2FkY2Y3NTM0N2NkYWNmYzY4OTY3NDA1NmY2NDUzZmUzMWYwYzY3ZjYzMGI2NDFlN2JiNTRmNjE5OTM4ZjMifQ==">
               <div class="siximg">
                   <img src="{{asset('public/frontend/images/other/asq.jpeg')}}">
               </div>
               <div class="sixtext">
           ASQ Courses available - Certified Six Sigma Green Belt and Black Belt, Lean Six Sigma Yellow Belt, Green Belt and Black Belt
               </div>
            </a>
         </li>
      </ul>
   </div>
</section>
<!-- Testimonials -->
<section class="testimonials">
   <div class="wid">
      <div class="headng">
         Testimonials
      </div>
      <div class="slideCvr">
         <ul id="testSlider" class="testSlider">
            @foreach($testimonials as $testimonial)
            <li>
               <div class="testim">
                  <p>
                     {!!$testimonial->content!!}
                  </p>
                  <span>
                  {{$testimonial->name}}
                  </span>
               </div>
            </li>
            @endforeach
         </ul>
      </div>
   </div>
</section>
<!-- Qualifications -->
<section class="qualifications">
   <div class="wid">
      <div class="headng">
         Qualifications / Accreditation
      </div>
      <ul id="brandsSlider" class="testSlider">
         @foreach($qualifications as $qualification)
         <li>
            <img src="{{asset('/storage/app/qualification/images/')}}/{{$qualification->image}}">
         </li>
         @endforeach
      </ul>
   </div>
</section>
@include('frontend.includes.footer')