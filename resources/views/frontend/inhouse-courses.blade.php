@include('frontend.includes.header')
<!-- ****Banner Section**** -->
<style>
    .coursedtls .coursecat .crcat li a:hover {
    color: #fff !important;
}
</style>
<section class="innerbanner" style="background: url({{asset('public/frontend/images/InHouseCourse.jpg')}}) no-repeat center top;">
    <div class="breadcrmb">
        <div class="wid">
            <h2>Services</h2>
            <div class="brdcrmb">
                <a href="{{route('index')}}">Home</a><Span>/</Span><a href="">Services</a><Span>/</Span><small>In House Courses</small>
            </div>
        </div>
    </div>
</section>
<!-- Inner page content section -->
<section class="innerpg coursedtls">
    <div class="wid">
        <div class="abt_menu">
             <div class="coursecat" style="width: 100%;">
                    <div class="secname" style="margin-top: 20px;">
                        Services
                    </div>
                    <ul class="crcat" id="month_list">
                        <li style="width: 100%;"><a  href="{{route('public-courses')}}" title="Public Courses" style="width: 100%;">Public Courses</a></li>
                <li style="width: 100%;"><a class="cat_active" href="{{route('in-house-courses')}}" title="TÜV NORD" style="width: 100%;">In House Courses</a></li>
                <li style="width: 100%;"><a href="{{route('integrated-e-learning')}}" title="Integrated E Learning" style="width: 100%;">Integrated E Learning</a></li>
               
                <li style="width: 100%;"><a href="{{route('conferences')}}" title="Conferences" style="width: 100%;">Conferences</a></li>  
                    </ul>
            </div>     
     </div>
        <div class="abtcnt mncntnt">
             <h1>{{$cmspage->name}}</h1>
            <p>
                {!!$cmspage->content!!}
            <a href="{{route('course.filter',date("m") )}}" class="odbttn">
         View All
         </a>
      </div>

        </div>
    </div>
</section>
@include('frontend.includes.footer')