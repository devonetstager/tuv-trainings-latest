@include('frontend.includes.header')

<!-- ****Banner Section**** -->
<section class="innerbanner" style="background: url({{ asset('public/frontend/images/newsbanner.jpg')}}) no-repeat center top;">
    <div class="breadcrmb">
        <div class="wid">
            <h2>News Details</h2>
            <div class="brdcrmb">
                <a href="{{route('index')}}">Home</a> / Media / <small>News</small>
            </div>
        </div>
    </div>
</section>


<!-- Inner page content section -->


<section class="newssecdet">
        <div class="wid">
                <div class="socshare">
                    @php
                        echo Share::currentPage()->facebook()->twitter()->linkedin();
                    @endphp
                        <!-- <ul>
                            <li><a href="#" class="fbpg"></a>
                            </li>
                            <li><a href="#" class="twpg"></a>
                            </li>
                            <li><a href="#" class="instapg"></a>
                            </li>
                        </ul> -->
                        <h2>{{$datas->title}}</h2>
                        <div class="datte">  {{   date('d,F, Y', strtotime($datas->news_date))}}</div>
                    </div>
                    <div class="newsindt">
                            <div class="newws-img">
                                    <img src="{{asset('/storage/app/news/imagebanner')}}/{{$datas->image_banner}}">
                                </div>
                        <div class="newws">
                            <p>{!! $datas->content !!} </p>
                        </div>
                    </div>
        </div>
    </section>
      <section class="newssec">
        <div class="wid">
            <div class="headng">
            Related listings
            </div>
            <ul>
                @foreach($newsdata as $news)
                <li>
                    <a href="{{route('news-detail',[encrypt($news->news_id)])}}">
                        <div class="newsimg">
                        <a href="{{route('news-detail',[encrypt($news->news_id)])}}">
                            <img src="{{asset('/storage/app/news/imagebanner')}}/{{$news->image_banner}}">
                    </a>
                        </div>
                        <div class="newdate">
                        <a  class="newdate" href="{{route('news-detail',[encrypt($news->news_id)])}}">
                           {{$news->news_date}}</a>
                        </div>
                        <p>
                           {{$news->title}}
                        </p>
                    </a>
                </li>
                @endforeach
             
            </ul>
        </div>
    </section>
    @include('frontend.includes.footer')