@include('frontend.includes.header')
<!-- ****Banner Section**** -->
<section class="innerbanner" style="background: url({{ asset('public/frontend/images/hrcibanner.jpg')}}) no-repeat center top;">
    <div class="breadcrmb">
        <div class="wid">
            <h2>NEBOSH Courses</h2>
            <div class="brdcrmb">
                <a href="{{route('index')}}">Home</a>/ NEBOSH<Span>/<small>{{$datas->name}}™</small>
            </div>
        </div>
    </div>
</section>


<!-- Inner page content section -->

<section class="innerpg">
    <div class="wid">
        <div class="abt_menu">
            <div class="crselogo">
               <img src="{{asset('storage/app/sigmacourses/')}}/{{$datas->course_image}}">
               <p>
               {{$datas->name}}
               </p>
            </div>
            <div class="crsebttn">
            <a href="{{route('sigma-registration',[encrypt($datas->six_sigma_id)])}}'" class="odbttn">
                Apply Now
            </a>
        </div>
        
        
        	<div class="vdoLft">
        	    <iframe width="560" height="315" src="{{$datas->youtube_link}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe>
        	    
            	<!--<iframe width="560" height="315" src="{!!$datas->youtube_link !!}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>-->
            </div>
        </div>
        <div class="abtcnt sigma_detail">
           {!!$datas->description!!}
           <div id="location">
            <h2>Location &amp; Date</h2>
            <ul class="locdate">
                <li>
                    <div class="year">
                    {{ date('Y', strtotime($datas->start_date))}}
                    </div>
                    <div class="detlss">
                        <ul>
                            <li>
                               <img src="{{ asset('public/frontend/images/date.png') }}">
                             {{ date('d M', strtotime($datas->start_date))}}-{{date('d M  Y', strtotime($datas->end_date))}}
                            </li>
                            <li>
                                 <img src="{{ asset('public/frontend/images/location.png') }}">
                                    {{ $datas->location}}
                            </li>
                        </ul>
                        <div class="regbtn">
                            <a href="{{route('sigma-registration',[encrypt($datas->six_sigma_id)])}}" class="odbttn ">Register Now</a>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
        <div id="fees">
            <h2>Fees</h2>
            <ul class="locdate">
                <li>
                    <div class="year">
                    {{ date('Y', strtotime($datas->start_date))}}
                    </div>
                    <div class="detlss fee">
                        <ul>
                            <li class="perpart">
                                Per participant
                            </li>
                            <li class="feees">
                                <span>{{$datas->currency->symbol}} {{ number_format($datas->fee)}}</span>
                            </li>
                        </ul>
                    </div>
                </li>
            </ul>
            <div class="condtns">
                Fees + VAT as applicable<br>
                Tax Registration Number : 100139824100506
            </div>
        </div>
        </div>
        
    </div>
</section>
    <section>
        <div class="wid">
            <div class="abtcnt">
                <div class="courses crseinner">
                        <div class="wid">
                            <ul>
                                @foreach($sigmacourses as $sigma)
                                            
                            <li>
                                <a href="{{route('sigma-detail', [encrypt($sigma->six_sigma_id)])}}">
                                    <div class="siximg">
                                        <img src="{{asset('storage/app/sigmacourses/')}}/{{$sigma->course_image}}">
                                    </div>
                                    <div class="sixtext">
                                    {{ $sigma->name}}
                                    </div>
                                </a>
                            </li>        
                               @endforeach                            
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@include('frontend.includes.footer')