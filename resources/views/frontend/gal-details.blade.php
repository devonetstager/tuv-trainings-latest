@include('frontend.includes.header')
<!-- ****Banner Section**** -->
<section class="innerbanner" style="background: url({{ asset('/public/frontend/images/gallerybanner.jpg')}}) no-repeat center top;">
    <div class="breadcrmb">
        <div class="wid">
            <h2>Gallery</h2>
            <div class="brdcrmb">
                <a href="{{route('index')}}">Home</a><Span>/</Span>About Us<Span>/</Span><small>Gallery</small>
            </div>
        </div>
    </div>
</section>


<!-- Inner page content section -->
<section class="gallery">
   <div class="wid">
      <ul class="grid lightgallery" id="lightgallery">
         @foreach($datas as $data)
         @if($data->photo)
         <li data-src="{{asset('storage/app/gallery/photo/')}}/{{$data->photo}}">
            <a href="{{asset('storage/app/gallery/photo/')}}/{{$data->photo}}">
               <div class="galimg">
                  <img src="{{asset('storage/app/gallery/photo/')}}/{{$data->photo}}">
               </div>
            </a>
         </li>
         @endif
         @if($data->vedio_url)
         <div class="pressImgVdoScn">
            <ul>
               <li>
                  <div class="cvrBox">
                     <iframe width="560" height="315" src="{{$data->vedio_url}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                  </div>
               </li>
            </ul>
         </div>
         @endif
         @endforeach
         
      </ul>
       {{$datas->links()}}
   </div>
</section>

<!--<section class="gallery">-->
<!--    <div class="wid">-->
<!--        <ul class="grid lightgallery" id="lightgallery">-->
<!--            @foreach($datas as $data)-->
<!--            <li data-src="{{asset('storage/app/gallery/photo/')}}/{{$data->photo}}">-->
<!--                <a href="{{asset('storage/app/gallery/photo/')}}/{{$data->photo}}">-->
<!--                <div class="galimg">-->
<!--                <img src="{{asset('storage/app/gallery/photo/')}}/{{$data->photo}}">-->
<!--                </div> -->
<!--                </a>-->
<!--            </li>-->
<!--            @endforeach-->
         
<!--        </ul>-->
<!--    </div>-->
<!--</section>-->


@include('frontend.includes.footer')