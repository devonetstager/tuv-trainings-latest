@include('frontend.includes.header')

<!-- ****Banner Section**** -->
<section class="innerbanner" style="background: url({{ asset('public/frontend/images/regbanner.jpg') }}) no-repeat center top;">
    <div class="breadcrmb">
        <div class="wid">
            <h2>Registration</h2>
            <div class="brdcrmb">
                <a href="index.html">Home</a><span>/</span><small>Registration</small>
            </div>
        </div>
    </div>
</section>


<!-- Inner page content section -->

<section class="contactuss regnow">
    <div class="wid">
            <div class="contfomr">
                    <h1>Course Details</h1>
                    <form>
                        <ul>
                            <li class="selct custom-select">
                                <select>
                                    <option class="category" value="0">Category</option>
                                    <option value="1">Project & Program Management</option>
                                    <option value="2">HSE</option>
                                    <option value="3">Mechanical</option>
                                    <option value="4">Construction</option>
                                    <option value="5">Marine</option>
                                    <option value="6">Oil & Gas</option>
                                    <option value="7">Management & Leadership</option>
                                    <option value="8">Human Resource</option>
                                    <option value="9">Quality</option>
                                    <option value="10">Information Technology</option>
                                    <option value="11">Marketing & Sales</option>
                                    <option value="12">Geology</option>
                                    <option value="13">Procurement & Supply</option>
                                    <option value="14">Electrical & Instrumentation</option>
                                    <option value="15">Accounting & Finance</option>
                                </select>
                            </li>
                            <li class="selct custom-select">
                                <select>
                                    <option class="category" value="0">Course Name</option>
                                    <option value="1">Project & Program Management</option>
                                    <option value="2">HSE</option>
                                    <option value="3">Mechanical</option>
                                    <option value="4">Construction</option>
                                    <option value="5">Marine</option>
                                    <option value="6">Oil & Gas</option>
                                    <option value="7">Management & Leadership</option>
                                    <option value="8">Human Resource</option>
                                    <option value="9">Quality</option>
                                    <option value="10">Information Technology</option>
                                    <option value="11">Marketing & Sales</option>
                                    <option value="12">Geology</option>
                                    <option value="13">Procurement & Supply</option>
                                    <option value="14">Electrical & Instrumentation</option>
                                    <option value="15">Accounting & Finance</option>
                                </select>
                            </li>
                        </ul>
                        <span>Available Dates</span>
                        <ul class="halfform">
                                <li class="selct custom-select">
                                    <select>
                                        <option class="category" value="0">Date</option>
                                    </select>
                                </li>
                                <li class="selct custom-select">
                                    <select>
                                        <option class="category" value="0">City</option>
                                        <option value="1">City Name1</option>
                                        <option value="2">City Name2</option>
                                    </select>
                                </li>
                                <li class="selct custom-select">
                                        <select>
                                            <option class="category" value="0">Language</option>
                                            <option value="1">Language 1</option>
                                            <option value="2">Language 2</option>
                                        </select>
                                    </li>
                                    <li class="selct custom-select">
                                        <select>
                                            <option class="category" value="0">Venue</option>
                                            <option value="1">Venue 1</option>
                                            <option value="2">Venue 2</option>
                                        </select>
                                    </li>
                                    <li class="rgradio">
                                        <ul>
                                            <li class="regtype">
                                                Register Type
                                            </li>
                                            <li>
                                                <input type="radio">
                                                <label>Company Paid</label>
                                            </li>
                                            <li>
                                                <input type="radio"> <label>Self Paid</label>
                                            </li>
                                        </ul>
                                    </li>
                            </ul>
                </div>
        <div class="contfomr">
            <h1>Personal Details</h1>
                <ul>
                    <li>
                        <input type="text" placeholder="Name">
                    </li>
                    <li>
                        <input type="email" placeholder="Email">
                    </li>
                    <li>
                        <input type="tel" placeholder="Phone">
                    </li>
                    <li>
                        <input type="text" placeholder="Company">
                    </li>
                    <li>
                        <input type="text" placeholder="Address">
                    </li>
                    <li>
                        <input type="text" placeholder="P.O Box">
                    </li>
                    <li>
                        <input type="text" placeholder="City">
                    </li>
                    <li>
                        <input type="text" placeholder="Country">
                    </li>
                </ul>
                <div class="regbuttons">
                        <div class="regbtn2">
                                <input type="submit" value="Register and Pay Later">
                            </div>
                        <div class="paybtn">
                            <input type="submit" value="Pay Now">
                        </div>
                </div>
            </form>
        </div>
    </div>
</section>



@include('frontend.includes.footer')