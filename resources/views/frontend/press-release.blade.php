@include('frontend.includes.header')
<!-- ****Banner Section**** -->
<section class="innerbanner" style="background: url({{asset('public/frontend/images/press_release.jpg')}}) no-repeat center top;">
    <div class="breadcrmb">
        <div class="wid">
            <h2>Media</h2>
            <div class="brdcrmb">
                <a href="{{route('index')}}">Home</a><Span>/</Span>Media<Span>/</Span><small>Press Release</small>
            </div>
        </div>
    </div>
</section>
<!-- Inner page content section -->
<section class="innerpg coursedtls">
    <div class="wid">
        <div class="abt_menu">
        <div class="coursecat" style="width: 100%;">
                    <div class="secname" style="margin-top: 20px;">
                        About Us
                    </div>
        <ul class="crcat" id="month_list">
                <li style="width: 100%;"><a href="{{route('tuv-middle-east')}}"  title="TÜV Middle East">TÜV Middle East</a></li>
                <li style="width: 100%;"><a href="{{route('tuv-nord')}}" title="TÜV NORD">TÜV NORD</a></li>
                <li style="width: 100%;"><a href="{{route('tuv-nord-training')}}" title="TÜV NORD Training Centre">TÜV NORD Training Centre</a></li>
                <li style="width: 100%;"><a href="{{route('qualification-list')}}" title="Qualifications">Qualifications</a></li>
                <li style="width: 100%;"><a href="{{route('clientele')}}" title="Clientele">Clientele</a></li>
                <li style="width: 100%;"><a href="{{route('testimonials-list')}}" title="Testimonials">Testimonials</a></li>
                <li style="width: 100%;"><a href="{{route('appreciation')}}" title="Appreciation Letters">Appreciation Letters</a></li>
                <li style="width: 100%;"><a class="cat_active" href="{{route('press-release')}}" title="Press Release">Press Release</a></li>
                <li style="width: 100%;"><a href="{{route('gallery')}}" title="Photo Gallery">Photo Gallery</a></li>
                <li style="width: 100%;"><a href="{{route('faq')}}" title="FAQ">FAQ</a></li>
            </ul>
        </div>
        </div>
        <div class="abtcnt">
            <h1>Press Release</h1>
<section class="pressReleaseScn">

<div class="wid">
{{-- <div class="pressReleaseBox">
	<div class="heading">Press Release 1</div>
<div class="pressDescription">
	<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries,</p>
    <p>but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
</div>
<div class="pressImgVdoScn">
	<ul>
        
    	<li>
        	<div class="cvrBox">
            	<img src="https://static.doubleclick.net/dynamic/5/88875538/11814691884427522239_12807243536607706362.jpeg">
            </div>
        </li>
    </ul>
</div>
</div>



<div class="pressReleaseBox">
	<div class="heading">Press Release 2</div>
	
<div class="pressDescription">
	<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries,</p>
    <p>but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
</div>
<div class="pressImgVdoScn">
	<ul>
        
    	<li>
        	<div class="cvrBox">
            	<img src="https://static.doubleclick.net/dynamic/5/88875538/15769820392483399614_16886230700783032899.jpeg">
            </div>
        </li>
        
        
    	<li>
        	<div class="cvrBox">
            	<img src="https://static.doubleclick.net/dynamic/5/88875538/11814691884427522239_12807243536607706362.jpeg">
            </div>
        </li>
    </ul>
</div>
</div>
 --}}
@foreach($press_release as $release)

<div class="pressReleaseBox">
	<div class="heading">Press Release</div>
	
<div class="pressDescription">
	{{-- <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries,</p>
    <p>but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p> --}}
</div>
<div class="pressImgVdoScn">
	<ul>
        
    	<li>
        	<div class="cvrBox">
            	<iframe width="560" height="315" src="{{$release->youtube_link}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
        </li>
        	<li>
        	<div class="cvrBox">
              <a  href="{{asset('storage/app/press/')}}/{{$release->pdf_filename}}" target="_blank" class="btnDwnld">
                  <span>View File </span></a> 
            </div>
        </li>
       
    </ul>
</div>
</div>
</div>
@endforeach
</section>
        </div>
    </div>
</section>
@include('frontend.includes.footer')