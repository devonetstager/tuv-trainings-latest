@include('frontend.includes.header')   

<!-- ****Banner Section**** -->
<section class="innerbanner" style="background: url({{ asset('public/frontend/images/behaviourbanner.jpg') }}) no-repeat center top;">
  
</section>


<!-- Inner page content section -->
    
<section class="categry">
<div class="breadcrmb">
        <div class="wid">
            <!--<h2>Sub Categories</h2>-->
            <div class="brdcrmb">
                <a href="{{route('index')}}">Home</a><Span>/</Span><small>{{$data->name}}</small>
            </div>
        </div>
    </div>
    <div class="wid">
        <div class="headng">
          {{$data->name}}
        </div>
        <ul>
            @foreach($subcategories as $category)
        <li>
            <a href="{{route('courses', [encrypt($category->category_id)])}} "> 
            <div class="catimg">
                <img src="{{asset('storage/app/categories/')}}/{{$category->image}}">
            </div>
            <div class="cathead">
            {{ str_limit(strip_tags($category->name), 20) }}
            </div>
            </a>
        </li>
        @endforeach
</ul>
    </div>
</section>

        </div>
    </div>
</section>
@include('frontend.includes.footer')