@include('frontend.includes.header')
         



<!-- ****Banner Section**** -->
<section class="innerbanner" style="background: url({{ asset('public/frontend/images/coursebanner.jpg')}}) no-repeat center top;">
    <div class="breadcrmb">
        <div class="wid">
            <h2>Search Results</h2>
            <div class="brdcrmb">
                <a href="{{route('index')}}">Home</a><Span>/</Span><small>Search Results</small>
            </div>
        </div>
    </div>
</section>

<section class="innerSerchForm">
     <div class="bannerform">
      <div class="wid">
        <form action="{{route('course.search')}}" method="post">
            <div id="courselist">
            </div>
            {{ csrf_field() }}
            <ul>
               <li class="type">
                  <input type="text" id="course" placeholder="Course Name" name="course">
               </li>
               <li class="selct custom-select">
                  <select name="category">
                     <option class="category" value="">Category</option>
                     <option class="category" value="">All Category</option>
                     @foreach($categories as $category)
                     <option class="category" value="{{$category->category_id}}">{{$category->name}}</option>
                     @endforeach
                  </select>
               </li>
               <li class="selct2 custom-select">
                  <select name="month">
                     <option class="category" value="">Month</option>
                     <option class="category" value="">All Months</option>
                     @foreach($months as $num => $name)
                     <option value="{{$num}}">{{$name}}</option>
                     @endforeach
                  </select>
               </li>
               <li class="buttn">
                  <input type="submit" value="Search">
               </li>
            </ul>
         </form>
      </div>
   </div>
</section>

<!-- Inner page content section -->

<section class="innerpg coursedtls">
    <div class="wid">
        <div class="coursecat">
            <div class="secname">
                Categories
            </div>
            <ul class="crcat">
            @foreach($maincategory as $category)
                <li>
               
                <img src="{{ asset('/storage/app/categories/icon/') }}/{{$category->icon}}">
                    <a href="{{route('courses', [encrypt($category->category_id)])}}" title="Project & Program Management">{{str_limit(strip_tags($category->name), 20)}}</a>
                </li>
                @endforeach
            </ul>
            <form>
                <div class="selct custom-select">
                  
                </div>
            </form>
            
        </div>
        <div class="abtcnt">
                <div class="courses crseinner">
                        <div class="wid">
                        @if(count($results) > 0)
                           <ul> @foreach($results as $result)
 
                                <li>
                                    <div class="courseimg">
                                        <img src="{{ asset('/storage/app/courses/courseimage/') }}/{{$result->image}}">
                                    </div>
                                    <a href="{{route('course-detail',[encrypt($result->course_id)])}}" class="crsbtn">
                                    {{str_limit(strip_tags($result->name), 40)}}
                                    </a>
                                    <div class="courceLoc">
                                                    @foreach($result->coursedetails as $details)
                                                    <a href="#" class="locIcon">{{str_limit(strip_tags($details->location), 10)}}</a>
                                                    <a href="#" class="timeIcon">{{ date('d M', strtotime($details->start_date))}}-{{date('d M  Y', strtotime($details->end_date))}}</a>
                                                    @endforeach
                                                    </div>
                                    <a href="{{route('course-registration', [encrypt($result->course_id)])}}" class="smlbtn">
                                        Register Now
                                    </a>
                                </li>
                                @endforeach
                                @else
                                <ul>
 
                                <li>
                                  
                                    <p>Sorry No Results Found.</p>
                                  
                                   
                                  
                                   
                                </li>
                                @endif
                           
                        
                            </ul>
                        </div>
                    </div>
        </div>
    </div>
</section>



@include('frontend.includes.footer')