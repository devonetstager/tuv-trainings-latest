<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>{{ isset($title) ? $title : 'Welcome to TÜV Middle East'}}</title>
<meta name="format-detection" content="TÜV Middle East is one of the world's leading certification body. TÜV Middle East can offer you a wide range of certifications and various accreditations"/>
<meta title="format-detection" content="TÜV Middle East is committed to becoming the leading competence center for inspection, training, certification and auditing, HSE advisory and testing in the Arab Peninsula. With its international experience and knowledge, TÜV Middle East is committed to add value to its clients by providing them with the professional technical advice needed to allow them to demonstrate the quality and confidence of the products and services they offer."/>
<meta description="format-detection" content="Leader in Testing, Inspection, Auditing, Certification and Training Services. Dedicated Support. Worldwide Recognized. Accredited Body. ISO Certified Company. Services: Testing, Training."/>
<meta keywords="format-detection" content="iso certiciation, api courses, oil and gas, drilling technology, inspection, certification, management system, technical service"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=1.0, minimum-scale=1.0, maximum-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<link rel="icon" type="image/png" sizes="32x32" href="{{ asset('public/frontend/images/favicon32.png') }}">
<link rel="icon" type="image/png" sizes="96x96" href="{{ asset('public/frontend/images/favicon96.png') }}">
<link rel="icon" type="image/png" sizes="16x16" href="{{ asset('public/frontend/images/favicon16.png') }}">
<link href="{{ asset('public/frontend/css/style.css') }}" rel="stylesheet" type="text/css">
<!--<link href="{{ asset('public/frontend/css/jquery.popup.css') }}" rel="stylesheet" type="text/css">-->
<script  type="text/javascript" src="{{ asset('public/frontend/js/jquery.min.js') }}"></script>
<script defer type="text/javascript" src="{{ asset('public/frontend/js/jquery.popup.js') }}"></script>
<script defer type="text/javascript" src="{{ asset('public/frontend/js/accordion.js?'.time()) }}"></script>
<meta name="google-site-verification" content="23SPW9sJb0gXrZPdVWgcnE1Mq0-BU9mRB5Sbd93BzV0" />
<!-- Global site tag (gtag.js) - Google Analytics -->
<script defer async src="https://www.googletagmanager.com/gtag/js?id=UA-143023641-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-143023641-1');
</script>
</head>
<style>
#search-query {
    width:300px;
    padding:5px 10px;
    margin-left:10px;
}
/****** jQuery Autocomplete CSS *************/
 .ui-corner-all {
    -moz-border-radius: 0;
    -webkit-border-radius: 0;
    border-radius: 0;
}
.ui-menu {
    border: 1px solid lightgray;
    font-family: Verdana, Arial, Helvetica, sans-serif;
    font-size: 15px;
}
.ui-menu .ui-menu-item a {
    color: #888;
}
.ui-menu .ui-menu-item:hover {
    display: block;
    text-decoration: none;
    color: #3D3D3D;
    cursor: pointer;
    background-color: lightgray;
    background-image: none;
    border: 1px solid lightgray;
}
.ui-widget-content .ui-state-hover, .ui-widget-content .ui-state-focus {
    border: 1px solid lightgray;
    background-image: none;
    background-color: lightgray;
    font-weight: bold;
    color: #3D3D3D;
}
</style>
<body>

<!-- ****Header Section**** -->

<header id="headerBar">
    

@if(!Session::has('vat_cookie'))
<div id="coockie_data" class="vatMsg"> In order to provide you with a pleasant online experience, we use cookies on our website.
By expressing your consent at tuv-middle-east you agree to the use of cookies.<br> <a href ="{{route('privacy.policy')}}">More Info..</a> <span id="vatOkBtn">OK</span></div>
@endif
    <div class="wid">
        <div class="tophead">
            <div class="logo">
                <a href="{{route('index')}}">
                    <img src="{{ asset('public/frontend/images/TUV_logo.png') }}" alt="TUV-logo" title="TUV Nord">
                </a>
            </div>
         
            <div class="tpcontct">
                  @foreach($contacts as $contact)
                <ul class="cntdtls">
                    <li>
                        <a href="tel:{{$contact->phone}}" title="{{$contact->phone}}">
                            Phone: {{$contact->phone}}
                        </a>
                    </li>
                           <li>                  
                        Email: <a href="mailto:{{$contact->email}}" title="mailto:{{$contact->email}}">{{$contact->email}}</a>
                    </li>
                    
                </ul>
                @endforeach
                <div class="offrs">
                    <a target="_blank" href="{{route('nebosh')}}" class="mn_b">Offers</a>
                </div>
                <!--<div id="google_translate_element"></div>-->
            </div>
        </div>
    </div>
    <div class="menuD">
    	<a href="#menu" id="nav-toggle" class="menu-link">
  	<span></span>
  </a>
  <nav id="menu" class="menu">
		<ul class="level-01">              
			<li><a href="{{ route('index')}}"title="Home">Home</a></li>
			<li><a href="#" title="About Us">About Us</a><span class="has-subnav"></span>
                   <ul class="level-1">
                    <li><a href="{{ route('tuv-middle-east')}}" title="TÜV Middle East">TÜV Middle East</a></li>
                    <li><a href="{{route('tuv-nord')}}" title="TÜV Nord">TÜV NORD</a></li>
                    <li><a href="{{route('tuv-nord-training')}}" title="TÜV Nord Training Centre">TÜV NORD Training Centre</a></li>
                    <li><a href="{{route('qualification-list')}}" title="Qualifications">Qualifications</a></li>
                    <li><a href="{{route('clientele')}}" title="Clientele">Clientele</a></li>
                   
                </ul>
            </li>
			<li><a href="{{route('courselist')}}" title="Courses">Courses</a><span class="has-subnav"></span>
                <ul class="level-1">
                @foreach($categories as $category)
                      <li><a 
                    @if($category->category_id==23) href="{{route('sub-categories',encrypt($category->category_id))}}" @else
                    href="{{route('courses', [encrypt($category->category_id)])}}" @endif
                     title="Project & Program Management"> {{$category->name}}</a></li>
                @endforeach    
                  
                </ul>
            </li>
            <li><a href="#" title="Media">Media</a><span class="has-subnav"></span>
                <ul class="level-1">
                    <li><a href="{{ route('corporate-news')}}" title="Corporate News">Corporate News</a></li>
                    <li><a href="{{ route('blob')}}" title="Training Blog">Technology Blog</a></li>
                     <li><a href="{{route('testimonials-list')}}" title="Testimonials">Testimonials</a></li>
                    <li><a href="{{route('appreciation')}}" title="Appreciation Letters">Appreciation Letters</a></li>
                    <li><a href="{{route('press-release')}}" title="Press Release">Press Release</a></li>
                    <li><a href="{{route('gallery')}}" title="Photo Gallery">Photo Gallery</a></li>
                </ul>
            </li>
               <li><a href="#" title="Services">Services</a><span class="has-subnav"></span>
                <ul class="level-1">
                    <li><a href="{{route('public-courses')}}" title="Public Courses">Public Courses</a></li>
                    <li><a href="{{route('in-house-courses')}}" title="{{route('public-courses')}}">In Houses Courses</a></li>
                    <li><a href="{{route('integrated-e-learning')}}" title="Integrated E-Learning">Integrated E-Learning</a></li>
                     <li><a href="{{route('conferences')}}" title="Conferences">Conferences</a></li>
                      <li><a href="{{route('partner')}}" title="partner">Become A Partner</a></li>
                      <li><a href="{{route('verification')}}" title="verification">Certificate/ Card Verification</a></li>
                </ul>
            </li>
            <li><a href="{{ route('contact-us')}}" title="Contact Us">Contact Us</a><span class="has-subnav"></span>
                <ul class="level-1">
                    <li><a href="{{route('contact-us')}}" title="FAQ">Contact Us</a></li>
                    <li><a href="{{route('faq')}}" title="FAQ">FAQ</a></li>
                    <li><a href="{{route('location')}}" title="Locations">Locations</a></li>
                     
                </ul>
            </li>
		</ul>
	</nav>
    </div>

</header>