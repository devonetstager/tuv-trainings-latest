<!-- Footer section -->
<footer>
   <div class="wid">
      <div class="footrlst">
         <div class="ftrlst ftrlinks">
            <div class="ftrhead">
               Quick Links
            </div>
            <ul>
               <li><a href="{{ route('index')}}" title="Home">Home</a></li>
               <li><a href="{{ route('tuv-middle-east')}}" title="About Us">About Us</a></li>
               <li><a href="{{route('courselist')}}" title="Courses">Courses</a></li>
               <li><a href="{{ route('blob')}}" title="Media">Media</a></li>
               <li><a href="{{ route('public-courses')}}" title="Services">Services</a></li>
               <li><a href="{{ route('contact-us')}}" title="Contact Us">Contact Us</a></li>
            </ul>
         </div>
         <div class="ftrlst ftradrs">
            <div class="ftrhead">
               Address
            </div>
            @foreach($contacts as $contact)
            </p>
            <div class="addrs">
               {{$contact->building}}    <br>
                 {{$contact->column1}}<br> 
               {{$contact->column2}}<br>
               {{$contact->pincode}}<br>
              {{$contact->area}}, {{$contact->state}},<br>
               {{$contact->country}}
            </div>
         </div>
         <div class="ftrlst ftrcnt">
            <div class="ftrhead">
               Contact Us
            </div>
            <ul class="cntdtls">
               <li>
               Phone :   <a href="tel:+971 2 4411148">  {{$contact->phone}}</a>
               </li>
               <li>
                  Fax :  {{$contact->fax}}
               </li>
               <li>
                Email :  <a href="mailto:{{$contact->email}}"> {{$contact->email}}</a>
               </li>
            </ul>
            <div class="socialmd">
               <ul>
                  <li>
                     <a target="_blank" href="{{$contact->facebook_url}}" class="fbpg"></a>
                  </li>
                  <li>
                     <a target="_blank" href="{{$contact->youtube_url}}" class="youtb"></a>
                  </li>
                  <li>
                     <a target="_blank" href="{{$contact->linkedin_url}}" class="instapg"></a>
                  </li>
                  <li>
                     <a target="_blank" href="{{$contact->twitter_url}}" class="twpg"></a>
                  </li>
               </ul>
            </div>
         </div>
         @endforeach
      </div>
      <div class="copryts">
         Copyright © {{ date('Y') }} TÜV MIDDLE EAST. All Rights Reserved.
      </div>
   </div>
</footer>
<script type="text/javascript" src="{{ asset('public/frontend/js/script.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/frontend/js/tuv.js') }}"></script>
<script>

</script>
<script>

</script>
</body>
</html>