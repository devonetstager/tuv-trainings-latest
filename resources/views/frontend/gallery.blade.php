@include('frontend.includes.header')
<!-- ****Banner Section**** -->
<section class="innerbanner" style="background: url({{ asset('/public/frontend/images/gallerybanner.jpg')}}) no-repeat center top;">
    <div class="breadcrmb">
        <div class="wid">
            <h2>Gallery</h2>
            <div class="brdcrmb">
                <a href="{{route('index')}}">Home</a><Span>/</Span>Media<Span>/</Span><small>Gallery</small>
            </div>
        </div>
    </div>
</section>


<!-- Inner page content section -->

<section class="gallery">
    <div class="wid">
        <ul >
            @foreach($galleries as $gallery)
            <li>
                <a href="{{route('gallery-details',[$gallery->gallery_id])}}">
                    <div class="galimg">
                        <img src="{{asset('storage/app/gallery/mainphoto/')}}/{{$gallery->main_photo}}">
                    </div>
                    <div class="galsec">
                        {{$gallery->title}}
                    </div>
                </a>
            </li>
            @endforeach          
        </ul>
        {{$galleries->links()}}
    </div>
</section>
@include('frontend.includes.footer')