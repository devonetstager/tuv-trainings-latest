@include('frontend.includes.header')
         

<!-- ****Banner Section**** -->
<section class="innerbanner" style="background:url({{ asset('public/frontend/images/qualificationsbanner.jpg') }}) no-repeat center top;">
    <div class="breadcrmb">
        <div class="wid">
            <h2>About Us</h2>
            <div class="brdcrmb">
                <a href="{{route('index')}}">Home</a><Span>/</Span>About Us<Span>/</Span><small>Qualifications</small>
            </div>
        </div>
    </div>
</section>


<!-- Inner page content section -->

<section class="innerpg coursedtls qualif">
    <div class="wid">
        <div class="abt_menu">
         <div class="coursecat" style="width: 100%;">
                    <div class="secname" style="margin-top: 20px;">
                        About Us
                    </div>
        <ul class="crcat" id="month_list">
              <li><a href="{{route('tuv-middle-east')}}"  title="TÜV Middle East">TÜV Middle East</a></li>
                <li style="width: 100%;"><a  href="{{route('tuv-nord')}}" title="TÜV NORD">TÜV NORD</a></li>
                <li style="width: 100%;"><a href="{{route('tuv-nord-training')}}" title="TÜV NORD Training Centre">TÜV NORD Training Centre</a></li>
                <li style="width: 100%;"><a class="cat_active" href="{{route('qualification-list')}}" title="Qualifications">Qualifications</a></li>
                <li style="width: 100%;"><a href="{{route('clientele')}}" title="Clientele">Clientele</a></li>
                <li style="width: 100%;"><a href="{{route('testimonials-list')}}" title="Testimonials">Testimonials</a></li>
                <li style="width: 100%;"><a href="{{route('appreciation')}}" title="Appreciation Letters">Appreciation Letters</a></li>
                <li style="width: 100%;"><a href="{{route('press-release')}}" title="Press Release">Press Release</a></li>
                <li style="width: 100%;"><a href="{{route('gallery')}}" title="Photo Gallery">Photo Gallery</a></li>
                <li style="width: 100%;"><a href="{{route('faq')}}" title="FAQ">FAQ</a></li>
            </ul>        
        </div>
        </div>
        <div class="abtcnt">
             <h1>Qualifications</h1>
            <div class="qualifications">
                <ul>
                @foreach($qualifications as $qualification)
                    <li>
                        <img src="{{asset('storage/app/qualification/images/')}}/{{$qualification->image}}">
                    </li>
                   @endforeach
                </ul>
            </div>
        </div>
    </div>
</section>



@include('frontend.includes.footer')
