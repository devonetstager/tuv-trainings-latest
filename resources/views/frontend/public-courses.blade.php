@include('frontend.includes.header')
<!-- ****Banner Section**** -->
<style>
    .coursedtls .coursecat .crcat li a:hover {
    color: #fff !important;
}
</style>
<section class="innerbanner" style="background: url({{asset('public/frontend/images/PublicCourses.jpg')}}) no-repeat center top;">
    <div class="breadcrmb">
        <div class="wid">
            <h2>Services</h2>
            <div class="brdcrmb">
                <a href="{{route('index')}}">Home</a><Span>/</Span><a href="">Services</a><Span>/</Span><small>Public Courses</small>
            </div>
        </div>
    </div>
</section>
<!-- Inner page content section -->
<section class="innerpg coursedtls">
    <div class="wid">
        <div class="abt_menu">
             <div class="coursecat" style="width: 100%;">
                    <div class="secname" style="margin-top: 20px;">
                        Services
                    </div>
                    <ul class="crcat" id="month_list">
                        <li style="width: 100%;"><a class="cat_active" href="{{route('public-courses')}}" title="Public Courses" style="width: 100%;">Public Courses</a></li>
                <li style="width: 100%;"><a  href="{{route('in-house-courses')}}" title="TÜV NORD" style="width: 100%;">In House Courses</a></li>
                <li style="width: 100%;"><a href="{{route('integrated-e-learning')}}" title="Integrated E Learning" style="width: 100%;">Integrated E Learning</a></li>
               
                <li style="width: 100%;"><a href="{{route('conferences')}}" title="Conferences" style="width: 100%;">Conferences</a></li>  
                    </ul>
            </div>     
     </div>
        <div class="abtcnt">
            <h1>Public Courses</h1>
            <p>
             <b>TÜV NORD Training Centre</b> offers unique training programs & personnel certifications of different levels, for technicians, engineers & managers in a variety of fields like Oil & Gas, Petrochemicals, Engineering, Management, HSE etc.   

TÜV NORD Training Centre provides national & internationally recognized and certified courses which are competitive with local needs as well as committed to excellence. We have an excellent team of instructors who deliver our courses to the satisfaction of the delegates. 

Attend our public sessions, and meet delegates from leading organizations. These training would include group discussions and sharing individual expertise and discussing problems.
            </p> <div class="vlbttn">
         <a href="{{route('course.filter',date("m") )}}" class="odbttn">
         View All
         </a>
      </div>
        </div>
    </div>
</section>
@include('frontend.includes.footer')