@include('frontend.includes.header')
<!-- ****Banner Section**** -->
<section class="innerbanner" style="background: url({{asset('public/frontend/images/press_release.jpg')}}) no-repeat center top;">
    <div class="breadcrmb">
        <div class="wid">
            <h2>Privacy Policy</h2>
            <div class="brdcrmb">
                <a href="{{route('index')}}">Home</a><Span>/</Span>About Us<Span>/</Span><small>Privacy-Policy</small>
            </div>
        </div>
    </div>
</section>
<!-- Inner page content section -->
<section class="innerpg cockiePage">
  

<div class="wid">
<div class="abtcnt">
<h3>THIS WEBSITE USES COOKIES TO IMPROVE YOUR EXPERIENCE</h3>
<p><strong> TÜV Middle East</strong> would only be stored in case the user’s Web browser setting permits the same. In case a user choses not to allow cookies, he could adjust
the browser s ettings to refuse the same. However, some parts of our sites may not fun ction optimally if the browser was to refuse cookies.</p>
<p> <strong>  TÜV Middle East </strong> uses cookies for the following general purposes:
To help us recognize the particular brows er as a previous visitor. We may also retain the user’s 
regi stration details on the cookie, if instructed to do so. (Such as if the user tick s the box that says 
that he should be signed in automatically in the future) To help us customize the content provided on our websites and on
other websites across the Internet. To help research the effectiveness of our website features, advertisements, and e-mail communications</p>

 

<p>We may also use technology called web beacons, which are similar to cookies, and are used to measure the effectiveness of email
campaigns to determine which emails have been viewed and acted upon.</p></div>
</div>

        </div>
    </div>
</section>
@include('frontend.includes.footer')