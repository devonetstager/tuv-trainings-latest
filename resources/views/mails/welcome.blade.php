
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>ACM</title>
<link href="https://fonts.googleapis.com/css?family=Montserrat:300,600,700" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Lato:400,700" rel="stylesheet">
</head>

<body style="margin:0;padding:0;">
<table width="650" border="0" align="center" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="center" valign="top"><img src="./images/logo.png" style="height:auto;" alt="TUV NORD"></td>
  </tr>
  <tr>
    <td align="center" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td height="147" align="center" valign="middle" bgcolor="#123b9a"><table width="650" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td align="center" valign="middle" style="color: #fff;font-size: 19px;line-height: 29px;font-weight: 300;text-align: center;"><font face="Montserrat, sans-serif">
       Hi Admin,<br>
        A Contact Enquiry has been recieved
        </font></td>
      </tr>
      
    </table></td>
  </tr>
  <tr>
    <td height="15" align="center" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td height="35" align="center" valign="top">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="23%" style="color:#000;font-size:14px;font-weight:300;"><font face="Montserrat, sans-serif">Name</font></td>
    <td width="2%" style="color:#000;font-size:14px;font-weight:300;"><font face="Montserrat, sans-serif">:</font></td>
    <td width="75%" style="color:#000;font-size:14px;font-weight:300;"><font face="Montserrat, sans-serif">[namefld]</font></td>
  </tr>
    <tr>
    <td width="23%" style="color:#000;font-size:14px;font-weight:300;"><font face="Montserrat, sans-serif">Phone</font></td>
    <td width="2%" style="color:#000;font-size:14px;font-weight:300;"><font face="Montserrat, sans-serif">:</font></td>
    <td width="75%" style="color:#000;font-size:14px;font-weight:300;"><font face="Montserrat, sans-serif">[telfld]</font></td>
  </tr>
  
    <tr>
    <td width="23%" style="color:#000;font-size:14px;font-weight:300;"><font face="Montserrat, sans-serif">Email</font></td>
    <td width="2%" style="color:#000;font-size:14px;font-weight:300;"><font face="Montserrat, sans-serif">:</font></td>
    <td width="75%" style="color:#000;font-size:14px;font-weight:300;"><font face="Montserrat, sans-serif">[emailfld]</font></td>
  </tr>
    <tr>
    <td width="23%" style="color:#000;font-size:14px;font-weight:300;"><font face="Montserrat, sans-serif">Company</font></td>
    <td width="2%" style="color:#000;font-size:14px;font-weight:300;"><font face="Montserrat, sans-serif">:</font></td>
    <td width="75%" style="color:#000;font-size:14px;font-weight:300;"><font face="Montserrat, sans-serif">[comapnyFld]</font></td>
  </tr>
    <tr>
    <td width="23%" style="color:#000;font-size:14px;font-weight:300;"><font face="Montserrat, sans-serif">Message</font></td>
    <td width="2%" style="color:#000;font-size:14px;font-weight:300;"><font face="Montserrat, sans-serif">:</font></td>
    <td width="75%" style="color:#000;font-size:14px;font-weight:300;"><font face="Montserrat, sans-serif">[messagefld]</font></td>
  </tr>
  
  
    </table>
 

    </td>
  </tr>
  <tr>
    <td align="center" valign="top" style="font-size:13px;font-weight:300;text-transform:uppercase;text-align:left;"><font face="Montserrat, sans-serif"></font></td>
  </tr>
  <tr>
    <td align="center" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="center" valign="top" style="border-top:2px #6aaa2d solid;">&nbsp;</td>
  </tr>
  <tr>
        <td width="650" align="center" valign="top" style="font-size:13px;font-weight:300;text-transform:uppercase;"><font face="Montserrat, sans-serif">Copyright © 2019 TUV MIDDLE EAST. All Rights Reserved.</font></td>
  </tr>
</table>

</body>
</html>
