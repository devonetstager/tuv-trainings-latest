@extends('admin.main')
@section('title','New-Location')
@section('content')

<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
    TUV
      <small>Manage</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Add New </a></li>
    </ol>
<section class="content">
    @if(Session::has('message'))
    <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-check"></i> Alert!</h4>
                      {{ Session::get('message') }}
      </div>
    @endif
        <!-- Default box -->
        <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title">New Location</h3>
  
            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                      title="Collapse">
                <i class="fa fa-minus"></i></button>
              <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                <i class="fa fa-times"></i></button>
            </div>
          </div>
          <div class="box-body">
          <div class="row">
        <div class="col-xs-12">
          <div class="box">
         
              <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">City Form</h3><br>
              <a href="{{route('locations')}}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>

            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form action="{{route('locations.save')}}" method="post" enctype="multipart/form-data">
          @csrf
              <div class="box-body">
                <div class="form-group col-md-6">
                  <label for="exampleInputEmail1">Name</label>
                  <input type="test" name="name" value="{{ old('name') }}" class="form-control"  placeholder="Name">
                  {!! $errors->first('name', '<p style="color:red;" class="help-block">:message</p>') !!}
                </div>
                <div class="form-group col-md-6">
                  <label for="exampleInputEmail1">Building</label>
                  <input type="test" name="building" class="form-control" value="{{ old('building') }}"  placeholder="Building">
                  {!! $errors->first('building', '<p style="color:red;" class="help-block">:message</p>') !!}
                </div>   
                <div class="form-group col-md-6">
                  <label for="exampleInputEmail1">Address Line 1</label>
                  <input type="test" name="addressline1" class="form-control" value="{{ old('addressline1') }}"  placeholder="Address Line 1">
                  {!! $errors->first('column1', '<p style="color:red;" class="help-block">:message</p>') !!}
                </div>     
                <div class="form-group col-md-6">
                  <label for="exampleInputEmail1">Address Line 2</label>
                  <input type="test" name="addressline2" class="form-control" value="{{ old('adressline2') }}"  placeholder="Address Line 2">
                  {!! $errors->first('column2', '<p style="color:red;" class="help-block">:message</p>') !!}
                </div>       
               
                <div class="form-group col-md-6">
                  <label for="exampleInputEmail1">State</label>
                  <input type="test" name="state" class="form-control" value="{{ old('state') }}"  placeholder="State">
                  {!! $errors->first('state', '<p style="color:red;" class="help-block">:message</p>') !!}
                </div>                                
                <div class="form-group col-md-6">
                  <label for="exampleInputEmail1">Country</label>
                  <input type="test" name="country" class="form-control" value="{{ old('country') }}" placeholder="Country">
                  {!! $errors->first('country', '<p style="color:red;" class="help-block">:message</p>') !!}
                </div>     
                <div class="form-group col-md-6">
                  <label for="exampleInputEmail1">Pincode</label>
                  <input type="test" name="pincode" class="form-control" value="{{ old('pincode') }}"  placeholder="Pincode">
                  {!! $errors->first('pincodePhone', '<p style="color:red;" class="help-block">:message</p>') !!}
                </div>     
                <div class="form-group col-md-6">
                  <label for="exampleInputEmail1">Phone</label>
                  <input type="test" name="phone" class="form-control"  value="{{ old('phone') }}"  placeholder="Phone">
                  {!! $errors->first('phone', '<p style="color:red;" class="help-block">:message</p>') !!}
                </div>     
                <!--<div class="form-group col-md-6">-->
                <!--  <label for="exampleInputEmail1">Fax</label>-->
                <!--  <input type="test" name="fax" class="form-control" value="{{ old('fax') }}"  placeholder="Fax">-->
                <!--  {!! $errors->first('fax', '<p style="color:red;" class="help-block">:message</p>') !!}-->
                <!--</div> -->
                <div class="form-group col-md-6">
                  <label for="exampleInputEmail1">Email</label>
                  <input type="email" name="email" class="form-control" value="{{ old('email') }}" placeholder="Email">
                  {!! $errors->first('email', '<p style="color:red;" class="help-block">:message</p>') !!}
                </div>
               
              
                <div class="form-group col-md-6">
                  <label for="exampleInputEmail1">Status</label>
                  <select class="form-control" name="status" >
                  <option value="1" >Active</option>                
                    <option value="0" >Inactive</option>                                      
                 </select>
                </div>
                <div class="form-group col-md-6">
                  <label for="exampleInputEmail1">Show Home</label>
                  <select class="form-control" name="show_home" >
                  <option value="1">Yes</option>
                  <option value="0" >No</option>                                    
                 </select>
                </div>                         
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
          <!-- /.box-body -->
          <div class="box-footer">
     
          </div>
          <!-- /.box-footer-->
        </div>
        <!-- /.box -->
  
      </section>
      <!-- /.content -->
    </div>
 
  @endsection