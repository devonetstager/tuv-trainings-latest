@extends('admin.main')
@section('title','Edit-Location')
@section('content')

<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
    {{$data['page_title']}}
      <small>{{$data['page_sub_title']}}</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Location form</a></li>
    </ol>
  </section>

<section class="content">
        @if(Session::has('message'))
           <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-check"></i> Alert!</h4>
                      {{ Session::get('message') }}
          </div>
        @endif
        @if(Session::has('err_message'))
      <div class="alert alert-danger alert-dismissible">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                      <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                      {{ Session::get('err_message') }}
      </div>
    @endif
        <!-- Default box -->
        <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title">Edit Location</h3><br>
            <a href="{{route('locations')}}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                      title="Collapse">
                <i class="fa fa-minus"></i></button>
              <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                <i class="fa fa-times"></i></button>
            </div>
          </div>
          <div class="box-body">
          <div class="row">
        <div class="col-xs-12">
          <div class="box">
         
              <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Location Form</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form action="{{route('locations.update')}}" method="post" enctype="multipart/form-data">
               @csrf
               <div class="box-body">
                <div class="form-group col-md-6">
                  <label for="exampleInputEmail1">Name</label>
                  <input type="test" name="name" value="{{$location->name}}" class="form-control"  placeholder="Name">
                  {!! $errors->first('name', '<p style="color:red;" class="help-block">:message</p>') !!}
                </div>
                <div class="form-group col-md-6">
                  <label for="exampleInputEmail1">Building</label>
                  <input type="test" name="building" value="{{$location->building}}" class="form-control"  placeholder="Building">
                  {!! $errors->first('building', '<p style="color:red;" class="help-block">:message</p>') !!}
                </div>   
                <div class="form-group col-md-6">
                  <label for="exampleInputEmail1">Address Line 1</label>
                  <input type="test" name="addressline1"  value="{{$location->addressline1}}" class="form-control"  placeholder="Address Line 1">
                  {!! $errors->first('column1', '<p style="color:red;" class="help-block">:message</p>') !!}
                </div>     
                <div class="form-group col-md-6">
                  <label for="exampleInputEmail1">Address Line 2</label>
                  <input type="test" name="addressline2" value="{{$location->addressline2}}" class="form-control"  placeholder="Address Line 2">
                  {!! $errors->first('column2', '<p style="color:red;" class="help-block">:message</p>') !!}
                </div>       
                <div class="form-group col-md-6">
                  <label for="exampleInputEmail1">Area</label>
                  <input type="test" name="area" value="{{$location->area}}" class="form-control"  placeholder="Area">
                  {!! $errors->first('area', '<p style="color:red;" class="help-block">:message</p>') !!}
                </div>     
                <div class="form-group col-md-6">
                  <label for="exampleInputEmail1">State</label>
                  <input type="test" name="state" value="{{$location->state}}" class="form-control"  placeholder="State">
                  {!! $errors->first('state', '<p style="color:red;" class="help-block">:message</p>') !!}
                </div>                                
                <div class="form-group col-md-6">
                  <label for="exampleInputEmail1">Country</label>
                  <input type="test" name="country" value="{{$location->country}}" class="form-control"  placeholder="Country">
                  {!! $errors->first('country', '<p style="color:red;" class="help-block">:message</p>') !!}
                </div>     
                <div class="form-group col-md-6">
                  <label for="exampleInputEmail1">Pincode</label>
                  <input type="test" name="pincode" value="{{$location->pincode}}" class="form-control"  placeholder="Pincode">
                  {!! $errors->first('pincodePhone', '<p style="color:red;" class="help-block">:message</p>') !!}
                </div>     
                <div class="form-group col-md-6">
                  <label for="exampleInputEmail1">Phone</label>
                  <input type="test" name="phone" value="{{$location->phone}}" class="form-control"  placeholder="Phone">
                  {!! $errors->first('phone', '<p style="color:red;" class="help-block">:message</p>') !!}
                </div>     
                <!--<div class="form-group col-md-6">-->
                <!--  <label for="exampleInputEmail1">Fax</label>-->
                <!--  <input type="test" name="fax" value="{{$location->fax}}" class="form-control"  placeholder="Fax">-->
                <!--  {!! $errors->first('name', '<p style="color:red;" class="help-block">:message</p>') !!}-->
                <!--</div> -->
                <div class="form-group col-md-6">
                  <label for="exampleInputEmail1">Email</label>
                  <input type="email" name="email" value="{{$location->email}}" class="form-control"  placeholder="Email">
                  {!! $errors->first('email', '<p style="color:red;" class="help-block">:message</p>') !!}
                </div>
                <div class="form-group col-md-6">
                  <label for="exampleInputEmail1">Status</label>
                  <select class="form-control" name="status" >                
                    <option value="0" @if($location->status == 0) selected="selected" @endif >Inactive</option>                   
                    <option value="1" @if($location->status == 1) selected="selected" @endif >Active</option>                      
                 </select>
                </div>
                <div class="form-group col-md-6">
                  <label for="exampleInputEmail1">Show Home</label>
                  <select class="form-control" name="show_home" >
                  <option value="1"@if($location->show_home == 1) selected="selected" @endif>Yes</option>
                  <option value="0" @if($location->show_home == 0) selected="selected" @endif>No</option>                                    
                 </select>
                </div>                         
              </div>
              <div class="form-group col-md-6">
              <input type="hidden" name="location_id" value="{{$location->location_id}}">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
          <!-- /.box-body -->
          <div class="box-footer">
         
          </div>
          <!-- /.box-footer-->
        </div>
        <!-- /.box -->
  
      </section>
      <!-- /.content -->
    </div>
 
  @endsection