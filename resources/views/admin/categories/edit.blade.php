@extends('admin.main')
@section('title','Edit-Category')
@section('content')

<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
    {{$data['page_title']}}
      <small>{{$data['page_sub_title']}}</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Category form</a></li>
    </ol>
  </section>

<section class="content">
        @if(Session::has('message'))
           <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-check"></i> Alert!</h4>
                      {{ Session::get('message') }}
          </div>
        @endif
        @if(Session::has('err_message'))
      <div class="alert alert-danger alert-dismissible">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                      <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                      {{ Session::get('err_message') }}
      </div>
    @endif
        <!-- Default box -->
        <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title">Edit Category</h3><br>
            <a href="{{route('admin.category')}}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>

  
            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                      title="Collapse">
                <i class="fa fa-minus"></i></button>
              <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                <i class="fa fa-times"></i></button>
            </div>
          </div>
          <div class="box-body">
          <div class="row">
        <div class="col-xs-12">
          <div class="box">
         
              <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Category Form</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form action="{{route('category.update')}}" method="post" enctype="multipart/form-data">
               @csrf
              <div class="box-body">
              <div class="form-group col-md-6">
                  <label for="exampleInputEmail1">Master</label>
                  <select class="form-control" name="master_id" >
                  <option value="0">master</option>
                  @foreach($masters as $master) 
                  <option value="{{$master->category_id}}" @if($master->master_id == $category->master_id) selected="selected"@endif>{{$master->name}} </option> 
                  @endforeach             
                 </select>
                 {!! $errors->first('master_id', '<p style="color:red;" class="help-block">:message</p>') !!}
                </div>       
                <div class="form-group col-md-6">
                  <label for="exampleInputEmail1">Category Name</label>
                  <input type="test" name="name" value="{{$category->name}}" class="form-control"   placeholder="Name">
                  {!! $errors->first('name', '<p style="color:red;" class="help-block">:message</p>') !!}

                </div>             
               
                <div class="form-group col-md-6">
                  <label for="exampleInputFile">Image</label>
                  <input type="file" name="image" id="exampleInputFile">
                     {!! $errors->first('image', '<p style="color:red;" class="help-block">:message</p>') !!}
                </div>
                <div class="form-group col-md-6">
                  <label for="exampleInputFile">Icon</label>
                  <input type="file" name="icon" id="exampleInputFile">
                     {!! $errors->first('icon', '<p style="color:red;" class="help-block">:message</p>') !!}
                </div>
                <div class="form-group col-md-6">
                  <label for="exampleInputEmail1">Status</label>
                  <select class="form-control" name="status" >
                  <option value="1">Active</option>
                   <option value="0">Inactive</option>                   
                                         
                 </select>
                </div>
                <div class="form-group col-md-6">
                  <label for="exampleInputEmail1">Show Home</label>
                  <select class="form-control" name="show_home" >
                  <option value="1" @if($category->show_home == 1) selected="selected" @endif>Yes</option>
                   <option value="0" @if($category->show_home == 0) selected="selected" @endif>No</option>                   
                                       
                 </select>
                </div>
              </div>
                <div class="form-group col-md-12">
                  <label for="exampleInputFile">Description</label>
                  <textarea id="over_view" name="description" rows="7" class="form-control ckeditor" placeholder="Write your message..">{{ $category->description }}</textarea>
                  {!! $errors->first('description', '<p style="color:red;" class="help-block">:message</p>') !!}                
                </div>    
              </div>
              <!-- /.box-body -->

              <div class="form-group col-md-6">
              <input type="hidden" name="category_id" value="{{$category->category_id}}">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
          <!-- /.box-body -->
          <div class="box-footer">
         
          </div>
          <!-- /.box-footer-->
        </div>
        <!-- /.box -->
  
      </section>
      <!-- /.content -->
    </div>
 
  @endsection