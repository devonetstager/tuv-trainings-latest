@extends('admin.main')
@section('title','New-Category')
@section('content')

<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
    TUV
      <small>Manage</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Add New </a></li>
    </ol>
<section class="content">
    @if(Session::has('message'))
    <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-check"></i> Alert!</h4>
                      {{ Session::get('message') }}
      </div>
    @endif
        <!-- Default box -->
        <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title">New Category</h3>
  
            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                      title="Collapse">
                <i class="fa fa-minus"></i></button>
              <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                <i class="fa fa-times"></i></button>
            </div>
          </div>
          <div class="box-body">
          <div class="row">
        <div class="col-xs-12">
          <div class="box">
         
              <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Category Form</h3><br>
              <a href="{{route('admin.category')}}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>

            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form action="{{route('category.save')}}" method="post" enctype="multipart/form-data">
          @csrf
              <div class="box-body">
              <div class="form-group col-md-6">
                  <label for="exampleInputEmail1">Master</label>
                  <select class="form-control" name="master_id" >
                  <!-- <option value="0">master</option> -->
                  @foreach($master_categories as $category) 
                  <option value="{{$category->category_id}}">{{$category->name}}</option> 
                  @endforeach             
                 </select>
                </div>       
                <div class="form-group col-md-6">
                  <label for="exampleInputEmail1">Category Name</label>
                  <input type="test" name="name" value="{{ old('name') }}" class="form-control"   placeholder="Name">
                  {!! $errors->first('name', '<p style="color:red;" class="help-block">:message</p>') !!}

                </div>             
                <div class="form-group col-md-6">
                  <label for="exampleInputFile">Image</label>
                  <input type="file" name="image" id="exampleInputFile">
                </div>
                <div class="form-group col-md-6">
                  <label for="exampleInputFile">Icon</label>
                  <input type="file" name="icon" id="exampleInputFile">
                </div>
                <div class="form-group col-md-6">
                  <label for="exampleInputEmail1">Status</label>
                  <select class="form-control" name="status" >
                  <option value="1">Active</option>
                  <option value="0">Inactive</option>               
                 </select>
                </div>
                <div class="form-group col-md-6">
                  <label for="exampleInputEmail1">Show Home</label>
                  <select class="form-control" name="show_home" >
                  <option value="1">Yes</option>
                   <option value="0">No</option>                   
                                       
                 </select>
                </div>
                  <div class="form-group col-md-12">
                  <label for="exampleInputFile">Description</label>
                  <textarea id="over_view" name="description" rows="7" class="form-control ckeditor" placeholder="Write your message..">{{ old('description') }}</textarea>
                  {!! $errors->first('description', '<p style="color:red;" class="help-block">:message</p>') !!}                
                </div>    
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
          <!-- /.box-body -->
          <div class="box-footer">
     
          </div>
          <!-- /.box-footer-->
        </div>
        <!-- /.box -->
  
      </section>
      <!-- /.content -->
    </div>
 
  @endsection