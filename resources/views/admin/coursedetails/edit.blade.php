@extends('admin.main')
@section('title','Edit-Course')
@section('content')

<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
    {{$data['page_title']}}
      <small>{{$data['page_sub_title']}}</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Edit Course </a></li>
    </ol>
  </section>

<section class="content">
        @if(Session::has('message'))
           <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-check"></i> Alert!</h4>
                      {{ Session::get('message') }}
          </div>
      @endif
      @if(Session::has('err_message'))
      <div class="alert alert-danger alert-dismissible">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                      <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                      {{ Session::get('err_message') }}
      </div>
    @endif
        <!-- Default box -->
        <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title">Edit Course</h3><br>
            <a href="{{route('admin.coursedetails')}}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>

            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                      title="Collapse">
                <i class="fa fa-minus"></i></button>
              <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                <i class="fa fa-times"></i></button>
            </div>
          </div>
          <div class="box-body">
          <div class="row">
        <div class="col-xs-12">
          <div class="box">
         
              <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Course Detail Form</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form action="{{route('coursedetails.update')}}" method="post" enctype="multipart/form-data">
               @csrf
              <div class="box-body">
              <div class="form-group col-md-6">
                  <label for="exampleInputEmail1">Course</label>
                  <select class="form-control" name="course_id" >
                  <option value="">Select Course</option>              
                  @foreach($courses as $course)
                      <option value="{{$course->course_id}}" @if($coursedetails->course_id == $course->course_id) selected="selected" @endif>{{$course->name}}</option>
                      @endforeach              
                 </select>
                 {!! $errors->first('course_id', '<p style="color:red;" class="help-block">:message</p>') !!}
                </div>
                  <div class="form-group col-md-6">
                  <label for="exampleInputEmail1">City</label>
                  <select class="form-control" name="city_id" >
                  <option value="">Select City</option>              
                  @foreach($cities as $city)
                      <option value="{{$city->city_id}}" @if($coursedetails->city_id == $city->city_id) selected="selected" @endif>{{$city->name}}</option>
                      @endforeach              
                 </select>
                 {!! $errors->first('city_id', '<p style="color:red;" class="help-block">:message</p>') !!}

                </div>
                 <div class="form-group col-md-6">
                  <label for="exampleInputEmail1">Currency</label>
                  <select class="form-control" name="currency_id" >
                  <option value="">Select Currency</option>              
                  @foreach($currencies as $currency)
                      <option value="{{$currency->currency_id}}" @if($coursedetails->currency_id == $currency->currency_id) selected="selected" @endif>{{$currency->name}}</option>
                      @endforeach              
                 </select>
                 {!! $errors->first('currency', '<p style="color:red;" class="help-block">:message</p>') !!}

                </div>                     
                <div class="form-group col-md-6">
                  <label for="exampleInputEmail1">Location</label>
                  <input type="text" name="location" value="{{$coursedetails->location}}" class="form-control"  placeholder="Location">
                  {!! $errors->first('location', '<p style="color:red;" class="help-block">:message</p>') !!}

                </div>                
                <div class="form-group col-md-6">
                  <label for="exampleInputEmail1">Start Date</label>
                  <input type="text" name="start_date" id="datepicker1" value="{{$coursedetails->start_date}}" class="form-control datepicker"  placeholder="Select Date">
                  {!! $errors->first('start_date', '<p style="color:red;" class="help-block">:message</p>') !!}

                </div> 
                <div class="form-group col-md-6">
                  <label for="exampleInputEmail1">End Date</label>
                  <input type="text" name="end_date" id="datepicker2" value="{{$coursedetails->end_date}}" class="form-control datepicker"  placeholder="Select Date">
                  {!! $errors->first('end_date', '<p style="color:red;" class="help-block">:message</p>') !!}

                </div>
                <div class="form-group col-md-6">
                  <label for="exampleInputEmail1">Course Fee</label>
                  <input type="text" name="fee" class="form-control" value="{{$coursedetails->fee}}"  placeholder="Course Fee">
                  {!! $errors->first('fee', '<p style="color:red;" class="help-block">:message</p>') !!}

                </div> 
                <div class="form-group col-md-6">
                  <label for="exampleInputEmail1">Status</label>
                  <select class="form-control" name="status" >
                  <option value="0"  @if($coursedetails->status == 0) selected="selected" @endif>Inactive</option>                   
                    <option value="1" @if($coursedetails->status == 1) selected="selected" @endif>Active</option>                          
                 </select>
                </div>
              </div>
                
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
              <input type="hidden" name="detail_id" value="{{$coursedetails->detail_id}}">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
          <!-- /.box-body -->
          <div class="box-footer">
         
          </div>
          <!-- /.box-footer-->
        </div>
        <!-- /.box -->
  
      </section>
      <!-- /.content -->
    </div>
 
  @endsection