@extends('admin.main')
@section('title','Add-Details')
@section('content')

<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
    {{$data['page_title']}}
      <small>{{$data['page_sub_title']}}</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Add New </a></li>
    </ol>
  </section>

<section class="content">
    @if(Session::has('message'))
    <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-check"></i> Alert!</h4>
                      {{ Session::get('message') }}
      </div>
    @endif
        <!-- Default box -->
        <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title">Add Details</h3>
  
            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                      title="Collapse">
                <i class="fa fa-minus"></i></button>
              <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                <i class="fa fa-times"></i></button>
            </div>
          </div>
          <div class="box-body">
          <div class="row">
        <div class="col-xs-12">
          <div class="box">
         
              <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Gallery Detail Form</h3><br>
              <a href="{{route('gallerydetail.index')}}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>

            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form action="{{route('gallerydetail.save')}}" method="post" enctype="multipart/form-data">
              @csrf
              <div class="box-body">
                 <div class="form-group col-md-4">
                  <label for="exampleInputEmail1">Gallery Type</label>
                  <select class="form-control" name="gallery_tpe"  id="file_type">
                  <option value="image">Image</option>              
                  <option value="vedio">Video</option>             
                 </select>
                 {!! $errors->first('gallery_tpe', '<p style="color:red;" class="help-block">:message</p>') !!}
                </div>  
              <div class="form-group col-md-4">
                  <label for="exampleInputEmail1">Gallery</label>
                  <select class="form-control" name="gallery_id" >
                  <option value="">Select Gallery</option>              
                  @foreach($gallery as $data)
                      <option value="{{$data->gallery_id}}">{{$data->title}}</option>
                  @endforeach              
                 </select>
                 {!! $errors->first('gallery_id', '<p style="color:red;" class="help-block">:message</p>') !!}
                </div>   
                </div> 
                  <div class="box-body">
                <div class="form-group col-md-4"  id="photo">
                  <label for="exampleInputFile">Photo</label>
                  <input type="file" name="photo[]" id="exampleInputFile" multiple>
                </div>   
                <div class="form-group col-md-4 inc" style="display:none" id="vedio">
                  <label for="exampleInputFile">Video Url</label>
                <input type="text" name="vedio_url[]" class="form-control"  placeholder="Video">
                <button class="btn btn-primary s" type="button" id="append">more</button>
                </div>    
                </div>
                  <div class="box-body"> 
                <div class="form-group col-md-6">
                  <label for="exampleInputEmail1">Status</label>
                  <select class="form-control" name="status" >
                  <option value="1">Active</option>  
                  <option value="0" >Inactive</option>                    
                 </select>
                </div>
                </div>
                <div class="form-group col-md-12">
                <button type="submit" class="btn btn-primary">Submit</button>
               </div>           
              </div>
          <!-- /.box-footer-->
        </div>
        <!-- /.box -->
  
      </section>
      <!-- /.content -->
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

 <script>
 $('#file_type').change(function() {
    if($(this).val() == "vedio"){
      $('#vedio').css('display', 'block');
      $('#photo').css('display', 'none');
    }
    else{
     $('#vedio').css('display', 'none');
     $('#photo').css('display', 'block');
    }
});

 </script>
 <script>
 jQuery(document).ready( function () {
        $("#append").click( function(e) {
          e.preventDefault();
        $(".inc").append('<div class="controls">\
                <input class="form-control" type="text" name="vedio_url[]" placeholder="vedio">\
                <a href="#" class="remove_this btn btn-danger s">remove</a>\
                <br>\
                <br>\
            </div>');
        return false;
        });

    jQuery(document).on('click', '.remove_this', function() {
        jQuery(this).parent().remove();
        return false;
        });
    $("input[type=submit]").click(function(e) {
      e.preventDefault();
      $(this).next("[name=textbox]")
      .val(
        $.map($(".inc :text"), function(el) {
          return el.value
        }).join(",\n")
      )
    })
  });
  </script>

  @endsection