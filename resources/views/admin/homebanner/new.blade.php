@extends('admin.main')
@section('title','New-Banner')
@section('content')

<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
    {{$data['page_title']}}
      <small>{{$data['page_sub_title']}}</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Add New </a></li>
    </ol>
  </section>

<section class="content">
    @if(Session::has('message'))
    <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-check"></i> Alert!</h4>
                      {{ Session::get('message') }}
      </div>
    @endif
        <!-- Default box -->
        <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title">New Banner</h3><br>
            <a href="{{route('home-banner')}}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>

  
            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                      title="Collapse">
                <i class="fa fa-minus"></i></button>
              <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                <i class="fa fa-times"></i></button>
            </div>
          </div>
          <div class="box-body">
          <div class="row">
        <div class="col-xs-12">
          <div class="box">
         
              <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Course Form</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form action="{{route('home-banner.save')}}" method="post" enctype="multipart/form-data">
          @csrf
              <div class="box-body">            
                <div class="form-group col-md-6">
                  <label for="exampleInputEmail1">Title</label>
                  <input type="test" name="title" class="form-control"  placeholder="Name">
                  {!! $errors->first('title', '<p style="color:red;" class="help-block">:message</p>') !!}
                </div>               
                <div class="form-group col-md-6">
                  <label for="exampleInputFile">Banner Image</label>
                  <input type="file" name="banner_image" id="exampleInputFile">
                  {!! $errors->first('banner_image', '<p style="color:red;" class="help-block">:message</p>') !!}
                </div>                        
              </div>           
              <div class="form-group col-md-6">
                  <label for="exampleInputEmail1">Status</label>
                  <select class="form-control" name="status" >  
                    <option value="1" >Active</option>                    
                    <option value="0" >Inactive</option>        
                   </select>
                </div>
                <div class="form-group col-md-6">
                  <label for="exampleInputEmail1">Show Home</label>
                  <select class="form-control" name="show_home" >
                  <option value="1">Yes</option>
                  <option value="0" >No</option>                                    
                 </select>
                </div>
              </div>
              
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
          <!-- /.box-body -->
          <div class="box-footer">
     
          </div>
          <!-- /.box-footer-->
        </div>
        <!-- /.box -->
  
      </section>
      <!-- /.content -->
    </div>
 
<script>
  CKEDITOR.replace( 'over_view' );
</script>
<script>
  CKEDITOR.replace( 'outline' );
</script>
  @endsection