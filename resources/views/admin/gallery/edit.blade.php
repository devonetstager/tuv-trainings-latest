@extends('admin.main')
@section('title','Edit-Gallery')
@section('content')

<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
    {{$data['page_title']}}
      <small>{{$data['page_sub_title']}}</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Edit Gallery</a></li>
    </ol>
  </section>
<section class="content">
        @if(Session::has('message'))
           <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-check"></i> Alert!</h4>
                      {{ Session::get('message') }}
          </div>
      @endif
        <!-- Default box -->
        <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title">Edit Gallery</h3><br>
            <a href="{{route('gallery.index')}}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>

            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                      title="Collapse">
                <i class="fa fa-minus"></i></button>
              <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                <i class="fa fa-times"></i></button>
            </div>
          </div>
          <div class="box-body">
          <div class="row">
        <div class="col-xs-12">
          <div class="box">
         
              <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Gallery Form</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form action="{{route('gallery.update')}}" method="post" enctype="multipart/form-data">
               @csrf
               <div class="box-body">
                <div class="form-group col-md-6">
                  <label for="exampleInputEmail1">Name</label>
                  <input type="test" name="title" value="{{$gallery->title}}" class="form-control"  placeholder="Name">
                  {!! $errors->first('title', '<p style="color:red;" class="help-block">:message</p>') !!}                

                </div>               
                <div class="form-group col-md-6">
                  <label for="exampleInputFile">Image</label>
                  <input type="file" name="main_photo" id="exampleInputFile">
                  {!! $errors->first('main_photo', '<p style="color:red;" class="help-block">:message</p>') !!}                

                </div>
                
              </div>
              <div class="form-group col-md-12">
                  <label for="exampleInputFile">Description</label>
                  <textarea id="description" name="description" rows="7" class="form-control ckeditor" placeholder="Write your message..">
                  {{$gallery->description}}
                  </textarea>
                  {!! $errors->first('description', '<p style="color:red;" class="help-block">:message</p>') !!}                

                </div>       
              </div>
              <div class="form-group col-md-6">
                <label for="exampleInputEmail1">Status</label>
                <select class="form-control" name="status" >
                  <option value="1" @if($gallery->status == 1) selected="selected" @endif>Active</option>
                  <option value="0" @if($gallery->status == 0) selected="selected" @endif>Inactive</option>                              
               </select>
              </div>
              <div class="form-group col-md-6">
                  <label for="exampleInputEmail1">Show Home</label>
                  <select class="form-control" name="show_home" >
                  <option value="1" @if($gallery->show_home == 1) selected="selected" @endif>Yes</option>
                   <option value="0" @if($gallery->show_home == 0) selected="selected" @endif>No</option>                   
                                       
                 </select>
                </div>
            </div>             
              <!-- /.box-body -->

              <div class="box-footer">
              <input type="hidden" name="gallery_id" value="{{$gallery->gallery_id}}">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
          <!-- /.box-body -->
          <div class="box-footer">
         
          </div>
          <!-- /.box-footer-->
        </div>
        <!-- /.box -->
  
      </section>
      <!-- /.content -->
    </div>
 
  @endsection