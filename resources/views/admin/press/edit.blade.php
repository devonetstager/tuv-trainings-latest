@extends('admin.main')
@section('title','Edit-Category')
@section('content')

<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
    {{$data['page_title']}}
      <small>{{$data['page_sub_title']}}</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Press Release form</a></li>
    </ol>
  </section>

<section class="content">
        @if(Session::has('message'))
           <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-check"></i> Alert!</h4>
                      {{ Session::get('message') }}
          </div>
        @endif
        @if(Session::has('err_message'))
      <div class="alert alert-danger alert-dismissible">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                      <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                      {{ Session::get('err_message') }}
      </div>
    @endif
        <!-- Default box -->
        <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title">Edit Press Release</h3><br>
            <a href="{{route('pressrelease.index')}}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>

  
            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                      title="Collapse">
                <i class="fa fa-minus"></i></button>
              <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                <i class="fa fa-times"></i></button>
            </div>
          </div>
          <div class="box-body">
          <div class="row">
        <div class="col-xs-12">
          <div class="box">
         
              <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Press Release Form</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form action="{{route('pressrelease.update')}}" method="post" enctype="multipart/form-data">
               @csrf
              <div class="box-body">
              <div class="row"> 
              <div class="form-group col-md-6">
                  <label for="exampleInputEmail1">YouTube Link</label>
                  <input type="test" name="youtube_link" value="{{$pressrelease->youtube_link}}" class="form-control"   placeholder="YouTube Link">
                  {!! $errors->first('youtube_link', '<p style="color:red;" class="help-block">:message</p>') !!}

                </div>             
                <div class="form-group col-md-4">
                  <label for="exampleInputFile">PDF File</label>
                  <input type="file" name="pdf_filename" id="exampleInputFile">
                </div> 
                <a href="{{asset('storage/app/press/')}}/{{$pressrelease->pdf_filename}}" class="btn btn-primary">View File</a> 
                </div>
                <div class="row"> 
          
                <div class="form-group col-md-6">
                  <label for="exampleInputEmail1">Status</label>
                  <select class="form-control" name="status" >
                  <option value="1">Active</option>
                  <option value="0">Inactive</option>               
                 </select>
                </div>
                </div>
             
              <!-- /.box-body -->

              <div class="form-group col-md-6">
              <input type="hidden" name="press_release_id" value="{{$pressrelease->press_release_id}}">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
          <!-- /.box-body -->
          <div class="box-footer">
         
          </div>
          <!-- /.box-footer-->
        </div>
        <!-- /.box -->
  
      </section>
      <!-- /.content -->
    </div>
 
  @endsection