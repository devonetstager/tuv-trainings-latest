@extends('admin.main')
@section('title','Edit-Course')
@section('content')

<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
    {{$data['page_title']}}
      <small>{{$data['page_sub_title']}}</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Edit Course</a></li>
    </ol>
  </section>
<section class="content">
        @if(Session::has('message'))
           <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-check"></i> Alert!</h4>
                      {{ Session::get('message') }}
          </div>
      @endif
        <!-- Default box -->
        <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title">Edit Course</h3><br>
            <a href="{{route('sigma.index')}}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>

            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                      title="Collapse">
                <i class="fa fa-minus"></i></button>
              <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                <i class="fa fa-times"></i></button>
            </div>
          </div>
          <div class="box-body">
          <div class="row">
        <div class="col-xs-12">
          <div class="box">
         
              <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Course Form</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form action="{{route('sigma.update')}}" method="post" enctype="multipart/form-data">
               @csrf
              <div class="box-body">
              <div class="form-group col-md-6">
                  <label for="exampleInputEmail1">Course Name</label>
                  <input type="test" name="name" class="form-control"  placeholder="Name" value="{{$sigma->name}}">
                  {!! $errors->first('name', '<p style="color:red;" class="help-block">:message</p>') !!}                

                </div>               
                <div class="form-group col-md-6">
                  <label for="exampleInputFile">Image</label>
                  <input type="file" name="course_image" id="exampleInputFile">
                </div>
               
              </div>
              <div class="form-group col-md-12">
                  <label for="exampleInputFile">Description</label>
                  <textarea id="description" name="description" rows="7" value="{{$sigma->description}}" class="form-control ckeditor" placeholder="Write your message..">
                  {{$sigma->description}}
                  </textarea>
                  {!! $errors->first('description', '<p style="color:red;" class="help-block">:message</p>') !!}                

                </div>                
              </div>   
                    
                <div class="form-group col-md-4">
                  <label for="exampleInputEmail1">Status</label>
                  <select class="form-control" name="status" >
                
                    <option value="0"  @if($sigma->status == 0) selected="selected" @endif>Inactive</option>                   
                    <option value="1" @if($sigma->status == 1) selected="selected" @endif>Active</option>                      
                 </select>
                </div>
                <div class="form-group col-md-6">
                  <label for="exampleInputEmail1">Show Home</label>
                  <select class="form-control" name="show_home" >
                  <option value="1" @if($sigma->show_home == 1) selected="selected" @endif>Yes</option>
                   <option value="0" @if($sigma->show_home == 0) selected="selected" @endif>No</option>                   
                                        
                 </select>
                </div>
              </div>
                
              </div>

              <div class="box-body">
                <div class="form-group col-md-6">
                  <label for="">City</label>
                  <select class="form-control" name="city_id" >
                  <option value="">Select City</option>              
                  @foreach($cities as $city)
                      <option value="{{$city->city_id}}" @if($sigma->city_id == $city->city_id) selected="selected" @endif>{{$city->name}}</option>
                      @endforeach              
                 </select>
                </div>
                 <div class="form-group col-md-6">
                  <label for="">Currency</label>
                  <select class="form-control" name="currency_id" >
                  <option value="">Select Currency</option>              
                  @foreach($currencies as $currency)
                      <option value="{{$currency->currency_id}}" @if($sigma->currency_id == $currency->currency_id) selected="selected" @endif>{{$currency->name}}</option>
                      @endforeach              
                 </select>
                </div>                     
                <div class="form-group col-md-6">
                  <label for="">Location</label>
                  <input type="text" name="location" value="{{$sigma->location}}" class="form-control"  placeholder="Location">
                </div>                
                <div class="form-group col-md-6">
                  <label for="">Start Date</label>
                  <input type="text" name="start_date" id="datepicker1" value="{{$sigma->start_date}}" class="form-control datepicker"  placeholder="Select Date">
                </div> 
                <div class="form-group col-md-6">
                  <label for="">End Date</label>
                  <input type="text" name="end_date" id="datepicker2" value="{{$sigma->end_date}}" class="form-control datepicker"  placeholder="Select Date">
                </div>
                <div class="form-group col-md-6">
                  <label for="">Course Fee</label>
                  <input type="text" name="fee" class="form-control" value="{{$sigma->fee}}"  placeholder="Course Fee">
                </div> 
              </div>
                
              </div>

              <!-- /.box-body -->

              <div class="box-footer">
              <input type="hidden" name="six_sigma_id" value="{{$sigma->six_sigma_id}}">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
          <!-- /.box-body -->
          <div class="box-footer">
         
          </div>
          <!-- /.box-footer-->
        </div>
        <!-- /.box -->
  
      </section>
      <!-- /.content -->
    </div>
 
  @endsection