@extends('admin.main')
@section('title','New-Course')
@section('content')

<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
    {{$data['page_title']}}
      <small>{{$data['page_sub_title']}}</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Add New </a></li>
    </ol>
  </section>

<section class="content">
    @if(Session::has('message'))
    <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-check"></i> Alert!</h4>
                      {{ Session::get('message') }}
      </div>
    @endif
        <!-- Default box -->
        <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title">New Course</h3><br>
            <a href="{{route('admin.courses')}}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>

  
            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                      title="Collapse">
                <i class="fa fa-minus"></i></button>
              <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                <i class="fa fa-times"></i></button>
            </div>
          </div>
          <div class="box-body">
          <div class="row">
        <div class="col-xs-12">
          <div class="box">
         
              <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Course Form</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form action="{{route('courses.save')}}" method="post" enctype="multipart/form-data">
          @csrf
              <div class="box-body">
              <div class="form-group col-md-6">
                  <label for="exampleInputEmail1">Category</label>
                  <select class="form-control" name="category_id" >
                  <option value="">Select Category</option>              
                  @foreach($sel_categories as $category)
                      <option value="{{$category->category_id}}">{{$category->name}}</option>
                      @endforeach              
                 </select>
                 {!! $errors->first('category_id', '<p style="color:red;" class="help-block">:message</p>') !!}
                </div>         
                <div class="form-group col-md-6">
                  <label for="exampleInputEmail1">Course Name</label>
                  <input type="test" name="name" value="{{ old('name') }}" class="form-control"  placeholder="Name">
                  {!! $errors->first('name', '<p style="color:red;" class="help-block">:message</p>') !!}
                </div>  
                <div class="form-group col-md-6">
                  <label for="exampleInputEmail1">Meta Title</label>
                  <input type="test" name="meta_title" value="{{ old('meta_title') }}" class="form-control"  placeholder="Meta Title">
                  {!! $errors->first('meta_title', '<p style="color:red;" class="help-block">:message</p>') !!}
                </div>  
                <div class="form-group col-md-6">
                  <label for="exampleInputEmail1">Meta Tags</label>
                  <input type="test" name="meta_tags" value="{{ old('meta_tags') }}" class="form-control"  placeholder="Meta Tags">
                  {!! $errors->first('meta_tags', '<p style="color:red;" class="help-block">:message</p>') !!}
                </div>  
                <div class="form-group col-md-12">
                  <label for="exampleInputEmail1">Meta Description</label>
                  <input type="test" name="meta_description" value="{{ old('meta_description') }}" class="form-control"  placeholder="Meta Description">
                  {!! $errors->first('meta_description', '<p style="color:red;" class="help-block">:message</p>') !!}
                </div>                        
                <div class="form-group col-md-4">
                  <label for="exampleInputFile">Image</label>
                  <input type="file" name="image" id="exampleInputFile">
                  {!! $errors->first('image', '<p style="color:red;" class="help-block">:message</p>') !!}
                </div>
                <div class="form-group col-md-4">
                  <label for="exampleInputFile">Brochure</label>
                  <input type="file" name="brochure" id="exampleInputFile">
                  {!! $errors->first('brochure', '<p style="color:red;" class="help-block">:message</p>') !!}
                </div> 
                <div class="form-group col-md-4">
                  <label for="exampleInputFile">Banner</label>(dimensions:minimum width=1920,min height=500)
                  <input type="file" name="banner_image" id="exampleInputFile">
                  {!! $errors->first('banner_image', '<p style="color:red;" class="help-block">:message</p>') !!}
                </div>                
              </div>
              <div class="form-group col-md-12">
                  <label for="exampleInputFile">Overview</label>
                  <textarea id="over_view" name="over_view" rows="7" class="form-control ckeditor" placeholder="Write your message..">{{ old('over_view') }}</textarea>
                  {!! $errors->first('over_view', '<p style="color:red;" class="help-block">:message</p>') !!}                
                </div>       
              </div>
              <div class="form-group col-md-12">
                  <label for="exampleInputFile">Outline</label>
                  <textarea id="outline" name="outline" rows="7" class="form-control ckeditor" placeholder="Write your message..">{{ old('outline') }}</textarea>
                  {!! $errors->first('outline', '<p style="color:red;" class="help-block">:message</p>') !!}                
              <div class="form-group col-md-6">
                  <label for="exampleInputEmail1">Status</label>
                  <select class="form-control" name="status" >                
                    <option value="0" >Up Coming</option>                   
                    <option value="1" >Normal</option>                      
                 </select>
                </div>
                <div class="form-group col-md-6">
                  <label for="exampleInputEmail1">Show Home</label>
                  <select class="form-control" name="show_home" >
                  <option value="1">Yes</option>
                  <option value="0" >No</option>                                    
                 </select>
                </div>
              </div>
              
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
          <!-- /.box-body -->
          <div class="box-footer">
     
          </div>
          <!-- /.box-footer-->
        </div>
        <!-- /.box -->
  
      </section>
      <!-- /.content -->
    </div>
 
<script>
  CKEDITOR.replace( 'over_view' );
</script>
<script>
  CKEDITOR.replace( 'outline' );
</script>
  @endsection