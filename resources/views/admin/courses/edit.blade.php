@extends('admin.main')
@section('title','Edit-Course')
@section('content')

<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
    {{$data['page_title']}}
      <small>{{$data['page_sub_title']}}</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Edit Course</a></li>
    </ol>
  </section>
<section class="content">
        @if(Session::has('message'))
           <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-check"></i> Alert!</h4>
                      {{ Session::get('message') }}
          </div>
      @endif
        <!-- Default box -->
        <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title">Edit Course</h3>
  
            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                      title="Collapse">
                <i class="fa fa-minus"></i></button>
              <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                <i class="fa fa-times"></i></button>
            </div>
          </div>
          <div class="box-body">
          <div class="row">
        <div class="col-xs-12">
          <div class="box">
         
              <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Course Form</h3><br>
              <a href="{{route('admin.courses')}}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>

            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form action="{{route('courses.update')}}" method="post" enctype="multipart/form-data">
               @csrf
              <div class="box-body">
   
              <div class="form-group col-md-6">
                  <label for="exampleInputEmail1">Category</label>
                  <select class="form-control" name="category_id" >
                  <option>Select Category</option>              
                  @foreach($allcategories as $category)
                      <option value="{{$category->category_id}}" @if($course->category_id == $category->category_id) selected="selected" @endif >{{$category->name}}</option>
                      @endforeach              
                 </select>
                 @if ($errors->has('category_id'))
                      <div class="alert alert-danger">
                          <ul>                      
                                  <li>{{ $errors->first('category_id') }}</li>                              
                          </ul>
                      </div>
                      @endif
                </div>         
              <div class="form-group col-md-6">
                  <label for="exampleInputEmail1">Course Name</label>
                  <input type="test" name="name" class="form-control"  placeholder="Name" value="{{$course->name}}">
                  {!! $errors->first('name', '<p style="color:red;" class="help-block">:message</p>') !!}
                </div> 
                <div class="form-group col-md-6">
                  <label for="exampleInputEmail1">Meta Title</label>
                  <input type="test" name="meta_title" value="{{$course->meta_title}}" class="form-control"  placeholder="Meta Title">
                  {!! $errors->first('meta_title', '<p style="color:red;" class="help-block">:message</p>') !!}
                </div>  
                <div class="form-group col-md-6">
                  <label for="exampleInputEmail1">Meta Tags</label>
                  <input type="test" name="meta_tags" value="{{$course->meta_tags}}" class="form-control"  placeholder="Meta Tags">
                  {!! $errors->first('meta_tags', '<p style="color:red;" class="help-block">:message</p>') !!}
                </div>  
                <div class="form-group col-md-12">
                  <label for="exampleInputEmail1">Meta Description</label>
                  <input type="test" name="meta_description" value="{{$course->meta_description}}" class="form-control"  placeholder="Meta Description">
                  {!! $errors->first('meta_description', '<p style="color:red;" class="help-block">:message</p>') !!}
                </div>                                      
                <div class="form-group col-md-4">
                  <label for="exampleInputFile">Image</label>
                  <input type="file" name="image" id="exampleInputFile">
                </div>
                <div class="form-group col-md-4">
                  <label for="exampleInputFile">Brochure</label>
                  <input type="file" name="brochure" id="exampleInputFile">
                  {!! $errors->first('brochure', '<p style="color:red;" class="help-block">:message</p>') !!}
                </div>
                <div class="form-group col-md-4">
                  <label for="exampleInputFile">Banner</label>(dimensions:minimum width=1920,min height=500)
                  <input type="file" name="banner_image" id="exampleInputFile">
                  {!! $errors->first('banner_image', '<p style="color:red;" class="help-block">:message</p>') !!}
                </div>          
              </div>
              <div class="form-group col-md-12">
                  <label for="exampleInputFile">Overview</label>
                  <textarea id="over_view" name="over_view" rows="7" value="{{$course->over_view}}" class="form-control ckeditor" placeholder="Write your message..">
                  {{$course->over_view}}
                  </textarea>
                  {!! $errors->first('over_view', '<p style="color:red;" class="help-block">:message</p>') !!}

                </div>
                
              </div>
              <div class="form-group col-md-12">
                  <label for="exampleInputFile">Outline</label>
                  <textarea id="outline" name="outline" rows="7"   class="form-control ckeditor" placeholder="Write your message..">
                  {{$course->outline}}
                  </textarea>
                 {!! $errors->first('outline', '<p style="color:red;" class="help-block">:message</p>') !!}
                </div>
                <div class="form-group col-md-6">
                  <label for="exampleInputEmail1">Status</label>
                  <select class="form-control" name="status" >
                
                    <option value="0"  @if($course->status == 0) selected="selected" @endif>Up Coming</option>                   
                    <option value="1" @if($course->status == 1) selected="selected" @endif>Normal</option>                      
                 </select>
                </div>
                <div class="form-group col-md-6">
                  <label for="exampleInputEmail1">Show Home</label>
                  <select class="form-control" name="show_home" >
                  <option value="1" @if($course->show_home == 1) selected="selected" @endif>Yes</option>
                   <option value="0" @if($course->show_home == 0) selected="selected" @endif>No</option>                   
                                       
                 </select>
                </div>
              </div>
                
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
              <input type="hidden" name="course_id" value="{{$course->course_id}}">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
          <!-- /.box-body -->
          <div class="box-footer">
         
          </div>
          <!-- /.box-footer-->
        </div>
        <!-- /.box -->
  
      </section>
      <!-- /.content -->
    </div>
 
  @endsection