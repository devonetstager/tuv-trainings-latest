@extends('admin.main')
@section('title','Edit-Webinar')
@section('content')

<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
    {{$data['page_title']}}
      <small>{{$data['page_sub_title']}}</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
      <!-- <li><a href="#">Edit Webinar </a></li> -->
    </ol>
  </section>

<section class="content">
        @if(Session::has('message'))
           <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-check"></i> Alert!</h4>
                      {{ Session::get('message') }}
          </div>
      @endif
      @if(Session::has('err_message'))
      <div class="alert alert-danger alert-dismissible">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                      <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                      {{ Session::get('err_message') }}
      </div>
    @endif
        <!-- Default box -->
        <div class="box">
          <div class="box-header with-border">
            <!-- <h3 class="box-title">Edit Webinar</h3><br> -->
            <a href="{{route('webinar.index')}}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>

            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                      title="Collapse">
                <i class="fa fa-minus"></i></button>
              <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                <i class="fa fa-times"></i></button>
            </div>
          </div>
          <div class="box-body">
          <div class="row">
        <div class="col-xs-12">
          <div class="box">
         
              <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <!-- <h3 class="box-title">News Form</h3> -->
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form action="{{route('webinar.update')}}" method="post" enctype="multipart/form-data">
               @csrf
              <div class="box-body">          
                <div class="form-group col-md-6">
                  <label for="exampleInputEmail1">Title</label>
                  <input type="text" name="title"  class="form-control" value="{{$webinar->title}}"  placeholder="Title">
                  {!! $errors->first('title', '<p style="color:red;" class="help-block">:message</p>') !!}                

                </div> 
                  <div class="form-group col-md-6">
                  <label for="exampleInputEmail1">Link</label>
                  <input type="text" name="webinar_link" class="form-control" value="{{$webinar->webinar_link}}"  placeholder="Link">                 
                  {!! $errors->first('webinar_link', '<p style="color:red;" class="help-block">:message</p>') !!}                
                </div> 
               
                 <div class="box-body">  
                <div class="form-group col-md-6">
                
                  <label for="exampleInputEmail1">Date</label>
                  <input type="text" name="webinar_date" id="datepicker" value="{{$webinar->date_webinar}}" class="form-control datepicker"  placeholder="Select Date">
                  {!! $errors->first('webinar_date', '<p style="color:red;" class="help-block">:message</p>') !!}                

                </div> 
               <div class="box-body">  
                  <label for="exampleInputFile">Image</label>
                  <input type="file" name="webinar_image" id="exampleInputFile">
                </div>  
               
                <div class="form-group col-md-6">
                  <label for="exampleInputEmail1">Status</label>
                  <select class="form-control" name="status" >
                  <option value="1" @if($webinar->status == 1) selected="selected" @endif>Active</option>  
                   <option value="0"  @if($webinar->status == 0) selected="selected" @endif>Inactive</option>                    
                 </select>
                </div>
                </div>
        
                <div class="box-body">  
              <!-- /.box-body -->
              <div class="box-footer">
                <input type="hidden" name="webinar_id" value="{{$webinar->webinar_id}}">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
          <!-- /.box-body -->
          <div class="box-footer">
         
          </div>
          <!-- /.box-footer-->
        </div>
        <!-- /.box -->
  
      </section>
      <!-- /.content -->
    </div>
 
  @endsection