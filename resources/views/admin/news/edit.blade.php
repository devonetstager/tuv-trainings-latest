@extends('admin.main')
@section('title','Edit-News')
@section('content')

<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
    {{$data['page_title']}}
      <small>{{$data['page_sub_title']}}</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Edit News </a></li>
    </ol>
  </section>

<section class="content">
        @if(Session::has('message'))
           <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-check"></i> Alert!</h4>
                      {{ Session::get('message') }}
          </div>
      @endif
      @if(Session::has('err_message'))
      <div class="alert alert-danger alert-dismissible">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                      <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                      {{ Session::get('err_message') }}
      </div>
    @endif
        <!-- Default box -->
        <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title">Edit News</h3><br>
            <a href="{{route('news.index')}}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>

            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                      title="Collapse">
                <i class="fa fa-minus"></i></button>
              <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                <i class="fa fa-times"></i></button>
            </div>
          </div>
          <div class="box-body">
          <div class="row">
        <div class="col-xs-12">
          <div class="box">
         
              <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">News Form</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form action="{{route('news.updatenews')}}" method="post" enctype="multipart/form-data">
               @csrf
              <div class="box-body">
              <div class="form-group col-md-6">
                  <label for="exampleInputEmail1">Category</label>
                  <select class="form-control" name="news_category" >
                  <option value="1" @if($news->news_category == 1) selected="selected" @endif>Corporate News</option>  
                   <option value="0" @if($news->news_category == 0) selected="selected" @endif>Technology Blog</option>                    
                 </select>
                </div>
              <div class="form-group col-md-6">
                  <label for="exampleInputEmail1">Title</label>
                  <input type="text" name="title" value="{{$news->title}}" class="form-control"   placeholder="Title">
                  {!! $errors->first('title', '<p style="color:red;" class="help-block">:message</p>') !!}
                </div>                
                <div class="form-group col-md-6">
                  <label for="exampleInputEmail1">Subtitle</label>
                  <input type="text" name="sub_title"  class="form-control" value="{{$news->sub_title}}"  placeholder="Select Date">
                
                </div> 
                <div class="form-group col-md-6">
                  <label for="exampleInputEmail1">Date</label>
                  <input type="text" name="news_date" id="datepicker" value="{{$news->news_date}}" class="form-control datepicker"  placeholder="Select Date">
                  {!! $errors->first('news_date', '<p style="color:red;" class="help-block">:message</p>') !!}

                </div>
                <div class="form-group col-md-6">
                  <label for="exampleInputEmail1">Author</label>
                  <input type="text" name="author" class="form-control" value="{{$news->author}}"  placeholder="Author">
                  {!! $errors->first('author', '<p style="color:red;" class="help-block">:message</p>') !!}

                </div> 
                <div class="form-group col-md-12">
                  <label for="exampleInputFile">Content</label>
                  <textarea  name="content" rows="7" class="form-control ckeditor">{{$news->content}}</textarea>
                  {!! $errors->first('content', '<p style="color:red;" class="help-block">:message</p>') !!}

                </div>  
                <div class="form-group col-md-6">
                  <label for="exampleInputFile">Image</label>
                  <input type="file" name="image_banner" id="exampleInputFile">
                  <img style="width: 25%" src="{{asset('storage/app/news/imagebanner')}}/{{$news->image_banner}}"  class="img-responsive">
                </div> 
              </div>
                <div class="form-group col-md-6">
                  <label for="exampleInputEmail1">Status</label>
                  <select class="form-control" name="status" >
                  <option value="0"  @if($news->status == 0) selected="selected" @endif>Inactive</option>                   
                    <option value="1" @if($news->status == 1) selected="selected" @endif>Active</option>                          
                 </select>
                </div>
                <div class="form-group col-md-6">
                  <label for="exampleInputEmail1">Show Home</label>
                  <select class="form-control" name="show_home" >
                  <option value="1" @if($news->show_home == 1) selected="selected" @endif>Yes</option>
                   <option value="0" @if($news->show_home == 0) selected="selected" @endif>No</option>                   
                                       
                 </select>
                </div>
              </div>
                
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
              <input type="hidden" name="news_id" value="{{$news->news_id}}">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
          <!-- /.box-body -->
          <div class="box-footer">
         
          </div>
          <!-- /.box-footer-->
        </div>
        <!-- /.box -->
  
      </section>
      <!-- /.content -->
    </div>
 
  @endsection