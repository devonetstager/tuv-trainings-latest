@extends('admin.main')
@section('title','Dashboard')
@section('content')

<div class="content-wrapper">
  <!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
    TUV
      <small>Manage</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Dashboard</a></li>
      <!--<li class="active">Blank page</li>-->
    </ol>
  <link rel="stylesheet" href="https://projectpreview.org/tuv-latest/public/admin/dist/css/skins/custom.css">
<section class="content">
<div class="row dashicons">
                    <!-- Column -->
                    <div class="col-md-6 col-lg-2 col-xlg-3">
                        <div class="card card-hover">
                            <a href="{{route('admin.courses')}}" class="box bg-cyan text-center">
                                <small class="font-light text-white"><i class="fa fa-graduation-cap"></i></small>
                                <span class="text-white">Course</span>
                            </a>
                        </div>
                    </div>
                    <!-- Column -->
                    <div class="col-md-6 col-lg-2 col-xlg-3">
                        <div class="card card-hover">
                            <a href="{{route('enquiry.quick')}}" class="box bg-cyan text-center">
                                <small class="font-light text-white"><i class="fa fa-paper-plane"></i></small>
                                <span class="text-white">Enquiry</span>
                            </a>
                        </div>
                    </div>
                     <!-- Column
                    <div class="col-md-6 col-lg-2 col-xlg-3">
                        <div class="card card-hover">
                            <a href="#" class="box bg-warning text-center">
                                <small class="font-light text-white"><i class="fa fa-bar-chart"></i></small>
                                <span class="text-white">Graphs</span>
                            </a>
                        </div>
                    </div> -->
                    <!-- Column -->
                    <div class="col-md-6 col-lg-2 col-xlg-3">
                        <div class="card card-hover">
                            <a href="{{route('country.index')}}" class="box bg-danger text-center">
                                <small class="font-light text-white"><i class="fa fa-map"></i></small>
                                <span class="text-white">Country</span>
                            </a>
                        </div>
                    </div>
                    <!-- Column -->
                    <div class="col-md-6 col-lg-2 col-xlg-3">
                        <div class="card card-hover">
                            <a href="{{route('currency.index')}}" class="box bg-info text-center">
                                <small class="font-light text-white"><i class="fa fa-money"></i></small>
                                <span class="text-white">Currency</span>
                            </a>
                        </div>
                    </div>
                    <!-- Column -->
                    <div class="col-md-6 col-lg-2 col-xlg-3">
                        <div class="card card-hover">
                            <a href="{{route('cms.index')}}" class="box bg-success text-center">
                                <small class="font-light text-white"><i class="fa fa-book"></i></small>
                                <span class="text-white">CMS Pages</span>
                            </a>
                        </div>
                    </div>
                     <!-- Column -->
                    
                    <!-- Column -->
                </div>
        <!-- Default box -->
        <!--<div class="box">-->
        <!--  <div class="box-header with-border">-->
        <!--    <h3 class="box-title"></h3>-->
  
        <!--    <div class="box-tools pull-right">-->
        <!--      <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"-->
        <!--              title="Collapse">-->
        <!--        <i class="fa fa-minus"></i></button>-->
        <!--      <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">-->
        <!--        <i class="fa fa-times"></i></button>-->
        <!--    </div>-->
        <!--  </div>-->
          
          <!--<div class="box-body">-->
          <!--  Start creating your amazing application!-->
          <!--</div>-->
          <!-- /.box-body -->
          <!--<div class="box-footer">-->
         
          <!--</div>-->
          <!-- /.box-footer-->
        <!--</div>-->
        <!-- /.box -->
  
      </section>
      <!-- /.content -->
    </div>
 
  @endsection