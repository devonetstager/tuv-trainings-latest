@extends('admin.main')
@section('title','View-Categories')
@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
@include('admin.themes.page_title_bar')




<section class="content">
    @if(Session::has('message'))
      <div class="alert alert-danger alert-dismissible">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                      <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                      {{ Session::get('message') }}
      </div>
    @endif

<!-- Default box -->
<div class="box">
  <div class="box-header with-border">



    <h3 class="box-title">Six Sigma Enquiry View</h3><br><br>
              <a href="{{route('enquiry.sixsigma')}}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
    <div class="box-tools pull-right">
      <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
              title="Collapse">
        <i class="fa fa-minus"></i></button>
      <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
        <i class="fa fa-times"></i></button>
    </div>
    
  </div>
  <div class="box-body">
    <h2>Name:{{$enquiry->name}}</h2>
    <h2></h2>
    <span id="photo_view"> 
         {{-- <img style="width: 25%" src="/tuv-nord-trainings/storage/app/courses/{{$course->image}}"  class="img-responsive"> --}}
    </span>
    <p>Date:{{$enquiry->created_at}}</p>
    <p>Mobile:{{$enquiry->mobile}}</p>
    <p>Email:{{$enquiry->email}}</p>
    <p>Address:{{$enquiry->address}}</p>    
    <p>Company:{{$enquiry->company}}</p>    
    <p>Message:{{$enquiry->message}}</p>    
  </div>
  <!-- /.box-body -->
  <div class="box-footer">
    {{-- Footer --}}
  </div>
  <!-- /.box-footer-->
</div>
<!-- /.box -->

</section>
      <!-- /.content -->
    </div>
 
  @endsection