@extends('admin.main')
@section('title','Enquiries')
@section('content')

<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>Enquiries </h1>
    <ol class="breadcrumb">
      <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Enquiries lists</a></li>
    </ol>
  </section>

<section class="content">
@if(Session::has('message'))
           <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-check"></i> Alert!</h4>
                      {{ Session::get('message') }}
          </div>
      @endif
        <!-- Default box -->
        <div class="box">
          <div class="box-header with-border">
          <h3 class="box-title">Enquiries
          </h3>
          </div>
          <div class="box-body">
          <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">             

              <div class="box-tools">
                <div class="input-group input-group-sm hidden-xs" style="width: 150px;">
                
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
            <table class="table table-bordered data-table">
                <thead>
                    <tr>
                        <th style="width:5%">No</th>
                        <th>Type</th>   
                        <th>Name</th>   
                        <th>Mobile</th> 
                        <th>Email</th> 
                        <th>Address</th> 
                        <th>Company</th> 
                        <th>Post</th> 
                        <th>City</th> 
                        <th>Country</th>           
                    </tr>
                </thead>
                <tbody>   
                  @foreach($enquiries as $enquiry)
                
                 {{-- <div style="display:none">
                 @php
                    $type = "quick"; 
                     @endphp 
                     @if($enquiry->type==0 )
                    $type = "quick enquiry";
                    @endif</div>  
                    @if($enquiry->type==1 )
                    $type = "upcoming";
                    @endif
                    @if($enquiry->type==2 )
                    $type = "six sigma";
                    @endif
                    @if($enquiry->type==3 )
                    $type = "hrci";
                    @endif
                      @if($enquiry->type==4 )
                    $type = "brochure";
                    @endif
                      @if($enquiry->type==5 )
                    $type = "calender";
                    @endif
                   </div> --}}
                  <tr>
                    <td>{{$loop->iteration}}</td>
                   
                    <td>{{$enquiry->type}}</td>
                    <td>{{$enquiry->name}}</td>
                    <td>{{$enquiry->mobile}}</td>
                    <td>{{$enquiry->email}}</td>
                    <td>{{$enquiry->address}}</td>
                    <td>{{$enquiry->company}}</td>
                    <td>{{$enquiry->post}}</td>
                    <td>{{$enquiry->city}}</td>
                    <td>{{$enquiry->country}}</td>
                  </tr>    
                  @endforeach
                </tbody>
            </table>
            {{$enquiries->links()}}
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
          </div>
          <!-- /.box-body -->
          <div class="box-footer">
       
          </div>
          <!-- /.box-footer-->
        </div>
        <!-- /.box -->
  
      </section>
      <!-- /.content -->
    </div>
  @endsection