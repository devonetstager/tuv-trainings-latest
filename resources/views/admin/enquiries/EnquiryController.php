<?php

namespace App\Http\Controllers\Admin;
use DataTables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use App\Models\Admin\Enquiry;
class EnquiryController
{
    public function list(){
        $enquiries = Enquiry::paginate(15);
        return view('admin.enquiries.list',compact('enquiries'));
    }
    public function upcoming_course_enquiry(Request $request)
    {
        if ($request->ajax()) {
            $data = Enquiry::latest()->where('type',1)->get();
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                        //    $btn  ='<a href="'. route('locations.edit', [encrypt($row->location_id)]) .'" class="edit btn btn-primary btn-success">Edit</a> '; 
                        //    $btn .='<a href="'. route('contacts.view', [encrypt($row->category_id)]) .'"  class="edit btn btn-primary btn-sm">View</a> ';
                           $btn ='<a href="'. route('enquiry.upcoming.delete', [encrypt($row->enquiry_id)]) .'" onclick="return confirmDelete();" class="edit btn btn-primary btn-danger">Delete</a>';
     
                            return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
        $data['page_title'] = 'Upcoming Course Enquiry';  
        $data['page_sub_title'] = 'Records';
        return view('admin.enquiries.upcoming-enquiry',compact('data'));
    }
    public function quick_enquiries(Request $request)
    {
        if ($request->ajax()) {
            $data = Enquiry::latest()->where('type',0)->get();
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                        //    $btn  ='<a href="'. route('locations.edit', [encrypt($row->location_id)]) .'" class="edit btn btn-primary btn-success">Edit</a> '; 
                        //    $btn .='<a href="'. route('contacts.view', [encrypt($row->category_id)]) .'"  class="edit btn btn-primary btn-sm">View</a> ';
                           $btn ='<a href="'. route('enquiry.quick.delete', [encrypt($row->enquiry_id)]) .'" onclick="return confirmDelete();" class="edit btn btn-primary btn-danger">Delete</a>';
     
                            return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
        $data['page_title'] = 'Quick Enquiries';  
        $data['page_sub_title'] = 'Records';
        return view('admin.enquiries.quick-enquiry',compact('data'));
    }
    public function contact_enquiry(Request $request)
    {
        if ($request->ajax()) {
            $data = Enquiry::latest()->where('type',8)->get();
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                        //    $btn  ='<a href="'. route('locations.edit', [encrypt($row->location_id)]) .'" class="edit btn btn-primary btn-success">Edit</a> '; 
                        //    $btn .='<a href="'. route('contacts.view', [encrypt($row->category_id)]) .'"  class="edit btn btn-primary btn-sm">View</a> ';
                           $btn ='<a href="'. route('enquiry.contact.delete', [encrypt($row->enquiry_id)]) .'" onclick="return confirmDelete();" class="edit btn btn-primary btn-danger">Delete</a>';
     
                            return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
        $data['page_title'] = 'Contact Enquiries';  
        $data['page_sub_title'] = 'Records';
        return view('admin.enquiries.contact',compact('data'));
    }
    public function six_sigma_enquiry(Request $request)
    {
        if ($request->ajax()) {
            $data = Enquiry::latest()->where('type',2)->get();
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                        //    $btn  ='<a href="'. route('locations.edit', [encrypt($row->location_id)]) .'" class="edit btn btn-primary btn-success">Edit</a> '; 
                        //    $btn .='<a href="'. route('contacts.view', [encrypt($row->category_id)]) .'"  class="edit btn btn-primary btn-sm">View</a> ';
                           $btn ='<a href="'. route('enquiry.upcoming.delete', [encrypt($row->enquiry_id)]) .'" onclick="return confirmDelete();" class="edit btn btn-primary btn-danger">Delete</a>';
     
                            return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
        $data['page_title'] = 'Six Sigma Course Enquiry';  
        $data['page_sub_title'] = 'Records';
        return view('admin.enquiries.six-sigma',compact('data'));
    }
    public function hrci_enquiry(Request $request)
    {
        if ($request->ajax()) {
            $data = Enquiry::latest()->where('type',3)->get();
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                        //    $btn  ='<a href="'. route('locations.edit', [encrypt($row->location_id)]) .'" class="edit btn btn-primary btn-success">Edit</a> '; 
                        //    $btn .='<a href="'. route('contacts.view', [encrypt($row->category_id)]) .'"  class="edit btn btn-primary btn-sm">View</a> ';
                           $btn ='<a href="'. route('enquiry.hrci.delete', [encrypt($row->enquiry_id)]) .'" onclick="return confirmDelete();" class="edit btn btn-primary btn-danger">Delete</a>';
     
                            return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
        $data['page_title'] = 'HRCI Enquiry';  
        $data['page_sub_title'] = 'Records';
        return view('admin.enquiries.hrci-enquiry',compact('data'));
    }
    public function brochure(Request $request)
    {
        if ($request->ajax()) {
            $data = Enquiry::latest()->where('type',4)->get();
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                        //    $btn  ='<a href="'. route('locations.edit', [encrypt($row->location_id)]) .'" class="edit btn btn-primary btn-success">Edit</a> '; 
                        //    $btn .='<a href="'. route('contacts.view', [encrypt($row->category_id)]) .'"  class="edit btn btn-primary btn-sm">View</a> ';
                           $btn ='<a href="'. route('enquiry.brochure.delete', [encrypt($row->enquiry_id)]) .'" onclick="return confirmDelete();" class="edit btn btn-primary btn-danger">Delete</a>';
     
                            return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
        $data['page_title'] = 'Brochure Request';  
        $data['page_sub_title'] = 'Records';
        return view('admin.enquiries.brochure',compact('data'));
    }
    public function calender(Request $request)
    {
        if ($request->ajax()) {
            $data = Enquiry::latest()->where('type',5)->get();
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                        //    $btn  ='<a href="'. route('locations.edit', [encrypt($row->location_id)]) .'" class="edit btn btn-primary btn-success">Edit</a> '; 
                        //    $btn .='<a href="'. route('contacts.view', [encrypt($row->category_id)]) .'"  class="edit btn btn-primary btn-sm">View</a> ';
                           $btn ='<a href="'. route('enquiry.calender.delete', [encrypt($row->enquiry_id)]) .'" onclick="return confirmDelete();" class="edit btn btn-primary btn-danger">Delete</a>';
     
                            return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
        $data['page_title'] = 'Calender Request';  
        $data['page_sub_title'] = 'Records';
        return view('admin.enquiries.calender',compact('data'));
    }
    public function partner(Request $request)
    {
        if ($request->ajax()) {
            $data = Enquiry::latest()->where('type',6)->get();
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                        //    $btn  ='<a href="'. route('locations.edit', [encrypt($row->location_id)]) .'" class="edit btn btn-primary btn-success">Edit</a> '; 
                        //    $btn .='<a href="'. route('contacts.view', [encrypt($row->category_id)]) .'"  class="edit btn btn-primary btn-sm">View</a> ';
                           $btn ='<a href="'. route('enquiry.partner.delete', [encrypt($row->enquiry_id)]) .'" onclick="return confirmDelete();" class="edit btn btn-primary btn-danger">Delete</a>';
     
                            return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
        $data['page_title'] = 'Become a Partner Request';  
        $data['page_sub_title'] = 'Records';
        return view('admin.enquiries.partner',compact('data'));
    }

    public function certificate(Request $request)
    {
        if ($request->ajax()) {
            $data = Enquiry::latest()->where('type',7)->get();
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                        //    $btn  ='<a href="'. route('locations.edit', [encrypt($row->location_id)]) .'" class="edit btn btn-primary btn-success">Edit</a> '; 
                        //    $btn .='<a href="'. route('contacts.view', [encrypt($row->category_id)]) .'"  class="edit btn btn-primary btn-sm">View</a> ';
                           $btn ='<a href="'. route('enquiry.calender.delete', [encrypt($row->enquiry_id)]) .'" onclick="return confirmDelete();" class="edit btn btn-primary btn-danger">Delete</a>';
     
                            return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
        $data['page_title'] = 'Certificate Request';  
        $data['page_sub_title'] = 'Records';
        return view('admin.enquiries.certificate',compact('data'));
    }

    public function upcoming_course_delete($enquiry_id){
        // dd($enquiry_id);
        $enquiry = Enquiry::find(decrypt($enquiry_id));   
         $enquiry->delete();
         return redirect(route('enquiry.upcoming'))->with('message','Successfully Deleted Data');
    }
    public function six_sigma_delete($enquiry_id){
        // dd($enquiry_id);
        $enquiry = Enquiry::find(decrypt($enquiry_id));   
            $enquiry->delete();
            return redirect(route('enquiry.sixsigma'))->with('message','Successfully Deleted Data');
    }
    public function hrci_delete($enquiry_id){
        // dd($enquiry_id);
        $enquiry = Enquiry::find(decrypt($enquiry_id));   
            $enquiry->delete();
            return redirect(route('enquiry.hrci'))->with('message','Successfully Deleted Data');
    }
    public function brochure_delete($enquiry_id){
        // dd($enquiry_id);
        $enquiry = Enquiry::find(decrypt($enquiry_id));   
            $enquiry->delete();
            return redirect(route('enquiry.brochure'))->with('message','Successfully Deleted Data');
    }
    public function calender_delete($enquiry_id){
        // dd($enquiry_id);
        $enquiry = Enquiry::find(decrypt($enquiry_id));   
            $enquiry->delete();
            return redirect(route('enquiry.calender'))->with('message','Successfully Deleted Data');
    }
    public function quick_enquiry_delete($enquiry_id){
        // dd($enquiry_id);
        $enquiry = Enquiry::find(decrypt($enquiry_id));   
            $enquiry->delete();
            return redirect(route('enquiry.quick'))->with('message','Successfully Deleted Data');
    }
    public function contact_enquiry_delete($enquiry_id){
        // dd($enquiry_id);
        $enquiry = Enquiry::find(decrypt($enquiry_id));   
            $enquiry->delete();
            return redirect(route('enquiry.contact'))->with('message','Successfully Deleted Data');
    }
    public function partner_enquiry_delete($enquiry_id){
        // dd($enquiry_id);
        $enquiry = Enquiry::find(decrypt($enquiry_id));   
            $enquiry->delete();
            return redirect(route('enquiry.partner'))->with('message','Successfully Deleted Data');
    }
    public function certificate_enquiry_delete($enquiry_id){
        // dd($enquiry_id);
        $enquiry = Enquiry::find(decrypt($enquiry_id));   
            $enquiry->delete();
            return redirect(route('enquiry.certificate'))->with('message','Successfully Deleted Data');
    }
    

    
}
