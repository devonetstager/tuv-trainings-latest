@extends('admin.main')
@section('title','Edit-Page')
@section('content')

<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
    {{$data['page_title']}}
      <small>{{$data['page_sub_title']}}</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Form</a></li>
    </ol>
  </section>

<section class="content">
        @if(Session::has('message'))
           <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-check"></i> Alert!</h4>
                      {{ Session::get('message') }}
          </div>
        @endif
        @if(Session::has('err_message'))
      <div class="alert alert-danger alert-dismissible">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                      <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                      {{ Session::get('err_message') }}
      </div>
    @endif
        <!-- Default box -->
        <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title">Edit Category</h3>
  
            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                      title="Collapse">
                <i class="fa fa-minus"></i></button>
              <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                <i class="fa fa-times"></i></button>
            </div>
          </div>
          <div class="box-body">
          <div class="row">
        <div class="col-xs-12">
          <div class="box">
         
              <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Form</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form action="{{route('cms.update')}}" method="post" enctype="multipart/form-data">
               @csrf
               <div class="box-body">
                <div class="form-group col-md-6">
                  <label for="exampleInputEmail1">Page Name</label>
                  <input type="test" name="name" class="form-control" value="{{$cmspage->name}}" required="required"  placeholder="Name">
                  @if ($errors->has('name'))
                  <div class="alert alert-danger">
                      <ul>                      
                          <li>{{ $errors->first('name') }}</li>                              
                      </ul>
                  </div>
                  @endif
                </div>
                <div class="form-group col-md-6">
                  <label for="exampleInputEmail1">Page Title</label>
                  <input type="test" name="title" class="form-control" value="{{$cmspage->title}}" required="required"  placeholder="Title">
                  @if ($errors->has('title'))
                  <div class="alert alert-danger">
                      <ul>                      
                          <li>{{ $errors->first('title') }}</li>                              
                      </ul>
                  </div>
                  @endif
                </div>
                <div class="form-group col-md-6">
                  <label for="exampleInputEmail1">Meta Title</label>
                  <input type="test" name="metakeytitle" class="form-control" value="{{$cmspage->metakeytitle}}"  required="required"  placeholder="Meta Title">
               
                </div>  
                <div class="form-group col-md-6">
                  <label for="exampleInputEmail1">Meta Description</label>
                  <input type="test" name="metadescription" class="form-control" value="{{$cmspage->metadescription}}"  required="required"  placeholder="Meta Title">
               
                </div> 
                  <div class="form-group col-md-12">
                  <label for="exampleInputFile">Content</label>
                  <textarea id="over_view" name="content" rows="7" class="form-control ckeditor" placeholder="Write your message..">
                  {{$cmspage->content}}
                  </textarea>
                  {!! $errors->first('content', '<p style="color:red;" class="help-block">:message</p>') !!}                
                </div>    
              </div>
                                
              </div>
              <!-- /.box-body -->

              <div class="form-group col-md-6">
              <input type="hidden" name="cmspage_id" value="{{$cmspage->cmspage_id}}">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
          <!-- /.box-body -->
          <div class="box-footer">
         
          </div>
          <!-- /.box-footer-->
        </div>
        <!-- /.box -->
  
      </section>
      <!-- /.content -->
    </div>
 
  @endsection