@extends('admin.main')
@section('title','New-CMS')
@section('content')

<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
    TUV
      <small>Manage</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Add New </a></li>
    </ol>
<section class="content">
    @if(Session::has('message'))
    <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-check"></i> Alert!</h4>
                      {{ Session::get('message') }}
      </div>
    @endif
        <!-- Default box -->
        <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title">New Page</h3>
  
            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                      title="Collapse">
                <i class="fa fa-minus"></i></button>
              <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                <i class="fa fa-times"></i></button>
            </div>
          </div>
          <div class="box-body">
          <div class="row">
        <div class="col-xs-12">
          <div class="box">
         
              <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">CMS Form</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form action="{{route('cms.save')}}" method="post" enctype="multipart/form-data">
          @csrf
              <div class="box-body">
                <div class="form-group col-md-6">
                  <label for="exampleInputEmail1">Page Name</label>
                  <input type="test" name="name" class="form-control"  required="required"  placeholder="Name">
                  @if ($errors->has('name'))
                  <div class="alert alert-danger">
                      <ul>                      
                          <li>{{ $errors->first('name') }}</li>                              
                      </ul>
                  </div>
                  @endif
                </div>
                <div class="form-group col-md-6">
                  <label for="exampleInputEmail1">Page Title</label>
                  <input type="test" name="title" class="form-control"  required="required"  placeholder="Title">
                  @if ($errors->has('title'))
                  <div class="alert alert-danger">
                      <ul>                      
                          <li>{{ $errors->first('title') }}</li>                              
                      </ul>
                  </div>
                  @endif
                </div>
                <div class="form-group col-md-6">
                  <label for="exampleInputEmail1">Meta Title</label>
                  <input type="test" name="metatitle" class="form-control"  required="required"  placeholder="Meta Key Title">
                  @if ($errors->has('metakeytitle'))
                  <div class="alert alert-danger">
                      <ul>                      
                          <li>{{ $errors->first('metakeytitle') }}</li>                              
                      </ul>
                  </div>
                  @endif
                </div>  
                  <div class="form-group col-md-12">
                  <label for="exampleInputFile">Content</label>
                  <textarea id="over_view" name="content" rows="7" class="form-control ckeditor" placeholder="Write your message.."></textarea>
                  {!! $errors->first('content', '<p style="color:red;" class="help-block">:message</p>') !!}                
                </div>    
              </div>
                                
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
          <!-- /.box-body -->
          <div class="box-footer">
     
          </div>
          <!-- /.box-footer-->
        </div>
        <!-- /.box -->
  
      </section>
      <!-- /.content -->
    </div>
 
  @endsection