@extends('admin.main')
@section('title','View-Categories')
@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
    {{$data['page_title']}}
    <small>{{$data['page_sub_title']}}</small>    </h1>
    <ol class="breadcrumb">
      <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Category View</a></li>
    </ol>
  </section>
<section class="content">
    @if(Session::has('message'))
      <div class="alert alert-danger alert-dismissible">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                      <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                      {{ Session::get('message') }}
      </div>
    @endif

<!-- Default box -->
<div class="box">
  <div class="box-header with-border">



    <h3 class="box-title">Category View</h3>

    <div class="box-tools pull-right">
      <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
              title="Collapse">
        <i class="fa fa-minus"></i></button>
      <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
        <i class="fa fa-times"></i></button>
    </div>
  </div>
  <div class="box-body">
    <h2>Name:<b>{{$category->name}}</b></h2>
    <h2></h2>
    <span id="photo_view"> 
         <img style="width: 25%" src="/tuv-nord-trainings/storage/app/categories/{{$category->image}}"  class="img-responsive">
    </span>
    <h3>Created_By:{{$category->user->name}}</h3>
    <h3>Updated_By:{{$category->user->name}}</h3>
    <h3>Created_On:{{$category->created_at}}</h3>
    <h3>Updated_On:{{$category->updated_at}}</h3>
    @if($category->status==1)
    <h3>Status:Yes</h3>
    @else
    <h3>Status:No</h3>
    @endif
    
  </div>
  <!-- /.box-body -->
  <div class="box-footer">
    Footer
  </div>
  <!-- /.box-footer-->
</div>
<!-- /.box -->

</section>
      <!-- /.content -->
    </div>
 
  @endsection