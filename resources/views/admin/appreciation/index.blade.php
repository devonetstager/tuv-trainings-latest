@extends('admin.main')
@section('title','Appreciation Letter')
@section('content')

<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
    {{$data['page_title']}}
      <small>{{$data['page_sub_title']}}</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Appreciation Letter</a></li>
    </ol>
  </section>

<section class="content">
@if(Session::has('message'))
           <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-check"></i> Alert!</h4>
                      {{ Session::get('message') }}
          </div>
      @endif
        <!-- Default box -->
        <div class="box">
          <div class="box-header with-border">
          <h3 class="box-title"> Appreciation Letter
          </h3>
  
            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                      title="Collapse">
                <i class="fa fa-minus"></i></button>
              <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                <i class="fa fa-times"></i></button>
            </div>
          </div>
          <div class="box-body">
          <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">             

              <div class="box-tools">
                <div class="input-group input-group-sm hidden-xs" style="width: 150px;">
                 
                  <div class="input-group-btn">
                    <a href="{{route('appeciation.letter.new')}}" class="btn btn-success" >Add New</a>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
            <table class="table table-bordered data-table">
              <thead>
                  <tr>
                      <th style="width:5%">No</th>
                      <th>Link</th>             
                      <th width="20%">Action</th>
                  </tr>
              </thead>
              <tbody>
                @foreach($letters as $letter)
                <tr role="row" class="odd">
                  <td class="sorting_1">{{ $loop->iteration }}</td>
                  <td>{{$letter->content}}</td>
                  <td>
                    <a href="{{route('appeciation.letter.edit',encrypt($letter->appreciation_id))}}" class="edit btn btn-primary btn-success">Edit</a> 
                    <a href="{{route('appeciation.letter.delete',encrypt($letter->appreciation_id))}}" onclick="return confirmDelete();" class="edit btn btn-primary btn-danger">Delete</a>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
          </div>
          <!-- /.box-body -->
          <div class="box-footer">
       
          </div>
          <!-- /.box-footer-->
        </div>
        <!-- /.box -->
  
      </section>
      <!-- /.content -->
    </div>
    @push('scripts')

            <script>
    
    function confirmDelete()
    {
      var r=confirm("Are you sure you want to delete");
      if (r==true)
      {
        return true;
      }else{
        return false;
      
      }
    }
        </script>
    @endpush
  @endsection