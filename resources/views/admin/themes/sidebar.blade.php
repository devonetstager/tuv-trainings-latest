<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="{{ asset('public/admin/dist/img/user2-160x160.jpg') }}" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>{{ Auth::user()->name }}</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
     
          </a>
        </li>
        <li class="treeview {{ (Request::segment(2) == 'courses' ) ? 'active' : '' }}">
          <a href="#">
            <i class="fa fa-graduation-cap"></i> <span>Manage Courses</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
         <ul class="treeview-menu">
            <li class="{{ (Request::segment(3) == 'category' ) ? 'active' : '' }}"><a href="{{route('admin.category')}}"><i class="fa fa-circle-o"></i> Categories</a></li>          
            <li class="{{ (Request::segment(3) == 'courses' ) ? 'active' : '' }}"><a href="{{route('admin.courses')}}"><i class="fa fa-circle-o"></i> Courses</a></li>
            <li class="{{ (Request::segment(3) == 'coursedetails' ) ? 'active' : '' }}"><a href="{{route('admin.coursedetails')}}"><i class="fa fa-circle-o"></i> Course Details</a></li>
            <li class="{{ (Request::segment(3) == 'six-sigma-courses' ) ? 'active' : '' }}"><a href="{{route('sigma.index')}}"><i class="fa fa-circle-o"></i> Six Sigma</a></li>
            <li class="{{ (Request::segment(3) == 'hrci-courses' ) ? 'active' : '' }}"><a href="{{route('hrci.index')}}"><i class="fa fa-circle-o"></i> HRCI Manage</a></li>
            <li class="{{ (Request::segment(3) == 'cips-certification' ) ? 'active' : '' }}"><a href="{{route('cips.index')}}"><i class="fa fa-circle-o"></i> CIPS Manage</a></li>
          </ul>
        </li>
        <li class="treeview {{ (Request::segment(2) == 'manage' ) ? 'active' : '' }}">
          <a href="#">
            <i class="fa fa-sitemap"></i> <span>Manage</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="{{ (Request::segment(3) == 'cms-pages' ) ? 'active' : '' }}"><a href="{{route('cms.index')}}"><i class="fa fa-circle-o text-aqua"></i> CMS Manage</a></li>
            <li class="{{ (Request::segment(3) == 'cities' ) ? 'active' : '' }}"><a href="{{route('city.index')}}"><i class="fa fa-circle-o  text-aqua"></i> City</a></li>
            <li class="{{ (Request::segment(3) == 'currency' ) ? 'active' : '' }}"><a href="{{route('currency.index')}}"><i class="fa fa-circle-o  text-aqua"></i> Currency</a></li>
            <li class="{{ (Request::segment(3) == 'countries' ) ? 'active' : '' }}"><a href="{{route('country.index')}}"><i class="fa fa-circle-o  text-aqua"></i> Country</a></li>  
            <li class="{{ (Request::segment(3) == 'Clientele-index' ) ? 'active' : '' }}"><a href="{{route('clientele.index')}}"><i class="fa fa-circle-o text-aqua"></i> Clientele</a></li>
              <li class="{{ (Request::segment(3) == 'faq-index' ) ? 'active' : '' }}"><a href="{{route('faq.index')}}"><i class="fa fa-circle-o text-aqua"></i> FAQ</a></li>   
            <li class="{{ (Request::segment(3) == 'galleries' ) ? 'active' : '' }}"><a href="{{route('gallery.index')}}"><i class="fa fa-circle-o text-aqua"></i>  Gallery</a></li>
            <li class="{{ (Request::segment(3) == 'gallery-details' ) ? 'active' : '' }}"><a href="{{route('gallerydetail.index')}}"><i class="fa fa-circle-o text-aqua"></i>  Gallery Detail</a></li>
            <li class="{{ (Request::segment(3) == 'home-banner' ) ? 'active' : '' }}"><a href="{{route('home-banner')}}"><i class="fa fa-circle-o text-aqua"></i> Home Banner</a></li>
            <li class="{{ (Request::segment(3) == 'locations' ) ? 'active' : '' }}"><a href="{{route('locations')}}"><i class="fa fa-circle-o text-aqua"></i> Locations</a></li>
            <li class="{{ (Request::segment(3) == 'news' ) ? 'active' : '' }}"><a href="{{route('news.index')}}"><i class="fa fa-circle-o text-aqua"></i> News</a></li>
            <li class="{{ (Request::segment(3) == 'testimonial' ) ? 'active' : '' }}"><a href="{{route('testimonial')}}"><i class="fa fa-circle-o text-aqua"></i> Testimonial</a></li>
            <li class="{{ (Request::segment(3) == 'qualification' ) ? 'active' : '' }}"><a href="{{route('qualification.index')}}"><i class="fa fa-circle-o text-aqua"></i> Qualifications</a></li>
            <li class="{{ (Request::segment(3) == 'press-release-index' ) ? 'active' : '' }}"><a href="{{route('pressrelease.index')}}"><i class="fa fa-circle-o text-aqua"></i> Press Release</a></li>
            <li class="{{ (Request::segment(3) == 'appeciation-letter-index' ) ? 'active' : '' }}"><a href="{{route('appeciation.letter.index')}}"><i class="fa fa-circle-o text-aqua"></i> Appreciation Letter</a></li>
            <li class="{{ (Request::segment(3) == 'Webinar-index' ) ? 'active' : '' }}"><a href="{{route('webinar.index')}}"><i class="fa fa-circle-o text-aqua"></i> Webinar</a></li>
          </ul>
        </li>
       <li class="treeview {{ (Request::segment(2) == 'enquiry' ) ? 'active' : '' }} ">
          <a href="#">
            <i class="fa fa-share"></i> <span>Enquiries</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            {{-- <li><a href="{{route('enquiry.list')}}"><i class="fa fa-circle-o text-aqua"></i>  Enquiry</a></li> --}}
            <li class="{{ (Request::segment(3) == 'quick-enquiries' ) ? 'active' : '' }}"><a href="{{route('enquiry.quick')}}"><i class="fa fa-circle-o text-aqua"></i> Quick Enquiry</a></li>
            <li class="{{ (Request::segment(3) == 'contact-enquiries' ) ? 'active' : '' }}"><a href="{{route('enquiry.contact')}}"><i class="fa fa-circle-o text-aqua"></i> Contact Enquiry</a></li>
            <li class="{{ (Request::segment(3) == 'offers' ) ? 'active' : '' }}"><a href="{{route('enquiry.offers')}}"><i class="fa fa-circle-o text-aqua"></i> Offer Enquiry</a></li>
            <li class="{{ (Request::segment(3) == 'upcoming-course-enquiry' ) ? 'active' : '' }}"><a href="{{route('enquiry.upcoming')}}"><i class="fa fa-circle-o text-aqua"></i> Course Enquiry</a></li>
            <li class="{{ (Request::segment(3) == 'six-sigma-enquiry' ) ? 'active' : '' }}"><a href="{{route('enquiry.sixsigma')}}"><i class="fa fa-circle-o text-aqua"></i> Six sigma Enquiry</a></li>
            <li class="{{ (Request::segment(3) == 'hrci-enquiry' ) ? 'active' : '' }}"><a href="{{route('enquiry.hrci')}}"><i class="fa fa-circle-o text-aqua"></i> HRCI Enquiry</a></li>
            <li class="{{ (Request::segment(3) == 'cips-enquiry' ) ? 'active' : '' }}"><a href="{{route('enquiry.cips')}}"><i class="fa fa-circle-o text-aqua"></i> CIPS Enquiry</a></li>
            <li class="{{ (Request::segment(3) == 'brochure-request' ) ? 'active' : '' }}"><a href="{{route('enquiry.brochure')}}"><i class="fa fa-circle-o text-aqua"></i> Brochure Request</a></li>
            <li class="{{ (Request::segment(3) == 'calender-request' ) ? 'active' : '' }}"><a href="{{route('enquiry.calender')}}"><i class="fa fa-circle-o text-aqua"></i> Calender Request</a></li>
             <li class="{{ (Request::segment(3) == 'become-partner' ) ? 'active' : '' }}"><a href="{{route('enquiry.partner')}}"><i class="fa fa-circle-o text-aqua"></i> Become a Partner</a></li> 
            <li class="{{ (Request::segment(3) == 'certificate-card-verification' ) ? 'active' : '' }}"><a href="{{route('enquiry.certificate')}}"><i class="fa fa-circle-o text-aqua"></i> Card/Certificate</a></li>  
          </ul>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>