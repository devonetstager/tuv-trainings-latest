@include('admin.themes.header')
@include('admin.themes.sidebar')

    @yield('content')
    
@include('admin.themes.footer')
