<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('locations', function (Blueprint $table) {
            $table->bigIncrements('location_id');
            $table->string('name');
            $table->string('building');
            $table->string('addressline1')->nullable();
            $table->string('addressline2')->nullable();
            $table->string('state')->nullable();
            $table->string('country');            
            $table->string('pincode');
            $table->string('phone');
            $table->string('fax')->nullable();
            $table->string('email');
            $table->string('latitude');
            $table->string('longitude');
            $table->string('created_by');
            $table->string('updated_by');
            $table->boolean('status')->default(1)->comment('1:Active,0:Inactive');
            $table->boolean('show_home')->default(1)->comment('1:Yes,0:No');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('locations');
    }
}
