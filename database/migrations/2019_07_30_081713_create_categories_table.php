<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->bigIncrements('category_id');
            $table->string('name');
            $table->unsignedInteger('master_id')->default(0);
            $table->string('icon');
            $table->string('image');
            $table->string('created_by');
            $table->string('updated_by');
            $table->boolean('status')->default(1)->comment('1:Active,0:Inactive');
            $table->boolean('show_home')->default(1)->comment('1:Yes,0:No');
            $table->timestamps();
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
