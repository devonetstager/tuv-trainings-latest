<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSixSigmaCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('six_sigma_courses', function (Blueprint $table) {
            $table->bigIncrements('six_sigma_id');
            $table->string('name');
            $table->string('course_image');
            $table->text('description');
            $table->double('fee');
            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->boolean('status')->default(1)->comment('1:Active,0:Inactive');
            $table->boolean('show_home')->default(1)->comment('1:Yes,0:No');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('six_sigma_courses');
    }
}
