<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCourseTableAddSeodetailsToCourses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('courses', function (Blueprint $table) {
            $table->string('meta_title')->nullable()->after('outline');
            $table->string('meta_tags')->nullable()->after('outline');
            $table->string('meta_description')->nullable()->after('outline');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('enquiries', function (Blueprint $table) {
            $table->dropColumn('meta_title');
            $table->dropColumn('meta_tags');
            $table->dropColumn('meta_description');
        });
    }
}
