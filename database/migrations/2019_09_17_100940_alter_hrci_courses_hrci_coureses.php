<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterHrciCoursesHrciCoureses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('hrci_courses', function (Blueprint $table) {
            $table->unsignedBigInteger('city_id')->nullable()->after('fee');
            $table->unsignedBigInteger('venue_id')->nullable()->after('fee');
            $table->unsignedBigInteger('language_id')->nullable()->after('fee');
            $table->string('location')->nullable()->after('fee');
            $table->unsignedInteger('currency_id')->nullable()->after('fee');
            $table->date('start_date')->nullable()->after('fee');
            $table->date('end_date')->nullable()->after('fee');
            $table->string('brochure')->nullable()->after('fee');
            $table->string('youtube_link')->nullable()->after('fee');
            $table->string('email')->nullable()->after('fee');
            $table->string('contact_number')->nullable()->after('fee');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
