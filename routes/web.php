<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes(['/password/reset'=>false]);
// Route::get('/clear-cache', function() {
//     $exitCode = Artisan::call('config:clear');
//     $exitCode = Artisan::call('cache:clear');
//     $exitCode = Artisan::call('config:cache');
//     return 'DONE'; //Return anything
// });
Route::group(['prefix' => 'admin','middleware' => ['auth']], function() {
Route::get('/manage', 'HomeController@index')->name('home');


Route::group(['prefix' => 'enquiry','as' => 'enquiry.'], function() {
      Route::get('enquiry', 'Admin\EnquiryController@list')->name('list');
    Route::get('quick-enquiry', 'Admin\EnquiryController@list')->name('quick.enquiry');
    Route::get('quick-enquiries', 'Admin\EnquiryController@quick_enquiries')->name('quick');
    Route::get('quick-enquiries-view/{id}', 'Admin\EnquiryController@quick_enquiries_view')->name('quick.view');
    Route::get('quick-enquiry-delete/{id}', 'Admin\EnquiryController@quick_enquiry_delete')->name('quick.delete');
    Route::get('upcoming-course-enquiry', 'Admin\EnquiryController@upcoming_course_enquiry')->name('upcoming');
    Route::get('upcoming-course-delete/{id}', 'Admin\EnquiryController@upcoming_course_delete')->name('upcoming.delete');
    Route::get('upcoming-enquiries-view/{id}', 'Admin\EnquiryController@upcoming_course_view')->name('upcoming.view');
    Route::get('six-sigma-enquiry', 'Admin\EnquiryController@six_sigma_enquiry')->name('sixsigma');
    Route::get('six-signa-enquiries-view/{id}', 'Admin\EnquiryController@sigma_enquiry_view')->name('sigma.view');
    Route::get('six-sigma-delete/{id}', 'Admin\EnquiryController@six_sigma_delete')->name('upcoming.delete');
    Route::get('hrci-enquiry', 'Admin\EnquiryController@hrci_enquiry')->name('hrci');
    Route::get('hrci-enquiries-view/{id}', 'Admin\EnquiryController@hrci_enquiry_view')->name('hrci.view');
    Route::get('hrci-enquiry-delete/{id}', 'Admin\EnquiryController@hrci_delete')->name('hrci.delete');
    Route::get('cips-enquiry', 'Admin\EnquiryController@cips_enquiry')->name('cips');
    Route::get('cips-enquiries-view/{id}', 'Admin\EnquiryController@cips_enquiry_view')->name('cips.view');
    Route::get('cips-enquiry-delete/{id}', 'Admin\EnquiryController@cips_delete')->name('cips.delete');
    Route::get('brochure-request', 'Admin\EnquiryController@brochure')->name('brochure');
    Route::get('brochure-enquiries-view/{id}', 'Admin\EnquiryController@brochure_enquiry_view')->name('brochure.view');
    Route::get('brochure-delete/{id}', 'Admin\EnquiryController@brochure_delete')->name('brochure.delete');
    Route::get('calender-request', 'Admin\EnquiryController@calender')->name('calender');
    Route::get('calender-enquiries-view/{id}', 'Admin\EnquiryController@calender_enquiry_view')->name('calender.view');
    Route::get('calender-delete/{id}', 'Admin\EnquiryController@calender_delete')->name('calender.delete');
    Route::get('become-partner', 'Admin\EnquiryController@partner')->name('partner');
    Route::get('partner-enquiries-view/{id}', 'Admin\EnquiryController@partner_enquiry_view')->name('partner.view');
    Route::get('become-partner-delete/{id}', 'Admin\EnquiryController@partner_enquiry_delete')->name('partner.delete');
    Route::get('certificate-card-verification', 'Admin\EnquiryController@certificate')->name('certificate');
    Route::get('certificate-enquiries-view/{id}', 'Admin\EnquiryController@certificate_enquiry_view')->name('certificate.view');
    Route::get('certificate-card-verification-delete/{id}', 'Admin\EnquiryController@certificate_enquiry_delete')->name('certificate.delete');
    Route::get('contact-enquiries', 'Admin\EnquiryController@contact_enquiry')->name('contact');
    Route::get('contact-enquiries-view/{id}', 'Admin\EnquiryController@contact_enquiry_view')->name('contact.view');
    Route::get('contact-enquiries-delete/{id}', 'Admin\EnquiryController@contact_enquiry_delete')->name('contact.delete');

    Route::get('offers', 'Admin\EnquiryController@offers')->name('offers');
    Route::get('offers-enquiries-view/{id}', 'Admin\EnquiryController@offers_enquiry_view')->name('offers.view');
    Route::get('offers-delete/{id}', 'Admin\EnquiryController@offers_enquiry_delete')->name('offers.delete');
});
Route::group(['prefix' => 'courses'], function() {
Route::get('category', 'Admin\CategoryController@index')->name('admin.category');
Route::get('category-new', 'Admin\CategoryController@new')->name('category.new');
Route::post('category-save', 'Admin\CategoryController@save')->name('category.save');
Route::get('category-edit/{id}', 'Admin\CategoryController@edit')->name('category.edit');
Route::post('category-update','Admin\CategoryController@update')->name('category.update');
Route::get('category-delete/{id}', 'Admin\CategoryController@delete')->name('category.delete');
Route::get('category-view/{id}', 'Admin\CategoryController@view')->name('category.view');

Route::get('courses', 'Admin\CourseController@index')->name('admin.courses');
Route::get('courses-new', 'Admin\CourseController@new')->name('courses.new');
Route::post('courses-save', 'Admin\CourseController@save')->name('courses.save');
Route::get('courses-edit/{id}', 'Admin\CourseController@edit')->name('courses.edit');
Route::post('courses-update','Admin\CourseController@update')->name('courses.update');
Route::get('courses-delete/{id}', 'Admin\CourseController@delete')->name('courses.delete');
Route::get('courses-view/{id}', 'Admin\CourseController@view')->name('courses.view');

Route::get('coursedetails', 'Admin\CourseDetailsController@index')->name('admin.coursedetails');
Route::get('coursedetails-new', 'Admin\CourseDetailsController@new')->name('coursedetails.new');
Route::post('coursedetails-save', 'Admin\CourseDetailsController@save')->name('coursedetails.save');
Route::get('coursedetails-edit/{id}', 'Admin\CourseDetailsController@edit')->name('coursedetails.edit');
Route::post('coursedetails-update','Admin\CourseDetailsController@update')->name('coursedetails.update');
Route::get('coursedetails-delete/{id}', 'Admin\CourseDetailsController@delete')->name('coursedetails.delete');
Route::get('coursedetails-view/{id}', 'Admin\CourseDetailsController@view')->name('coursedetails.view');



Route::get('six-sigma-courses', 'Admin\SixSigmaCourseController@index')->name('sigma.index');
Route::get('sigma-new', 'Admin\SixSigmaCourseController@new')->name('sigma.new');
Route::post('sigma-save', 'Admin\SixSigmaCourseController@save')->name('sigma.save');
Route::get('sigma-edit/{id}', 'Admin\SixSigmaCourseController@edit')->name('sigma.edit');
Route::post('sigma-update','Admin\SixSigmaCourseController@update')->name('sigma.update');
Route::get('sigma-delete/{id}', 'Admin\SixSigmaCourseController@delete')->name('sigma.delete');
Route::get('sigma-view/{id}', 'Admin\SixSigmaCourseController@view')->name('sigma.view');

Route::get('hrci-courses', 'Admin\HrciCourseController@index')->name('hrci.index');
Route::get('hrci-new', 'Admin\HrciCourseController@new')->name('hrci.new');
Route::post('hrci-save', 'Admin\HrciCourseController@save')->name('hrci.save');
Route::get('hrci-edit/{id}', 'Admin\HrciCourseController@edit')->name('hrci.edit');
Route::post('hrci-update','Admin\HrciCourseController@update')->name('hrci.update');
Route::get('hrci-delete/{id}', 'Admin\HrciCourseController@delete')->name('hrci.delete');
Route::get('hrci-view/{id}', 'Admin\HrciCourseController@view')->name('hrci.view');

Route::get('cips-certification', 'Admin\CipsCourseController@index')->name('cips.index');
Route::get('cips-new', 'Admin\CipsCourseController@new')->name('cips.new');
Route::post('cips-save', 'Admin\CipsCourseController@save')->name('cips.save');
Route::get('cips-edit/{id}', 'Admin\CipsCourseController@edit')->name('cips.edit');
Route::post('cips-update','Admin\CipsCourseController@update')->name('cips.update');
Route::get('cips-delete/{id}', 'Admin\CipsCourseController@delete')->name('cips.delete');
Route::get('cips-view/{id}', 'Admin\CipsCourseController@view')->name('cips.view');
 });

Route::group(['prefix' => 'manage'], function() {
Route::get('cms-pages', 'Admin\CmsPageController@index')->name('cms.index');
Route::get('cms-new', 'Admin\CmsPageController@new')->name('cms.new');
Route::post('cms-save', 'Admin\CmsPageController@save')->name('cms.save');
Route::get('cms-edit/{id}', 'Admin\CmsPageController@edit')->name('cms.edit');
Route::post('cms-update','Admin\CmsPageController@update')->name('cms.update');
Route::get('cms-delete/{id}', 'Admin\CmsPageController@delete')->name('cms.delete');
Route::get('cms-view/{id}', 'Admin\CmsPageController@view')->name('cms.view');

Route::get('countries', 'Admin\CountryController@index')->name('country.index');
Route::get('country-new', 'Admin\CountryController@new')->name('country.new');
Route::post('country-save', 'Admin\CountryController@save')->name('country.save');
Route::get('country-edit/{id}', 'Admin\CountryController@edit')->name('country.edit');
Route::post('country-update','Admin\CountryController@update')->name('country.update');
Route::get('country-delete/{id}', 'Admin\CountryController@delete')->name('country.delete');
Route::get('country-view/{id}', 'Admin\CountryController@view')->name('country.view');

Route::get('cities', 'Admin\CityController@index')->name('city.index');
Route::get('city-new', 'Admin\CityController@new')->name('city.new');
Route::post('city-save', 'Admin\CityController@save')->name('city.save');
Route::get('city-edit/{id}', 'Admin\CityController@edit')->name('city.edit');
Route::post('city-update','Admin\CityController@update')->name('city.update');
Route::get('city-delete/{id}', 'Admin\CityController@delete')->name('city.delete');
Route::get('city-view/{id}', 'Admin\CityController@view')->name('city.view');

Route::get('currency', 'Admin\CurrencyController@index')->name('currency.index');
Route::get('currency-new', 'Admin\CurrencyController@new')->name('currency.new');
Route::post('currency-save', 'Admin\CurrencyController@save')->name('currency.save');
Route::get('currency-edit/{id}', 'Admin\CurrencyController@edit')->name('currency.edit');
Route::post('currency-update','Admin\CurrencyController@update')->name('currency.update');
Route::get('currency-delete/{id}', 'Admin\CurrencyController@delete')->name('currency.delete');

Route::get('contacts', 'Admin\ContactController@index')->name('contacts.index');
Route::get('contacts-new', 'Admin\ContactController@new')->name('contacts.new');
Route::post('contacts-save', 'Admin\ContactController@save')->name('contacts.save');
Route::get('contacts-edit/{id}', 'Admin\ContactController@edit')->name('contacts.edit');
Route::post('contacts-update','Admin\ContactController@update')->name('contacts.update');
Route::get('contacts-delete/{id}', 'Admin\ContactController@delete')->name('contacts.delete');
Route::get('contacts-view/{id}', 'Admin\ContactController@view')->name('contacts.view');

Route::get('locations', 'Admin\LocationController@index')->name('locations');
Route::get('locations-new', 'Admin\LocationController@new')->name('locations.new');
Route::post('locations-save', 'Admin\LocationController@save')->name('locations.save');
Route::get('locations-edit/{id}', 'Admin\LocationController@edit')->name('locations.edit');
Route::post('locations-update','Admin\LocationController@update')->name('locations.update');
Route::get('locations-delete/{id}', 'Admin\LocationController@delete')->name('locations.delete');

Route::get('home-banner', 'Admin\HomeBannerController@index')->name('home-banner');
Route::get('home-banner-new', 'Admin\HomeBannerController@new')->name('home-banner.new');
Route::post('home-banner-save', 'Admin\HomeBannerController@save')->name('home-banner.save');
Route::get('home-banner-edit/{id}', 'Admin\HomeBannerController@edit')->name('home-banner.edit');
Route::post('home-banner-update','Admin\HomeBannerController@update')->name('home-banner.update');
Route::get('home-banner-delete/{id}', 'Admin\HomeBannerController@delete')->name('home-banner.delete');

Route::get('testimonial', 'Admin\TestimonialController@index')->name('testimonial');
Route::get('testimonial-new', 'Admin\TestimonialController@new')->name('testimonial.new');
Route::post('testimonial-save', 'Admin\TestimonialController@save')->name('testimonial.save');
Route::get('testimonial-edit/{id}', 'Admin\TestimonialController@edit')->name('testimonial.edit');
Route::post('testimonial-update','Admin\TestimonialController@update')->name('testimonial.update');
Route::get('testimonial-delete/{id}', 'Admin\TestimonialController@delete')->name('testimonial.delete');

Route::get('qualification', 'Admin\QualificationController@index')->name('qualification.index');
Route::get('qualification-new', 'Admin\QualificationController@new')->name('qualification.new');
Route::post('qualification-save', 'Admin\QualificationController@save')->name('qualification.save');
Route::get('qualification-edit/{id}', 'Admin\QualificationController@edit')->name('qualification.edit');
Route::post('qualification-update','Admin\QualificationController@update')->name('qualification.update');
Route::get('qualification-delete/{id}', 'Admin\QualificationController@delete')->name('qualification.delete');
Route::get('qualification-view/{id}', 'Admin\QualificationController@view')->name('qualification.view');

Route::get('galleries', 'Admin\GalleryController@index')->name('gallery.index');
Route::get('gallery-new', 'Admin\GalleryController@new')->name('gallery.new');
Route::post('gallery-save', 'Admin\GalleryController@save')->name('gallery.save');
Route::get('gallery-edit/{id}', 'Admin\GalleryController@edit')->name('gallery.edit');
Route::post('gallery-update','Admin\GalleryController@update')->name('gallery.update');
Route::get('gallery-delete/{id}', 'Admin\GalleryController@delete')->name('gallery.delete');
Route::get('gallery-view/{id}', 'Admin\GalleryController@view')->name('gallery.view');

Route::get('gallery-details', 'Admin\GalleryDetailsController@index')->name('gallerydetail.index');
Route::get('gallerydetail-new', 'Admin\GalleryDetailsController@new')->name('gallerydetail.new');
Route::post('gallerydetail-save', 'Admin\GalleryDetailsController@save')->name('gallerydetail.save');
Route::get('gallerydetail-edit/{id}', 'Admin\GalleryDetailsController@edit')->name('gallerydetail.edit');
Route::post('gallerydetail-update','Admin\GalleryDetailsController@update')->name('gallerydetail.update');
Route::get('gallerydetail-delete/{id}', 'Admin\GalleryDetailsController@delete')->name('gallerydetail.delete');

Route::get('faq-index', 'Admin\FAQController@index')->name('faq.index');
Route::get('faq-new', 'Admin\FAQController@new')->name('faq.new');
Route::post('faq-save', 'Admin\FAQController@save')->name('faq.save');
Route::get('faq-edit/{id}', 'Admin\FAQController@edit')->name('faq.edit');
Route::post('faq-update','Admin\FAQController@update')->name('faq.update');
Route::get('faq-delete/{id}', 'Admin\FAQController@delete')->name('faq.delete');

Route::get('press-release-index', 'Admin\PressReleaseController@index')->name('pressrelease.index');
Route::get('press-release-new', 'Admin\PressReleaseController@new')->name('pressrelease.new');
Route::post('press-release-save', 'Admin\PressReleaseController@save')->name('pressrelease.save');
Route::get('pressr-elease-edit/{id}', 'Admin\PressReleaseController@edit')->name('pressrelease.edit');
Route::post('press-release-update','Admin\PressReleaseController@update')->name('pressrelease.update');
Route::get('press-release-delete/{id}', 'Admin\PressReleaseController@delete')->name('pressrelease.delete');

Route::get('appeciation-letter-index', 'Admin\AppreciationLetterController@index')->name('appeciation.letter.index');
Route::get('appeciation-letter-new', 'Admin\AppreciationLetterController@new')->name('appeciation.letter.new');
Route::post('appeciation-letter-save', 'Admin\AppreciationLetterController@save')->name('appeciation.letter.save');
Route::get('appeciation-letter-edit/{id}', 'Admin\AppreciationLetterController@edit')->name('appeciation.letter.edit');
route::post('appeciation-update','Admin\AppreciationLetterController@update')->name('appeciation.letter.update');
Route::get('appeciation-letter-delete/{id}', 'Admin\AppreciationLetterController@delete')->name('appeciation.letter.delete');

Route::get('Clientele-index', 'Admin\ClienteleController@index')->name('clientele.index');
Route::get('Clientele-new', 'Admin\ClienteleController@new')->name('clientele.new');
Route::post('Clientele-save', 'Admin\ClienteleController@save')->name('clientele.save');
Route::get('Clientele-edit/{id}', 'Admin\ClienteleController@edit')->name('clientele.edit');
route::post('clintale-letter-update','Admin\ClienteleController@update')->name('clientele.update');
Route::get('Clientele-delete/{id}', 'Admin\ClienteleController@delete')->name('clientele.delete');

Route::get('Webinar-index', 'Admin\WebinarController@index')->name('webinar.index');
Route::get('Webinar-new', 'Admin\WebinarController@new')->name('webinar.new');
Route::post('Webinar-save', 'Admin\WebinarController@save')->name('webinar.save');
Route::get('Webinar-edit/{id}', 'Admin\WebinarController@edit')->name('webinar.edit');
route::post('Webinar-letter-update','Admin\WebinarController@update')->name('webinar.update');
Route::get('Webinar-delete/{id}', 'Admin\WebinarController@delete')->name('webinar.delete');

Route::resource('news', 'Admin\NewsController');
Route::post('news-update','Admin\NewsController@updatenews')->name('news.updatenews');
Route::get('news-delete/{id}', 'Admin\NewsController@delete')->name('news.delete');

});



});
//Front End -----------------------------------------------------------------------------------------
Route::get('/', 'Frontend\HomeController@index')->name('index');
Route::get('sub-categories/{id}', 'Frontend\HomeController@sub_categories')->name('sub-categories');
Route::get('registration', 'Frontend\HomeController@registration')->name('registration');
Route::get('course-registration/{id}', 'Frontend\HomeController@course_registration')->name('course-registration');
Route::get('sigma-registration/{id}', 'Frontend\HomeController@sigma_registration')->name('sigma-registration');
Route::get('hrci-registration/{id}', 'Frontend\HomeController@hrci_registration')->name('hrci-registration');

Route::get('appreciation-letters', 'Frontend\HomeController@appreciation')->name('appreciation');
Route::get('press-release', 'Frontend\HomeController@press_release')->name('press-release');
Route::get('tuv-middle-east', 'Frontend\HomeController@tuv_middle_east')->name('tuv-middle-east');
Route::get('tuv-nord', 'Frontend\HomeController@tuv_nord')->name('tuv-nord');
Route::get('tuv-nord-training', 'Frontend\HomeController@tuv_nord_training')->name('tuv-nord-training');
Route::get('gallery', 'Frontend\HomeController@gallery')->name('gallery');
Route::get('gallery-details/{id}', 'Frontend\HomeController@gallery_details')->name('gallery-details');
Route::get('clientele', 'Frontend\HomeController@clientele')->name('clientele');
Route::get('faq', 'Frontend\HomeController@faq')->name('faq');
Route::get('contact-us', 'Frontend\HomeController@contact_us')->name('contact-us');
Route::post('enquiry', 'Frontend\HomeController@enquiry')->name('enquiry');
Route::get('media', 'Frontend\HomeController@media')->name('media');
Route::get('services', 'Frontend\HomeController@services')->name('services');
Route::get('blog', 'Frontend\HomeController@blob')->name('blob');
Route::get('location', 'Frontend\HomeController@location')->name('location');
Route::get('conferences', 'Frontend\HomeController@conference')->name('conferences');
Route::get('public-courses', 'Frontend\HomeController@public_courses')->name('public-courses');
Route::get('integrated-e-learning', 'Frontend\HomeController@integrated_learning')->name('integrated-e-learning');
Route::get('in-house-courses', 'Frontend\HomeController@inhouse_courses')->name('in-house-courses');

Route::get('corporate-news', 'Frontend\HomeController@corporate_news')->name('corporate-news');
Route::get('course-detail/{id}', 'Frontend\HomeController@course_detail')->name('course-detail');
Route::get('news-detail/{id}', 'Frontend\HomeController@news_detail')->name('news-detail');
Route::get('sigma-detail/{id}', 'Frontend\HomeController@sigma_course_detail')->name('sigma-detail');
Route::get('hrci-detail/{id}', 'Frontend\HomeController@hrci_course_detail')->name('hrci-detail');
Route::get('courses/{id}', 'Frontend\HomeController@courses')->name('courses');
Route::get('courselist', 'Frontend\FrontendController@courselist')->name('courselist');
Route::get('corporate-news', 'Frontend\HomeController@newslist')->name('corporate-news');
Route::get('qualifications', 'Frontend\HomeController@qualification')->name('qualification-list');
Route::get('testimonials', 'Frontend\HomeController@testimonials')->name('testimonials-list');
Route::get('partner', 'Frontend\HomeController@partner')->name('partner');
Route::get('verification', 'Frontend\HomeController@verification')->name('verification');
Route::get('privacy-policy', 'Frontend\HomeController@privacy_policy')->name('privacy.policy');
Route::get('nebosh', 'Frontend\HomeController@nebosh')->name('nebosh');
Route::get('success', 'Frontend\HomeController@success')->name('success');
Route::post('nebosh-submit', 'Frontend\HomeController@nebosh_submit')->name('nebosh.submit');
Route::post('certificate-enquiry', 'Frontend\HomeController@certificate_enquiry')->name('certificate.enquiry');
Route::get('cips-registration/{id}', 'Frontend\FrontendController@cips_registration')->name('cips-registration');
Route::get('cips-details/{id}', 'Frontend\FrontendController@cips_course_details')->name('cips-details');
Route::post('cips-register', 'Frontend\FrontendController@cips_course_registration')->name('cips.registration.complete');


//Front End Search -----------------------------------------------------------------------------------------
Route::any('course-search', 'Frontend\HomeSearchController@course_search')->name('course.search');
Route::post('search-auto-complete', 'Frontend\HomeSearchController@search_auto_complete')->name('search-auto-complete');
Route::post('registration-complete', 'Frontend\HomeController@user_course_registration')->name('registration.complete');
Route::post('sigma-register', 'Frontend\HomeController@sigma_course_registration')->name('sigma.register.complete');
Route::post('hrci-register', 'Frontend\HomeController@hrci_course_registration')->name('hrci.registration.complete');
Route::get('fetch-course', 'Frontend\HomeSearchController@fetch_scourse')->name('fetch-course');
Route::post('quick-enquiry', 'Frontend\HomeController@quick_enquiry')->name('quick-enquiry');
Route::post('contact-enquiry', 'Frontend\HomeController@contact_enquiry')->name('contact-enquiry');
Route::post('brochure-download/{id}', 'Frontend\HomeController@brochure_send')->name('brochure-download');
Route::post('calander-download', 'Frontend\HomeController@calanderDownload')->name('calander.download');
Route::post('set-vat-cookie', 'Frontend\HomeController@set_vat_cookie')->name('set-vat-cookie');
Route::get('course-filter/{id}', 'Frontend\HomeController@course_filter')->name('course.filter');
Route::get('page-not-found', 'Frontend\FrontendController@page_not_found')->name('pagenotfound');
Route::get('page', 'Frontend\FrontendController@page')->name('pagenotfound');