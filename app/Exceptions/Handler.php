<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

use Symfony\Component\Debug\ExceptionHandler as SymfonyExceptionHandler;
use App\Mail\ExceptionOccured;
use Illuminate\Auth\AuthenticationException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    // protected function unauthenticated($request, AuthenticationException $exception)
    // {
    // return $request->expectsJson()
    // ? response()->json(['message' => 'Unauthenticated.'], 401)
    // : redirect((route('admin.login.post')));
    // }
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
         if($this->isHttpException($exception))
        {   
            switch ($exception->getStatusCode()) 
                {
                   
                // not found
                case 404:
                return redirect()->route('pagenotfound');
                break;
                 case 403:
                return redirect()->route('pagenotfound');
                break;

                // internal error
                case '500':
                return redirect()->route('pagenotfound');
                break;

                default:
                    return redirect()->route('pagenotfound');
                break;
            }
        }
        else
        {
               return parent::render($request, $exception);
        }
    }
}
