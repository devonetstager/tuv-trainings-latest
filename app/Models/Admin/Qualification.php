<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Qualification extends Model
{
    protected $primaryKey = 'qualification_id';
    protected $fillable = ['name','description','image','created_by','updated_by','status','show_home'];
        public function user(){
        return $this->hasOne(\App\User::class, 'id' ,'created_by');
        }  
}
