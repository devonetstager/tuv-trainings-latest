<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Cmspage extends Model
{
    protected $primaryKey = 'cmspage_id';
    protected $fillable = ['name','status','title','metakeytitle','metakeyword',
                            'metadescription','content','created_by','updated_by'];

    public function user(){
        return $this->hasOne(\App\User::class, 'id' ,'created_by');
    }                         
}
