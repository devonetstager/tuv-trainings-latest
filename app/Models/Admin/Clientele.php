<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Clientele extends Model
{
    protected $primaryKey = 'clientele_id';
    protected $fillable = [
        'company_name','status',
    ];
}
