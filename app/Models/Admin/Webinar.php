<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Webinar extends Model
{
  protected $primaryKey = 'webinar_id';
  protected $fillable = ['title','webinar_image','webinar_link','webinar_date','status'];
  public function setWebinarDateAttribute($value){
        $this->attributes['webinar_date'] = date('Y-m-d',strtotime($value));

  }
  public function getDateWebinarAttribute()
  {
     return date("d-m-Y", strtotime($this->webinar_date));
  }
  protected $appends = ['date_webinar'];
  protected $date = ['webinar_date'];

}
