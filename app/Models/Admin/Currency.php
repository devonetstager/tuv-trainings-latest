<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    protected $primaryKey = 'currency_id';
    protected $fillable = ['name','country_id','symbol','status'];

    public function country(){
        return $this->hasOne(Country::class, 'country_id' ,'country_id');
    }   
    public function coursedetails(){
        return $this->hasMany(CourseDetail::class, 'currency_id' ,'currency_id');
    }    
}
