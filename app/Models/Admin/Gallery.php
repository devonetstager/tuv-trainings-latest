<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    protected $primaryKey = 'gallery_id';
    protected $fillable = ['title','description','main_photo','updated_by','show_home','status','created_by'];
    public function user(){
        return $this->hasOne(\App\User::class, 'id' ,'created_by');
    }   
    public function gallerydetails(){
        return $this->hasMany('App\Models\Admin\GalleryDetail','gallery_id','gallery_id');
       }
}
