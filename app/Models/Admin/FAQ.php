<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class FAQ extends Model
{
    protected $primaryKey = 'faq_id';
    protected $fillable = ['question','answer','show_home','status'];
}
