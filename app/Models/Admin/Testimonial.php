<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Testimonial extends Model
{
    protected $primaryKey = 'testimonial_id';
    protected $fillable = ['image','name','content','status','show_home']; 
}
