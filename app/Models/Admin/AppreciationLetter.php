<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class AppreciationLetter extends Model
{
    protected $primaryKey = 'appreciation_id';
    protected $fillable = [
        'content','pdf_filename','status'
    ];
}
