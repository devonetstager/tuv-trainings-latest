<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $primaryKey = 'city_id';
    protected $fillable = ['name','country_id','created_by','updated_by','show_home'];

    public function user(){
        return $this->hasOne(\App\User::class, 'id' ,'created_by');
    }
    public function country(){
        return $this->hasOne(Country::class, 'country_id' ,'country_id');
    }                         
}
