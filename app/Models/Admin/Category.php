<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $primaryKey = 'category_id';
    protected $fillable = [
        'name','master_id', 'image','icon','created_by','updated_by','status','show_home','description'
    ];
    public function user(){
        return $this->hasOne(\App\User::class, 'id' ,'created_by');
    }
    public function courses(){
    	return $this->hasMany('App\Models\Admin\Course');
    }
}
