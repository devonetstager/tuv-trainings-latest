<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class SixSigmaCourse extends Model
{
    protected $primaryKey = 'six_sigma_id';
    protected $fillable = ['name','description','fee','course_image','created_by','updated_by','status','show_home',
    'city_id','venue_id','language_id','location','youtube_link','currency_id','start_date','end_date','brochure',
    'email','contact_number'];
    public function user(){
      return $this->hasOne(\App\User::class, 'id' ,'created_by');
  }   
}
