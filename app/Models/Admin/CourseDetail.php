<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class CourseDetail extends Model
{
    protected $tableName='course_details';
   protected $primaryKey = 'detail_id';
   protected $fillable = ['course_id','location','currency_id','city_id','start_date','end_date','status','fee']; 

    public function course(){
        return $this->hasOne(Course::class, 'course_id' ,'course_id');
    }
     public function currency(){
        return $this->hasOne(Currency::class, 'currency_id' ,'currency_id');
    }
    public function city(){
        return $this->hasOne(City::class,'city_id','city_id')->withDefault([
            'name'=>''
        ]);
    }
    public function getCityNameAttribute(){
        return $this->city->name;
    }
    protected $appends = ['city_name'];
}
