<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class CIPS extends Model
{
    protected $primaryKey = 'cips_id';
    protected $fillable = ['name','cips_image','fee','location','start_date','end_date','description','youtube_link','currency_id','city_id'];
}
