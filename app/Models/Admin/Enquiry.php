<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Enquiry extends Model
{
    protected $primaryKey = 'enquiry_id';

    protected $table = 'enquiries';

    protected $fillable = [
        'name','type','mobile', 'email','address','company','post','message','document',
        'city','country','course_id','six_sigma_id','hrci_id','enquiry_url','cips_id'
    ];

    public function course(){
        return $this->hasOne(Course::Class,'course_id','course_id');
    }
    public function hrci(){
        return $this->hasOne(HrciCourse::Class,'hrci_id','hrci_id');
    }
    public function sixsigma(){
        return $this->hasOne(SixSigmaCourse::Class,'six_sigma_id','six_sigma_id');
    }

    public function getTypeTextAttribute(){
        switch ($this->type) {
            case '0':
                return "Quick Enquiry";
                break;
            case '1':
                return "Upcoming Courses";
                break;
            case '2':
                return "Six Sigma";
                break;
            case '3':
                return "HRCI";
                break;
            case '4':
                return "Brochure";
                break;
            case '5':
                return "Calendar";
                break;
            case '6':
                return "Become A Partner Enquiry";
                break;
            case '7':
                return "Certificate Enquiry";
                break;
            case '8':
                return "Contact Enquiry";
                break;
            
        }
    }

   
    public function getDateCreatedAttribute(){
        return date('d-M-Y',strtotime($this->created_at));
    }
    protected $appends = ['type_text','date_created'];
}
