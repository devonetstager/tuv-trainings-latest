<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $primaryKey = 'news_id';
    protected $fillable = ['news_category','title','content','sub_title','image_banner','image_thumb','updated_by','show_home','status','author','news_date','created_by'];
    public function user(){
        return $this->hasOne(\App\User::class, 'id' ,'created_by');
    }   
}
