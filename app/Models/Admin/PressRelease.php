<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class PressRelease extends Model
{
    protected $primaryKey = 'press_release_id';
    protected $fillable = [
        'youtube_link','pdf_filename', 'status',
    ];
}
