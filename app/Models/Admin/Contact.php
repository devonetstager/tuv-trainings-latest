<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $primaryKey = 'contact_id';
    protected $fillable = ['name','building','created_by','updated_by','show_home','area','column1','column2','state','country',
    'pincode','phone','fax','email','facebook_url','twitter_url','linkedin_url','youtube_url','created_by','updated_by','status'];

    public function user(){
        return $this->hasOne(\App\User::class, 'id' ,'created_by');
    }
}
