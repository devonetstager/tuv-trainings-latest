<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    protected $primaryKey = 'location_id';
    protected $fillable = ['name','building','created_by','updated_by','show_home','addressline1','addressline2','state','country',
    'pincode','phone','fax','email','latitude','longitude','created_by','updated_by','status'];

    public function user(){
        return $this->hasOne(\App\User::class, 'id' ,'created_by');
    }
}
