<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $primaryKey = 'country_id';
    protected $fillable = ['name','short_name','country_code','status'];
    public function cities(){
        return $this->hasMany('App\Models\Admin\City','country_id','country_id');
       }

}
