<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class HomeBanner extends Model
{
    protected $primaryKey = 'home_banner_id';
    protected $fillable = ['banner_image','title','status','show_home']; 
}
