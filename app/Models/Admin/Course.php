<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{

   protected $tableName='courses';
   protected $primaryKey = 'course_id';
   protected $fillable = ['name','meta_title','meta_tags','meta_description','over_view','outline','image','banner_image',
   'created_by','updated_by','category_id','brochure','status','show_home'];
   public function user(){
      return $this->hasOne(\App\User::class, 'id' ,'created_by');
  }   
  public function coursedetails(){
   return $this->hasMany('App\Models\Admin\CourseDetail','course_id','course_id');
  }
  public function category(){
   return $this->hasOne(Category::class, 'category_id' ,'category_id');
}
}
