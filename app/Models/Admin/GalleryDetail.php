<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class GalleryDetail extends Model
{
   
    // protected $tableName='course_details';
   protected $primaryKey = 'gallery_detail_id';
   protected $fillable = ['gallery_id','photo','vedio_url','status']; 

    public function gallery(){
        return $this->hasOne(Gallery::class, 'gallery_id' ,'gallery_id');
    }
    public function getCreatedDateAttribute(){
        return date("d-m-Y", strtotime($this->created_at));
    }
    protected $appends = ['created_date'];
}
