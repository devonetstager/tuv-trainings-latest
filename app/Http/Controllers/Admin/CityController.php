<?php

namespace App\Http\Controllers\Admin;
use DataTables;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Models\Admin\City;
use App\Models\Admin\Country;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CityController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = City::latest()->get();
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                           $btn  ='<a href="'. route('city.edit', [encrypt($row->city_id)]) .'" class="edit btn btn-primary btn-success">Edit</a> '; 
                        //    $btn .='<a href="'. route('cities.view', [encrypt($row->category_id)]) .'"  class="edit btn btn-primary btn-sm">View</a> ';
                           $btn .='<a href="'. route('city.delete', [encrypt($row->city_id)]) .'" onclick="return confirmDelete();" class="edit btn btn-primary btn-danger">Delete</a>';
     
                            return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
        $data['page_title'] = 'City';
        $data['page_sub_title'] = 'Records';
        return view('admin.cities.index',compact('data'));
    }
    public function new(){
        $countries = Country::orderBy('country_id','ASC')->get();
        return view('admin.cities.new',compact('countries'));
    }
    public function save(Request $request){
        // dd($request);
        $validator = Validator::make($request->all(),['country_id' => 'required','name' => 'required'],
        ['country_id.required'=>'Please Select One Country']);
        // $validator = Validator::make( $request->all(), $rules, $messages );
        if ($validator->passes())
        {
            $input = request()->except('_token');
            City::create($input);
            return back()->with('message','Successfully Created New City');
        }else{
            return back()->withErrors($validator);
        }
       
       
    }
    public function edit($city_id){
        
        try{
        $city = City::find(decrypt($city_id));
        $countries = Country::orderBy('country_id','ASC')->get();
        $data['page_title'] = 'Edit ';
        $data['page_sub_title'] = 'Records';
        return view('admin.cities.edit',compact('city','data','countries'));
        }catch(\Exception $ex){
            return back()->with('err_message','Invalid city');

        }
    }

    
    public function update(Request $request){
        $input = request()->except('_token');
           
       City::find(request('city_id'))->update($input);
       return back()->with('message','Successfully Updated Category');
    }
   public function delete($category_id){
    $city = City::find(decrypt($category_id));   
     $city->delete();
     return redirect(route('city.index'))->with('message','Successfully Deleted Data');
    }
   
}
