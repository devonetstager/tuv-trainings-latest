<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Models\Admin\Course;
use App\Models\Admin\City;
use App\Models\Admin\Currency;
use App\Models\Admin\CourseDetail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;

class CourseDetailsController extends Controller
{
    public function index(Request $request){
        if ($request->ajax()) {
            $data = CourseDetail::with('course')->latest()->get();

            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                           $btn  ='<a href="'. route('coursedetails.edit', [encrypt($row->detail_id)]) .'" class="edit btn btn-primary btn-success">Edit</a> ';                          
                           $btn .='<a href="'. route('coursedetails.delete', [encrypt($row->detail_id)]) .'" onclick="return confirmDelete();" class="edit btn btn-primary btn-danger">Delete</a>';
                           return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
        $data['page_title'] = 'Course Detail';
        $data['page_sub_title'] = 'Records';
        return view('admin.coursedetails.index' , compact('data'));
    }
    public function new(){
        $data['page_title'] = 'New';
        $data['page_sub_title'] = 'Record';
        $courses = Course::all();
         $currencies = Currency::all();
        $cities = City::all();
        return view('admin.coursedetails.new',compact('courses','data','cities','currencies'));
    }
    public function save(Request $request){      
        $validator = Validator::make($request->all(),['course_id' => 'required','location' => 'required',
        'fee' => 'required','start_date' => 'required','end_date' => 'required',
        'course_id' => 'required','city_id' => 'required'],
        [
            'course_id.required' => 'Please Select a Course!',
            'city_id.required' => 'Please Select a City!',
        ]);
        if ($validator->passes()){
            $input = request()->except('_token');
            $input['start_date'] = date('Y-m-d',strtotime($request->start_date));
            $input['end_date'] = date('Y-m-d',strtotime($request->end_date));
            CourseDetail::create($input);
            return back()->with('message','Details Added Successfully');
        }else{
            return back()->withErrors($validator)->withInput(Input::all());
        }    
    }
    public function edit($detail_id){
        $cities = City::all();
        $data['page_title'] = 'New';
        $data['page_sub_title'] = 'Record';
        try{
            $coursedetails= CourseDetail::find(decrypt($detail_id));
            $courses = Course::all();
             $currencies = Currency::all();
            return view('admin.coursedetails.edit',compact('coursedetails','courses','data','cities','currencies'));
        }catch(\Exception $ex){
            return back()->with('err_message','Invalid course');
        }       
    }
    public function update(Request $request){        
        $validator = Validator::make($request->all(),['course_id' => 'required','location' => 'required',
        'fee' => 'required','start_date' => 'required','end_date' => 'required',
        'course_id' => 'required','city_id' => 'required'],
        [
            'course_id.required' => 'Please Select a Course!',
            'city_id.required' => 'Please Select a City!',
        ]);
        $input = $request->all();
        $input['start_date'] = date('Y-m-d',strtotime($request->start_date));
            $input['end_date'] = date('Y-m-d',strtotime($request->end_date));
        if ($validator->passes()){
            CourseDetail::find(request('detail_id'))->update($input);
            return back()->with('message','Details Successfully Updated');
        }else{
            return back()->withErrors($validator);
        }
    }
    public function delete($detail_id){
        $course = CourseDetail::find(decrypt($detail_id));       
         $course->delete();
         return redirect(route('admin.coursedetails'))->with('message',' Data Deleted Successfully ');
    }
}
