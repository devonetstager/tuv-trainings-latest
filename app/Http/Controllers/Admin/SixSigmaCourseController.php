<?php

namespace App\Http\Controllers\Admin;
use DataTables;
use Illuminate\Support\Facades\Auth;
use App\User;
use Illuminate\Support\Facades\Storage;
use App\Models\Admin\SixSigmaCourse;
use App\Models\Admin\City;
use App\Models\Admin\Currency;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SixSigmaCourseController extends Controller
{
    public function index(Request $request){
        if ($request->ajax()) {
            $data = SixSigmaCourse::latest()->get();
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                           $btn  ='<a href="'. route('sigma.edit', [encrypt($row->six_sigma_id)]) .'" class="edit btn btn-primary btn-success">Edit</a> '; 
                        //    $btn .='<a href="'. route('sigma.view', [encrypt($row->six_sigma_id)]) .'"  class="edit btn btn-primary btn-sm">View</a> ';
                           $btn .='<a href="'. route('sigma.delete', [encrypt($row->six_sigma_id)]) .'" class="edit btn btn-primary btn-danger">Delete</a>';
     
                            return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
            $data['page_title'] = 'Six-Sigma';
            $data['page_sub_title'] = 'Records';
            return view('admin.sixsigmacourses.index',compact('data'));
        
    }
    public function new(){
        $cities = City::all();
        $currencies = Currency::all();
        $data['page_title'] = 'New';
        $data['page_sub_title'] = 'Record';
        return view('admin.sixsigmacourses.new',compact('data','cities','currencies'));
    }
    public function save(Request $request){    
       
        $validator = Validator::make($request->all(),['name' => 'required','description' => 'required',
        'fee' => 'required','course_image' => 'required|mimes:jpeg,png,jpg,gif,svg|max:2048']);
        if ($validator->passes())
        {
            $input = request()->except('_token','course_image');
            $created_by = Auth::id();
            $updated_by = Auth::id();
            $input['created_by'] = $created_by; 
            $input['updated_by'] = $updated_by; 
            if ($request->hasFile('course_image')){
            $filename = time()."_".str_random(40).'.'.request('course_image')->extension();
            $request->course_image->storeAs('sigmacourses/',$filename);
            $input['course_image'] = $filename;   
            $input['start_date'] = date('Y-m-d',strtotime(request('start_date')));
            $input['end_date'] = date('Y-m-d',strtotime(request('end_date')));  
             
            }
            SixSigmaCourse::create($input);
            return back()->with('message','Successfully Created New Course');
        }else{
            return back()->withErrors($validator);
        }
    
    
    }
    public function edit($six_sigma_id){
        $sigma = SixSigmaCourse::find(decrypt($six_sigma_id));
        $cities = City::all();
        $currencies = Currency::all();
        $data['page_title']='Edit';
        $data['page_sub_title']='Record';
        return view('admin.sixsigmacourses.edit',compact('sigma','data','cities','currencies'));
    }
    public function update(Request $request){
        // dd($request);
        $validator = Validator::make($request->all(),['name' => 'required','description' => 'required',
        'fee' => 'required|numeric',]);
        if ($validator->passes())
        {
            $input = request()->except('_token','course_image');
            $basicInfo = SixSigmaCourse::find(request('six_sigma_id'));

            if ($request->hasFile('course_image')) {
                if($basicInfo->course_image){
                $exists = Storage::disk('local')->exists('sigmacourses/'.$basicInfo->course_image);

                    if($exists){
                        Storage::disk('local')->delete('sigmacourses/'.$basicInfo->course_image);
                    }
                }
                $filename = time()."_".str_random(40).'.'.request('course_image')->extension();
                $request->course_image->storeAs('sigmacourses/',$filename);
                $input['course_image'] = $filename;
            }
            $input['start_date'] = date('Y-m-d',strtotime(request('start_date')));
            $input['end_date'] = date('Y-m-d',strtotime(request('end_date')));
            SixSigmaCourse::find(request('six_sigma_id'))->update($input);
            return back()->with('message','Successfully Updated Course');
        }else{
            return back()->withErrors($validator);
        }
    }
    public function delete($six_sigma_id){
        $sigma = SixSigmaCourse::find(decrypt($six_sigma_id));
        $exists = Storage::disk('local')->exists('sigmacourses/'.$sigma->course_image);
         if($exists){
           Storage::disk('local')->delete('sigmacourses/'.$sigma->course_image);
         }
         $sigma->delete();
         return redirect(route('sigma.index'))->with('message','Successfully Deleted Course');
     }
}
