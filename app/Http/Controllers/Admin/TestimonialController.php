<?php

namespace App\Http\Controllers\Admin;
use DataTables;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Models\Admin\Testimonial;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TestimonialController extends Controller
{
    public function index(Request $request){
   
        if ($request->ajax()) {
            $data = Testimonial::latest()->get();
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                           $btn  ='<a href="'. route('testimonial.edit', [encrypt($row->testimonial_id)]) .'" class="edit btn btn-primary btn-success">Edit</a> '; 
                        //    $btn .='<a href="'. route('courses.view', [encrypt($row->course_id)]) .'"  class="edit btn btn-primary btn-sm">View</a> ';
                           $btn .='<a href="'. route('testimonial.delete', [encrypt($row->testimonial_id)]) .'" onclick="return confirmDelete();" class="edit btn btn-primary btn-danger">Delete</a>';
     
                            return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
            $data['page_title'] = 'Testimonial';
            $data['page_sub_title'] = 'Records';
           return view('admin.testimonials.index',compact('data'));
       }
       public function new(){
        $data['page_title'] = 'New';
        $data['page_sub_title'] = 'Record';
        return view('admin.testimonials.new',compact('data'));
        }
        public function save(Request $request){
            $validator = Validator::make($request->all(),['content' => 'required','name' => 'required',
            'image' => 'mimes:jpeg,png,jpg,gif,svg']);
            if ($validator->passes()){
            // {   dd($request);
                $input = request()->except('_token','image');
                if ($request->hasFile('image')){
                    $filename = time()."_".str_random(40).'.'.request('image')->extension();
                    $request->image->storeAs('testimonial/image/',$filename);
                    $input['image'] = $filename;      
                }
                Testimonial::create($input);
                return back()->with('message','Successfully Created New Tesimonial');
            }else{
                return back()->withErrors($validator);
            }    
        
        }
        public function edit($testimonial_id){
            $testimonial = Testimonial::find(decrypt($testimonial_id));
            $data['page_title']='Edit';
            $data['page_sub_title']='Record';
            return view('admin.testimonials.edit',compact('testimonial','data'));
        }
        public function update(Request $request){
           
            $validator = Validator::make($request->all(),['content' => 'required','name' => 'required',
            'image' => 'mimes:jpeg,png,jpg,gif,svg']);
             if ($validator->passes()){
                $input = request()->except('_token','image');
                $basicInfo = Testimonial::find(request('testimonial_id'));    
                if ($request->hasFile('image')) {
                    if($basicInfo->image){
                    $exists = Storage::disk('local')->exists('testimonial/image/'.$basicInfo->image);    
                        if($exists){
                            Storage::disk('local')->delete('testimonial/image/'.$basicInfo->image);
                        }
                    }
               $filename = time()."_".str_random(40).'.'.request('image')->extension();
               $request->image->storeAs('testimonial/image/',$filename);
               $input['image'] = $filename;
                }
              
                Testimonial::find(request('testimonial_id'))->update($input);
                return back()->with('message','Data Updated Successfully  ');
            }else{
                return back()->withErrors($validator);
            }
        }
        public function delete($testimonial_id){
            $testimonial = Testimonial::find(decrypt($testimonial_id));
            $exists = Storage::disk('local')->exists('testimonial/image/'.$testimonial->image);
             if($exists){
               Storage::disk('local')->delete('testimonial/image/'.$testimonial->image);
             }
             $testimonial->delete();
             return redirect(route('testimonial'))->with('message','Successfully Deleted Data');
        }
}
