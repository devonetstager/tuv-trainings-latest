<?php

namespace App\Http\Controllers\Admin;
use DataTables;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Models\Admin\HomeBanner;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeBannerController extends Controller
{
    public function index(Request $request){
   
        if ($request->ajax()) {
            $data = HomeBanner::latest()->get();
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                           $btn  ='<a href="'. route('home-banner.edit', [encrypt($row->home_banner_id)]) .'" class="edit btn btn-primary btn-success">Edit</a> '; 
                        //    $btn .='<a href="'. route('courses.view', [encrypt($row->course_id)]) .'"  class="edit btn btn-primary btn-sm">View</a> ';
                           $btn .='<a href="'. route('home-banner.delete', [encrypt($row->home_banner_id)]) .'" onclick="return confirmDelete();" class="edit btn btn-primary btn-danger">Delete</a>';
     
                            return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
            $data['page_title'] = 'Homebanner';
            $data['page_sub_title'] = 'Records';
           return view('admin.homebanner.index',compact('data'));
       }
       public function new(){
        $data['page_title'] = 'New';
        $data['page_sub_title'] = 'Record';
        return view('admin.homebanner.new',compact('data'));
        }
        public function save(Request $request){
         
            $validator = Validator::make($request->all(),['title' => 'required',
            'banner_image' => 'required|mimes:jpeg,png,jpg,gif,svg|dimensions:min_width=700,min_height=400|max:2048'],
            [
                'banner_image.required' => 'The banner image should be width:1920 height:500 pixels ! (jpeg,png,jpg,gif,svg)',
            ]);
            if ($validator->passes())
            {
                $input = request()->except('_token','banner_images');
                $created_by = Auth::id();
                $updated_by = Auth::id();
                // $input['created_by'] = $created_by; 
                // $input['updated_by'] = $updated_by; 
                if ($request->hasFile('banner_image')){
                    $filename = time()."_".str_random(40).'.'.request('banner_image')->extension();
                    $request->banner_image->storeAs('homebanner/',$filename);
                    $input['banner_image'] = $filename;      
                }
                HomeBanner::create($input);
                return back()->with('message','Successfully Created New homebanner');
            }else{
                return back()->withErrors($validator);
            }
        
        
        }
        public function edit($home_banner_id){
            $homebanner = HomeBanner::find(decrypt($home_banner_id));
            $data['page_title']='Edit';
            $data['page_sub_title']='Record';
            return view('admin.homebanner.edit',compact('homebanner','data'));
        }
        public function update(Request $request){
            $validator = Validator::make($request->all(),['title' => 'required',
            'banner_image' => 'required|mimes:jpeg,png,jpg,gif,svg|dimensions:min_width=700,min_height=400|max:2048'],
            ['banner_image.required' => 'The banner image should be width:1920 height:500 pixels ! (jpeg,png,jpg,gif,svg)',
            ]);
             if ($validator->passes()){ 
                $input = request()->except('_token','banner_image');
                $basicInfo = HomeBanner::find(request('home_banner_id'));    
                if ($request->hasFile('banner_image')) {
                    if($basicInfo->banner_image){
                    $exists = Storage::disk('local')->exists('homebanner/'.$basicInfo->banner_image);    
                        if($exists){
                            Storage::disk('local')->delete('homebanner/'.$basicInfo->banner_image);
                        }
                    }
                $filename = time()."_".str_random(40).'.'.request('banner_image')->extension();
                $request->banner_image->storeAs('homebanner/',$filename);
                $input['banner_image'] = $filename;
                }
              
                HomeBanner::find(request('home_banner_id'))->update($input);
                return back()->with('message','Data Updated Successfully');
            }else{
                return back()->withErrors($validator);
            }
        }
        public function delete($home_banner_id){
            $homebanner = HomeBanner::find(decrypt($home_banner_id));
            $exists = Storage::disk('local')->exists('homebanner/'.$homebanner->banner_image);
             if($exists){
               Storage::disk('local')->delete('homebanner/'.$homebanner->banner_image);
             }
             $homebanner->delete();
             return redirect(route('home-banner'))->with('message','Successfully Deleted Data');
        }
}
