<?php

namespace App\Http\Controllers\Admin;
use DataTables;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use App\Models\Admin\Webinar;
use App\Http\Controllers\Controller;

class WebinarController
{
   public function index(){
        if (request()->ajax()) {
            $data = Webinar::latest()->get();
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                            $btn  ='<a href="'. route('webinar.edit', [encrypt($row->webinar_id)]) .'" class="edit btn btn-primary btn-success">Edit</a> ';                          
                            $btn .='<a href="'. route('webinar.delete', [encrypt($row->webinar_id)]) .'" onclick="return confirmDelete();" class="edit btn btn-primary btn-danger">Delete</a>';
                            return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
        $data['page_title'] = 'Webinar';
        $data['page_sub_title'] = 'Records';
        return view('admin.webinar.index' , compact('data'));
    }
     public function new(){
        $data['page_title'] = 'New';
        $data['page_sub_title'] = 'Record';
        return view('admin.webinar.new',compact('data'));
    }

    public function save()
    {
       
        $validator = Validator::make(request()->all(),['title' => 'required','webinar_link' => 'required']);
        if ($validator->passes()){
            $input = request()->except('_token');
            $input['news_date'] = date('Y-m-d',strtotime(request()->news_date));           
            if (request()->hasFile('webinar_image')){
                $image       = request('webinar_image');
                $filename = time()."_".str_random(40).'.'.request('webinar_image')->extension();               
                request()->webinar_image->storeAs('/webinar/image/',$filename);
                $input['webinar_image'] = $filename;
            }
            Webinar::create($input);
            return back()->with('message','Details Added Successfully');
        }else{
            return back()->withErrors($validator);
        }
    }

     public function edit($id){
        $webinar = Webinar::find(decrypt($id));
        $data['page_title']='Edit';
        $data['page_sub_title']='Record';
        return view('admin.webinar.edit',compact('webinar','data'));
    }

    public function update(){
    $input = request()->except('_token','webinar_image');
    $basicInfo = Webinar::find(request('webinar_id'));
    $validator = Validator::make(request()->all(),['title' => 'required',
    'webinar_link' => 'required']);
        if($validator->passes()){        
            if ( request()->hasFile('webinar_image')) {
                if($basicInfo->webinar_image){
                $exists = Storage::disk('local')->exists('/webinar/image/'.$basicInfo->webinar_image);
                    if($exists){
                        Storage::disk('local')->delete('/webinar/image/'.$basicInfo->webinar_image);
                    }
                }
                $filename = time()."_".str_random(40).'.'.request('webinar_image')->extension();
                request()->webinar_image->storeAs('/webinar/image/',$filename);
                $input['webinar_image'] = $filename;
            }
        
            Webinar::find(request('webinar_id'))->update($input);
        }
    return back()->with('message','Successfully Updated Press Data');
    }

    public function delete($webinar_id){
        $data = Webinar::find(decrypt($webinar_id));
        $exists = Storage::disk('local')->exists('/webinar/image/'.$data->webinar_image);
        if($exists){
            Storage::disk('local')->delete('/webinar/image/'.$data->webinar_image);
        }
        $data->delete();
     return redirect(route('webinar.index'))->with('message','Successfully Deleted Data');
    }
}
