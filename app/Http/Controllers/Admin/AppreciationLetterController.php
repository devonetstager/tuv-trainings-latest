<?php

namespace App\Http\Controllers\Admin;
use App\Models\Admin\AppreciationLetter;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AppreciationLetterController extends Controller
{
    public function index(){
        $letters = AppreciationLetter::paginate(15);
        $data['page_title'] = 'Edit ';
        $data['page_sub_title'] = 'Records';
        return view('admin.appreciation.index',compact('data','letters'));
    }
      
    
    public function new(){
        return view('admin.appreciation.new');
    }
    public function save(){
      
       
        $validator = Validator::make(request()->all(),['content' => 'required',
        'pdf_filename' => 'required']);
        if ($validator->passes())
        {
            $input = request()->except('_token','pdf_filename');
          
            if (request()->hasFile('pdf_filename')){
    
            $filename = time()."_".str_random(40).'.'.request('pdf_filename')->extension();
            request()->pdf_filename->storeAs('appreciation/',$filename);
            $input['pdf_filename'] = $filename;      
            }
        //   dd($input);
            AppreciationLetter::create($input);
            return back()->with('message','Successfully Created New Appreciation Letter');
        }else{
            return back()->withErrors($validator)->withInput(Input::all());
        }
       
       
    }
    public function edit($appreciation_id){    
            //   dd(decrypt($appreciation_id));    
        try{
            $letter = AppreciationLetter::find(decrypt($appreciation_id));
      
                //   dd($letter);
        $data['page_title'] = 'Edit ';
        $data['page_sub_title'] = 'Records';
        return view('admin.appreciation.edit',compact('data','letter'));
        }catch(\Exception $ex){
            return back()->with('err_message','Invalid category');
        }
    }    
    public function update(){   
        $input = request()->except('_token','pdf_filename');
        $basicInfo = AppreciationLetter::find(request('appreciation_id'));
        $validator = Validator::make(request()->all(),['content' => 'required']);
        if($validator->passes()){    
            if ( request()->hasFile('pdf_filename')) {
                if($basicInfo->pdf_filename){
                $exists = Storage::disk('local')->exists('appreciation/'.$basicInfo->pdf_filename);
    
                    if($exists){
                        Storage::disk('local')->delete('appreciation/'.$basicInfo->pdf_filename);
                    }
                }
                $filename = time()."_".str_random(40).'.'.request('pdf_filename')->extension();
                 request()->pdf_filename->storeAs('appreciation/',$filename);
                $input['pdf_filename'] = $filename;
                // dd($input);
            }
        
            AppreciationLetter::find(request('appreciation_id'))->update($input);
        }
       return back()->with('message','Successfully Updated Appreciation Letter');
    }
    public function delete($appreciation_id){
    $press = AppreciationLetter::find(decrypt($appreciation_id));
       $exists = Storage::disk('local')->exists('appreciation/'.$press->pdf_filename);
     if($exists){
        Storage::disk('local')->delete('appreciation/'.$press->pdf_filename);
     }
     $press->delete();
     return redirect(route('appeciation.letter.index'))->with('message','Successfully Deleted Data');
    }
}
