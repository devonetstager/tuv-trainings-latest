<?php

namespace App\Http\Controllers\Admin;

use DataTables;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Models\Admin\Cmspage;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CmsPageController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Cmspage::orderby('cmspage_id','ASC')->get();
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                           $btn  ='<a href="'. route('cms.edit', [encrypt($row->cmspage_id)]) .'" class="edit btn btn-primary btn-success">Manage</a> '; 
                        //    $btn .='<a href="'. route('Cmspage.view', [encrypt($row->cmspage_id)]) .'"  class="edit btn btn-primary btn-sm">View</a> ';
                        //    $btn .='<a href="'. route('cms.delete', [encrypt($row->cmspage_id)]) .'" class="edit btn btn-primary btn-danger">Delete</a>';
     
                            return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
        $data['page_title'] = 'CMS';
        $data['page_sub_title'] = 'Records';
        return view('admin.cms.index',compact('data'));
    }
    public function new(){
        return view('admin.cms.new');
    }
    public function save(Request $request){
        $rules = [
            'name' => 'required',
        ];
        $messages = [
            'required' => 'Name field is required.'
        ];
        $validator = Validator::make( $request->all(), $rules, $messages );
        if ($validator->passes())
        {
            $input = request()->except('_token','image');
            $created_by = Auth::id();
            $updated_by = Auth::id();
            $input['created_by'] = $created_by; 
            $input['updated_by'] = $updated_by; 
          
            Cmspage::create($input);
            return back()->with('message','Successfully Created New Cmspage');
        }else{
            return back()->withErrors($validator);
        }
       
       
    }
    public function edit($cmspage_id){
        
        try{
        $cmspage = Cmspage::find(decrypt($cmspage_id));
        $data['page_title'] = 'Edit ';
        $data['page_sub_title'] = 'Records';
        return view('admin.cms.edit',compact('cmspage','data'));
        }catch(\Exception $ex){
            return back()->with('err_message','Invalid Cmspage');

        }
    }

    
    public function update(Request $request){
        $input = request()->except('_token');
         
       Cmspage::find(request('cmspage_id'))->update($input);
       return back()->with('message','Successfully Updated Cmspage');
   }
   public function delete($cmspage_id){
    $Cmspage = Cmspage::find(decrypt($cmspage_id));
    $exists = Storage::disk('local')->exists('categories/'.$Cmspage->image);
     if($exists){
       Storage::disk('local')->delete('categories/'.$Cmspage->image);
     }
     $Cmspage->delete();
     return redirect(route('admin.Cmspage'))->with('message','Successfully Deleted Cmspage');
 }
    public function view($cmspage_id){
        $data['page_title'] = 'View ';
        $data['page_sub_title'] = 'Records';
        try{
            $Cmspage = Cmspage::with(['user'])->find(decrypt($cmspage_id));
            return view('admin.categories.view',compact('Cmspage','data'));
        }catch(\Exception $ex){
            return back()->with('message','Invalid Cmspage','data');
        }   
    }
}
