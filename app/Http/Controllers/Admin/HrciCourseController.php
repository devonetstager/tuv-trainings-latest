<?php

namespace App\Http\Controllers\Admin;
use DataTables;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Models\Admin\City;
use App\Models\Admin\Currency;
use Illuminate\Support\Facades\Storage;
use App\Models\Admin\HrciCourse;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HrciCourseController extends Controller
{
    public function index(Request $request){
        if ($request->ajax()) {
            $data = HrciCourse::latest()->get();
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                           $btn  ='<a href="'. route('hrci.edit', [encrypt($row->hrci_id)]) .'" class="edit btn btn-primary btn-success">Edit</a> '; 
                        //    $btn .='<a href="'. route('sigma.view', [encrypt($row->six_sigma_id)]) .'"  class="edit btn btn-primary btn-sm">View</a> ';
                           $btn .='<a href="'. route('hrci.delete', [encrypt($row->hrci_id)]) .'" onclick="return confirmDelete();" class="edit btn btn-primary btn-danger">Delete</a>';
     
                            return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
            $data['page_title'] = 'HRCI';
            $data['page_sub_title'] = 'Records';
            return view('admin.hrci.index',compact('data'));
        
    }
    public function new(){
        $cities = City::all();
        $currencies = Currency::all();
        $data['page_title'] = 'New';
        $data['page_sub_title'] = 'Record';
        return view('admin.hrci.new',compact('data','cities','currencies'));
    }
    public function save(Request $request){      
    //   dd($request);
        $validator = Validator::make($request->all(),['name' => 'required','description' => 'required',
        'fee' => 'required',]);
        if ($validator->passes())
        {
            $input = request()->except('_token','hrci_image');
            $created_by = Auth::id();
            $updated_by = Auth::id();
            $input['created_by'] = $created_by; 
            $input['updated_by'] = $updated_by; 
            if ($request->hasFile('hrci_image')){
            $filename = time()."_".str_random(40).'.'.request('hrci_image')->extension();
            $request->hrci_image->storeAs('hrci/hrciimage',$filename);
            $input['hrci_image'] = $filename;      
            }
            $input['start_date'] = date('Y-m-d',strtotime(request('start_date')));
            $input['end_date'] = date('Y-m-d',strtotime(request('end_date')));
            HrciCourse::create($input);
            return back()->with('message','Successfully Created New Course');
        }else{
            return back()->withErrors($validator);
        }
    
    
    }

    public function edit($hrci_id){
        $cities = City::all();
        $currencies = Currency::all();
        $hrci = HrciCourse::find(decrypt($hrci_id));
        $data['page_title']='Edit';
        $data['page_sub_title']='Record';
        return view('admin.hrci.edit',compact('hrci','data','cities','currencies'));
    }
    public function update(Request $request){
       
        // $validator = Validator::make($request->all(),['name' => 'required','description' => 'required',
        // 'fee' => 'required|numeric','hrci_image' => 'required|mimes:jpeg,png,jpg,gif,svg|max:2048']);
        // if ($validator->passes())
        // {
            //  dd($request);
            $input = request()->except('_token','hrci_image');
            $basicInfo = HrciCourse::find(request('hrci_id'));

            if ($request->hasFile('hrci_image')) {
                if($basicInfo->hrci_image){ 
                $exists = Storage::disk('local')->exists('hrci/hrciimage/'.$basicInfo->hrci_image);

                    if($exists){
                       
                        Storage::disk('local')->delete('hrci/hrciimage/'.$basicInfo->hrci_image);
                    }
                }
           $filename = time()."_".str_random(40).'.'.request('hrci_image')->extension();
           $request->hrci_image->storeAs('hrci/hrciimage/',$filename);
           $input['hrci_image'] = $filename;
            }
            $input['start_date'] = date('Y-m-d',strtotime(request('start_date')));
            $input['end_date'] = date('Y-m-d',strtotime(request('end_date')));
            HrciCourse::find(request('hrci_id'))->update($input);
            return back()->with('message','Successfully Updated Data');
        // }else{
        //     return back()->withErrors($validator);
        // }
    }

    public function delete($hrci_id){
        $hrci = HrciCourse::find(decrypt($hrci_id));
        $exists = Storage::disk('local')->exists('hrci/hrciimage/'.$hrci->hrci_image);

        if($exists){
                       
            Storage::disk('local')->delete('hrci/hrciimage/'.$hrci->hrci_image);
        }
         $hrci->delete();
         return redirect(route('hrci.index'))->with('message','Successfully Deleted Data');
     }
}
