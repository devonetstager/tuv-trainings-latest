<?php

namespace App\Http\Controllers\Admin;
use DataTables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use App\Models\Admin\Enquiry;
class EnquiryController
{
    // All Enquiries are saving in 1 table with ttype 0 to 8
    // type 0 => quick enquiry
    // type 1 => course enquiry
    // type 2 => sigma enquiry
    // type 3 => hrci enquiry
    // type 4 => brochure enquiry
    // type 5 => calender enquiry
    // type 6 => become a partner enquiry
    // type 7 => certificate enquiry
    // type 8 => contact enquiry

    public function list(){
        $enquiries = Enquiry::paginate(15);
        return view('admin.enquiries.list',compact('enquiries'));
    }
    public function upcoming_course_enquiry(Request $request)
    {
        if ($request->ajax()) {
            $data = Enquiry::latest()->where('type',1)->get();
            return Datatables::of($data)
            ->addColumn('url', function($row) {
                return '<a target="_blank" href="'. $row->enquiry_url .'">URL</a>';
            })
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                      
                        $btn ='<a href="'. route('enquiry.upcoming.delete', [encrypt($row->enquiry_id)]) .'" onclick="return confirmDelete();" class="edit btn btn-primary btn-danger">Delete</a>';
                        $btn .='&nbsp;<a href="'. route('enquiry.upcoming.view', [encrypt($row->enquiry_id)]) .'"  class="edit btn btn-primary btn-sm">View</a> ';
                          
                           return $btn;
                    })
                    ->rawColumns(['url','action'])
                    ->make(true);
        }
        $data['page_title'] = 'Upcoming Course Enquiry';  
        $data['page_sub_title'] = 'Records';
        return view('admin.enquiries.upcoming-enquiry',compact('data'));
    }
    public function upcoming_course_view($enquiry_id){
        $data['page_title'] = 'View';
        $data['page_sub_title'] = 'Records';
        try{
            $enquiry = Enquiry::find(decrypt($enquiry_id));
            return view('admin.enquiries.upcoming-course-view',compact('data','enquiry'));
        }catch(\Exception $ex){
            return back()->with('message','Invalid category','data');
        }   
    }
    public function quick_enquiries(Request $request)
    {
        if ($request->ajax()) {
            $data = Enquiry::latest()->where('type',0)->get();
            return Datatables::of($data)          
            ->addColumn('url', function($row) {
                return '<a target="_blank" href="'. $row->enquiry_url .'">URL</a>';
            })
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                        //    $btn  ='<a href="'. route('locations.edit', [encrypt($row->location_id)]) .'" class="edit btn btn-primary btn-success">Edit</a> '; 
                        //    $btn .='<a href="'. route('contacts.view', [encrypt($row->category_id)]) .'"  class="edit btn btn-primary btn-sm">View</a> ';
                        // $btn ='<a href="'.$row->enquiry_url.'">URL</a> ';   
                        $btn ='<a href="'. route('enquiry.quick.delete', [encrypt($row->enquiry_id)]) .'" onclick="return confirmDelete();" class="edit btn btn-primary btn-danger">Delete</a>';
                           $btn .='&nbsp;<a href="'. route('enquiry.quick.view', [encrypt($row->enquiry_id)]) .'"  class="edit btn btn-primary btn-sm">View</a> '; 
                          
                           return $btn;
                    })
                    ->rawColumns(['url','action'])
                    ->make(true);
        }
        $data['page_title'] = 'Quick Enquiries';  
        $data['page_sub_title'] = 'Records';
        return view('admin.enquiries.quick-enquiry',compact('data'));
    }
    public function quick_enquiries_search(Request $request)
    {
        if ($request->ajax()) {
            $data = Enquiry::latest()->where('type',0)
            ->whereBetween('start_date', array($request->start_date,$request->end_date))->get();
            return Datatables::of($data)
            
            ->addColumn('url', function($row) {
                return '<a target="_blank" href="'. $row->enquiry_url .'">URL</a>';
            })
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                        //    $btn  ='<a href="'. route('locations.edit', [encrypt($row->location_id)]) .'" class="edit btn btn-primary btn-success">Edit</a> '; 
                        //    $btn .='<a href="'. route('contacts.view', [encrypt($row->category_id)]) .'"  class="edit btn btn-primary btn-sm">View</a> ';
                        // $btn ='<a href="'.$row->enquiry_url.'">URL</a> ';   
                        $btn ='<a href="'. route('enquiry.quick.delete', [encrypt($row->enquiry_id)]) .'" onclick="return confirmDelete();" class="edit btn btn-primary btn-danger">Delete</a>';
                           $btn .='&nbsp;<a href="'. route('enquiry.quick.view', [encrypt($row->enquiry_id)]) .'"  class="edit btn btn-primary btn-sm">View</a> '; 
                          
                           return $btn;
                    })
                    ->rawColumns(['url','action'])
                    ->make(true);
        }
        $data['page_title'] = 'Quick Enquiries';  
        $data['page_sub_title'] = 'Records';
        return view('admin.enquiries.quick-enquiry',compact('data'));
    }

    public function quick_enquiries_view($enquiry_id){
        $data['page_title'] = 'View';
        $data['page_sub_title'] = 'Records';
        try{
            $enquiry = Enquiry::find(decrypt($enquiry_id));
            return view('admin.enquiries.quick-enquiry-view',compact('data','enquiry'));
        }catch(\Exception $ex){
            return back()->with('message','Invalid category','data');
        }   
    }
    public function contact_enquiry(Request $request)
    {
        if ($request->ajax()) {
            $data = Enquiry::latest()->where('type',8)->get();
            return Datatables::of($data)
            ->addColumn('url', function($row) {
                return '<a target="_blank" href="'. $row->enquiry_url .'">URL</a>';
            })
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                     $btn ='<a href="'. route('enquiry.contact.delete', [encrypt($row->enquiry_id)]) .'" onclick="return confirmDelete();" class="edit btn btn-primary btn-danger">Delete</a>';
                    $btn .='&nbsp;<a href="'. route('enquiry.contact.view', [encrypt($row->enquiry_id)]) .'"  class="edit btn btn-primary btn-sm">View</a> '; 
                          
                          
                           return $btn;
                    })
                    ->rawColumns(['url','action'])
                    ->make(true);
        }
        $data['page_title'] = 'Contact Enquiry';
        $data['page_sub_title'] = 'Records';
        return view('admin.enquiries.contact',compact('data'));

    }
    public function contact_enquiry_view($enquiry_id){
        
        $data['page_title'] = 'View';
        $data['page_sub_title'] = 'Records';
        try{
            $enquiry = Enquiry::find(decrypt($enquiry_id));
            return view('admin.enquiries.contact-enquiry-view',compact('data','enquiry'));
        }catch(\Exception $ex){
            return back()->with('message','Invalid category','data');
        }   
    }
    public function six_sigma_enquiry(Request $request)
    {
        if ($request->ajax()) {
            $data = Enquiry::latest()->where('type',2)->get();
            return Datatables::of($data)
            ->addColumn('url', function($row) {
                return '<a target="_blank" href="'. $row->enquiry_url .'">URL</a>';
            })
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                   $btn ='<a href="'. route('enquiry.upcoming.delete', [encrypt($row->enquiry_id)]) .'" onclick="return confirmDelete();" class="edit btn btn-primary btn-danger">Delete</a>';
                           $btn .='&nbsp;<a href="'. route('enquiry.sigma.view', [encrypt($row->enquiry_id)]) .'"  class="edit btn btn-primary btn-sm">View</a> '; 
                           
                           return $btn;
                    })
                    ->rawColumns(['url','action'])
                    ->make(true);
        }
        $data['page_title'] = 'Six Sigma Course Enquiry';  
        $data['page_sub_title'] = 'Records';
        return view('admin.enquiries.six-sigma',compact('data'));
    }
    public function sigma_enquiry_view($enquiry_id){
        $data['page_title'] = 'View';
        $data['page_sub_title'] = 'Records';
        try{
            $enquiry = Enquiry::find(decrypt($enquiry_id));
            return view('admin.enquiries.sigma-view',compact('data','enquiry'));
        }catch(\Exception $ex){
            return back()->with('message','Invalid category','data');
        }   
    }

    public function hrci_enquiry(Request $request)
    {
        if ($request->ajax()) {
            $data = Enquiry::latest()->where('type',3)->get();
            return Datatables::of($data)
            ->addColumn('url', function($row) {
                return '<a target="_blank" href="'. $row->enquiry_url .'">URL</a>';
            })
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                        $btn ='<a href="'. route('enquiry.hrci.delete', [encrypt($row->enquiry_id)]) .'" onclick="return confirmDelete();" class="edit btn btn-primary btn-danger">Delete</a>';
                           $btn .='&nbsp;<a href="'. route('enquiry.hrci.view', [encrypt($row->enquiry_id)]) .'"  class="edit btn btn-primary btn-sm">View</a> '; 
                           return $btn;
                    })
                    ->rawColumns(['url','action'])
                    ->make(true);
        }
        $data['page_title'] = 'HRCI Enquiry';  
        $data['page_sub_title'] = 'Records';
        return view('admin.enquiries.hrci-enquiry',compact('data'));
    }
  
    public function hrci_enquiry_view($enquiry_id){
        $data['page_title'] = 'View';
        $data['page_sub_title'] = 'Records';
        try{
            $enquiry = Enquiry::find(decrypt($enquiry_id));
            return view('admin.enquiries.hrci-view',compact('data','enquiry'));
        }catch(\Exception $ex){
            return back()->with('message','Invalid category','data');
        }   
    }
    public function cips_enquiry(Request $request)
    {
        if ($request->ajax()) {
            $data = Enquiry::latest()->where('type',10)->get();
            return Datatables::of($data)
            ->addColumn('url', function($row) {
                return '<a target="_blank" href="'. $row->enquiry_url .'">URL</a>';
            })
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                        $btn ='<a href="'. route('enquiry.cips.delete', [encrypt($row->enquiry_id)]) .'" onclick="return confirmDelete();" class="edit btn btn-primary btn-danger">Delete</a>';
                           $btn .='&nbsp;<a href="'. route('enquiry.cips.view', [encrypt($row->enquiry_id)]) .'"  class="edit btn btn-primary btn-sm">View</a> '; 
                           return $btn;
                    })
                    ->rawColumns(['url','action'])
                    ->make(true);
        }
        $data['page_title'] = 'CIPS Enquiry';  
        $data['page_sub_title'] = 'Records';
        return view('admin.enquiries.cips-enquiry',compact('data'));
    }
    public function cips_enquiry_view($enquiry_id){
        $data['page_title'] = 'View';
        $data['page_sub_title'] = 'Records';
        try{
            $enquiry = Enquiry::find(decrypt($enquiry_id));
            return view('admin.enquiries.cips-view',compact('data','enquiry'));
        }catch(\Exception $ex){
            return back()->with('message','Invalid category','data');
        }   
    }
    public function brochure(Request $request)
    {
        if ($request->ajax()) {
            $data = Enquiry::latest()->where('type',4)->get();
            return Datatables::of($data)
            ->addColumn('url', function($row) {
                return '<a target="_blank" href="'. $row->enquiry_url .'">URL</a>';
            })
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                      $btn ='<a href="'. route('enquiry.brochure.delete', [encrypt($row->enquiry_id)]) .'" onclick="return confirmDelete();" class="edit btn btn-primary btn-danger">Delete</a>';
                           $btn .='&nbsp;<a href="'. route('enquiry.brochure.view', [encrypt($row->enquiry_id)]) .'"  class="edit btn btn-primary btn-sm">View</a> ';
                           return $btn;
                    })
                    ->rawColumns(['url','action'])
                    ->make(true);
        }
        $data['page_title'] = 'Brochure Request';  
        $data['page_sub_title'] = 'Records';
        return view('admin.enquiries.brochure',compact('data'));
    }
    public function brochure_enquiry_view($enquiry_id){
        $data['page_title'] = 'View';
        $data['page_sub_title'] = 'Records';
        try{
            $enquiry = Enquiry::find(decrypt($enquiry_id));
            return view('admin.enquiries.brochure-view',compact('data','enquiry'));
        }catch(\Exception $ex){
            return back()->with('message','Invalid category','data');
        }   
    }
    public function calender(Request $request)
    {
        if ($request->ajax()) {
            $data = Enquiry::latest()->where('type',5)->get();
            return Datatables::of($data)
            ->addColumn('url', function($row) {
                return '<a target="_blank" href="'. $row->enquiry_url .'">URL</a>';
            })
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                      $btn ='<a href="'. route('enquiry.calender.delete', [encrypt($row->enquiry_id)]) .'" onclick="return confirmDelete();" class="edit btn btn-primary btn-danger">Delete</a>';
                           $btn .='&nbsp;<a href="'. route('enquiry.calender.view', [encrypt($row->enquiry_id)]) .'"  class="edit btn btn-primary btn-sm">View</a> '; 
                          
                           return $btn;
                    })
                    ->rawColumns(['url','action'])
                    ->make(true);
        }
        $data['page_title'] = 'Calender Request';  
        $data['page_sub_title'] = 'Records';
        return view('admin.enquiries.calender',compact('data'));
    }

    public function calender_enquiry_view($enquiry_id){
        $data['page_title'] = 'View';
        $data['page_sub_title'] = 'Records';
        try{
            $enquiry = Enquiry::find(decrypt($enquiry_id));
            return view('admin.enquiries.calender-view',compact('data','enquiry'));
        }catch(\Exception $ex){
            return back()->with('message','Invalid calender','data');
        }   
    }
    public function partner(Request $request)
    {
        if ($request->ajax()) {
            $data = Enquiry::latest()->where('type',6)->get();
            return Datatables::of($data)
            ->addColumn('url', function($row) {
                return '<a target="_blank" href="'. $row->enquiry_url .'">URL</a>';
            })
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                        $btn ='<a href="'. route('enquiry.partner.delete', [encrypt($row->enquiry_id)]) .'" onclick="return confirmDelete();" class="edit btn btn-primary btn-danger">Delete</a>';
                           $btn .='&nbsp;<a href="'. route('enquiry.partner.view', [encrypt($row->enquiry_id)]) .'"  class="edit btn btn-primary btn-sm">View</a> '; 
                            
                          
                           return $btn;
                    })
                    ->rawColumns(['url','action'])
                    ->make(true);
        }
        $data['page_title'] = 'Become a Partner Request';  
        $data['page_sub_title'] = 'Records';
        return view('admin.enquiries.partner',compact('data'));
    }
    public function partner_enquiry_view($enquiry_id){
        $data['page_title'] = 'View';
        $data['page_sub_title'] = 'Records';
        try{
            $enquiry = Enquiry::find(decrypt($enquiry_id));
            return view('admin.enquiries.partner-view',compact('data','enquiry'));
        }catch(\Exception $ex){
            return back()->with('message','Invalid Partner','data');
        }   
    }

    public function certificate(Request $request)
    {
        if ($request->ajax()) {
            $data = Enquiry::latest()->where('type',7)->get();
            return Datatables::of($data)
            ->addColumn('url', function($row) {
                return '<a target="_blank" href="'. $row->enquiry_url .'">URL</a>';
            })
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                      $btn ='<a href="'. route('enquiry.calender.delete', [encrypt($row->enquiry_id)]) .'" onclick="return confirmDelete();" class="edit btn btn-primary btn-danger">Delete</a>';
                           $btn .='&nbsp;<a href="'. route('enquiry.certificate.view', [encrypt($row->enquiry_id)]) .'"  class="edit btn btn-primary btn-sm">View</a> '; 
 
                           return $btn;
                    })
                    ->rawColumns(['url','action'])
                    ->make(true);
        }
        $data['page_title'] = 'Certificate Request';  
        $data['page_sub_title'] = 'Records';
        return view('admin.enquiries.certificate',compact('data'));
    }
    public function certificate_enquiry_view($enquiry_id){
        $data['page_title'] = 'View';
        $data['page_sub_title'] = 'Records';
        try{
            $enquiry = Enquiry::find(decrypt($enquiry_id));
            return view('admin.enquiries.certificate-view',compact('data','enquiry'));
        }catch(\Exception $ex){
            return back()->with('message','Invalid Partner','data');
        }   
    }

    public function offers(Request $request)
    {
        if ($request->ajax()) {
            $data = Enquiry::latest()->where('type',9)->get();
            return Datatables::of($data)
            ->addColumn('url', function($row) {
                return '<a target="_blank" href="'. $row->enquiry_url .'">URL</a>';
            })
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                      $btn ='<a href="'. route('enquiry.offers.delete', [encrypt($row->enquiry_id)]) .'" onclick="return confirmDelete();" class="edit btn btn-primary btn-danger">Delete</a>';
                      $btn .='&nbsp;<a href="'. route('enquiry.offers.view', [encrypt($row->enquiry_id)]) .'"  class="edit btn btn-primary btn-sm">View</a> '; 
 
                           return $btn;
                    })
                    ->rawColumns(['url','action'])
                    ->make(true);
        }
        $data['page_title'] = 'Offers Request';  
        $data['page_sub_title'] = 'Records';
        return view('admin.enquiries.nebosh',compact('data'));
    }
    public function offers_enquiry_view($enquiry_id){
        $data['page_title'] = 'View';
        $data['page_sub_title'] = 'Records';
        try{
            $enquiry = Enquiry::find(decrypt($enquiry_id));
            return view('admin.enquiries.nebosh-view',compact('data','enquiry'));
        }catch(\Exception $ex){
            return back()->with('message','Invalid Partner','data');
        }   
    }

    public function upcoming_course_delete($enquiry_id){
        // dd($enquiry_id);
        $enquiry = Enquiry::find(decrypt($enquiry_id));   
         $enquiry->delete();
         return back()->with('message','Successfully Deleted Data');
    }
    public function six_sigma_delete($enquiry_id){
        // dd($enquiry_id);
        $enquiry = Enquiry::find(decrypt($enquiry_id));   
            $enquiry->delete();
            return back()->with('message','Successfully Deleted Data');
    }
    public function hrci_delete($enquiry_id){
        // dd($enquiry_id);
        $enquiry = Enquiry::find(decrypt($enquiry_id));   
            $enquiry->delete();
            return back()->with('message','Successfully Deleted Data');
    }
    public function brochure_delete($enquiry_id){
        // dd($enquiry_id);
        $enquiry = Enquiry::find(decrypt($enquiry_id));   
            $enquiry->delete();
            return back()->with('message','Successfully Deleted Data');
    }
    public function calender_delete($enquiry_id){
        // dd($enquiry_id);
        $enquiry = Enquiry::find(decrypt($enquiry_id));   
            $enquiry->delete();
            return back()->with('message','Successfully Deleted Data');
    }
    public function quick_enquiry_delete($enquiry_id){
        // dd($enquiry_id);
        $enquiry = Enquiry::find(decrypt($enquiry_id));   
            $enquiry->delete();
            return back()->with('message','Successfully Deleted Data');
    }
    public function contact_enquiry_delete($enquiry_id){
        // dd($enquiry_id);
        $enquiry = Enquiry::find(decrypt($enquiry_id));   
            $enquiry->delete();
            return back()->with('message','Successfully Deleted Data');
    }
    public function partner_enquiry_delete($enquiry_id){
        // dd($enquiry_id);
        $enquiry = Enquiry::find(decrypt($enquiry_id));   
            $enquiry->delete();
            return back()->with('message','Successfully Deleted Data');
    }
    public function certificate_enquiry_delete($enquiry_id){
        // dd($enquiry_id);
        $enquiry = Enquiry::find(decrypt($enquiry_id));   
            $enquiry->delete();
            return back()->with('message','Successfully Deleted Data');
    }
    public function offers_enquiry_delete($enquiry_id){
        // dd($enquiry_id);
        $enquiry = Enquiry::find(decrypt($enquiry_id));   
            $enquiry->delete();
            return back()->with('message','Successfully Deleted Data');
    }
     public function cips_delete($enquiry_id){
        // dd($enquiry_id);
        $enquiry = Enquiry::find(decrypt($enquiry_id));   
            $enquiry->delete();
            return back()->with('message','Successfully Deleted Data');
    }

    
}
