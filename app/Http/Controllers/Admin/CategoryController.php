<?php

namespace App\Http\Controllers\Admin;
use DataTables;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\Admin\Category;
use App\User;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Category::latest()->get();
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                           $btn  ='<a href="'. route('category.edit', [encrypt($row->category_id)]) .'" class="edit btn btn-primary btn-success">Edit</a> '; 
                        //    $btn .='<a href="'. route('category.view', [encrypt($row->category_id)]) .'"  class="edit btn btn-primary btn-sm">View</a> ';
                           $btn .='<a href="'. route('category.delete', [encrypt($row->category_id)]) .'" onclick="return confirmDelete();" class="edit btn btn-primary btn-danger">Delete</a>';
     
                            return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
        $data['page_title'] = 'Category';
        $data['page_sub_title'] = 'Records';
        return view('admin.categories.index',compact('data'));
    }
    public function new(){
        $master_categories = Category::all();
        return view('admin.categories.new',compact('master_categories'));
    }
    public function save(Request $request){
       
        $validator = Validator::make($request->all(),['master_id' => 'required','name' => 'required',
        'image' => 'required','icon' => 'required']);
        if ($validator->passes())
        {
            $input = request()->except('_token','image');
            $created_by = Auth::id();
            $updated_by = Auth::id();
            $input['created_by'] = $created_by; 
            $input['updated_by'] = $updated_by; 
            if ($request->hasFile('image')){
    
            $filename = time()."_".str_random(40).'.'.request('image')->extension();
            $request->image->storeAs('categories/',$filename);
            $input['image'] = $filename;      
            }
            if ($request->hasFile('icon')){
    
                $filename = time()."_".str_random(40).'.'.request('icon')->extension();
                $request->icon->storeAs('categories/icon/',$filename);
                $input['icon'] = $filename;      
                }
            Category::create($input);
            return back()->with('message','Successfully Created New Category');
        }else{
            return back()->withErrors($validator)->withInput(Input::all());
        }
       
       
    }
    public function edit($category_id){        
        try{
        $category = Category::find(decrypt($category_id));
        $masters = Category::all();
        $data['page_title'] = 'Edit ';
        $data['page_sub_title'] = 'Records';
        return view('admin.categories.edit',compact('category','data','masters'));
        }catch(\Exception $ex){
            return back()->with('err_message','Invalid category');
        }
    }    
    public function update(Request $request){
        $input = request()->except('_token','image');
        $basicInfo = Category::find(request('category_id'));
        $validator = Validator::make($request->all(),['master_id' => 'required','name' => 'required']);
        if($validator->passes()){        
            if ($request->hasFile('image')) {
                if($basicInfo->image){
                $exists = Storage::disk('local')->exists('categories/'.$basicInfo->image);

                    if($exists){
                        Storage::disk('local')->delete('categories/'.$basicInfo->image);
                    }
                }
                $filename = time()."_".str_random(40).'.'.request('image')->extension();
                $request->image->storeAs('categories/',$filename);
                $input['image'] = $filename;
            }
            if ($request->hasFile('icon')) {          
                if($basicInfo->icon){
                $exists = Storage::disk('local')->exists('categories/icon/'.$basicInfo->icon);

                    if($exists){
                    Storage::disk('local')->delete('categories/icon/'.$basicInfo->icon);
                    }
                }
                $filename = time()."_".str_random(40).'.'.request('icon')->extension();
                $request->icon->storeAs('categories/icon/',$filename);
                $input['icon'] = $filename;
                // dd($input);
            }
       Category::find(request('category_id'))->update($input);
        }
       return back()->with('message','Successfully Updated Category');
   }
   public function delete($category_id){
    $category = Category::find(decrypt($category_id));
    $exists = Storage::disk('local')->exists('categories/'.$category->image);
     if($exists){
       Storage::disk('local')->delete('categories/'.$category->image);
     }
     $category->delete();
     return redirect(route('admin.category'))->with('message','Successfully Deleted Category');
    }
   
    public function view($category_id){
        $data['page_title'] = 'View ';
        $data['page_sub_title'] = 'Records';
        try{
            $category = Category::with(['user'])->find(decrypt($category_id));
            return view('admin.categories.view',compact('category','data'));
        }catch(\Exception $ex){
            return back()->with('message','Invalid category','data');
        }   
    }
}
