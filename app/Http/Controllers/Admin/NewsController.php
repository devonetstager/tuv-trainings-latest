<?php

namespace App\Http\Controllers\Admin;
use DataTables;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Models\Admin\News;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use Intervention\Image\ImageManagerStatic as Image;
use App\Http\Controllers\Controller;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request){
        if ($request->ajax()) {
            $data = News::with('user')->latest()->get();

            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                           $btn  ='<a href="'. route('news.edit', [encrypt($row->news_id)]) .'" class="edit btn btn-primary btn-success">Edit</a> ';                          
                           $btn .='<a href="'. route('news.delete', [encrypt($row->news_id)]) .'" onclick="return confirmDelete();" class="edit btn btn-primary btn-danger">Delete</a>';
                           return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
        $data['page_title'] = 'News';
        $data['page_sub_title'] = 'Records';
        return view('admin.news.index' , compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        $data['page_title'] = 'New';
        $data['page_sub_title'] = 'Record';
        return view('admin.news.new',compact('data'));
    }
   

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
        $validator = Validator::make($request->all(),['title' => 'required','news_date' => 'required','author'=>'required','content'=>'required']);
        if ($validator->passes()){
            $input = request()->except('_token');
            $input['news_date'] = date('Y-m-d',strtotime($request->news_date));
            $created_by = Auth::id();
            $updated_by = Auth::id();
            $input['created_by'] = $created_by; 
            $input['updated_by'] = $updated_by; 
            if ($request->hasFile('image_banner')){
                $image       = request('image_banner');
                $filename = time()."_".str_random(40).'.'.request('image_banner')->extension();
                
                $image_resizes = Image::make($image->getRealPath());
                $large = $image_resizes->resize(127, 67);
                Storage::put('news/imagethumb/'.$filename, $large->stream()->detach());
                
                $request->image_banner->storeAs('/news/imagebanner/',$filename);
                $input['image_banner'] = $filename;
            }
            News::create($input);
            return back()->with('message','Details Added Successfully');
        }else{
            return back()->withErrors($validator);
        }
    }

    public function edit($id){
        $news = News::find(decrypt($id));
        $data['page_title']='Edit';
        $data['page_sub_title']='Record';
        return view('admin.news.edit',compact('news','data'));
    }

    public function updatenews(Request $request)
    {
        $validator = Validator::make($request->all(),['title' => 'required','news_date' => 'required','author'=>'required','content'=>'required']);
        if ($validator->passes())
        {
            $input = request()->except('_token');
            $input['news_date'] = date('Y-m-d',strtotime($request->news_date));
            $basicInfo = News::find(request('news_id'));
            if ($request->hasFile('image_banner')) {
                $image       = request('image_banner');
                if($basicInfo->image_banner){
                    $existsOriginal = Storage::disk('local')->exists('news/imagebanner'.$basicInfo->image_banner);
                    if($existsOriginal){
                        Storage::disk('local')->delete('news/imagebanner'.$basicInfo->image_banner);
                    }
                    $existsThumb = Storage::disk('local')->exists('news/imagethumb'.$basicInfo->image_banner);
                    if($existsThumb){
                        Storage::disk('local')->delete('news/imagethumb'.$basicInfo->image_banner);
                    }
                }
                $filename = time()."_".str_random(40).'.'.request('image_banner')->extension();
                $image_resizes = Image::make($image->getRealPath());
                $large = $image_resizes->resize(127, 67);  // Resize here
                Storage::put('news/imagethumb/'.$filename, $large->stream()->detach());  // Store Thumb

                
                $request->image_banner->storeAs('news/imagebanner',$filename);  // store original
                $input['image_banner'] = $filename;
            }
            News::find(request('news_id'))->update($input);
            return back()->with('message','Successfully Updated Data');
        }else{
            return back()->withErrors($validator);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($news_id)
    {
        // dd($news_id);
        $news = News::find(decrypt($news_id));
        $exists = Storage::disk('local')->exists('news/imagethumb/'.$news->image_thumb);
         if($exists){
           Storage::disk('local')->delete('news/imagethumb/'.$news->image_thumb);
         }
         $exists = Storage::disk('local')->exists('news/imagebanner/'.$news->image_banner);
         if($exists){
           Storage::disk('local')->delete('news/imagebanner/'.$news->image_banner);
         }
         $news->delete();
         return redirect(route('news.index'))->with('message','Successfully Deleted Data');
    }
}
