<?php

namespace App\Http\Controllers\Admin;
use DataTables;
use App\Models\Admin\Country;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CountryController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Country::latest()->get();
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                           $btn  ='<a href="'. route('country.edit', [encrypt($row->country_id)]) .'" class="edit btn btn-primary btn-success">Edit</a> '; 
                        //    $btn .='<a href="'. route('country.view', [encrypt($row->country_id)]) .'"  class="edit btn btn-primary btn-sm">View</a> ';
                           $btn .='<a href="'. route('country.delete', [encrypt($row->country_id)]) .'" onclick="return confirmDelete();" class="edit btn btn-primary btn-danger">Delete</a>';
     
                            return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
        $data['page_title'] = 'country';
        $data['page_sub_title'] = 'Records';
        return view('admin.country.index',compact('data'));
    }
    public function new(){
        return view('admin.country.new');
    }
    public function save(Request $request){
     
        $validator = Validator::make($request->all(),['name' => 'required']);
       
        if ($validator->passes())
        {
            $input = request()->except('_token');
            Country::create($input);
            return back()->with('message','Successfully Created New country');
        }else{
            return back()->withErrors($validator);
        }
       
       
    }
    public function edit($country_id){        
        try{
        $country = Country::find(decrypt($country_id));
        $data['page_title'] = 'Edit ';
        $data['page_sub_title'] = 'Records';
        return view('admin.country.edit',compact('country','data'));
        }catch(\Exception $ex){
            return back()->with('err_message','Invalid country');

        }
    }

    
    public function update(Request $request){
        $input = request()->except('_token');
           
       Country::find(request('country_id'))->update($input);
       return back()->with('message','Successfully Updated Data!');
    }
   public function delete($country_id){
    $country = Country::find(decrypt($country_id));   
     $country->delete();
     return redirect(route('country.index'))->with('message','Successfully Deleted Data');
    }
}
