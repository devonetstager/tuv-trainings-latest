<?php

namespace App\Http\Controllers\Admin;
use App\Models\Admin\Clientele;
use Illuminate\Support\Facades\Validator;
use DataTables;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;

class ClienteleController
{
    public function index(){
        // $data = Clientele::latest()->get();
        // dd($data);
        if (request()->ajax()) {
            $data = Clientele::latest()->get();
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                           $btn  ='<a href="'. route('clientele.edit', [encrypt($row->clientele_id)]) .'" class="edit btn btn-primary btn-success">Edit</a> '; 
                           $btn .='<a href="'. route('clientele.delete', [encrypt($row->clientele_id)]) .'" onclick="return confirmDelete();" class="edit btn btn-primary btn-danger">Delete</a>';
     
                            return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
        // $clienteles = Clientele::paginate(10);
       
        $data['page_title'] = 'Clientale ';
        $data['page_sub_title'] = 'Records';
        return view('admin.clientele.index',compact('data'));
    }
      
    
    public function new(){
        return view('admin.clientele.new');
    }
    public function save(){      
       
        $validator = Validator::make(request()->all(),['company_name' => 'required']);
        if ($validator->passes())
        {
            $input = request()->except('_token');
            Clientele::create($input);
            return back()->with('message','Successfully Created New Clientele');
        }else{
            return back()->withErrors($validator)->withInput(Input::all());
        }        
    }
    public function edit($clientele_id){    
            //   dd(decrypt($clientele_id));    
        try{
            $clientele = Clientele::find(decrypt($clientele_id));
      
                //   dd($letter);
        $data['page_title'] = 'Edit ';
        $data['page_sub_title'] = 'Records';
        return view('admin.clientele.edit',compact('data','clientele'));
        }catch(\Exception $ex){
            return back()->with('err_message','Invalid category');
        }
    }    
    public function update(){   
        $input = request()->except('_token');
        $basicInfo = Clientele::find(request('clientele_id'));
        $validator = Validator::make(request()->all(),['company_name' => 'required']);
        if($validator->passes()){            
            Clientele::find(request('clientele_id'))->update($input);
        }
       return back()->with('message','Successfully Updated Clientele');
    }
    public function delete($clientele_id){
    $press = Clientele::find(decrypt($clientele_id));
      
     $press->delete();
     return redirect(route('clientele.index'))->with('message','Successfully Deleted Data');
    }
}
