<?php

namespace App\Http\Controllers\Admin;
use App\Models\Admin\PressRelease;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
class PressReleaseController
{
  public function index(){
    $pressreleases = PressRelease::paginate(15);
    $data['page_title'] = 'Edit ';
    $data['page_sub_title'] = 'Records';
    return view('admin.press.index',compact('data','pressreleases'));
}
  

public function new(){
    return view('admin.press.new');
}
public function save(){
    // dd(request()->all());
   
    $validator = Validator::make(request()->all(),['pdf_filename' => 'required']);
    if ($validator->passes())
    {
        $input = request()->except('_token','pdf_filename');
      
        if (request()->hasFile('pdf_filename')){

        $filename = time()."_".str_random(40).'.'.request('pdf_filename')->extension();
        request()->pdf_filename->storeAs('press/',$filename);
        $input['pdf_filename'] = $filename;      
        }
    
        PressRelease::create($input);
        return back()->with('message','Successfully Created New Press Release');
    }else{
        return back()->withErrors($validator)->withInput(Input::all());
    }
   
   
}
public function edit($press_release_id){        
    try{
        $pressrelease = PressRelease::find(decrypt($press_release_id));
        // dd(decrypt($press_release_id));
    $data['page_title'] = 'Edit ';
    $data['page_sub_title'] = 'Records';
    return view('admin.press.edit',compact('data','pressrelease'));
    }catch(\Exception $ex){
        return back()->with('err_message','Invalid category');
    }
}    
public function update(){
    $input = request()->except('_token','pdf_filename');
    $basicInfo = PressRelease::find(request('press_release_id'));
    $validator = Validator::make(request()->all(),['youtube_link' => 'required',
    'pdf_filename' => 'required']);
    if($validator->passes()){        
        if ( request()->hasFile('pdf_filename')) {
            if($basicInfo->pdf_filename){
            $exists = Storage::disk('local')->exists('press/'.$basicInfo->pdf_filename);

                if($exists){
                    Storage::disk('local')->delete('press/'.$basicInfo->pdf_filename);
                }
            }
            $filename = time()."_".str_random(40).'.'.request('pdf_filename')->extension();
             request()->pdf_filename->storeAs('press/',$filename);
            $input['pdf_filename'] = $filename;
        }
    
        PressRelease::find(request('press_release_id'))->update($input);
    }
   return back()->with('message','Successfully Updated Press Release');
}
public function delete($press_release_id){
$press = PressRelease::find(decrypt($press_release_id));
   $exists = Storage::disk('local')->exists('press/'.$press->pdf_filename);
 if($exists){
    Storage::disk('local')->delete('press/'.$press->pdf_filename);
 }
 $press->delete();
 return redirect(route('pressrelease.index'))->with('message','Successfully Deleted Data');
}
}
