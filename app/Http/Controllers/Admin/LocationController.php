<?php

namespace App\Http\Controllers\Admin;
use DataTables;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Models\Admin\Location;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LocationController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Location::latest()->get();
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                           $btn  ='<a href="'. route('locations.edit', [encrypt($row->location_id)]) .'" class="edit btn btn-primary btn-success">Edit</a> '; 
                        //    $btn .='<a href="'. route('contacts.view', [encrypt($row->category_id)]) .'"  class="edit btn btn-primary btn-sm">View</a> ';
                           $btn .='<a href="'. route('locations.delete', [encrypt($row->location_id)]) .'" onclick="return confirmDelete();" class="edit btn btn-primary btn-danger">Delete</a>';
     
                            return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
        $data['page_title'] = 'Location';
        $data['page_sub_title'] = 'Records';
        return view('admin.locations.index',compact('data'));
    }
    public function new(){
        return view('admin.locations.new');
    }
    public function save(Request $request){
        $validator = Validator::make($request->all(),['name' => 'required',
        'building' => 'required','country' => 'required','state' => 'required',
        'email' => 'required|email','addressline1' => 'required','addressline2' => 'required',]);
        if ($validator->passes()){
            $input = request()->except('_token');
            $created_by = Auth::id();
            $updated_by = Auth::id();
            $input['created_by'] = $created_by; 
            $input['updated_by'] = $updated_by; 

            $address = $request->building.','.$request->addressline1.','.$request->addressline2.','.$request->state;
           
            $coordinates =  $this->get_location($address); 
            $input['latitude'] = $coordinates['lat']; 
            $input['longitude'] = $coordinates['lng']; 
            Location::create($input);
            return back()->with('message','Successfully Created New Location');
        }else{
            return redirect()->route('locations.new')->withInput(Input::all())->withErrors($validator);
        }      
       
    }
    public function get_location($address){ 
        $address = urlencode($address);

        $url = 'https://maps.googleapis.com/maps/api/geocode/json?address='.$address.'&key='.env('MAP_API_KEY' , 'AIzaSyAVtVeosvqc2P8Fn4xN9wBEGWsFl2wFHWw');
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($ch);
        curl_close($ch);
        $response_a = json_decode($response);
        // dd($response_a);
        if($response_a == null){
            $status == 'ZERO_RESULTS';
        }else{
            $status = $response_a->status;
        }
        
        if ( $status == 'ZERO_RESULTS' ){
            return FALSE;
        }else if($response_a->status=="OVER_QUERY_LIMIT"){
            return false;
        }
        else{
            $return = array('lat' => $response_a->results[0]->geometry->location->lat, 'long' => $long = $response_a->results[0]->geometry->location->lng);
            return $return;
        }


    }
    public function edit($location_id){   
        try{
        $location = Location::find(decrypt($location_id)); 
        $data['page_title'] = 'Edit ';
        $data['page_sub_title'] = 'Records';
        return view('admin.locations.edit',compact('location','data'));
        }catch(\Exception $ex){
            return back()->with('err_message','Invalid Contact');

        }
    }    
    public function update(Request $request){
     $validator = Validator::make($request->all(),['name' => 'required',
        'building' => 'required','country' => 'required','state' => 'required',
        'email' => 'required|email','addressline1' => 'required','addressline2' => 'required',]);
        if ($validator->passes()){
        $input = request()->except('_token'); 
        $updated_by = Auth::id();
        $input['updated_by'] = $updated_by;          
        Location::find(request('location_id'))->update($input);
        return back()->with('message','Successfully Updated Location');
        }else{
            return back()->withErrors($validator);;
        }
       
    }
   public function delete($location_id){
    $location = Location::find(decrypt($location_id));   
     $location->delete();
     return redirect(route('locations'))->with('message','Successfully Deleted Data');
    }
}
