<?php

namespace App\Http\Controllers\Admin;
use DataTables;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Models\Admin\Gallery;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GalleryController extends Controller
{
    public function index(Request $request){
   
        if ($request->ajax()) {
            $data = Gallery::latest()->get();
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                           $btn  ='<a href="'. route('gallery.edit', [encrypt($row->gallery_id)]) .'" class="edit btn btn-primary btn-success">Edit</a> '; 
                        //    $btn .='<a href="'. route('courses.view', [encrypt($row->course_id)]) .'"  class="edit btn btn-primary btn-sm">View</a> ';
                           $btn .='<a href="'. route('gallery.delete', [encrypt($row->gallery_id)]) .'" onclick="return confirmDelete();" class="edit btn btn-primary btn-danger">Delete</a>';
     
                            return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
            $data['page_title'] = 'Gallery';
            $data['page_sub_title'] = 'Records';
           return view('admin.gallery.index',compact('data'));
       }
       public function new(){
        $data['page_title'] = 'New';
        $data['page_sub_title'] = 'Record';
               // dd($categories);
        return view('admin.gallery.new',compact('data'));
        }
        public function save(Request $request){
         
              
            $validator = Validator::make($request->all(),['title' => 'required','main_photo' => 'required|mimes:jpeg,png,jpg,gif,svg|max:2048'],
            [
                'category_id.required' => 'Please Select a Category!',
            ]);
            if ($validator->passes())
            {
                $input = request()->except('_token','main_photo');
                $created_by = Auth::id();
                $updated_by = Auth::id();
                $input['created_by'] = $created_by; 
                $input['updated_by'] = $updated_by; 
                if ($request->hasFile('main_photo')){
                    $filename = time()."_".str_random(40).'.'.request('main_photo')->extension();
                    $request->main_photo->storeAs('gallery/mainphoto',$filename);
                    $input['main_photo'] = $filename;      
                }
                Gallery::create($input);
                return back()->with('message','Successfully Created New Gallery');
            }else{
                return back()->withErrors($validator);
            }
        
        
        }
        public function edit($gallery_id){
            $gallery = Gallery::find(decrypt($gallery_id));
            $data['page_title']='Edit';
            $data['page_sub_title']='Record';
            return view('admin.gallery.edit',compact('gallery','data'));
        }
        public function update(Request $request){
           
            $validator = Validator::make($request->all(),['title' => 'required','main_photo' => '|mimes:jpeg,png,jpg,gif,svg|max:2048']);
            if ($validator->passes())
            {
                $input = request()->except('_token','main_photo');
                $basicInfo = Gallery::find(request('gallery_id'));    
                if ($request->hasFile('main_photo')) {
                    if($basicInfo->image){
                    $exists = Storage::disk('local')->exists('gallery/mainphoto/'.$basicInfo->main_photo);    
                        if($exists){
                            Storage::disk('local')->delete('gallery/mainphoto/'.$basicInfo->main_photo);
                        }
                    }
               $filename = time()."_".str_random(40).'.'.request('main_photo')->extension();
               $request->main_photo->storeAs('gallery/mainphoto/',$filename);
               $input['main_photo'] = $filename;
                }
              
                Gallery::find(request('gallery_id'))->update($input);
                return back()->with('message','Successfully Updated Course');
            }else{
                return back()->withErrors($validator);
            }
        }
        public function delete($gallery_id){
            $gallery = Gallery::find(decrypt($gallery_id));
            $exists = Storage::disk('local')->exists('gallery/mainphoto/'.$gallery->main_photo);
             if($exists){
               Storage::disk('local')->delete('gallery/mainphoto/'.$gallery->main_photo);
             }
             $gallery->delete();
             return redirect(route('gallery.index'))->with('message','Successfully Deleted Course');
        }
}
