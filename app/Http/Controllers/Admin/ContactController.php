<?php

namespace App\Http\Controllers\Admin;
use DataTables;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Models\Admin\Contact;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ContactController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Contact::latest()->get();
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                           $btn  ='<a href="'. route('contacts.edit', [encrypt($row->contact_id)]) .'" class="edit btn btn-primary btn-success">Edit</a> '; 
                        //    $btn .='<a href="'. route('contacts.view', [encrypt($row->category_id)]) .'"  class="edit btn btn-primary btn-sm">View</a> ';
                        //   $btn .='<a href="'. route('contacts.delete', [encrypt($row->contact_id)]) .'" class="edit btn btn-primary btn-danger">Delete</a>';
     
                            return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
        $data['page_title'] = 'Contacts';
        $data['page_sub_title'] = 'Records';
        return view('admin.contacts.index',compact('data'));
    }
    public function new(){
        return view('admin.contacts.new');
    }
    public function save(Request $request){
        $validator = Validator::make($request->all(),['name' => 'required',
        'building' => 'required','area' => 'required','country' => 'required',
        'email' => 'required|email','pincode' => 'required',]);
        if ($validator->passes()){
        // {   dd($request);
            $input = request()->except('_token');
            $created_by = Auth::id();
            $updated_by = Auth::id();
            $input['created_by'] = $created_by; 
            $input['updated_by'] = $updated_by; 
            Contact::create($input);
            return back()->with('message','Successfully Created New Contact');
        }else{
            return redirect()->route('contacts.new')->withInput()->withErrors($validator);
        }      
       
    }
    public function edit($contact_id){        
        try{
        $contact = Contact::first();
        $data['page_title'] = 'Edit ';
        $data['page_sub_title'] = 'Records';
        return view('admin.contacts.edit',compact('contact','data'));
        }catch(\Exception $ex){
            return back()->with('err_message','Invalid Contact');

        }
    }    
    public function update(Request $request){
        $input = request()->except('_token');
           
       Contact::find(request('contact_id'))->update($input);
       return back()->with('message','Address Successfully Updated');
    }
   public function delete($contact_id){
    $Contact = Contact::find(decrypt($contact_id));   
     $Contact->delete();
     return redirect(route('contact.index'))->with('message','Successfully Deleted Data');
    }
}
