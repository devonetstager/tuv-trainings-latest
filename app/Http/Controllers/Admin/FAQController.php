<?php

namespace App\Http\Controllers\Admin;
use DataTables;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Models\Admin\FAQ;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FAQController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = FAQ::latest()->get();
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                           $btn  ='<a href="'. route('faq.edit', [encrypt($row->faq_id)]) .'" class="edit btn btn-primary btn-success">Edit</a> '; 
                        //    $btn .='<a href="'. route('cities.view', [encrypt($row->category_id)]) .'"  class="edit btn btn-primary btn-sm">View</a> ';
                           $btn .='<a href="'. route('faq.delete', [encrypt($row->faq_id)]) .'" onclick="return confirmDelete();" class="edit btn btn-primary btn-danger">Delete</a>';
     
                            return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
        $data['page_title'] = 'FAQ';
        $data['page_sub_title'] = 'Records';
        return view('admin.faq.index',compact('data'));
    }
    public function new(){
        $data['page_title'] = 'New';
        $data['page_sub_title'] = 'FAQ';
       
        return view('admin.faq.new',compact('data'));
    }
    public function save(Request $request){
        // dd($request);
        $validator = Validator::make($request->all(),['question' => 'required','answer' => 'required']);
        // $validator = Validator::make( $request->all(), $rules, $messages );
        if ($validator->passes())
        {
            $input = request()->except('_token');
            FAQ::create($input);
            return back()->with('message','Successfully Created New FAQ');
        }else{
            return back()->withErrors($validator);
        }
       
       
    }

    public function edit($faq_id){
        try{
        $faq = FAQ::find(decrypt($faq_id));   
        $data['page_title'] = 'Edit ';
        $data['page_sub_title'] = 'Records';
        return view('admin.faq.edit',compact('faq','data'));
        }catch(\Exception $ex){
            return back()->with('err_message','Invalid FAQ');

        }
    }
    public function update(Request $request){
        $input = request()->except('_token');
         
        FAQ::find(request('faq_id'))->update($input);
       return back()->with('message','Successfully Updated FAQ');
    }
    public function delete($faq_id){
        $delete = FAQ::find(decrypt($faq_id));   
         $delete->delete();
         return redirect(route('faq.index'))->with('message','Successfully Deleted Data');
        }
}
