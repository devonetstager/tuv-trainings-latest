<?php

namespace App\Http\Controllers\Admin;
use DataTables;
use Illuminate\Support\Facades\Auth;
use App\User;
use Illuminate\Support\Facades\Storage;
use App\Models\Admin\Qualification;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class QualificationController extends Controller
{
    public function index(Request $request){
        if ($request->ajax()) {
            $data = Qualification::latest()->get();
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                           $btn  ='<a href="'. route('qualification.edit', [encrypt($row->qualification_id)]) .'" class="edit btn btn-primary btn-success">Edit</a> '; 
                        //    $btn .='<a href="'. route('qualifications.view', [encrypt($row->qualification_id)]) .'"  class="edit btn btn-primary btn-sm">View</a> ';
                           $btn .='<a href="'. route('qualification.delete', [encrypt($row->qualification_id)]) .'" onclick="return confirmDelete();" class="edit btn btn-primary btn-danger">Delete</a>';
     
                            return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
            $data['page_title'] = 'Qualification';
            $data['page_sub_title'] = 'Records';
            return view('admin.qualifications.index',compact('data'));
        
    }
    public function new(){
        $data['page_title'] = 'New';
        $data['page_sub_title'] = 'Record';
        return view('admin.qualifications.new',compact('data'));
    }
     public function save(Request $request){      
      
        $validator = Validator::make($request->all(),['name' => 'required','description' => 'required',
        'image' => 'required|mimes:jpeg,png,jpg,gif,svg|max:2048']);
        if ($validator->passes())
        {
            $input = request()->except('_token','image');
            $created_by = Auth::id();
            $updated_by = Auth::id();
            $input['created_by'] = $created_by; 
            $input['updated_by'] = $updated_by; 
            if ($request->hasFile('image')){
            $filename = time()."_".str_random(40).'.'.request('image')->extension();
            $request->image->storeAs('qualification/images/',$filename);
            $input['image'] = $filename;      
            }
            Qualification::create($input);
            return back()->with('message','Successfully Created New Qualification');
        }else{
            return back()->withErrors($validator);
        }
    
    
    }

    public function edit($qualification_id){
        $qualifications = Qualification::find(decrypt($qualification_id));
        $data['page_title']='Edit';
        $data['page_sub_title']='Record';
        return view('admin.qualifications.edit',compact('qualifications','data'));
    }
    public function update(Request $request){
        // dd($request);
        $validator = Validator::make($request->all(),['name' => 'required','description' => 'required']);
        if ($validator->passes())
        {
            $input = request()->except('_token','image');
            $basicInfo = Qualification::find(request('qualification_id'));

            if ($request->hasFile('image')) {
                if($basicInfo->image){
                $exists = Storage::disk('local')->exists('qualification/images/'.$basicInfo->image);

                    if($exists){
                        Storage::disk('local')->delete('qualification/images/'.$basicInfo->image);
                    }
                }
                $filename = time()."_".str_random(40).'.'.request('image')->extension();
                $request->image->storeAs('qualification/images/',$filename);
                $input['image'] = $filename;
            }
            Qualification::find(request('qualification_id'))->update($input);
            return back()->with('message','Successfully Updated Qualification');
        }else{
            return back()->withErrors($validator);
        }
    }
    public function delete($qualification_id){
        $qualifications = Qualification::find(decrypt($qualification_id));
        $exists = Storage::disk('local')->exists('qualificationscourses/'.$qualifications->course_image);
         if($exists){
            Storage::disk('local')->delete('qualification/images/'.$basicInfo->image);
         }
         $qualifications->delete();
         return redirect(route('qualification.index'))->with('message','Successfully Deleted Qualification');
     }
}
