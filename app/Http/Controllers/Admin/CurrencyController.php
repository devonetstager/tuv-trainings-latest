<?php

namespace App\Http\Controllers\Admin;
use DataTables;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Models\Admin\City;
use App\Models\Admin\Country;
use Illuminate\Support\Facades\Validator;
use App\Models\Admin\Currency;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CurrencyController
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Currency::latest()->get();
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                           $btn  ='<a href="'. route('currency.edit', [encrypt($row->currency_id)]) .'" class="edit btn btn-primary btn-success">Edit</a> '; 
                        //    $btn .='<a href="'. route('cities.view', [encrypt($row->category_id)]) .'"  class="edit btn btn-primary btn-sm">View</a> ';
                           $btn .='<a href="'. route('currency.delete', [encrypt($row->currency_id)]) .'" onclick="return confirmDelete();" class="edit btn btn-primary btn-danger">Delete</a>';
     
                            return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
        $data['page_title'] = 'Currency';
        $data['page_sub_title'] = 'Records';
        return view('admin.currency.index',compact('data'));
    }
    public function new(){
        $countries = Country::orderBy('country_id','ASC')->get();
        return view('admin.currency.new',compact('countries'));
    }
    public function save(Request $request){
        // dd($request);
        $validator = Validator::make($request->all(),['country_id' => 'required','name' => 'required'],
        ['country_id.required'=>'Please Select One Country']);
        // $validator = Validator::make( $request->all(), $rules, $messages );
        if ($validator->passes())
        {
            $input = request()->except('_token');
            Currency::create($input);
            return back()->with('message','Successfully Created New Currency');
        }else{
            return back()->withErrors($validator);
        }
       
       
    }
    public function edit($currency_id){
        
        try{
        $currency = Currency::find(decrypt($currency_id));
        $countries = Country::orderBy('country_id','ASC')->get();
        $data['page_title'] = 'Edit ';
        $data['page_sub_title'] = 'Records';
        return view('admin.currency.edit',compact('currency','data','countries'));
        }catch(\Exception $ex){
            return back()->with('err_message','Invalid Currency');

        }
    }
    
    public function update(Request $request){
        $input = request()->except('_token');
           
        Currency::find(request('currency_id'))->update($input);
       return back()->with('message','Successfully Updated Currency');
    }
   public function delete($currency_id){
    $delete = Currency::find(decrypt($currency_id));   
     $delete->delete();
     return redirect(route('currency.index'))->with('message','Successfully Deleted Data');
    }
}
