<?php

namespace App\Http\Controllers\Admin;
use App\Models\Admin\CIPS;
use App\Models\Admin\City;
use App\Models\Admin\Currency;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CipsCourseController
{
    public function index(){
        $cips_courses = CIPS::paginate(15);
        $data['page_title'] = 'CIPS ';
        $data['page_sub_title'] = 'Records';
        return view('admin.cips.index',compact('data','cips_courses'));
    }    

    public function new(){
        $cities = City::all();
        $currencies = Currency::all();
        return view('admin.cips.new',compact('cities','currencies'));
    }
    public function save(){
        $validator = Validator::make(request()->all(),['name' => 'required','description' => 'required','fee' => 'required','cips_image' => 'required',
        'location' => 'required']);
        if ($validator->passes()){
            $input = request()->except('_token','cips_image');        
            if (request()->hasFile('cips_image')){
            $filename = time()."_".str_random(40).'.'.request('cips_image')->extension();
            request()->cips_image->storeAs('cips/',$filename);
            $input['cips_image'] = $filename;    
            $input['start_date'] = date('Y-m-d',strtotime(request()->start_date));
            $input['end_date'] = date('Y-m-d',strtotime(request()->end_date));  
            }        
            CIPS::create($input);
            return back()->with('message','Successfully Created New cips Release');
        }else{
            return back()->withErrors($validator)->withInput(Input::all());
        }
    
    
    }
public function edit($cips_id){     
    $cities = City::all();
    $currencies = Currency::all();
    try{
        $cips = CIPS::find(decrypt($cips_id));
    $data['page_title'] = 'Edit ';
    $data['page_sub_title'] = 'Records';
    return view('admin.cips.edit',compact('data','cips','cities','currencies'));
    }catch(\Exception $ex){
        return back()->with('err_message','Invalid Data');
    }
}    
public function update(){
  
    $input = request()->except('_token','cips_image');
    $basicInfo = CIPS::find(request('cips_id'));
    $validator = Validator::make(request()->all(),['name' => 'required',
    'fee' => 'required']);
    if($validator->passes()){        
        if ( request()->hasFile('cips_image')) {
            if($basicInfo->cips_image){
            $exists = storage::disk('local')->exists('cips/'.$basicInfo->cips_image);

                if($exists){
                    Storage::disk('local')->delete('cips/'.$basicInfo->cips_image);
                }
            }
            $filename = time()."_".str_random(40).'.'.request('cips_image')->extension();
             request()->cips_image->storeAs('cips/',$filename);  
           
            $input['cips_image'] = $filename;
        }
        $input['start_date'] = date('Y-m-d',strtotime(request()->start_date));
        $input['end_date'] = date('Y-m-d',strtotime(request()->end_date));  
        CIPS::find(request('cips_id'))->update($input);
    }
   return back()->with('message','Successfully Updated cips Release');
}
public function delete($cips_id){
$cips = CIPS::find(decrypt($cips_id));
   $exists = Storage::disk('local')->exists('cips/'.$cips->cips_image);
 if($exists){
    Storage::disk('local')->delete('cips/'.$cips->cips_image);
 }
 $cips->delete();
 return redirect(route('cips.index'))->with('message','Successfully Deleted Data');
}
}
