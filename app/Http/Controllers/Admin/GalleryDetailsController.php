<?php

namespace App\Http\Controllers\Admin;
use DataTables;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Models\Admin\Gallery;
use App\Models\Admin\GalleryDetail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GalleryDetailsController extends Controller
{
    public function index(Request $request){
        if ($request->ajax()) {
            $data = GalleryDetail::with('gallery')->latest()->get();

            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                           $btn  ='<a href="'. route('gallerydetail.edit', [encrypt($row->gallery_detail_id)]) .'" class="edit btn btn-primary btn-success">Edit</a> ';                          
                           $btn .='<a href="'. route('gallerydetail.delete', [encrypt($row->gallery_detail_id)]) .'" onclick="return confirmDelete();" class="edit btn btn-primary btn-danger">Delete</a>';
                           return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
        $data['page_title'] = 'Gallery Detail';
        $data['page_sub_title'] = 'Records';
        return view('admin.gallerydetails.index' , compact('data'));
    }
    public function new(){
        $data['page_title'] = 'New';
        $data['page_sub_title'] = 'Record';
        $gallery = Gallery::all();
        return view('admin.gallerydetails.new',compact('gallery','data'));
    }
    public function save(Request $request){
        $validator = Validator::make($request->all(),['gallery_id' => 'required'],
        [
            'gallery_id.required' => 'Please Select a Gallery!'
        ]);
        if ($validator->passes()){
            $input = request()->except('_token','photo');
            $created_by = Auth::id();
            $updated_by = Auth::id();
            $input['created_by'] = $created_by; 
            $input['updated_by'] = $updated_by;
            if ($request->vedio_url){
                // dd($request);
               $datas =    $request->vedio_url;           
                foreach ($datas as $data) {                  
                $input['vedio_url'] = $data;
                GalleryDetail::create($input);
                }
                    
            }
            if ($request->hasFile('photo')){
                $files = $request->file('photo');
              
                foreach ($files as $file) {
                    $filename =  time()."_".str_random(40).'.'.$file->extension();
                    $file->storeAs('gallery/photo/',$filename);
                $input['photo'] = $filename;
                GalleryDetail::create($input);
                }
                    
            }
           
            return back()->with('message','Successfully Created New Gallery');
        }else{
            return back()->withErrors($validator);
        }
    }
    public function edit($gallery_detail_id){
        $data['page_title'] = 'New';
        $data['page_sub_title'] = 'Record';
        try{
            $gallerydetails= GalleryDetail::find(decrypt($gallery_detail_id));
            $galleries = gallery::all();
         
            return view('admin.gallerydetails.edit',compact('gallerydetails','galleries','data'));
        }catch(\Exception $ex){
            return back()->with('err_message','Invalid Gallery');
        }       
    }
    public function update(Request $request){        
       
        $input = request()->except('_token','photo');
        $validator = Validator::make($request->all(),['gallery_id' => 'required'],
        [
            'gallery_id.required' => 'Please Select a Gallery!'
        ]);
        $basicInfo = GalleryDetail::find(request('gallery_detail_id'));
        if ($validator->passes())
        {
            if ($request->vedio_url){                      
                $input['vedio_url'] = $request->vedio_url;             
            }
       if ($request->hasFile('photo')) {
           if($basicInfo->photo){
           $exists = Storage::disk('local')->exists('gallery/photo/'.$basicInfo->photo);

               if($exists){
                Storage::disk('local')->delete('gallery/photo/'.$basicInfo->photo);
               }
           }
           $filename = time()."_".str_random(40).'.'.request('photo')->extension();
           $request->photo->storeAs('gallery/photo/',$filename);
           $input['photo'] = $filename;
           $input['vedio_url'] = NULL; 
       }
    //    dd($input);
       GalleryDetail::find($request->gallery_detail_id)->update($input);
       return back()->with('message','Successfully Created New Gallery');
   }else{
       return back()->withErrors($validator);
   }
   
    }
    public function delete($gallery_detail_id){
        $data = GalleryDetail::find(decrypt($gallery_detail_id)); 
        $exists = Storage::disk('local')->exists('gallery/photo/'.$data->photo);
        if($exists){
            Storage::disk('local')->delete('gallery/photo/'.$data->photo);
           }      
         $data->delete();
         return redirect(route('gallerydetail.index'))->with('message',' Data Deleted Successfully ');
    }
}
