<?php

namespace App\Http\Controllers\Admin;
use DataTables;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Models\Admin\Course;
use App\Models\Admin\Category;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CourseController extends Controller
{
   public function index(Request $request){
   
    if ($request->ajax()) {
        $data = Course::with('category')->latest()->get();
        return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                       $btn  ='<a href="'. route('courses.edit', [encrypt($row->course_id)]) .'" class="edit btn btn-primary btn-success">Edit</a> '; 
                    //    $btn .='<a href="'. route('courses.view', [encrypt($row->course_id)]) .'"  class="edit btn btn-primary btn-sm">View</a> ';
                       $btn .='<a href="'. route('courses.delete', [encrypt($row->course_id)]) .'" onclick="return confirmDelete();" class="edit btn btn-primary btn-danger">Delete</a>';
 
                        return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
    }
        $data['page_title'] = 'Course';
        $data['page_sub_title'] = 'Records';
       return view('admin.courses.index',compact('data'));
   }
   public function new(){
    $data['page_title'] = 'New';
    $data['page_sub_title'] = 'Record';
    $sel_categories =  Category::all();
    // dd($categories);
    return view('admin.courses.new',compact('data','sel_categories'));
    }
    public function save(Request $request){
     
          
        $validator = Validator::make($request->all(),['name' => 'required','category_id' => 'required','over_view' => 'required',
        'outline' => 'required','category_id' => 'required','image' => 'required|mimes:jpeg,png,jpg,gif,svg|max:2048',
        'banner_image' => '|mimes:jpeg,png,jpg,gif,svg|dimensions:min_width=1920,min_height=500|max:2048','brochure' => 'mimes:doc,pdf,docx,zip'],
        ['category_id.required' => 'Please Select a Category!','banner_image.required' => 'The banner image should be width:1920 height:500 pixels !',
        ]);
        if ($validator->passes()){
            $input = request()->except('_token','image','brochure','banner_image');
            $created_by = Auth::id();
            $updated_by = Auth::id();
            $input['created_by'] = $created_by; 
            $input['updated_by'] = $updated_by; 
            if ($request->hasFile('image')){
                $filename = time()."_".str_random(40).'.'.request('image')->extension();
                $request->image->storeAs('courses/courseimage',$filename);
                $input['image'] = $filename;      
            }
            if ($request->hasFile('brochure')){
                $filename = time()."_".str_random(40).'.'.request('brochure')->extension();
                $request->brochure->storeAs('courses/brochure',$filename);
                $input['brochure'] = $filename;      
            }
            if ($request->hasFile('banner_image')){
                $filename = time()."_".str_random(40).'.'.request('banner_image')->extension();
                $request->banner_image->storeAs('courses/bannerimage',$filename);
                $input['banner_image'] = $filename;      
            }
            Course::create($input);
            return back()->with('message','Successfully Created New Course');
        }else{
            return back()->withErrors($validator)->withInput(Input::all());
        }
    
    
    }
    public function edit($course_id){
        $course = Course::find(decrypt($course_id));
        $data['page_title']='Edit';
        $data['page_sub_title']='Record';
        $allcategories =  Category::all();
        
        // dd($categories);
        return view('admin.courses.edit',compact('course','data','allcategories'));
    }
    public function update(Request $request){
       
        $validator = Validator::make($request->all(),
        ['name' => 'required','over_view' => 'required','brochure' => 'required|mimes:doc,pdf,docx,zip','outline' => 'required','category_id' => 'required','brochure' => 'mimes:doc,pdf,docx,zip',
        'banner_image' => 'mimes:jpeg,png,jpg,gif,svg|dimensions:min_width=1920,min_height=500|max:2048',
        ['banner_image.required' => 'The banner image should be width:1920 height:500 pixels !',
        ]]);
        if ($validator->passes())
        {
            $input = request()->except('_token','image','brochure');
            $basicInfo = Course::find(request('course_id'));

            if ($request->hasFile('image')) {
                if($basicInfo->image){
                $exists = Storage::disk('local')->exists('courses/courseimage/'.$basicInfo->image);

                    if($exists){
                        Storage::disk('local')->delete('courses/courseimage/'.$basicInfo->image);
                    }
                }
                $filename = time()."_".str_random(40).'.'.request('image')->extension();
                $request->image->storeAs('courses/courseimage/',$filename);
                $input['image'] = $filename;
            }
            if ($request->hasFile('brochure')) {
                if($basicInfo->brochure){
                $exists = Storage::disk('local')->exists('courses/brochure/'.$basicInfo->brochure);

                    if($exists){
                        Storage::disk('local')->delete('courses/brochure/'.$basicInfo->brochure);
                    }
                }
                $filename = time()."_".str_random(40).'.'.request('brochure')->extension();
                $request->brochure->storeAs('courses/brochure/',$filename);
                $input['brochure'] = $filename;
            }
            if ($request->hasFile('banner_image')) {
                if($basicInfo->banner_image){
                $exists = Storage::disk('local')->exists('courses/bannerimage/'.$basicInfo->banner_image);

                    if($exists){
                        Storage::disk('local')->delete('courses/bannerimage/'.$basicInfo->banner_image);
                    }
                }
                $filename = time()."_".str_random(40).'.'.request('banner_image')->extension();
                $request->banner_image->storeAs('courses/bannerimage/',$filename);
                $input['banner_image'] = $filename;
            }
            Course::find(request('course_id'))->update($input);
            return back()->with('message','Successfully Updated Course');
        }else{
            return back()->withErrors($validator);
        }
    }
    public function delete($course_id){
        $course = Course::find(decrypt($course_id));
        $exists = Storage::disk('local')->exists('courses/image'.$course->image);
         if($exists){
           Storage::disk('local')->delete('courses/image/'.$course->image);
         }
         $exists = Storage::disk('local')->exists('courses/brochure'.$course->brochure);
         if($exists){
            Storage::disk('local')->delete('courses/brochure/'.$course->brochure);
          }
          $exists = Storage::disk('local')->exists('courses/bannerimage/'.$course->banner_image);
          if($exists){
            Storage::disk('local')->delete('courses/bannerimage/'.$course->banner_image);
          }
         $course->delete();
         return redirect(route('admin.courses'))->with('message','Successfully Deleted Course');
     }
    public function view($course_id){
        $data['page_title']='View';
        $data['page_sub_title']='Record';
        try{
            $course = Course::with(['user'])->find(decrypt($course_id));
            return view('admin.courses.view',compact('course','data'));
        }catch(\Exception $ex){
            return back()->with('message','Invalid Course');
        }
    }
}
