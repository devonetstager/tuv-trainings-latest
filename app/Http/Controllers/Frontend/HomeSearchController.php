<?php

namespace App\Http\Controllers\Frontend;
use App\Models\Admin\Category;
use App\Models\Admin\Course;
use App\Models\Admin\CourseDetail;
use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeSearchController extends Controller
{
    // Frontend Course Search 
   public function course_search(Request $request){
    
        // input req parameters 
        
       $course_name  = $request->course;
       $category_id  = $request->category;
       $month = $request->month;
       // query string result    
    //    $results = Course::with(['category'=>
    //     function ($query) use ($category_id,$month){

    //    $query->whereMonth('start_date',$month)->where('status', 1);
    //     }
    //    ])
    //    ->where('category_id', $category_id)
    //    ->where('name', 'like',"%$course_name%")
    //         ->get();
     
    //     
    /*$results = Course::join('course_details as tcd','tcd.course_id','=','courses.course_id')
    ->where('tcd.status',1);

    if($course_name){
        $results->where('courses.name', 'like',"%$course_name%")->get();
        // dd($results); 
        }
    if($category_id){
        $results->where('courses.category_id',$category_id);
    }
    if($month){
            $results->whereMonth('tcd.start_date',$month);  
    }
    $results = $results->get();   
    // dd($results);
    */
            $results = Course::with('coursedetails')->where(function($query) use ($course_name,$category_id,$month){
                if($course_name){
                    $query->where('name', 'like',"%$course_name%")->get();
                    // dd($results); 
                    }
                if($category_id){
                    if($category_id=="allcategory"){
                        $query->orderBy('category_id','ASC');
                    }else{
                        $query->where('category_id',$category_id);
                    }
                   
                }
                if($month){
                    if($month=="allmonth"){
                        $query->whereHas('coursedetails', function($q)use ($month){
                            $q->orderBy('start_date','ASC');                        
                        });
                    }else{
                    $query->whereHas('coursedetails', function($q)use ($month){
                        $q->whereMonth('start_date',$month);                        
                    });
                    }
                }
            });
        
            $results = $results->get();  
    // dd($month); 

    $categories = Category::orderBy('category_id', 'DESC')->get();
    $maincategory = Category::orderBy('name', 'ASC')->get();
     $months = array(1 => 'January', 2 => 'February', 3 => 'March', 4 => 'April',
         5 => 'May', 6 => 'June',7 => 'July', 8 => 'August', 9 => 'September', 10 => 'October',
          11 => 'November', 12 => 'December');   
       return view('frontend.course-search-result',compact('categories','results','maincategory','months'));

   } 
   public function search_auto_complete(Request $request){
      
        if($request->ajax())
        {
        $query = $request->get('query');
       
        $data = Course::select('name')->where('name','like',"%$query%")->get(); 
         
        $output = '<ul class="dropdown-menu" style="display:block; position:relative">';
        foreach($data as $row)
        {
        $output .= '
        <li><a href="#">'.$row->name.'</a></li>
        ';
        }
        $output .= '</ul>';
        echo $output;
        }
    
      

   }
   public function fetch_scourse(){
        $category_id = $_GET['category_id'];
      
        $query = Course::where('category_id', '=', $category_id)->get(); 
       
        $data =  '<option value="">Select Course</option>';
		foreach ($query as $row){
          $data .= '<option value="'.$row->course_id.'">'.$row->name.'</option>';
       
        }
        echo json_encode($data);
    }
   
   
}