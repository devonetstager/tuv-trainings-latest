<?php

namespace App\Http\Controllers\Frontend;
use App\Models\Admin\Category;
use App\Models\Admin\Course;
use App\Models\Admin\CourseDetail;
use App\Models\Admin\Gallery;
use App\Models\Admin\GalleryDetail;
use App\Models\Admin\HomeBanner;
use App\Models\Admin\Testimonial;
use App\Models\Admin\Location;
use App\Models\Admin\Contact;
use App\Models\Admin\News;
use App\Models\Admin\Currency;
use App\Models\Admin\Enquiry;
use App\Models\Admin\SixSigmaCourse;
use App\Models\Admin\HrciCourse;
use App\Models\Admin\Qualification;
use App\Models\Admin\Webinar;
use App\Models\Admin\FAQ;
use App\Models\Admin\Cmspage;
use App\Models\Admin\PressRelease;
use App\Models\Admin\AppreciationLetter;
use App\Models\Admin\Clientele;
use  App\Models\Admin\City;
use App\Models\Admin\CIPS;
use Illuminate\Http\Request;
use Mail;
use App\Mail\CourseRegistration;
use App\Mail\QuickEnquiry;
use App\Mail\BrochureSend;
use App\Mail\CalanderSend;
use App\Mail\CertificateVerificationEnquiry;
use App\Mail\BecomeAPartnerEnquiry;
use App\Mail\ContactEnquiry;
use App\Mail\OffersEnquiry;
use Session;
use Illuminate\Support\Facades\Validator;
class HomeController
{
    public function index(){
        $data_now = date('Y-m-d');
        $data_now_two_month = date('Y-m-d' , strtotime("first day of +1 month")); 
        $data_now_three_month = date('Y-m-d' , strtotime("first day of +2 month")); 
        $month =  date('m', strtotime($data_now));
        $courses = Course::select('name','image','course_id')->whereHas('coursedetails', function($query) use ($month) {
             $query->select(['location','start_date','end_date'])->whereMonth('start_date',$month)->orderBy('start_date', 'DESC');
                })->where('status',0)->where('show_home',1)->limit(18)->get();
        
        $newsdata = News::where('status',1)->where('show_home',1)->get();
        $homebanner = HomeBanner::where('status',1)->where('show_home',1)->get();
        $testimonials = Testimonial::where('status',1)->where('show_home',1)->get();
        $sigmacourses = SixSigmaCourse::where('status',1)->where('show_home',1)->get();
        $hrcicourses = HrciCourse::where('status',1)->get();
        $cipscourses = CIPS::where('status',1)->where('show_home',1)->get();
        $qualifications = Qualification::orderBy('order_by', 'ASC')->get()->where('show_home',1);
        $webinars = Webinar::where('status',1)->latest()->limit(6)->get();
        $months = array(1 => 'January', 2 => 'February', 3 => 'March', 4 => 'April',
         5 => 'May', 6 => 'June',7 => 'July', 8 => 'August', 9 => 'September', 10 => 'October',
          11 => 'November', 12 => 'December');   
          $title="Welcome to TÜV MIDDLE EAST - Home Page";    
               
        return view('frontend.index',compact('title','homebanner','testimonials','courses','newsdata','sigmacourses','hrcicourses','cipscourses','months','qualifications','webinars'));
    }
    public  function sub_categories($category_id){  
        try{    
            $subcategories = Category::where('master_id',decrypt($category_id))->where('status',1)->get();
            $data = Category::find(decrypt($category_id));
            return view('frontend.sub-categories',compact('subcategories','data'));
        }catch(\Exception $ex){
            return back()->with('err_message','Invalid Data');
        }
    }
    public  function registration(){        
        $categories = Category::orderBy('name', 'ASC')->get();      
        return view('frontend.registration',compact('categories'));
    }
    public  function course_registration($course_id){ 
        $categories = Category::orderBy('name', 'ASC')->get();
        $courses = Course::orderBy('name','ASC')->get();
        $selectedcourse = Course::find(decrypt($course_id));
        $months = array(1 => 'January.', 2 => 'February', 3 => 'March', 4 => 'April',
        5 => 'May', 6 => 'June',7 => 'July', 8 => 'August', 9 => 'September', 10 => 'October',
         11 => 'November', 12 => 'December'); 
        $title="TÜV  Middle East - $selectedcourse->name";   
        return view('frontend.course-registration',compact('selectedcourse','categories','courses','course_id','months','title'));
    }
   public  function sigma_registration($six_sigma_id){
        $categories = Category::orderBy('name', 'ASC')->get();
        $sigmacourses =  SixSigmaCourse::orderBy('name', 'ASC')->get();
       
        $selectedcourse = SixSigmaCourse::find(decrypt($six_sigma_id)); 
        $city = City::find($selectedcourse->city_id);
        $selectedcourse['city']=$city;       
        //   return $city;    
       return view('frontend.sigmaregister',compact('categories','sigmacourses','selectedcourse'));
    }
   public  function hrci_registration($hrci_id){
        $categories = Category::orderBy('name', 'ASC')->get();
        $hrcicourses =  HrciCourse::orderBy('name', 'ASC')->get();
        $selectedcourse = HrciCourse::find(decrypt($hrci_id));    
        $city = City::find($selectedcourse->city_id);
        $selectedcourse['city']=$city;
        return view('frontend.hrciregister',compact('categories','hrcicourses','selectedcourse'));
    }
    public  function tuv_middle_east(){
        $categories = Category::orderBy('name', 'ASC')->get();
        $cmspage = Cmspage::find(1);
        $title ="$cmspage->title";
        return view('frontend.tuv-middle-east',compact('categories','cmspage','title'));
    }
   public  function contact_us(){  
    $title="TÜV  Middle East - Contact Us";      
     
        $contact = Contact::first();
        return view('frontend.contactus',compact('title','contact'));
    }
    // partner enquiry
    public function enquiry(Request $request){
        $validator = Validator::make(request()->all(),['recaptcha' => 'required','name'=>'required','email'=>'required',
        'address'=>'required','company'=>'required','mobile'=>'required|numeric']);
        $captcha = request()->recaptcha;     
        $response = json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6Lfjh7oUAAAAAKbhi3dDUPNe5eMA1iZPtbRS3N1Y&response=".$captcha."&remoteip=".$_SERVER['REMOTE_ADDR']), true);
        if($response['success'] == false){ 
            return back()->withInput()->withErrors($validator);
        }else{
            if($validator->passes()){ 
                $data = array(
                    'type'=>6,'enquiry_url'=>$request->enquiry_url,
                    'name'=>$request->name,
                    'mobile'=>$request->mobile, 
                    'email'=>$request->email,
                    'company'=>$request->company,
                    'address'=>$request->address, 
                    'message'=>$request->message,          
                );   
                if ($request->hasFile('document')){           
                    $filename = time()."_".str_random(40).'.'.request('document')->extension();
                    $request->document->storeAs('enquiry/document',$filename);
                    $data['document'] = $filename;   
                        
                    } 
                Enquiry::create($data);
                Mail::to('hopei@tuv-nord.me')->send(new BecomeAPartnerEnquiry($data));
                $categories = Category::all();
                return view('frontend.enquiry',compact('categories'));
            }else{
                return back()->withInput()->withErrors($validator);
            }    
        }
        
    }
    public  function corporate_news(){    
        $title="NEWS";         
        return view('frontend.corporate-news',compact('title'));
    }
   public  function course_detail($course_id){      
        $categories = Category::all();
         $months = array(1 => 'January.', 2 => 'February', 3 => 'March', 4 => 'April',
        5 => 'May', 6 => 'June',7 => 'July', 8 => 'August', 9 => 'September', 10 => 'October',
         11 => 'November', 12 => 'December'); 
        $allcategories = Category::orderBy('name', 'ASC')->where('show_home',1)->where('status',1)->get();
        $category_id = Course::where('course_id',decrypt($course_id))->first('category_id');       
        $related_course = Course::where('category_id', $category_id->category_id)->take(5)->get();
        $datas = Course::with(['category','coursedetails'])->find(decrypt($course_id));
        $coursedetails = CourseDetail::whereHas('currency', function($query) use($course_id) {
            $query->where('status', '=',1);
        })->with([
            'currency' => function($q1) use($course_id)  {               
                    $q1;
                }])->where('course_id',(decrypt($course_id)))->first();
        $title="TÜV  Middle East - $datas->name";         
        return view('frontend.course-detail',compact('title','categories','datas','related_course','allcategories','coursedetails','months'));
    }
    public  function courses($category_id){
        $allcategories = Category::orderBy('name', 'ASC')->get();
         $months = array(1 => 'January', 2 => 'February', 3 => 'March', 4 => 'April',
         5 => 'May', 6 => 'June',7 => 'July', 8 => 'August', 9 => 'September', 10 => 'October',
          11 => 'November', 12 => 'December');   
        $category_id = decrypt($category_id);
        $datas = Course::all()->where('category_id',$category_id);  
        $category_name = Category::find($category_id);
         $title="TÜV  Middle East - $category_name->name";       
        return view('frontend.courses',compact('title','allcategories','datas','category_id','category_name','months'));
    }

    public function course_filter($month){
        // dd($month);
        $courses = Course::whereHas('coursedetails', function($query) use ($month) {
            $query->whereMonth('start_date',$month);
        })->get();
        $title="Courses";   
        return view('frontend.course-filter',compact('title','courses'));

    }
    public function newslist(){
        $newsdata = News::where('news_category',1)->get();
        $title="TÜV Middle East - Corporate News";   
        return view('frontend.newslist',compact('title','newsdata'));

    }
    public  function sigma_course_detail($six_sigma_id){      
        $categories = Category::all();
        $sigmacourses = SixSigmaCourse::limit(5)->get();
        $datas = SixSigmaCourse::find(decrypt($six_sigma_id));
        $currency = Currency::find($datas->currency_id);
        $datas['currency'] =$currency;
        $title="TÜV  Middle East - $datas->name";                
        return view('frontend.sigma-details',compact('title','categories','datas','sigmacourses'));
    }
   public  function hrci_course_detail($hrci_id){     
        $categories = Category::all();
       $hrcicourses = HrciCourse::inRandomOrder()->limit(4)->get();
       $datas = HrciCourse::find(decrypt($hrci_id));
       $currency = Currency::find($datas->currency_id);
       $datas['currency'] =$currency; 
       $title="TÜV  Middle East - $datas->name";             
       return view('frontend.hrci-details',compact('title','categories','datas','hrcicourses'));
   }
    public  function news_detail($news_id){      
        $categories = Category::all();
        $datas = News::find(decrypt($news_id)); 
        $newsdata = News::inRandomOrder()->limit(6)->get();  
        $title="TÜV  Middle East - $datas->title";             
        return view('frontend.news-details',compact('title','categories','datas','newsdata'));
    }
    public  function user_course_registration(Request $request){
        $validator = Validator::make($request->all(),['recaptcha' => 'required','name'=>'required','email'=>'required',
                                                        'address'=>'required','company'=>'required','mobile'=>'required|numeric']);
                        
        $captcha = $request->recaptcha;     
        $response = json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6Lfjh7oUAAAAAKbhi3dDUPNe5eMA1iZPtbRS3N1Y&response=".$captcha."&remoteip=".$_SERVER['REMOTE_ADDR']), true);
        if($response['success'] == false){ 
        return back()->withInput()->withErrors($validator);
        }else{
            if($validator->passes()){
                $data = [
                    'type'=>1,'enquiry_url'=>$request->enquiry_url,
                    'name'=>$request->name,
                    'course_id'=>decrypt($request->course_id),
                    'mobile'=>$request->mobile, 
                    'email'=>$request->email,
                    'address'=>$request->address, 
                    'company'=>$request->company,
                    'post'=>$request->post,
                    'city'=>$request->city,
                    'country'=>$request->country,
                ];
                Enquiry::create($data);
                Mail::to($request->email)->queue(new CourseRegistration($data));
            }else{
                return back()->withInput()->withErrors($validator);
        }    } 
       
        $title="registration";        
        return redirect()->route('success');
    }
    public  function sigma_course_registration(Request $request){
        $validator = Validator::make($request->all(),['recaptcha' => 'required','name'=>'required','email'=>'required',
        'address'=>'required','company'=>'required','mobile'=>'required|numeric']);
        $captcha = $request->recaptcha;     
        $response = json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6Lfjh7oUAAAAAKbhi3dDUPNe5eMA1iZPtbRS3N1Y&response=".$captcha."&remoteip=".$_SERVER['REMOTE_ADDR']), true);
        if($response['success'] == false){ 
            return back()->withInput()->withErrors($validator);
        }else{
            if($validator->passes()){
            $data = [
                'type'=>2,'enquiry_url'=>$request->enquiry_url,
                'name'=>$request->name,
                'six_sigma_id'=>$request->six_sigma_id,
                'mobile'=>$request->mobile, 
                'email'=>$request->email,
                'address'=>$request->address, 
                'company'=>$request->company,
                'post'=>$request->post,
                'city'=>$request->city,
                'country'=>$request->country,
            ];
            Enquiry::create($data);
            Mail::to($request->email)->queue(new CourseRegistration($data)); 
            $title="registration";       
            return view('frontend.success',compact('title'));
            }else{
                return back()->withInput()->withErrors($validator);
            }
        }
       
    }
    public  function hrci_course_registration(Request $request){
        $validator = Validator::make($request->all(),['recaptcha' => 'required','name'=>'required','email'=>'required',
        'address'=>'required','company'=>'required','mobile'=>'required|numeric']);
        $captcha = $request->recaptcha;     
        $response = json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6Lfjh7oUAAAAAKbhi3dDUPNe5eMA1iZPtbRS3N1Y&response=".$captcha."&remoteip=".$_SERVER['REMOTE_ADDR']), true);
        if($response['success'] == false){ 
            return back()->withInput()->withErrors($validator);
        }else{
            if($validator->passes()){
                $data = [
                    'type'=>3,'enquiry_url'=>$request->enquiry_url,
                    'name'=>$request->name,
                    'hrci_id'=>$request->hrci_id,
                    'mobile'=>$request->mobile, 
                    'email'=>$request->email,
                    'address'=>$request->address, 
                    'company'=>$request->company,
                    'post'=>$request->post,
                    'city'=>$request->city,
                    'country'=>$request->country,
                ];
                Enquiry::create($data);
                Mail::to($request->email)->send(new CourseRegistration($data));
            }else{
                return back()->withInput()->withErrors($validator);
            }
        }
        
        $title="registration";       
        return view('frontend.success',compact('title'));
    }
    public function quick_enquiry(Request $request){   
      $validator = Validator::make($request->all(),['name'=>'required','email'=>'required']);
         if($validator->passes()){
        $data = ['type'=>0,'enquiry_url'=>$request->enquiry_url,
            'name'=>$request->name,
            'mobile'=>$request->mobile, 
            'email'=>$request->email,
            'address'=>$request->address,
            'message'=>$request->message
        ]; 
        Enquiry::create($data);    
        Mail::to('hopei@tuv-nord.me')->send(new QuickEnquiry($data));
        $categories = Category::all();
        return view('frontend.enquiry',compact('categories'));
        }else{
        return view('frontend.enquiry',compact('categories'));

        }
    }
    public function contact_enquiry(Request $request){ 
      $validator = Validator::make($request->all(),['recaptcha' => 'required','name'=>'required','email'=>'required']);
        $captcha = $request->recaptcha;     
        $response = json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6Lfjh7oUAAAAAKbhi3dDUPNe5eMA1iZPtbRS3N1Y&response=".$captcha."&remoteip=".$_SERVER['REMOTE_ADDR']), true);
    
    if($response['success'] == false){
      return back()->withInput()->withErrors($validator);
    }
    else{
        if($validator->passes()){
            $data = ['type'=>8,'enquiry_url'=>$request->enquiry_url,
            'name'=>$request->name,
            'mobile'=>$request->mobile, 
            'email'=>$request->email,
            'address'=>$request->company,
            'message'=>$request->message
        ]; 
        Enquiry::create($data);    
        Mail::to('hopei@tuv-nord.me')->queue(new ContactEnquiry($data));
        }else{
            return back()->withInput()->withErrors($validator);
        } 
    }
      
        $categories = Category::all();
        return view('frontend.enquiry',compact('categories'));
    }
    public function brochure_send(Request $request,$course_id){
        $validator = Validator::make($request->all(),['name'=>'required','email'=>'required']);  
         if($validator->passes()){    
        $data = Course::find(decrypt($course_id));       
        $data['customer_name'] = $request->name;
        $input=['type'=> 4,'enquiry_url'=>$request->enquiry_url,'name'=>$request->name,'mobile'=>$request->mobile,'email'=>$request->email];
       
        Enquiry::create($input);  
        Mail::to($request->email)->send(new BrochureSend($data));
        $categories = Category::all();
        return view('frontend.brochure',compact('categories'));
         }else{
        return view('frontend.brochure',compact('categories'));
         }


    }

    public function calanderDownload(Request $request){  
         $validator = Validator::make($request->all(),['name'=>'required','email'=>'required']);  
         if($validator->passes()){ 
        $data = [
            'type'=>5,'enquiry_url'=>$request->enquiry_url,
            'name'=>$request->name,
            'mobile'=>$request->mobile, 
            'email'=>$request->email,
        ];
        Enquiry::create($data);    
        $data['customer_name'] = request('name');  
        Mail::to(request('email'))->queue(new CalanderSend($data));
        return view('frontend.calanderDownload');
        }else{
        return view('frontend.calanderDownload');
         
        }
    }
    public function certificate_enquiry(Request $request){  
        $validator = Validator::make(request()->all(),['recaptcha' => 'required','name'=>'required','email'=>'required',
        'address'=>'required','company'=>'required','mobile'=>'required|numeric']);
        $captcha = request()->recaptcha;     
        $response = json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6Lfjh7oUAAAAAKbhi3dDUPNe5eMA1iZPtbRS3N1Y&response=".$captcha."&remoteip=".$_SERVER['REMOTE_ADDR']), true);
        if($response['success'] == false){ 
            return back()->withInput()->withErrors($validator);
        }else{
                if($validator->passes()){
                    $data = [
                        'type'=>7,'enquiry_url'=>$request->enquiry_url,
                        'name'=>$request->name,
                        'mobile'=>$request->mobile, 
                        'company'=>$request->company,
                        'email'=>$request->email,
                        'address'=>$request->address,
                        'country'=>$request->country,
                        'company'=>$request->company,
                        'message'=>$request->message
                    ]; 
                    if ($request->hasFile('document')){
                        $filename = time()."_".str_random(40).'.'.request('document')->extension();
                        $request->document->storeAs('enquiry/document',$filename);
                        $data['document'] = $filename;   
                            
                        } 
                    Enquiry::create($data);    
                    $data['customer_name'] = request('name');  
                    $title= "Success";
                    Mail::to('hopei@tuv-nord.me')->send(new CertificateVerificationEnquiry($data));
                return redirect()->route('success',compact('title'));
            }else{
                return back()->withInput()->withErrors($validator);
            }
        }    
    }

    public function qualification(){
        $title="TÜV Middle East - Qualification";  
        $qualifications = Qualification::orderBy('name', 'ASC')->where('show_home',1)->get();
        return view('frontend.qualifications',compact('qualifications','title'));
    }
       public function testimonials(){
        
        $testimonials = Testimonial::where('status',1)->get();
         $title="TÜV Middle East - Testimonials";  
        return view('frontend.testimonials',compact('testimonials','title'));
    }

    public function tuv_nord(){
       $cmspage = Cmspage::find(2);
         $title ="$cmspage->title";
        return view('frontend.tuv-nord',compact('cmspage','title'));

    }
    public function tuv_nord_training(){
         $cmspage = Cmspage::find(3);
        $title ="$cmspage->title";
        return view('frontend.tuv-nord-training',compact('cmspage','title'));
        
    }
   public function gallery(){
        
        $galleries = Gallery::latest()->paginate(15); ;
         $title="TÜV Middle East - Gallery";  
        return view('frontend.gallery',compact('galleries','title'));
        
    }
    public function gallery_details($gallery_id){
        $datas = GalleryDetail::where('gallery_id',$gallery_id)->paginate(15); 
        return view('frontend.gal-details',compact('datas'));
        
    }
    public function appreciation(){
          $letters = AppreciationLetter::all();
           $title="TÜV Middle East - Appreciation Letters";  
       
        return view('frontend.appreciation',compact('letters','title'));
        
    }     
    public function press_release(){
        $press_release = PressRelease::all();
         $title="TÜV Middle East - Press Release";  
   
           return view('frontend.press-release',compact('press_release','title'));
           
    }
    public function clientele(){
            $clienteles = Clientele::all();
            $title="TÜV Middle East - Clientele";  
               return view('frontend.clientele',compact('clienteles','title'));
               
    }
    public function faq(){
        $faqs = FAQ::where('show_home',1)->get();
         $title="TÜV Middle East - FAQ";  
               return view('frontend.faq',compact('faqs','title'));
               
    }       
    public function blob(){
    $technews = News::where('news_category',0)->get();
     $title="TÜV Middle East - Blog";  
           return view('frontend.blob',compact('technews','title'));
           
    } 
 public function location(){
        $locations = Location::orderBy('location_id','asc')->where('show_home',1)->get();
         $title="TÜV Middle East - Location";  
               return view('frontend.location',compact('locations','title'));
               
    }
    public function conference(){
           $cmspage = Cmspage::find(6);
            $title="TÜV Middle East - CONFERENCES";  
               return view('frontend.conference',compact('cmspage','title'));
               
    }
    public function public_courses(){
        $title="TÜV Middle East - PUBLIC COURSES";  
               return view('frontend.public-courses',compact('title'));
               
    }
    public function integrated_learning(){
        $cmspage = Cmspage::find(5);
         $title="TÜV Middle East - INTEGRATED E-LEARNING";  
               return view('frontend.integrated-learning',compact('cmspage','title'));
               
    } 
      public function partner(){
        $title="TÜV  Middle East - Become a Partner";  
               return view('frontend.partner',compact('title'));
               
    } 
    public function inhouse_courses(){
       $cmspage = Cmspage::find(4);
        $title="TÜV Middle East - IN HOUSE COURSES";  
               return view('frontend.inhouse-courses',compact('cmspage','title'));
               
    }  
    public function verification(){
         $title="TÜV Middle East - VERIFICATIONS";  
               return view('frontend.certificates',compact('title'));
               
    } 
   public function privacy_policy(){
   
           return view('frontend.cookie-details');
           
    }     
    public function set_vat_cookie(Request $request){
        $data =  Session::put('vat_cookie', 1);
         json_encode($data);
    }
    public function nebosh_submit(Request $request){  
        $validator = Validator::make($request->all(),['recaptcha' => 'required','mobile'=>'required|numeric','name'=>'required','email'=>'required|email','city'=>'required']);
        $captcha = $request->recaptcha;     
        $response = json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6Lfjh7oUAAAAAKbhi3dDUPNe5eMA1iZPtbRS3N1Y&response=".$captcha."&remoteip=".$_SERVER['REMOTE_ADDR']), true);

        if($response['success'] == false){
            return back()->withInput()->withErrors($validator);
        }else{
            if($validator->passes()){
                $data = [
                    'type'=>9,'enquiry_url'=>$request->enquiry_url,
                    'name'=>$request->name,
                    'mobile'=>$request->mobile, 
                    'email'=>$request->email,
                    'city'=>$request->city,
                    'message'=>$request->message
                ]; 
            Enquiry::create($data);
            Mail::to('hopei@tuv-nord.me')->send(new OffersEnquiry($data));
            return redirect()->route('success');         
            }else{
                return back()->withInput()->withErrors($validator);
            }        
        }
    }
    public function nebosh(){
   
        return view('frontend.nebosh');
        
    } 
      public function success(){       
      
        return view('frontend.enquiry');
    }
                  

}
