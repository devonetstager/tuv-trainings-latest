<?php

namespace App\Http\Controllers\Frontend;
use App\Http\Controllers\Controller;
use App\Models\Admin\CIPS;
use App\Models\Admin\Enquiry;
use App\Models\Admin\Currency;
use App\Models\Admin\Category;
use App\Models\Admin\City;
use App\Mail\CourseRegistration;
use DB;
use Illuminate\Support\Facades\Validator;

class FrontendController
{
    public  function cips_course_details($cips_id){     
       $cipscourses = CIPS::inRandomOrder()->limit(4)->get();
       $datas = CIPS::find($cips_id);
       $currency = Currency::find($datas->currency_id);
       $datas['currency'] =$currency;
      
       $title="TÜV  Middle East - $datas->name";         
       return view('frontend.cips-details',compact('title','datas','cipscourses'));
   }
    public  function cips_registration($cips_id){
    $cipscourses =  CIPS::orderBy('name', 'ASC')->get();
    $selectedcourse = CIPS::find($cips_id);    
    $city = City::find($selectedcourse->city_id);
    $selectedcourse['city']=$city;
    $title = "TUV Middle East - $selectedcourse->name ";
    return view('frontend.cipsregister',compact('cipscourses','selectedcourse'));
    }
    public  function cips_course_registration(){
        $validator = Validator::make(request()->all(),['recaptcha' => 'required','name'=>'required','email'=>'required',
        'address'=>'required','company'=>'required','mobile'=>'required|numeric']);
        $captcha = request()->recaptcha;     
        $response = json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6Lfjh7oUAAAAAKbhi3dDUPNe5eMA1iZPtbRS3N1Y&response=".$captcha."&remoteip=".$_SERVER['REMOTE_ADDR']), true);
        if($response['success'] == false){ 
            return back()->withInput()->withErrors($validator);
        }else{
                if($validator->passes()){
                $data = [
                    'type'=>10,'enquiry_url'=>request()->enquiry_url,
                    'name'=>request()->name,
                    'cips_id'=>request()->cips_id,
                    'mobile'=>request()->mobile, 
                    'email'=>request()->email,
                    'address'=>request()->address, 
                    'company'=>request()->company,
                    'post'=>request()->post,
                    'city'=>request()->city,
                    'country'=>request()->country,
                ];
                Enquiry::create($data);
                Mail::to(request()->email)->send(new CourseRegistration($data)); 
                $title= "Registration - ".$data['name'];       
                return redirect()->route('success',compact('title'));
            }else{
                return back()->withInput()->withErrors($validator);
            }
        }    
    }
    public function page_not_found(){
    
        return view('frontend.pagenotfound');
    }  public function page(){
    
        return view('mails.calander');
    }
     public function show_login_form(){
    
        return view('auth.login');
    }
        public function courselist(){
         $months = array(1 => 'January', 2 => 'February', 3 => 'March', 4 => 'April',
         5 => 'May', 6 => 'June',7 => 'July', 8 => 'August', 9 => 'September', 10 => 'October',
          11 => 'November', 12 => 'December');
        $courses=DB::table('courses as t1')
    ->leftjoin('course_details as t2' , 't2.course_id' , '=' , 't1.course_id')
    ->where('t1.show_home' , 1)
    ->orderBy('t2.start_date' , 'asc')
    -> paginate(40);
    $allcategories = Category::orderBy('name', 'ASC')->get();
     $title="TÜV Middle East - Courses";
   
        return view('frontend.courselist',compact('title','courses','months','allcategories'));

    }
}
