<?php
namespace App\Http\ViewComposer;
use Illuminate\Contracts\View\View;
use App\Models\Admin\Contact;
use App\Models\Admin\Category;
class footerComposer{

    public function compose(View $view) {
        $contacts = Contact::where('status',1)->where('show_home',1)->get();
        $categories = Category::orderBy('name', 'ASC')->where('show_home',1)->where('master_id',0)->orWhere('master_id',1)->get();
        $view->with(['contacts'=>$contacts,'categories'=>$categories]);
    }
}



?>