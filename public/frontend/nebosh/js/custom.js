 $(document).ready(function () {
 	$('#hp-slider').owlCarousel({
 		items: 1,
 		margin: 0,
		autoplay:true,
		loop: true,
 	});
 	$('#news').owlCarousel({
 		loop: true,
 		margin: 0,
 		nav: true,
		dots: false,
 		navText: ['<img src="assets/img/news-arrow-left.png">', '<img src="assets/img/news-arrow-right.png">'],
 		items: 1,
 		singleItem: true,
 	});
 	$('#testimonials-slider').owlCarousel({
 		loop: true,
 		margin: 0,
 		nav: false,
 		items: 1,
 		singleItem: true,
		autoplay:true,
 	});
 	$('#qul-slider').owlCarousel({
 		loop: true,
 		margin: 10,
 		nav: true,
 		navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>'],
 		dots: false,
 		responsive: {
 			0: {
 				items: 1,
 			},
 			600: {
 				items: 2
 			},
			768: {
 				items: 3
 			},
 			1000: {
 				items: 4,
 				margin: 80,
 			}
 		}
 	});
	 
 
    AOS.init({
          offset:     120,

	  delay:      100,

	   easing: 'ease-in-sine',

	  duration:  500,

	  disable:    window.innerWidth < 768, // Condition when AOS should be disabled. e.g. 'mobile'

	  once:       true,

	  startEvent: 'DOMContentLoaded'
      });
	  
	  
$("a[href='#top']").click(function() {
  $("html, body").animate({ scrollTop: 0 }, "slow");
  
  return false;
});


 });


	$(window).scroll(function() {   
	  if ($(this).scrollTop() > 300) {
        $('#top').fadeIn();
    } else {
        $('#top').fadeOut();
    }
		
	});





