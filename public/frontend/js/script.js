$(function() {
        $("body").addClass("js");
        var e = $("#menu"),
            t = $(".menu-link"),
            o = $(".has-subnav");
        t.click(function(o) {
            o.preventDefault(), t.toggleClass("active1"), e.toggleClass("active1")
        }), o.click(function(e) {
            e.preventDefault(), $(this).toggleClass("active1").next("ul").toggleClass("active1")
        })
    }), $(window).resize(function() {
        $(window).width() > 925 && $("#menu").removeClass("active1")
    }), document.querySelector("#nav-toggle").addEventListener("click", function() {
        this.classList.toggle(".active1")
    }), $(window).scroll(function() {
        $("header").toggleClass("aniPos", $(this).scrollTop() > 0)
    }), $(function() {
        $("body").addClass("js"), $("#menu").find(".menu-item-has-children").prepend('<span class="has-subnav"></span>');
        var e = $("#menu"),
            t = $(".menu-link"),
            o = $(".has-subnav");
        t.click(function(o) {
            o.preventDefault(), t.toggleClass(".active"), e.toggleClass(".active")
        }), o.click(function(e) {
            e.preventDefault(), $(this).toggleClass("active1").next("a").next("ul").toggleClass("active1")
        }), $(".caret").click(function() {
            $(".dropdown-menu").toggle()
        })
    }), jQuery(".wpcf7-form-control-wrap").mouseover(function() {
        $obj = $("span.wpcf7-not-valid-tip", this), $obj.css("display", "none")
    }),
    function(e, t) {
        "use strict";
        var o = {
            item: 3,
            autoWidth: !1,
            slideMove: 1,
            slideMargin: 10,
            addClass: "",
            mode: "slide",
            useCSS: !0,
            cssEasing: "ease",
            easing: "linear",
            speed: 400,
            auto: !1,
            pauseOnHover: !1,
            loop: !1,
            slideEndAnimation: !0,
            pause: 2e3,
            keyPress: !1,
            controls: !0,
            prevHtml: "",
            nextHtml: "",
            rtl: !1,
            adaptiveHeight: !1,
            vertical: !1,
            verticalHeight: 500,
            vThumbWidth: 100,
            thumbItem: 10,
            pager: !0,
            gallery: !1,
            galleryMargin: 5,
            thumbMargin: 5,
            currentPagerPosition: "middle",
            enableTouch: !0,
            enableDrag: !0,
            freeMove: !0,
            swipeThreshold: 40,
            responsive: [],
            onBeforeStart: function(e) {},
            onSliderLoad: function(e) {},
            onBeforeSlide: function(e, t) {},
            onAfterSlide: function(e, t) {},
            onBeforeNextSlide: function(e, t) {},
            onBeforePrevSlide: function(e, t) {}
        };
        e.fn.lightSlider = function(t) {
            if (0 === this.length) return this;
            if (this.length > 1) return this.each(function() {
                e(this).lightSlider(t)
            }), this;
            var i = {},
                s = e.extend(!0, {}, o, t),
                l = {},
                n = this;
            i.$el = this, "fade" === s.mode && (s.vertical = !1);
            var a = n.children(),
                r = e(window).width(),
                d = null,
                c = 0,
                u = 0,
                h = !1,
                g = 0,
                p = "",
                f = 0,
                m = !0 === s.vertical ? "height" : "width",
                v = !0 === s.vertical ? "margin-bottom" : "margin-right",
                b = 0,
                y = 0,
                w = 0,
                $ = 0,
                C = null,
                x = "ontouchstart" in document.documentElement,
                T = {
                    chbreakpoint: function() {
                        if (r = e(window).width(), s.responsive.length) {
                            var t;
                            if (!1 === s.autoWidth && (t = s.item), r < s.responsive[0].breakpoint)
                                for (var o = 0; o < s.responsive.length; o++) r < s.responsive[o].breakpoint && (s.responsive[o].breakpoint, d = s.responsive[o]);
                            if (null != d)
                                for (var i in d.settings) d.settings.hasOwnProperty(i) && (void 0 !== l[i] && null !== l[i] || (l[i] = s[i]), s[i] = d.settings[i]);
                            if (!e.isEmptyObject(l) && r > s.responsive[0].breakpoint)
                                for (var n in l) l.hasOwnProperty(n) && (s[n] = l[n]);
                            !1 === s.autoWidth && b > 0 && w > 0 && t !== s.item && (f = Math.round(b / ((w + s.slideMargin) * s.slideMove)))
                        }
                    },
                    calSW: function() {
                        !1 === s.autoWidth && (w = (g - (s.item * s.slideMargin - s.slideMargin)) / s.item)
                    },
                    calWidth: function(e) {
                        var t = !0 === e ? p.find(".lslide").length : a.length;
                        if (!1 === s.autoWidth) u = t * (w + s.slideMargin);
                        else {
                            u = 0;
                            for (var o = 0; o < t; o++) u += parseInt(a.eq(o).width()) + s.slideMargin
                        }
                        return u
                    }
                };
            return (i = {
                doCss: function() {
                    return !(!s.useCSS || ! function() {
                        for (var e = ["transition", "MozTransition", "WebkitTransition", "OTransition", "msTransition", "KhtmlTransition"], t = document.documentElement, o = 0; o < e.length; o++)
                            if (e[o] in t.style) return !0
                    }())
                },
                keyPress: function() {
                    s.keyPress && e(document).on("keyup.lightslider", function(t) {
                        e(":focus").is("input, textarea") || (t.preventDefault ? t.preventDefault() : t.returnValue = !1, 37 === t.keyCode ? n.goToPrevSlide() : 39 === t.keyCode && n.goToNextSlide())
                    })
                },
                controls: function() {
                    s.controls && (n.after('<div class="lSAction"><a class="lSPrev">' + s.prevHtml + '</a><a class="lSNext">' + s.nextHtml + "</a></div>"), s.autoWidth ? T.calWidth(!1) < g && p.find(".lSAction").hide() : c <= s.item && p.find(".lSAction").hide(), p.find(".lSAction a").on("click", function(t) {
                        return t.preventDefault ? t.preventDefault() : t.returnValue = !1, "lSPrev" === e(this).attr("class") ? n.goToPrevSlide() : n.goToNextSlide(), !1
                    }))
                },
                initialStyle: function() {
                    var e = this;
                    "fade" === s.mode && (s.autoWidth = !1, s.slideEndAnimation = !1), s.auto && (s.slideEndAnimation = !1), s.autoWidth && (s.slideMove = 1, s.item = 1), s.loop && (s.slideMove = 1, s.freeMove = !1), s.onBeforeStart.call(this, n), T.chbreakpoint(), n.addClass("lightSlider").wrap('<div class="lSSlideOuter ' + s.addClass + '"><div class="lSSlideWrapper"></div></div>'), p = n.parent(".lSSlideWrapper"), !0 === s.rtl && p.parent().addClass("lSrtl"), s.vertical ? (p.parent().addClass("vertical"), g = s.verticalHeight, p.css("height", g + "px")) : g = n.outerWidth(), a.addClass("lslide"), !0 === s.loop && "slide" === s.mode && (T.calSW(), T.clone = function() {
                        if (T.calWidth(!0) > g) {
                            for (var t = 0, o = 0, i = 0; i < a.length && (o++, !((t += parseInt(n.find(".lslide").eq(i).width()) + s.slideMargin) >= g + s.slideMargin)); i++);
                            var l = !0 === s.autoWidth ? o : s.item;
                            if (l < n.find(".clone.left").length)
                                for (var r = 0; r < n.find(".clone.left").length - l; r++) a.eq(r).remove();
                            if (l < n.find(".clone.right").length)
                                for (var d = a.length - 1; d > a.length - 1 - n.find(".clone.right").length; d--) f--, a.eq(d).remove();
                            for (var c = n.find(".clone.right").length; c < l; c++) n.find(".lslide").eq(c).clone().removeClass("lslide").addClass("clone right").appendTo(n), f++;
                            for (var u = n.find(".lslide").length - n.find(".clone.left").length; u > n.find(".lslide").length - l; u--) n.find(".lslide").eq(u - 1).clone().removeClass("lslide").addClass("clone left").prependTo(n);
                            a = n.children()
                        } else a.hasClass("clone") && (n.find(".clone").remove(), e.move(n, 0))
                    }, T.clone()), T.sSW = function() {
                        c = a.length, !0 === s.rtl && !1 === s.vertical && (v = "margin-left"), !1 === s.autoWidth && a.css(m, w + "px"), a.css(v, s.slideMargin + "px"), u = T.calWidth(!1), n.css(m, u + "px"), !0 === s.loop && "slide" === s.mode && !1 === h && (f = n.find(".clone.left").length)
                    }, T.calL = function() {
                        a = n.children(), c = a.length
                    }, this.doCss() && p.addClass("usingCss"), T.calL(), "slide" === s.mode ? (T.calSW(), T.sSW(), !0 === s.loop && (b = e.slideValue(), this.move(n, b)), !1 === s.vertical && this.setHeight(n, !1)) : (this.setHeight(n, !0), n.addClass("lSFade"), this.doCss() || (a.fadeOut(0), a.eq(f).fadeIn(0))), !0 === s.loop && "slide" === s.mode ? a.eq(f).addClass("active") : a.first().addClass("active")
                },
                pager: function() {
                    var e = this;
                    if (T.createPager = function() {
                            $ = (g - (s.thumbItem * s.thumbMargin - s.thumbMargin)) / s.thumbItem;
                            var t = p.find(".lslide"),
                                o = p.find(".lslide").length,
                                i = 0,
                                l = "",
                                a = 0;
                            for (i = 0; i < o; i++) {
                                "slide" === s.mode && (s.autoWidth ? a += (parseInt(t.eq(i).width()) + s.slideMargin) * s.slideMove : a = i * ((w + s.slideMargin) * s.slideMove));
                                var r = t.eq(i * s.slideMove).attr("data-thumb");
                                if (!0 === s.gallery ? l += '<li style="width:100%;' + m + ":" + $ + "px;" + v + ":" + s.thumbMargin + 'px"><a href="#"><img src="' + r + '" /></a></li>' : l += '<li><a href="#">' + (i + 1) + "</a></li>", "slide" === s.mode && a >= u - g - s.slideMargin) {
                                    i += 1;
                                    var d = 2;
                                    s.autoWidth && (l += '<li><a href="#">' + (i + 1) + "</a></li>", d = 1), i < d ? (l = null, p.parent().addClass("noPager")) : p.parent().removeClass("noPager");
                                    break
                                }
                            }
                            var c = p.parent();
                            c.find(".lSPager").html(l), !0 === s.gallery && (!0 === s.vertical && c.find(".lSPager").css("width", s.vThumbWidth + "px"), y = i * (s.thumbMargin + $) + .5, c.find(".lSPager").css({
                                property: y + "px",
                                "transition-duration": s.speed + "ms"
                            }), !0 === s.vertical && p.parent().css("padding-right", s.vThumbWidth + s.galleryMargin + "px"), c.find(".lSPager").css(m, y + "px"));
                            var h = c.find(".lSPager").find("li");
                            h.first().addClass("active"), h.on("click", function() {
                                return !0 === s.loop && "slide" === s.mode ? f += h.index(this) - c.find(".lSPager").find("li.active").index() : f = h.index(this), n.mode(!1), !0 === s.gallery && e.slideThumb(), !1
                            })
                        }, s.pager) {
                        var t = "lSpg";
                        s.gallery && (t = "lSGallery"), p.after('<ul class="lSPager ' + t + '"></ul>');
                        var o = s.vertical ? "margin-left" : "margin-top";
                        p.parent().find(".lSPager").css(o, s.galleryMargin + "px"), T.createPager()
                    }
                    setTimeout(function() {
                        T.init()
                    }, 0)
                },
                setHeight: function(e, t) {
                    var o = null,
                        i = this;
                    o = s.loop ? e.children(".lslide ").first() : e.children().first();
                    var l = function() {
                        var i = o.outerHeight(),
                            s = 0,
                            l = i;
                        t && (i = 0, s = 100 * l / g), e.css({
                            height: i + "px",
                            "padding-bottom": s + "%"
                        })
                    };
                    l(), o.find("img").length ? o.find("img")[0].complete ? (l(), C || i.auto()) : o.find("img").load(function() {
                        setTimeout(function() {
                            l(), C || i.auto()
                        }, 100)
                    }) : C || i.auto()
                },
                active: function(e, t) {
                    this.doCss() && "fade" === s.mode && p.addClass("on");
                    var o, i, l = 0;
                    f * s.slideMove < c ? (e.removeClass("active"), this.doCss() || "fade" !== s.mode || !1 !== t || e.fadeOut(s.speed), l = !0 === t ? f : f * s.slideMove, !0 === t && (i = (o = e.length) - 1, l + 1 >= o && (l = i)), !0 === s.loop && "slide" === s.mode && (l = !0 === t ? f - n.find(".clone.left").length : f * s.slideMove, !0 === t && (i = (o = e.length) - 1, l + 1 === o ? l = i : l + 1 > o && (l = 0))), this.doCss() || "fade" !== s.mode || !1 !== t || e.eq(l).fadeIn(s.speed), e.eq(l).addClass("active")) : (e.removeClass("active"), e.eq(e.length - 1).addClass("active"), this.doCss() || "fade" !== s.mode || !1 !== t || (e.fadeOut(s.speed), e.eq(l).fadeIn(s.speed)))
                },
                move: function(e, t) {
                    !0 === s.rtl && (t = -t), this.doCss() ? !0 === s.vertical ? e.css({
                        transform: "translate3d(0px, " + -t + "px, 0px)",
                        "-webkit-transform": "translate3d(0px, " + -t + "px, 0px)"
                    }) : e.css({
                        transform: "translate3d(" + -t + "px, 0px, 0px)",
                        "-webkit-transform": "translate3d(" + -t + "px, 0px, 0px)"
                    }) : !0 === s.vertical ? e.css("position", "relative").animate({
                        top: -t + "px"
                    }, s.speed, s.easing) : e.css("position", "relative").animate({
                        left: -t + "px"
                    }, s.speed, s.easing);
                    var o = p.parent().find(".lSPager").find("li");
                    this.active(o, !0)
                },
                fade: function() {
                    this.active(a, !1);
                    var e = p.parent().find(".lSPager").find("li");
                    this.active(e, !0)
                },
                slide: function() {
                    var e = this;
                    T.calSlide = function() {
                        u > g && (b = e.slideValue(), e.active(a, !1), b > u - g - s.slideMargin ? b = u - g - s.slideMargin : b < 0 && (b = 0), e.move(n, b), !0 === s.loop && "slide" === s.mode && (f >= c - n.find(".clone.left").length / s.slideMove && e.resetSlide(n.find(".clone.left").length), 0 === f && e.resetSlide(p.find(".lslide").length)))
                    }, T.calSlide()
                },
                resetSlide: function(e) {
                    var t = this;
                    p.find(".lSAction a").addClass("disabled"), setTimeout(function() {
                        f = e, p.css("transition-duration", "0ms"), b = t.slideValue(), t.active(a, !1), i.move(n, b), setTimeout(function() {
                            p.css("transition-duration", s.speed + "ms"), p.find(".lSAction a").removeClass("disabled")
                        }, 50)
                    }, s.speed + 100)
                },
                slideValue: function() {
                    var e = 0;
                    if (!1 === s.autoWidth) e = f * ((w + s.slideMargin) * s.slideMove);
                    else {
                        e = 0;
                        for (var t = 0; t < f; t++) e += parseInt(a.eq(t).width()) + s.slideMargin
                    }
                    return e
                },
                slideThumb: function() {
                    var e;
                    switch (s.currentPagerPosition) {
                        case "left":
                            e = 0;
                            break;
                        case "middle":
                            e = g / 2 - $ / 2;
                            break;
                        case "right":
                            e = g - $
                    }
                    var t = f - n.find(".clone.left").length,
                        o = p.parent().find(".lSPager");
                    "slide" === s.mode && !0 === s.loop && (t >= o.children().length ? t = 0 : t < 0 && (t = o.children().length));
                    var i = t * ($ + s.thumbMargin) - e;
                    i + g > y && (i = y - g - s.thumbMargin), i < 0 && (i = 0), this.move(o, i)
                },
                auto: function() {
                    s.auto && (clearInterval(C), C = setInterval(function() {
                        n.goToNextSlide()
                    }, s.pause))
                },
                pauseOnHover: function() {
                    var t = this;
                    s.auto && s.pauseOnHover && (p.on("mouseenter", function() {
                        e(this).addClass("ls-hover"), n.pause(), s.auto = !0
                    }), p.on("mouseleave", function() {
                        e(this).removeClass("ls-hover"), p.find(".lightSlider").hasClass("lsGrabbing") || t.auto()
                    }))
                },
                touchMove: function(e, t) {
                    if (p.css("transition-duration", "0ms"), "slide" === s.mode) {
                        var o = b - (e - t);
                        if (o >= u - g - s.slideMargin)
                            if (!1 === s.freeMove) o = u - g - s.slideMargin;
                            else {
                                var i = u - g - s.slideMargin;
                                o = i + (o - i) / 5
                            }
                        else o < 0 && (!1 === s.freeMove ? o = 0 : o /= 5);
                        this.move(n, o)
                    }
                },
                touchEnd: function(e) {
                    if (p.css("transition-duration", s.speed + "ms"), "slide" === s.mode) {
                        var t = !1,
                            o = !0;
                        (b -= e) > u - g - s.slideMargin ? (b = u - g - s.slideMargin, !1 === s.autoWidth && (t = !0)) : b < 0 && (b = 0);
                        var i = function(e) {
                            var o = 0;
                            if (t || e && (o = 1), s.autoWidth)
                                for (var i = 0, l = 0; l < a.length && (i += parseInt(a.eq(l).width()) + s.slideMargin, f = l + o, !(i >= b)); l++);
                            else {
                                var n = b / ((w + s.slideMargin) * s.slideMove);
                                f = parseInt(n) + o, b >= u - g - s.slideMargin && n % 1 != 0 && f++
                            }
                        };
                        e >= s.swipeThreshold ? (i(!1), o = !1) : e <= -s.swipeThreshold && (i(!0), o = !1), n.mode(o), this.slideThumb()
                    } else e >= s.swipeThreshold ? n.goToPrevSlide() : e <= -s.swipeThreshold && n.goToNextSlide()
                },
                enableDrag: function() {
                    var t = this;
                    if (!x) {
                        var o = 0,
                            i = 0,
                            l = !1;
                        p.find(".lightSlider").addClass("lsGrab"), p.on("mousedown", function(t) {
                            if (u < g && 0 !== u) return !1;
                            "lSPrev" !== e(t.target).attr("class") && "lSNext" !== e(t.target).attr("class") && (o = !0 === s.vertical ? t.pageY : t.pageX, l = !0, t.preventDefault ? t.preventDefault() : t.returnValue = !1, p.scrollLeft += 1, p.scrollLeft -= 1, p.find(".lightSlider").removeClass("lsGrab").addClass("lsGrabbing"), clearInterval(C))
                        }), e(window).on("mousemove", function(e) {
                            l && (i = !0 === s.vertical ? e.pageY : e.pageX, t.touchMove(i, o))
                        }), e(window).on("mouseup", function(n) {
                            if (l) {
                                p.find(".lightSlider").removeClass("lsGrabbing").addClass("lsGrab"), l = !1;
                                var a = (i = !0 === s.vertical ? n.pageY : n.pageX) - o;
                                Math.abs(a) >= s.swipeThreshold && e(window).on("click.ls", function(t) {
                                    t.preventDefault ? t.preventDefault() : t.returnValue = !1, t.stopImmediatePropagation(), t.stopPropagation(), e(window).off("click.ls")
                                }), t.touchEnd(a)
                            }
                        })
                    }
                },
                enableTouch: function() {
                    var e = this;
                    if (x) {
                        var t = {},
                            o = {};
                        p.on("touchstart", function(e) {
                            o = e.originalEvent.targetTouches[0], t.pageX = e.originalEvent.targetTouches[0].pageX, t.pageY = e.originalEvent.targetTouches[0].pageY, clearInterval(C)
                        }), p.on("touchmove", function(i) {
                            if (u < g && 0 !== u) return !1;
                            var l = i.originalEvent;
                            o = l.targetTouches[0];
                            var n = Math.abs(o.pageX - t.pageX),
                                a = Math.abs(o.pageY - t.pageY);
                            !0 === s.vertical ? (3 * a > n && i.preventDefault(), e.touchMove(o.pageY, t.pageY)) : (3 * n > a && i.preventDefault(), e.touchMove(o.pageX, t.pageX))
                        }), p.on("touchend", function() {
                            if (u < g && 0 !== u) return !1;
                            var i;
                            i = !0 === s.vertical ? o.pageY - t.pageY : o.pageX - t.pageX, e.touchEnd(i)
                        })
                    }
                },
                build: function() {
                    var t = this;
                    t.initialStyle(), this.doCss() && (!0 === s.enableTouch && t.enableTouch(), !0 === s.enableDrag && t.enableDrag()), e(window).on("focus", function() {
                        t.auto()
                    }), e(window).on("blur", function() {
                        clearInterval(C)
                    }), t.pager(), t.pauseOnHover(), t.controls(), t.keyPress()
                }
            }).build(), T.init = function() {
                T.chbreakpoint(), !0 === s.vertical ? (g = s.item > 1 ? s.verticalHeight : a.outerHeight(), p.css("height", g + "px")) : g = p.outerWidth(), !0 === s.loop && "slide" === s.mode && T.clone(), T.calL(), "slide" === s.mode && n.removeClass("lSSlide"), "slide" === s.mode && (T.calSW(), T.sSW()), setTimeout(function() {
                    "slide" === s.mode && n.addClass("lSSlide")
                }, 1e3), s.pager && T.createPager(), !0 === s.adaptiveHeight && !1 === s.vertical && n.css("height", a.eq(f).outerHeight(!0)), !1 === s.adaptiveHeight && ("slide" === s.mode ? !1 === s.vertical ? i.setHeight(n, !1) : i.auto() : i.setHeight(n, !0)), !0 === s.gallery && i.slideThumb(), "slide" === s.mode && i.slide(), !1 === s.autoWidth ? a.length <= s.item ? p.find(".lSAction").hide() : p.find(".lSAction").show() : T.calWidth(!1) < g && 0 !== u ? p.find(".lSAction").hide() : p.find(".lSAction").show()
            }, n.goToPrevSlide = function() {
                if (f > 0) s.onBeforePrevSlide.call(this, n, f), f--, n.mode(!1), !0 === s.gallery && i.slideThumb();
                else if (!0 === s.loop) {
                    if (s.onBeforePrevSlide.call(this, n, f), "fade" === s.mode) f = parseInt((c - 1) / s.slideMove);
                    n.mode(!1), !0 === s.gallery && i.slideThumb()
                } else !0 === s.slideEndAnimation && (n.addClass("leftEnd"), setTimeout(function() {
                    n.removeClass("leftEnd")
                }, 400))
            }, n.goToNextSlide = function() {
                var e = !0;
                "slide" === s.mode && (e = i.slideValue() < u - g - s.slideMargin);
                f * s.slideMove < c - s.slideMove && e ? (s.onBeforeNextSlide.call(this, n, f), f++, n.mode(!1), !0 === s.gallery && i.slideThumb()) : !0 === s.loop ? (s.onBeforeNextSlide.call(this, n, f), f = 0, n.mode(!1), !0 === s.gallery && i.slideThumb()) : !0 === s.slideEndAnimation && (n.addClass("rightEnd"), setTimeout(function() {
                    n.removeClass("rightEnd")
                }, 400))
            }, n.mode = function(e) {
                !0 === s.adaptiveHeight && !1 === s.vertical && n.css("height", a.eq(f).outerHeight(!0)), !1 === h && ("slide" === s.mode ? i.doCss() && (n.addClass("lSSlide"), "" !== s.speed && p.css("transition-duration", s.speed + "ms"), "" !== s.cssEasing && p.css("transition-timing-function", s.cssEasing)) : i.doCss() && ("" !== s.speed && n.css("transition-duration", s.speed + "ms"), "" !== s.cssEasing && n.css("transition-timing-function", s.cssEasing))), e || s.onBeforeSlide.call(this, n, f), "slide" === s.mode ? i.slide() : i.fade(), p.hasClass("ls-hover") || i.auto(), setTimeout(function() {
                    e || s.onAfterSlide.call(this, n, f)
                }, s.speed), h = !0
            }, n.play = function() {
                n.goToNextSlide(), s.auto = !0, i.auto()
            }, n.pause = function() {
                s.auto = !1, clearInterval(C)
            }, n.refresh = function() {
                T.init()
            }, n.getCurrentSlideCount = function() {
                var e = f;
                if (s.loop) {
                    var t = p.find(".lslide").length,
                        o = n.find(".clone.left").length;
                    e = f <= o - 1 ? t + (f - o) : f >= t + o ? f - t - o : f - o
                }
                return e + 1
            }, n.getTotalSlideCount = function() {
                return p.find(".lslide").length
            }, n.goToSlide = function(e) {
                f = s.loop ? e + n.find(".clone.left").length - 1 : e, n.mode(!1), !0 === s.gallery && i.slideThumb()
            }, n.destroy = function() {
                n.lightSlider && (n.goToPrevSlide = function() {}, n.goToNextSlide = function() {}, n.mode = function() {}, n.play = function() {}, n.pause = function() {}, n.refresh = function() {}, n.getCurrentSlideCount = function() {}, n.getTotalSlideCount = function() {}, n.goToSlide = function() {}, n.lightSlider = null, T = {
                    init: function() {}
                }, n.parent().parent().find(".lSAction, .lSPager").remove(), n.removeClass("lightSlider lSFade lSSlide lsGrab lsGrabbing leftEnd right").removeAttr("style").unwrap().unwrap(), n.children().removeAttr("style"), a.removeClass("lslide active"), n.find(".clone").remove(), a = null, C = null, h = !1, f = 0)
            }, setTimeout(function() {
                s.onSliderLoad.call(this, n)
            }, 10), e(window).on("resize orientationchange", function(e) {
                setTimeout(function() {
                    e.preventDefault ? e.preventDefault() : e.returnValue = !1, T.init()
                }, 200)
            }), this
        }
    }(jQuery), $(document).ready(function() {
        $("#bannerSlide").lightSlider({
            item: 1,
            auto: !0,
            loop: !0,
            slideMove: 1,
            mode: "slide",
            addClass: "bannerSlide",
            easing: "cubic-bezier(0.25, 0, 0.25, 1)",
            speed: 2e3,
            pause: 6e3,
            adaptiveHeight: !0
        }), $("#brands").lightSlider({
            item: 6,
            auto: !0,
            loop: !0,
            slideMove: 1,
            mode: "slide",
            addClass: "brands",
            easing: "cubic-bezier(0.25, 0, 0.25, 1)",
            speed: 1e3,
            pause: 3e3,
            adaptiveHeight: !0,
            responsive: [{
                breakpoint: 1024,
                settings: {
                    item: 6,
                    slideMargin: 6
                }
            }, {
                breakpoint: 995,
                settings: {
                    item: 4,
                    slideMove: 1
                }
            }, {
                breakpoint: 767,
                settings: {
                    item: 3,
                    slideMove: 1
                }
            }, {
                breakpoint: 480,
                settings: {
                    item: 2,
                    slideMove: 1
                }
            }]
        }), $("#servicesSlider").lightSlider({
            item: 3,
            auto: !0,
            loop: !0,
            slideMove: 1,
            mode: "slide",
            addClass: "servicesSlider",
            easing: "cubic-bezier(0.25, 0, 0.25, 1)",
            speed: 1e3,
            pause: 3e3,
            adaptiveHeight: !0,
            responsive: [{
                breakpoint: 800,
                settings: {
                    item: 3,
                    slideMargin: 6
                }
            }, {
                breakpoint: 767,
                settings: {
                    item: 1,
                    slideMove: 1
                }
            }]
        })
    }),
    function(e) {
        var t, o, i = e(window),
            s = {},
            l = [],
            n = [],
            a = null,
            r = [],
            d = null,
            c = /(iPad|iPhone|iPod)/g.test(navigator.userAgent),
            u = {
                _init: function(t) {
                    var o = e(t),
                        i = o.data("popupoptions");
                    n[t.id] = !1, l[t.id] = 0, o.data("popup-initialized") || (o.attr("data-popup-initialized", "true"), u._initonce(t)), i.autoopen && setTimeout(function() {
                        u.show(t, 0)
                    }, 0)
                },
                _initonce: function(o) {
                    var i, s, l, n, r = e(o),
                        h = e("body"),
                        g = r.data("popupoptions");
                    (a = parseInt(h.css("margin-right"), 10), d = void 0 !== document.body.style.webkitTransition || void 0 !== document.body.style.MozTransition || void 0 !== document.body.style.msTransition || void 0 !== document.body.style.OTransition || void 0 !== document.body.style.transition, "tooltip" == g.type && (g.background = !1, g.scrolllock = !1), g.backgroundactive && (g.background = !1, g.blur = !1, g.scrolllock = !1), g.scrolllock) && (void 0 === t && (n = (l = e('<div style="width:50px;height:50px;overflow:auto"><div/></div>').appendTo("body")).children(), t = n.innerWidth() - n.height(99).innerWidth(), l.remove()));
                    if (r.attr("id") || r.attr("id", "j-popup-" + parseInt(1e8 * Math.random(), 10)), r.addClass("popup_content"), g.background && !e("#" + o.id + "_background").length) {
                        h.append('<div id="' + o.id + '_background" class="popup_background"></div>');
                        var p = e("#" + o.id + "_background");
                        p.css({
                            opacity: 0,
                            display: "none",
                            visibility: "hidden",
                            backgroundColor: g.color,
                            position: "fixed",
                            top: 0,
                            right: 0,
                            bottom: 0,
                            left: 0
                        }), g.setzindex && !g.autozindex && p.css("z-index", "100000"), g.transition && p.css("transition", g.transition)
                    }
                    h.append(o), r.wrap('<div id="' + o.id + '_wrapper" class="popup_wrapper" />'), (i = e("#" + o.id + "_wrapper")).css({
                        opacity: 0,
                        display: "none",
                        visibility: "hidden",
                        position: "absolute"
                    }), c && i.css("cursor", "pointer"), "overlay" == g.type && i.css("overflow", "auto"), r.css({
                        opacity: 0,
                        display: "none",
                        visibility: "hidden",
                        display: "inline-block"
                    }), g.setzindex && !g.autozindex && i.css("z-index", "100001"), g.outline || r.css("outline", "none"), g.transition && (r.css("transition", g.transition), i.css("transition", g.transition)), r.attr("aria-hidden", !0), "overlay" == g.type && (r.css({
                        textAlign: "left",
                        position: "relative",
                        verticalAlign: "middle"
                    }), s = {
                        position: "fixed",
                        width: "100%",
                        height: "100%",
                        top: 0,
                        left: 0,
                        textAlign: "center"
                    }, g.backgroundactive && (s.position = "absolute", s.height = "0", s.overflow = "visible"), i.css(s), i.append('<div class="popup_align" />'), e(".popup_align").css({
                        display: "inline-block",
                        verticalAlign: "middle",
                        height: "100%"
                    })), r.attr("role", "dialog");
                    var f = g.openelement ? g.openelement : "." + o.id + "_open";
                    e(f).each(function(t, o) {
                        e(o).attr("data-popup-ordinal", t), o.id || e(o).attr("id", "open_" + parseInt(1e8 * Math.random(), 10))
                    }), r.attr("aria-labelledby") || r.attr("aria-label") || r.attr("aria-labelledby", e(f).attr("id")), "hover" == g.action ? (g.keepfocus = !1, e(f).on("mouseenter", function(t) {
                        u.show(o, e(this).data("popup-ordinal"))
                    }), e(f).on("mouseleave", function(e) {
                        u.hide(o)
                    })) : e(document).on("click", f, function(t) {
                        t.preventDefault();
                        var i = e(this).data("popup-ordinal");
                        setTimeout(function() {
                            u.show(o, i)
                        }, 0)
                    }), g.closebutton && u.addclosebutton(o), g.detach ? r.hide().detach() : i.hide()
                },
                show: function(s, c) {
                    var g = e(s);
                    if (!g.data("popup-visible")) {
                        g.data("popup-initialized") || u._init(s), g.attr("data-popup-initialized", "true");
                        var p = e("body"),
                            f = g.data("popupoptions"),
                            m = e("#" + s.id + "_wrapper"),
                            v = e("#" + s.id + "_background");
                        if (h(s, c, f.beforeopen), n[s.id] = c, setTimeout(function() {
                                r.push(s.id)
                            }, 0), f.autozindex) {
                            for (var b = document.getElementsByTagName("*"), y = b.length, w = 0, $ = 0; $ < y; $++) {
                                var C = e(b[$]).css("z-index");
                                "auto" !== C && w < (C = parseInt(C, 10)) && (w = C)
                            }
                            l[s.id] = w, f.background && l[s.id] > 0 && e("#" + s.id + "_background").css({
                                zIndex: l[s.id] + 1
                            }), l[s.id] > 0 && m.css({
                                zIndex: l[s.id] + 2
                            })
                        }
                        f.detach ? (m.prepend(s), g.show()) : m.show(), o = setTimeout(function() {
                            m.css({
                                visibility: "visible",
                                display: "inline-block",
                                opacity: 1
                            }), e("html").addClass("popup_visible").addClass("popup_visible_" + s.id), m.addClass("popup_wrapper_visible")
                        }, 20), f.scrolllock && (p.css("overflow", "hidden"), p.height() > i.height() && p.css("margin-right", a + t)), f.backgroundactive && g.css({
                            top: (i.height() - (g.get(0).offsetHeight + parseInt(g.css("margin-top"), 10) + parseInt(g.css("margin-bottom"), 10))) / 2 + "px"
                        }), g.css({
                            visibility: "visible",
                            display: "inline-block",
                            opacity: 1
                        }), f.background && (v.css({
                            visibility: "visible",
                            opacity: f.opacity
                        }), setTimeout(function() {
                            v.css({
                                opacity: f.opacity
                            })
                        }, 0)), g.data("popup-visible", !0), u.reposition(s, c), g.data("focusedelementbeforepopup", document.activeElement), f.keepfocus && (g.attr("tabindex", -1), setTimeout(function() {
                            "closebutton" === f.focuselement ? e("#" + s.id + " ." + s.id + "_close:first").focus() : f.focuselement ? e(f.focuselement).focus() : g.focus()
                        }, f.focusdelay)), e(f.pagecontainer).attr("aria-hidden", !0), g.attr("aria-hidden", !1), h(s, c, f.onopen), d ? m.one("transitionend", function() {
                            h(s, c, f.opentransitionend)
                        }) : h(s, c, f.opentransitionend), "tooltip" == f.type && e(window).on("resize." + s.id, function() {
                            u.reposition(s, c)
                        })
                    }
                },
                hide: function(t, i) {
                    var s = e.inArray(t.id, r);
                    if (-1 !== s) {
                        o && clearTimeout(o);
                        var l = e("body"),
                            c = e(t),
                            u = c.data("popupoptions"),
                            g = e("#" + t.id + "_wrapper"),
                            p = e("#" + t.id + "_background");
                        c.data("popup-visible", !1), 1 === r.length ? e("html").removeClass("popup_visible").removeClass("popup_visible_" + t.id) : e("html").hasClass("popup_visible_" + t.id) && e("html").removeClass("popup_visible_" + t.id), r.splice(s, 1), g.hasClass("popup_wrapper_visible") && g.removeClass("popup_wrapper_visible"), u.keepfocus && !i && setTimeout(function() {
                            e(c.data("focusedelementbeforepopup")).is(":visible") && c.data("focusedelementbeforepopup").focus()
                        }, 0), g.css({
                            visibility: "hidden",
                            display: "none",
                            opacity: 0
                        }), c.css({
                            visibility: "hidden",
                            display: "none",
                            opacity: 0
                        }), u.background && p.css({
                            visibility: "hidden",
                            display: "none",
                            opacity: 0
                        }), e(u.pagecontainer).attr("aria-hidden", !1), c.attr("aria-hidden", !0), h(t, n[t.id], u.onclose), d && "0s" !== c.css("transition-duration") ? c.one("transitionend", function(e) {
                            c.data("popup-visible") || (u.detach ? c.hide().detach() : g.hide()), u.scrolllock && setTimeout(function() {
                                l.css({
                                    overflow: "visible",
                                    "margin-right": a
                                })
                            }, 10), h(t, n[t.id], u.closetransitionend)
                        }) : (u.detach ? c.hide().detach() : g.hide(), u.scrolllock && setTimeout(function() {
                            l.css({
                                overflow: "visible",
                                "margin-right": a
                            })
                        }, 10), h(t, n[t.id], u.closetransitionend)), "tooltip" == u.type && e(window).off("resize." + t.id)
                    }
                },
                toggle: function(t, o) {
                    e(t).data("popup-visible") ? u.hide(t) : setTimeout(function() {
                        u.show(t, o)
                    }, 0)
                },
                reposition: function(t, o) {
                    var s = e(t),
                        l = s.data("popupoptions"),
                        n = e("#" + t.id + "_wrapper");
                    e("#" + t.id + "_background");
                    if (o = o || 0, "tooltip" == l.type) {
                        var a;
                        n.css({
                            position: "absolute"
                        });
                        var r = (a = l.tooltipanchor ? e(l.tooltipanchor) : l.openelement ? e(l.openelement).filter('[data-popup-ordinal="' + o + '"]') : e("." + t.id + '_open[data-popup-ordinal="' + o + '"]')).offset();
                        "right" == l.horizontal ? n.css("left", r.left + a.outerWidth() + l.offsetleft) : "leftedge" == l.horizontal ? n.css("left", r.left + a.outerWidth() - a.outerWidth() + l.offsetleft) : "left" == l.horizontal ? n.css("right", i.width() - r.left - l.offsetleft) : "rightedge" == l.horizontal ? n.css("right", i.width() - r.left - a.outerWidth() - l.offsetleft) : n.css("left", r.left + a.outerWidth() / 2 - s.outerWidth() / 2 - parseFloat(s.css("marginLeft")) + l.offsetleft), "bottom" == l.vertical ? n.css("top", r.top + a.outerHeight() + l.offsettop) : "bottomedge" == l.vertical ? n.css("top", r.top + a.outerHeight() - s.outerHeight() + l.offsettop) : "top" == l.vertical ? n.css("bottom", i.height() - r.top - l.offsettop) : "topedge" == l.vertical ? n.css("bottom", i.height() - r.top - s.outerHeight() - l.offsettop) : n.css("top", r.top + a.outerHeight() / 2 - s.outerHeight() / 2 - parseFloat(s.css("marginTop")) + l.offsettop)
                    } else "overlay" == l.type && (l.horizontal ? n.css("text-align", l.horizontal) : n.css("text-align", "center"), l.vertical ? s.css("vertical-align", l.vertical) : s.css("vertical-align", "middle"))
                },
                addclosebutton: function(t) {
                    var o;
                    o = e(t).data("popupoptions").closebuttonmarkup ? e(s.closebuttonmarkup).addClass(t.id + "_close") : '<button class="popup_close ' + t.id + '_close" title="Close" aria-label="Close"><span aria-hidden="true">×</span></button>', e(t).data("popup-initialized") && e(t).append(o)
                }
            },
            h = function(t, o, i) {
                var s = e(t).data("popupoptions"),
                    l = s.openelement ? s.openelement : "." + t.id + "_open",
                    n = e(l + '[data-popup-ordinal="' + o + '"]');
                "function" == typeof i && i.call(e(t), t, n)
            };
        e(document).on("keydown", function(t) {
            if (r.length) {
                var o = r[r.length - 1],
                    i = document.getElementById(o);
                e(i).data("popupoptions").escape && 27 == t.keyCode && u.hide(i)
            }
        }), e(document).on("click", function(t) {
            if (r.length) {
                var o = r[r.length - 1],
                    i = document.getElementById(o),
                    s = e(i).data("popupoptions").closeelement ? e(i).data("popupoptions").closeelement : "." + i.id + "_close";
                e(t.target).closest(s).length && (t.preventDefault(), u.hide(i)), e(i).data("popupoptions").blur && !e(t.target).closest("#" + o).length && 2 !== t.which && e(t.target).is(":visible") && (e(i).data("popupoptions").background ? (u.hide(i), t.preventDefault()) : u.hide(i, !0))
            }
        }), e(document).on("keydown", function(t) {
            if (r.length && 9 == t.which) {
                var o = r[r.length - 1],
                    i = document.getElementById(o),
                    s = e(i).find("*").filter("a[href], area[href], input:not([disabled]), select:not([disabled]), textarea:not([disabled]), button:not([disabled]), iframe, object, embed, *[tabindex], *[contenteditable]").filter(":visible"),
                    l = e(":focus"),
                    n = s.length,
                    a = s.index(l);
                0 === n ? (e(i).focus(), t.preventDefault()) : t.shiftKey ? 0 === a && (s.get(n - 1).focus(), t.preventDefault()) : a == n - 1 && (s.get(0).focus(), t.preventDefault())
            }
        }), e.fn.popup = function(t) {
            return this.each(function() {
                var o = e(this);
                if ("object" == typeof t) {
                    var i = e.extend({}, e.fn.popup.defaults, o.data("popupoptions"), t);
                    o.data("popupoptions", i), s = o.data("popupoptions"), u._init(this)
                } else "string" == typeof t ? (o.data("popupoptions") || (o.data("popupoptions", e.fn.popup.defaults), s = o.data("popupoptions")), u[t].call(this, this)) : (o.data("popupoptions") || (o.data("popupoptions", e.fn.popup.defaults), s = o.data("popupoptions")), u._init(this))
            })
        }, e.fn.popup.defaults = {
            type: "overlay",
            autoopen: !1,
            background: !0,
            backgroundactive: !1,
            color: "black",
            opacity: "0.5",
            horizontal: "center",
            vertical: "middle",
            offsettop: 0,
            offsetleft: 0,
            escape: !0,
            blur: !0,
            setzindex: !0,
            autozindex: !1,
            scrolllock: !1,
            closebutton: !1,
            closebuttonmarkup: null,
            keepfocus: !0,
            focuselement: null,
            focusdelay: 50,
            outline: !1,
            pagecontainer: null,
            detach: !1,
            openelement: null,
            closeelement: null,
            transition: null,
            tooltipanchor: null,
            beforeopen: null,
            onclose: null,
            onopen: null,
            opentransitionend: null,
            closetransitionend: null
        }
    }(jQuery), $(document).ready(function() {
        $(".myPopupDv").popup()
    }),
    function(e, t) {
        "function" == typeof define && define.amd ? define(["jquery"], function(e) {
            return t(e)
        }) : "object" == typeof module && module.exports ? module.exports = t(require("jquery")) : t(e.jQuery)
    }(this, function(e) {
        ! function() {
            "use strict";

            function t(t, i) {
                if (this.el = t, this.$el = e(t), this.s = e.extend({}, o, i), this.s.dynamic && "undefined" !== this.s.dynamicEl && this.s.dynamicEl.constructor === Array && !this.s.dynamicEl.length) throw "When using dynamic mode, you must also define dynamicEl as an Array.";
                return this.modules = {}, this.lGalleryOn = !1, this.lgBusy = !1, this.hideBartimeout = !1, this.isTouch = "ontouchstart" in document.documentElement, this.s.slideEndAnimatoin && (this.s.hideControlOnEnd = !1), this.s.dynamic ? this.$items = this.s.dynamicEl : "this" === this.s.selector ? this.$items = this.$el : "" !== this.s.selector ? this.s.selectWithin ? this.$items = e(this.s.selectWithin).find(this.s.selector) : this.$items = this.$el.find(e(this.s.selector)) : this.$items = this.$el.children(), this.$slide = "", this.$outer = "", this.init(), this
            }
            var o = {
                mode: "lg-slide",
                cssEasing: "ease",
                easing: "linear",
                speed: 600,
                height: "100%",
                width: "100%",
                addClass: "",
                startClass: "lg-start-zoom",
                backdropDuration: 150,
                hideBarsDelay: 6e3,
                useLeft: !1,
                closable: !0,
                loop: !0,
                escKey: !0,
                keyPress: !0,
                controls: !0,
                slideEndAnimatoin: !0,
                hideControlOnEnd: !1,
                mousewheel: !0,
                getCaptionFromTitleOrAlt: !0,
                appendSubHtmlTo: ".lg-sub-html",
                subHtmlSelectorRelative: !1,
                preload: 1,
                showAfterLoad: !0,
                selector: "",
                selectWithin: "",
                nextHtml: "",
                prevHtml: "",
                index: !1,
                iframeMaxWidth: "100%",
                download: !0,
                counter: !0,
                appendCounterTo: ".lg-toolbar",
                swipeThreshold: 50,
                enableSwipe: !0,
                enableDrag: !0,
                dynamic: !1,
                dynamicEl: [],
                galleryId: 1
            };
            t.prototype.init = function() {
                var t = this;
                t.s.preload > t.$items.length && (t.s.preload = t.$items.length);
                var o = window.location.hash;
                o.indexOf("lg=" + this.s.galleryId) > 0 && (t.index = parseInt(o.split("&slide=")[1], 10), e("body").addClass("lg-from-hash"), e("body").hasClass("lg-on") || (setTimeout(function() {
                    t.build(t.index)
                }), e("body").addClass("lg-on"))), t.s.dynamic ? (t.$el.trigger("onBeforeOpen.lg"), t.index = t.s.index || 0, e("body").hasClass("lg-on") || setTimeout(function() {
                    t.build(t.index), e("body").addClass("lg-on")
                })) : t.$items.on("click.lgcustom", function(o) {
                    try {
                        o.preventDefault(), o.preventDefault()
                    } catch (e) {
                        o.returnValue = !1
                    }
                    t.$el.trigger("onBeforeOpen.lg"), t.index = t.s.index || t.$items.index(this), e("body").hasClass("lg-on") || (t.build(t.index), e("body").addClass("lg-on"))
                })
            }, t.prototype.build = function(t) {
                var o = this;
                o.structure(), e.each(e.fn.lightGallery.modules, function(t) {
                    o.modules[t] = new e.fn.lightGallery.modules[t](o.el)
                }), o.slide(t, !1, !1, !1), o.s.keyPress && o.keyPress(), o.$items.length > 1 ? (o.arrow(), setTimeout(function() {
                    o.enableDrag(), o.enableSwipe()
                }, 50), o.s.mousewheel && o.mousewheel()) : o.$slide.on("click.lg", function() {
                    o.$el.trigger("onSlideClick.lg")
                }), o.counter(), o.closeGallery(), o.$el.trigger("onAfterOpen.lg"), o.$outer.on("mousemove.lg click.lg touchstart.lg", function() {
                    o.$outer.removeClass("lg-hide-items"), clearTimeout(o.hideBartimeout), o.hideBartimeout = setTimeout(function() {
                        o.$outer.addClass("lg-hide-items")
                    }, o.s.hideBarsDelay)
                }), o.$outer.trigger("mousemove.lg")
            }, t.prototype.structure = function() {
                var t, o = "",
                    i = "",
                    s = 0,
                    l = "",
                    n = this;
                for (e("body").append('<div class="lg-backdrop"></div>'), e(".lg-backdrop").css("transition-duration", this.s.backdropDuration + "ms"), s = 0; s < this.$items.length; s++) o += '<div class="lg-item"></div>';
                if (this.s.controls && this.$items.length > 1 && (i = '<div class="lg-actions"><button class="lg-prev lg-icon">' + this.s.prevHtml + '</button><button class="lg-next lg-icon">' + this.s.nextHtml + "</button></div>"), ".lg-sub-html" === this.s.appendSubHtmlTo && (l = '<div class="lg-sub-html"></div>'), t = '<div class="lg-outer ' + this.s.addClass + " " + this.s.startClass + '"><div class="lg" style="width:' + this.s.width + "; height:" + this.s.height + '"><div class="lg-inner">' + o + '</div><div class="lg-toolbar lg-group"><span class="lg-close lg-icon"></span></div>' + i + l + "</div></div>", e("body").append(t), this.$outer = e(".lg-outer"), this.$slide = this.$outer.find(".lg-item"), this.s.useLeft ? (this.$outer.addClass("lg-use-left"), this.s.mode = "lg-slide") : this.$outer.addClass("lg-use-css3"), n.setTop(), e(window).on("resize.lg orientationchange.lg", function() {
                        setTimeout(function() {
                            n.setTop()
                        }, 100)
                    }), this.$slide.eq(this.index).addClass("lg-current"), this.doCss() ? this.$outer.addClass("lg-css3") : (this.$outer.addClass("lg-css"), this.s.speed = 0), this.$outer.addClass(this.s.mode), this.s.enableDrag && this.$items.length > 1 && this.$outer.addClass("lg-grab"), this.s.showAfterLoad && this.$outer.addClass("lg-show-after-load"), this.doCss()) {
                    var a = this.$outer.find(".lg-inner");
                    a.css("transition-timing-function", this.s.cssEasing), a.css("transition-duration", this.s.speed + "ms")
                }
                setTimeout(function() {
                    e(".lg-backdrop").addClass("in")
                }), setTimeout(function() {
                    n.$outer.addClass("lg-visible")
                }, this.s.backdropDuration), this.s.download && this.$outer.find(".lg-toolbar").append('<a id="lg-download" target="_blank" download class="lg-download lg-icon"></a>'), this.prevScrollTop = e(window).scrollTop()
            }, t.prototype.setTop = function() {
                if ("100%" !== this.s.height) {
                    var t = e(window).height(),
                        o = (t - parseInt(this.s.height, 10)) / 2,
                        i = this.$outer.find(".lg");
                    t >= parseInt(this.s.height, 10) ? i.css("top", o + "px") : i.css("top", "0px")
                }
            }, t.prototype.doCss = function() {
                return !! function() {
                    var e = ["transition", "MozTransition", "WebkitTransition", "OTransition", "msTransition", "KhtmlTransition"],
                        t = document.documentElement,
                        o = 0;
                    for (o = 0; o < e.length; o++)
                        if (e[o] in t.style) return !0
                }()
            }, t.prototype.isVideo = function(e, t) {
                var o;
                if (o = this.s.dynamic ? this.s.dynamicEl[t].html : this.$items.eq(t).attr("data-html"), !e) return o ? {
                    html5: !0
                } : (console.error("lightGallery :- data-src is not pvovided on slide item " + (t + 1) + ". Please make sure the selector property is properly configured. More info - http://sachinchoolur.github.io/lightGallery/demos/html-markup.html"), !1);
                var i = e.match(/\/\/(?:www\.)?youtu(?:\.be|be\.com|be-nocookie\.com)\/(?:watch\?v=|embed\/)?([a-z0-9\-\_\%]+)/i),
                    s = e.match(/\/\/(?:www\.)?vimeo.com\/([0-9a-z\-_]+)/i),
                    l = e.match(/\/\/(?:www\.)?dai.ly\/([0-9a-z\-_]+)/i),
                    n = e.match(/\/\/(?:www\.)?(?:vk\.com|vkontakte\.ru)\/(?:video_ext\.php\?)(.*)/i);
                return i ? {
                    youtube: i
                } : s ? {
                    vimeo: s
                } : l ? {
                    dailymotion: l
                } : n ? {
                    vk: n
                } : void 0
            }, t.prototype.counter = function() {
                this.s.counter && e(this.s.appendCounterTo).append('<div id="lg-counter"><span id="lg-counter-current">' + (parseInt(this.index, 10) + 1) + '</span> / <span id="lg-counter-all">' + this.$items.length + "</span></div>")
            }, t.prototype.addHtml = function(t) {
                var o, i, s = null;
                if (this.s.dynamic ? this.s.dynamicEl[t].subHtmlUrl ? o = this.s.dynamicEl[t].subHtmlUrl : s = this.s.dynamicEl[t].subHtml : (i = this.$items.eq(t)).attr("data-sub-html-url") ? o = i.attr("data-sub-html-url") : (s = i.attr("data-sub-html"), this.s.getCaptionFromTitleOrAlt && !s && (s = i.attr("title") || i.find("img").first().attr("alt"))), !o)
                    if (null != s) {
                        var l = s.substring(0, 1);
                        "." !== l && "#" !== l || (s = this.s.subHtmlSelectorRelative && !this.s.dynamic ? i.find(s).html() : e(s).html())
                    } else s = "";
                ".lg-sub-html" === this.s.appendSubHtmlTo ? o ? this.$outer.find(this.s.appendSubHtmlTo).load(o) : this.$outer.find(this.s.appendSubHtmlTo).html(s) : o ? this.$slide.eq(t).load(o) : this.$slide.eq(t).append(s), null != s && ("" === s ? this.$outer.find(this.s.appendSubHtmlTo).addClass("lg-empty-html") : this.$outer.find(this.s.appendSubHtmlTo).removeClass("lg-empty-html")), this.$el.trigger("onAfterAppendSubHtml.lg", [t])
            }, t.prototype.preload = function(e) {
                var t = 1,
                    o = 1;
                for (t = 1; t <= this.s.preload && !(t >= this.$items.length - e); t++) this.loadContent(e + t, !1, 0);
                for (o = 1; o <= this.s.preload && !(e - o < 0); o++) this.loadContent(e - o, !1, 0)
            }, t.prototype.loadContent = function(t, o, i) {
                var s, l, n, a, r, d, c = this,
                    u = !1,
                    h = function(t) {
                        for (var o = [], i = [], s = 0; s < t.length; s++) {
                            var n = t[s].split(" ");
                            "" === n[0] && n.splice(0, 1), i.push(n[0]), o.push(n[1])
                        }
                        for (var a = e(window).width(), r = 0; r < o.length; r++)
                            if (parseInt(o[r], 10) > a) {
                                l = i[r];
                                break
                            }
                    };
                c.s.dynamic ? (c.s.dynamicEl[t].poster && (u = !0, n = c.s.dynamicEl[t].poster), d = c.s.dynamicEl[t].html, l = c.s.dynamicEl[t].src, c.s.dynamicEl[t].responsive && h(c.s.dynamicEl[t].responsive.split(",")), a = c.s.dynamicEl[t].srcset, r = c.s.dynamicEl[t].sizes) : (c.$items.eq(t).attr("data-poster") && (u = !0, n = c.$items.eq(t).attr("data-poster")), d = c.$items.eq(t).attr("data-html"), l = c.$items.eq(t).attr("href") || c.$items.eq(t).attr("data-src"), c.$items.eq(t).attr("data-responsive") && h(c.$items.eq(t).attr("data-responsive").split(",")), a = c.$items.eq(t).attr("data-srcset"), r = c.$items.eq(t).attr("data-sizes"));
                var g = !1;
                c.s.dynamic ? c.s.dynamicEl[t].iframe && (g = !0) : "true" === c.$items.eq(t).attr("data-iframe") && (g = !0);
                var p = c.isVideo(l, t);
                if (!c.$slide.eq(t).hasClass("lg-loaded")) {
                    if (g) c.$slide.eq(t).prepend('<div class="lg-video-cont lg-has-iframe" style="max-width:' + c.s.iframeMaxWidth + '"><div class="lg-video"><iframe class="lg-object" frameborder="0" src="' + l + '"  allowfullscreen="true"></iframe></div></div>');
                    else if (u) {
                        var f;
                        f = p && p.youtube ? "lg-has-youtube" : p && p.vimeo ? "lg-has-vimeo" : "lg-has-html5", c.$slide.eq(t).prepend('<div class="lg-video-cont ' + f + ' "><div class="lg-video"><span class="lg-video-play"></span><img class="lg-object lg-has-poster" src="' + n + '" /></div></div>')
                    } else p ? (c.$slide.eq(t).prepend('<div class="lg-video-cont "><div class="lg-video"></div></div>'), c.$el.trigger("hasVideo.lg", [t, l, d])) : c.$slide.eq(t).prepend('<div class="lg-img-wrap"><img class="lg-object lg-image" src="' + l + '" /></div>');
                    if (c.$el.trigger("onAferAppendSlide.lg", [t]), s = c.$slide.eq(t).find(".lg-object"), r && s.attr("sizes", r), a) {
                        s.attr("srcset", a);
                        try {
                            picturefill({
                                elements: [s[0]]
                            })
                        } catch (e) {
                            console.warn("lightGallery :- If you want srcset to be supported for older browser please include picturefil version 2 javascript library in your document.")
                        }
                    }
                    ".lg-sub-html" !== this.s.appendSubHtmlTo && c.addHtml(t), c.$slide.eq(t).addClass("lg-loaded")
                }
                c.$slide.eq(t).find(".lg-object").on("load.lg error.lg", function() {
                    var o = 0;
                    i && !e("body").hasClass("lg-from-hash") && (o = i), setTimeout(function() {
                        c.$slide.eq(t).addClass("lg-complete"), c.$el.trigger("onSlideItemLoad.lg", [t, i || 0])
                    }, o)
                }), p && p.html5 && !u && c.$slide.eq(t).addClass("lg-complete"), !0 === o && (c.$slide.eq(t).hasClass("lg-complete") ? c.preload(t) : c.$slide.eq(t).find(".lg-object").on("load.lg error.lg", function() {
                    c.preload(t)
                }))
            }, t.prototype.slide = function(t, o, i, s) {
                var l = this.$outer.find(".lg-current").index(),
                    n = this;
                if (!n.lGalleryOn || l !== t) {
                    var a = this.$slide.length,
                        r = n.lGalleryOn ? this.s.speed : 0;
                    if (!n.lgBusy) {
                        var d, c, u;
                        if (this.s.download)(d = n.s.dynamic ? !1 !== n.s.dynamicEl[t].downloadUrl && (n.s.dynamicEl[t].downloadUrl || n.s.dynamicEl[t].src) : "false" !== n.$items.eq(t).attr("data-download-url") && (n.$items.eq(t).attr("data-download-url") || n.$items.eq(t).attr("href") || n.$items.eq(t).attr("data-src"))) ? (e("#lg-download").attr("href", d), n.$outer.removeClass("lg-hide-download")) : n.$outer.addClass("lg-hide-download");
                        if (this.$el.trigger("onBeforeSlide.lg", [l, t, o, i]), n.lgBusy = !0, clearTimeout(n.hideBartimeout), ".lg-sub-html" === this.s.appendSubHtmlTo && setTimeout(function() {
                                n.addHtml(t)
                            }, r), this.arrowDisable(t), s || (t < l ? s = "prev" : t > l && (s = "next")), o) this.$slide.removeClass("lg-prev-slide lg-current lg-next-slide"), a > 2 ? (c = t - 1, u = t + 1, 0 === t && l === a - 1 ? (u = 0, c = a - 1) : t === a - 1 && 0 === l && (u = 0, c = a - 1)) : (c = 0, u = 1), "prev" === s ? n.$slide.eq(u).addClass("lg-next-slide") : n.$slide.eq(c).addClass("lg-prev-slide"), n.$slide.eq(t).addClass("lg-current");
                        else n.$outer.addClass("lg-no-trans"), this.$slide.removeClass("lg-prev-slide lg-next-slide"), "prev" === s ? (this.$slide.eq(t).addClass("lg-prev-slide"), this.$slide.eq(l).addClass("lg-next-slide")) : (this.$slide.eq(t).addClass("lg-next-slide"), this.$slide.eq(l).addClass("lg-prev-slide")), setTimeout(function() {
                            n.$slide.removeClass("lg-current"), n.$slide.eq(t).addClass("lg-current"), n.$outer.removeClass("lg-no-trans")
                        }, 50);
                        n.lGalleryOn ? (setTimeout(function() {
                            n.loadContent(t, !0, 0)
                        }, this.s.speed + 50), setTimeout(function() {
                            n.lgBusy = !1, n.$el.trigger("onAfterSlide.lg", [l, t, o, i])
                        }, this.s.speed)) : (n.loadContent(t, !0, n.s.backdropDuration), n.lgBusy = !1, n.$el.trigger("onAfterSlide.lg", [l, t, o, i])), n.lGalleryOn = !0, this.s.counter && e("#lg-counter-current").text(t + 1)
                    }
                    n.index = t
                }
            }, t.prototype.goToNextSlide = function(e) {
                var t = this,
                    o = t.s.loop;
                e && t.$slide.length < 3 && (o = !1), t.lgBusy || (t.index + 1 < t.$slide.length ? (t.index++, t.$el.trigger("onBeforeNextSlide.lg", [t.index]), t.slide(t.index, e, !1, "next")) : o ? (t.index = 0, t.$el.trigger("onBeforeNextSlide.lg", [t.index]), t.slide(t.index, e, !1, "next")) : t.s.slideEndAnimatoin && !e && (t.$outer.addClass("lg-right-end"), setTimeout(function() {
                    t.$outer.removeClass("lg-right-end")
                }, 400)))
            }, t.prototype.goToPrevSlide = function(e) {
                var t = this,
                    o = t.s.loop;
                e && t.$slide.length < 3 && (o = !1), t.lgBusy || (t.index > 0 ? (t.index--, t.$el.trigger("onBeforePrevSlide.lg", [t.index, e]), t.slide(t.index, e, !1, "prev")) : o ? (t.index = t.$items.length - 1, t.$el.trigger("onBeforePrevSlide.lg", [t.index, e]), t.slide(t.index, e, !1, "prev")) : t.s.slideEndAnimatoin && !e && (t.$outer.addClass("lg-left-end"), setTimeout(function() {
                    t.$outer.removeClass("lg-left-end")
                }, 400)))
            }, t.prototype.keyPress = function() {
                var t = this;
                this.$items.length > 1 && e(window).on("keyup.lg", function(e) {
                    t.$items.length > 1 && (37 === e.keyCode && (e.preventDefault(), t.goToPrevSlide()), 39 === e.keyCode && (e.preventDefault(), t.goToNextSlide()))
                }), e(window).on("keydown.lg", function(e) {
                    !0 === t.s.escKey && 27 === e.keyCode && (e.preventDefault(), t.$outer.hasClass("lg-thumb-open") ? t.$outer.removeClass("lg-thumb-open") : t.destroy())
                })
            }, t.prototype.arrow = function() {
                var e = this;
                this.$outer.find(".lg-prev").on("click.lg", function() {
                    e.goToPrevSlide()
                }), this.$outer.find(".lg-next").on("click.lg", function() {
                    e.goToNextSlide()
                })
            }, t.prototype.arrowDisable = function(e) {
                !this.s.loop && this.s.hideControlOnEnd && (e + 1 < this.$slide.length ? this.$outer.find(".lg-next").removeAttr("disabled").removeClass("disabled") : this.$outer.find(".lg-next").attr("disabled", "disabled").addClass("disabled"), e > 0 ? this.$outer.find(".lg-prev").removeAttr("disabled").removeClass("disabled") : this.$outer.find(".lg-prev").attr("disabled", "disabled").addClass("disabled"))
            }, t.prototype.setTranslate = function(e, t, o) {
                this.s.useLeft ? e.css("left", t) : e.css({
                    transform: "translate3d(" + t + "px, " + o + "px, 0px)"
                })
            }, t.prototype.touchMove = function(t, o) {
                var i = o - t;
                Math.abs(i) > 15 && (this.$outer.addClass("lg-dragging"), this.setTranslate(this.$slide.eq(this.index), i, 0), this.setTranslate(e(".lg-prev-slide"), -this.$slide.eq(this.index).width() + i, 0), this.setTranslate(e(".lg-next-slide"), this.$slide.eq(this.index).width() + i, 0))
            }, t.prototype.touchEnd = function(e) {
                var t = this;
                "lg-slide" !== t.s.mode && t.$outer.addClass("lg-slide"), this.$slide.not(".lg-current, .lg-prev-slide, .lg-next-slide").css("opacity", "0"), setTimeout(function() {
                    t.$outer.removeClass("lg-dragging"), e < 0 && Math.abs(e) > t.s.swipeThreshold ? t.goToNextSlide(!0) : e > 0 && Math.abs(e) > t.s.swipeThreshold ? t.goToPrevSlide(!0) : Math.abs(e) < 5 && t.$el.trigger("onSlideClick.lg"), t.$slide.removeAttr("style")
                }), setTimeout(function() {
                    t.$outer.hasClass("lg-dragging") || "lg-slide" === t.s.mode || t.$outer.removeClass("lg-slide")
                }, t.s.speed + 100)
            }, t.prototype.enableSwipe = function() {
                var e = this,
                    t = 0,
                    o = 0,
                    i = !1;
                e.s.enableSwipe && e.doCss() && (e.$slide.on("touchstart.lg", function(o) {
                    e.$outer.hasClass("lg-zoomed") || e.lgBusy || (o.preventDefault(), e.manageSwipeClass(), t = o.originalEvent.targetTouches[0].pageX)
                }), e.$slide.on("touchmove.lg", function(s) {
                    e.$outer.hasClass("lg-zoomed") || (s.preventDefault(), o = s.originalEvent.targetTouches[0].pageX, e.touchMove(t, o), i = !0)
                }), e.$slide.on("touchend.lg", function() {
                    e.$outer.hasClass("lg-zoomed") || (i ? (i = !1, e.touchEnd(o - t)) : e.$el.trigger("onSlideClick.lg"))
                }))
            }, t.prototype.enableDrag = function() {
                var t = this,
                    o = 0,
                    i = 0,
                    s = !1,
                    l = !1;
                t.s.enableDrag && t.doCss() && (t.$slide.on("mousedown.lg", function(i) {
                    t.$outer.hasClass("lg-zoomed") || t.lgBusy || e(i.target).text().trim() || (i.preventDefault(), t.manageSwipeClass(), o = i.pageX, s = !0, t.$outer.scrollLeft += 1, t.$outer.scrollLeft -= 1, t.$outer.removeClass("lg-grab").addClass("lg-grabbing"), t.$el.trigger("onDragstart.lg"))
                }), e(window).on("mousemove.lg", function(e) {
                    s && (l = !0, i = e.pageX, t.touchMove(o, i), t.$el.trigger("onDragmove.lg"))
                }), e(window).on("mouseup.lg", function(n) {
                    l ? (l = !1, t.touchEnd(i - o), t.$el.trigger("onDragend.lg")) : (e(n.target).hasClass("lg-object") || e(n.target).hasClass("lg-video-play")) && t.$el.trigger("onSlideClick.lg"), s && (s = !1, t.$outer.removeClass("lg-grabbing").addClass("lg-grab"))
                }))
            }, t.prototype.manageSwipeClass = function() {
                var e = this.index + 1,
                    t = this.index - 1;
                this.s.loop && this.$slide.length > 2 && (0 === this.index ? t = this.$slide.length - 1 : this.index === this.$slide.length - 1 && (e = 0)), this.$slide.removeClass("lg-next-slide lg-prev-slide"), t > -1 && this.$slide.eq(t).addClass("lg-prev-slide"), this.$slide.eq(e).addClass("lg-next-slide")
            }, t.prototype.mousewheel = function() {
                var e = this;
                e.$outer.on("mousewheel.lg", function(t) {
                    t.deltaY && (t.deltaY > 0 ? e.goToPrevSlide() : e.goToNextSlide(), t.preventDefault())
                })
            }, t.prototype.closeGallery = function() {
                var t = this,
                    o = !1;
                this.$outer.find(".lg-close").on("click.lg", function() {
                    t.destroy()
                }), t.s.closable && (t.$outer.on("mousedown.lg", function(t) {
                    o = !!(e(t.target).is(".lg-outer") || e(t.target).is(".lg-item ") || e(t.target).is(".lg-img-wrap"))
                }), t.$outer.on("mousemove.lg", function() {
                    o = !1
                }), t.$outer.on("mouseup.lg", function(i) {
                    (e(i.target).is(".lg-outer") || e(i.target).is(".lg-item ") || e(i.target).is(".lg-img-wrap") && o) && (t.$outer.hasClass("lg-dragging") || t.destroy())
                }))
            }, t.prototype.destroy = function(t) {
                var o = this;
                t || (o.$el.trigger("onBeforeClose.lg"), e(window).scrollTop(o.prevScrollTop)), t && (o.s.dynamic || this.$items.off("click.lg click.lgcustom"), e.removeData(o.el, "lightGallery")), this.$el.off(".lg.tm"), e.each(e.fn.lightGallery.modules, function(e) {
                    o.modules[e] && o.modules[e].destroy()
                }), this.lGalleryOn = !1, clearTimeout(o.hideBartimeout), this.hideBartimeout = !1, e(window).off(".lg"), e("body").removeClass("lg-on lg-from-hash"), o.$outer && o.$outer.removeClass("lg-visible"), e(".lg-backdrop").removeClass("in"), setTimeout(function() {
                    o.$outer && o.$outer.remove(), e(".lg-backdrop").remove(), t || o.$el.trigger("onCloseAfter.lg")
                }, o.s.backdropDuration + 50)
            }, e.fn.lightGallery = function(o) {
                return this.each(function() {
                    if (e.data(this, "lightGallery")) try {
                        e(this).data("lightGallery").init()
                    } catch (e) {
                        console.error("lightGallery has not initiated properly")
                    } else e.data(this, "lightGallery", new t(this, o))
                })
            }, e.fn.lightGallery.modules = {}
        }()
    }),
    function(e, t) {
        "function" == typeof define && define.amd ? define(["jquery"], function(e) {
            return t(e)
        }) : "object" == typeof exports ? module.exports = t(require("jquery")) : t(jQuery)
    }(0, function(e) {
        ! function() {
            "use strict";
            var t = {
                    autoplay: !1,
                    pause: 5e3,
                    progressBar: !0,
                    fourceAutoplay: !1,
                    autoplayControls: !0,
                    appendAutoplayControlsTo: ".lg-toolbar"
                },
                o = function(o) {
                    return this.core = e(o).data("lightGallery"), this.$el = e(o), !(this.core.$items.length < 2) && (this.core.s = e.extend({}, t, this.core.s), this.interval = !1, this.fromAuto = !0, this.canceledOnTouch = !1, this.fourceAutoplayTemp = this.core.s.fourceAutoplay, this.core.doCss() || (this.core.s.progressBar = !1), this.init(), this)
                };
            o.prototype.init = function() {
                var e = this;
                e.core.s.autoplayControls && e.controls(), e.core.s.progressBar && e.core.$outer.find(".lg").append('<div class="lg-progress-bar"><div class="lg-progress"></div></div>'), e.progress(), e.core.s.autoplay && e.$el.one("onSlideItemLoad.lg.tm", function() {
                    e.startlAuto()
                }), e.$el.on("onDragstart.lg.tm touchstart.lg.tm", function() {
                    e.interval && (e.cancelAuto(), e.canceledOnTouch = !0)
                }), e.$el.on("onDragend.lg.tm touchend.lg.tm onSlideClick.lg.tm", function() {
                    !e.interval && e.canceledOnTouch && (e.startlAuto(), e.canceledOnTouch = !1)
                })
            }, o.prototype.progress = function() {
                var e, t, o = this;
                o.$el.on("onBeforeSlide.lg.tm", function() {
                    o.core.s.progressBar && o.fromAuto && (e = o.core.$outer.find(".lg-progress-bar"), t = o.core.$outer.find(".lg-progress"), o.interval && (t.removeAttr("style"), e.removeClass("lg-start"), setTimeout(function() {
                        t.css("transition", "width " + (o.core.s.speed + o.core.s.pause) + "ms ease 0s"), e.addClass("lg-start")
                    }, 20))), o.fromAuto || o.core.s.fourceAutoplay || o.cancelAuto(), o.fromAuto = !1
                })
            }, o.prototype.controls = function() {
                var t = this;
                e(this.core.s.appendAutoplayControlsTo).append('<span class="lg-autoplay-button lg-icon"></span>'), t.core.$outer.find(".lg-autoplay-button").on("click.lg", function() {
                    e(t.core.$outer).hasClass("lg-show-autoplay") ? (t.cancelAuto(), t.core.s.fourceAutoplay = !1) : t.interval || (t.startlAuto(), t.core.s.fourceAutoplay = t.fourceAutoplayTemp)
                })
            }, o.prototype.startlAuto = function() {
                var e = this;
                e.core.$outer.find(".lg-progress").css("transition", "width " + (e.core.s.speed + e.core.s.pause) + "ms ease 0s"), e.core.$outer.addClass("lg-show-autoplay"), e.core.$outer.find(".lg-progress-bar").addClass("lg-start"), e.interval = setInterval(function() {
                    e.core.index + 1 < e.core.$items.length ? e.core.index++ : e.core.index = 0, e.fromAuto = !0, e.core.slide(e.core.index, !1, !1, "next")
                }, e.core.s.speed + e.core.s.pause)
            }, o.prototype.cancelAuto = function() {
                clearInterval(this.interval), this.interval = !1, this.core.$outer.find(".lg-progress").removeAttr("style"), this.core.$outer.removeClass("lg-show-autoplay"), this.core.$outer.find(".lg-progress-bar").removeClass("lg-start")
            }, o.prototype.destroy = function() {
                this.cancelAuto(), this.core.$outer.find(".lg-progress-bar").remove()
            }, e.fn.lightGallery.modules.autoplay = o
        }()
    }),
    function(e, t) {
        "function" == typeof define && define.amd ? define(["jquery"], function(e) {
            return t(e)
        }) : "object" == typeof exports ? module.exports = t(require("jquery")) : t(jQuery)
    }(0, function(e) {
        ! function() {
            "use strict";
            var t = {
                    fullScreen: !0
                },
                o = function(o) {
                    return this.core = e(o).data("lightGallery"), this.$el = e(o), this.core.s = e.extend({}, t, this.core.s), this.init(), this
                };
            o.prototype.init = function() {
                var e = "";
                if (this.core.s.fullScreen) {
                    if (!(document.fullscreenEnabled || document.webkitFullscreenEnabled || document.mozFullScreenEnabled || document.msFullscreenEnabled)) return;
                    e = '<span class="lg-fullscreen lg-icon"></span>', this.core.$outer.find(".lg-toolbar").append(e), this.fullScreen()
                }
            }, o.prototype.requestFullscreen = function() {
                var e = document.documentElement;
                e.requestFullscreen ? e.requestFullscreen() : e.msRequestFullscreen ? e.msRequestFullscreen() : e.mozRequestFullScreen ? e.mozRequestFullScreen() : e.webkitRequestFullscreen && e.webkitRequestFullscreen()
            }, o.prototype.exitFullscreen = function() {
                document.exitFullscreen ? document.exitFullscreen() : document.msExitFullscreen ? document.msExitFullscreen() : document.mozCancelFullScreen ? document.mozCancelFullScreen() : document.webkitExitFullscreen && document.webkitExitFullscreen()
            }, o.prototype.fullScreen = function() {
                var t = this;
                e(document).on("fullscreenchange.lg webkitfullscreenchange.lg mozfullscreenchange.lg MSFullscreenChange.lg", function() {
                    t.core.$outer.toggleClass("lg-fullscreen-on")
                }), this.core.$outer.find(".lg-fullscreen").on("click.lg", function() {
                    document.fullscreenElement || document.mozFullScreenElement || document.webkitFullscreenElement || document.msFullscreenElement ? t.exitFullscreen() : t.requestFullscreen()
                })
            }, o.prototype.destroy = function() {
                this.exitFullscreen(), e(document).off("fullscreenchange.lg webkitfullscreenchange.lg mozfullscreenchange.lg MSFullscreenChange.lg")
            }, e.fn.lightGallery.modules.fullscreen = o
        }()
    }),
    function(e, t) {
        "function" == typeof define && define.amd ? define(["jquery"], function(e) {
            return t(e)
        }) : "object" == typeof exports ? module.exports = t(require("jquery")) : t(jQuery)
    }(0, function(e) {
        ! function() {
            "use strict";
            var t = {
                    pager: !1
                },
                o = function(o) {
                    return this.core = e(o).data("lightGallery"), this.$el = e(o), this.core.s = e.extend({}, t, this.core.s), this.core.s.pager && this.core.$items.length > 1 && this.init(), this
                };
            o.prototype.init = function() {
                var t, o, i, s = this,
                    l = "";
                if (s.core.$outer.find(".lg").append('<div class="lg-pager-outer"></div>'), s.core.s.dynamic)
                    for (var n = 0; n < s.core.s.dynamicEl.length; n++) l += '<span class="lg-pager-cont"> <span class="lg-pager"></span><div class="lg-pager-thumb-cont"><span class="lg-caret"></span> <img src="' + s.core.s.dynamicEl[n].thumb + '" /></div></span>';
                else s.core.$items.each(function() {
                    s.core.s.exThumbImage ? l += '<span class="lg-pager-cont"> <span class="lg-pager"></span><div class="lg-pager-thumb-cont"><span class="lg-caret"></span> <img src="' + e(this).attr(s.core.s.exThumbImage) + '" /></div></span>' : l += '<span class="lg-pager-cont"> <span class="lg-pager"></span><div class="lg-pager-thumb-cont"><span class="lg-caret"></span> <img src="' + e(this).find("img").attr("src") + '" /></div></span>'
                });
                (o = s.core.$outer.find(".lg-pager-outer")).html(l), (t = s.core.$outer.find(".lg-pager-cont")).on("click.lg touchend.lg", function() {
                    var t = e(this);
                    s.core.index = t.index(), s.core.slide(s.core.index, !1, !0, !1)
                }), o.on("mouseover.lg", function() {
                    clearTimeout(i), o.addClass("lg-pager-hover")
                }), o.on("mouseout.lg", function() {
                    i = setTimeout(function() {
                        o.removeClass("lg-pager-hover")
                    })
                }), s.core.$el.on("onBeforeSlide.lg.tm", function(e, o, i) {
                    t.removeClass("lg-pager-active"), t.eq(i).addClass("lg-pager-active")
                })
            }, o.prototype.destroy = function() {}, e.fn.lightGallery.modules.pager = o
        }()
    }),
    function(e, t) {
        "function" == typeof define && define.amd ? define(["jquery"], function(e) {
            return t(e)
        }) : "object" == typeof exports ? module.exports = t(require("jquery")) : t(jQuery)
    }(0, function(e) {
        ! function() {
            "use strict";
            var t = {
                    thumbnail: !0,
                    animateThumb: !0,
                    currentPagerPosition: "middle",
                    thumbWidth: 100,
                    thumbHeight: "80px",
                    thumbContHeight: 100,
                    thumbMargin: 5,
                    exThumbImage: !1,
                    showThumbByDefault: !0,
                    toogleThumb: !0,
                    pullCaptionUp: !0,
                    enableThumbDrag: !0,
                    enableThumbSwipe: !0,
                    swipeThreshold: 50,
                    loadYoutubeThumbnail: !0,
                    youtubeThumbSize: 1,
                    loadVimeoThumbnail: !0,
                    vimeoThumbSize: "thumbnail_small",
                    loadDailymotionThumbnail: !0
                },
                o = function(o) {
                    return this.core = e(o).data("lightGallery"), this.core.s = e.extend({}, t, this.core.s), this.$el = e(o), this.$thumbOuter = null, this.thumbOuterWidth = 0, this.thumbTotalWidth = this.core.$items.length * (this.core.s.thumbWidth + this.core.s.thumbMargin), this.thumbIndex = this.core.index, this.core.s.animateThumb && (this.core.s.thumbHeight = "100%"), this.left = 0, this.init(), this
                };
            o.prototype.init = function() {
                var e = this;
                this.core.s.thumbnail && this.core.$items.length > 1 && (this.core.s.showThumbByDefault && setTimeout(function() {
                    e.core.$outer.addClass("lg-thumb-open")
                }, 700), this.core.s.pullCaptionUp && this.core.$outer.addClass("lg-pull-caption-up"), this.build(), this.core.s.animateThumb && this.core.doCss() ? (this.core.s.enableThumbDrag && this.enableThumbDrag(), this.core.s.enableThumbSwipe && this.enableThumbSwipe(), this.thumbClickable = !1) : this.thumbClickable = !0, this.toogle(), this.thumbkeyPress())
            }, o.prototype.build = function() {
                function t(e, t, o) {
                    var n, a = i.core.isVideo(e, o) || {},
                        r = "";
                    a.youtube || a.vimeo || a.dailymotion ? a.youtube ? n = i.core.s.loadYoutubeThumbnail ? "//img.youtube.com/vi/" + a.youtube[1] + "/" + i.core.s.youtubeThumbSize + ".jpg" : t : a.vimeo ? i.core.s.loadVimeoThumbnail ? (n = "//i.vimeocdn.com/video/error_" + l + ".jpg", r = a.vimeo[1]) : n = t : a.dailymotion && (n = i.core.s.loadDailymotionThumbnail ? "//www.dailymotion.com/thumbnail/video/" + a.dailymotion[1] : t) : n = t, s += '<div data-vimeo-id="' + r + '" class="lg-thumb-item" style="width:' + i.core.s.thumbWidth + "px; height: " + i.core.s.thumbHeight + "; margin-right: " + i.core.s.thumbMargin + 'px"><img src="' + n + '" /></div>', r = ""
                }
                var o, i = this,
                    s = "",
                    l = "";
                switch (this.core.s.vimeoThumbSize) {
                    case "thumbnail_large":
                        l = "640";
                        break;
                    case "thumbnail_medium":
                        l = "200x150";
                        break;
                    case "thumbnail_small":
                        l = "100x75"
                }
                if (i.core.$outer.addClass("lg-has-thumb"), i.core.$outer.find(".lg").append('<div class="lg-thumb-outer"><div class="lg-thumb lg-group"></div></div>'), i.$thumbOuter = i.core.$outer.find(".lg-thumb-outer"), i.thumbOuterWidth = i.$thumbOuter.width(), i.core.s.animateThumb && i.core.$outer.find(".lg-thumb").css({
                        width: i.thumbTotalWidth + "px",
                        position: "relative"
                    }), this.core.s.animateThumb && i.$thumbOuter.css("height", i.core.s.thumbContHeight + "px"), i.core.s.dynamic)
                    for (var n = 0; n < i.core.s.dynamicEl.length; n++) t(i.core.s.dynamicEl[n].src, i.core.s.dynamicEl[n].thumb, n);
                else i.core.$items.each(function(o) {
                    i.core.s.exThumbImage ? t(e(this).attr("href") || e(this).attr("data-src"), e(this).attr(i.core.s.exThumbImage), o) : t(e(this).attr("href") || e(this).attr("data-src"), e(this).find("img").attr("src"), o)
                });
                i.core.$outer.find(".lg-thumb").html(s), (o = i.core.$outer.find(".lg-thumb-item")).each(function() {
                    var t = e(this),
                        o = t.attr("data-vimeo-id");
                    o && e.getJSON("//www.vimeo.com/api/v2/video/" + o + ".json?callback=?", {
                        format: "json"
                    }, function(e) {
                        t.find("img").attr("src", e[0][i.core.s.vimeoThumbSize])
                    })
                }), o.eq(i.core.index).addClass("active"), i.core.$el.on("onBeforeSlide.lg.tm", function() {
                    o.removeClass("active"), o.eq(i.core.index).addClass("active")
                }), o.on("click.lg touchend.lg", function() {
                    var t = e(this);
                    setTimeout(function() {
                        (i.thumbClickable && !i.core.lgBusy || !i.core.doCss()) && (i.core.index = t.index(), i.core.slide(i.core.index, !1, !0, !1))
                    }, 50)
                }), i.core.$el.on("onBeforeSlide.lg.tm", function() {
                    i.animateThumb(i.core.index)
                }), e(window).on("resize.lg.thumb orientationchange.lg.thumb", function() {
                    setTimeout(function() {
                        i.animateThumb(i.core.index), i.thumbOuterWidth = i.$thumbOuter.width()
                    }, 200)
                })
            }, o.prototype.setTranslate = function(e) {
                this.core.$outer.find(".lg-thumb").css({
                    transform: "translate3d(-" + e + "px, 0px, 0px)"
                })
            }, o.prototype.animateThumb = function(e) {
                var t = this.core.$outer.find(".lg-thumb");
                if (this.core.s.animateThumb) {
                    var o;
                    switch (this.core.s.currentPagerPosition) {
                        case "left":
                            o = 0;
                            break;
                        case "middle":
                            o = this.thumbOuterWidth / 2 - this.core.s.thumbWidth / 2;
                            break;
                        case "right":
                            o = this.thumbOuterWidth - this.core.s.thumbWidth
                    }
                    this.left = (this.core.s.thumbWidth + this.core.s.thumbMargin) * e - 1 - o, this.left > this.thumbTotalWidth - this.thumbOuterWidth && (this.left = this.thumbTotalWidth - this.thumbOuterWidth), this.left < 0 && (this.left = 0), this.core.lGalleryOn ? (t.hasClass("on") || this.core.$outer.find(".lg-thumb").css("transition-duration", this.core.s.speed + "ms"), this.core.doCss() || t.animate({
                        left: -this.left + "px"
                    }, this.core.s.speed)) : this.core.doCss() || t.css("left", -this.left + "px"), this.setTranslate(this.left)
                }
            }, o.prototype.enableThumbDrag = function() {
                var t = this,
                    o = 0,
                    i = 0,
                    s = !1,
                    l = !1,
                    n = 0;
                t.$thumbOuter.addClass("lg-grab"), t.core.$outer.find(".lg-thumb").on("mousedown.lg.thumb", function(e) {
                    t.thumbTotalWidth > t.thumbOuterWidth && (e.preventDefault(), o = e.pageX, s = !0, t.core.$outer.scrollLeft += 1, t.core.$outer.scrollLeft -= 1, t.thumbClickable = !1, t.$thumbOuter.removeClass("lg-grab").addClass("lg-grabbing"))
                }), e(window).on("mousemove.lg.thumb", function(e) {
                    s && (n = t.left, l = !0, i = e.pageX, t.$thumbOuter.addClass("lg-dragging"), (n -= i - o) > t.thumbTotalWidth - t.thumbOuterWidth && (n = t.thumbTotalWidth - t.thumbOuterWidth), n < 0 && (n = 0), t.setTranslate(n))
                }), e(window).on("mouseup.lg.thumb", function() {
                    l ? (l = !1, t.$thumbOuter.removeClass("lg-dragging"), t.left = n, Math.abs(i - o) < t.core.s.swipeThreshold && (t.thumbClickable = !0)) : t.thumbClickable = !0, s && (s = !1, t.$thumbOuter.removeClass("lg-grabbing").addClass("lg-grab"))
                })
            }, o.prototype.enableThumbSwipe = function() {
                var e = this,
                    t = 0,
                    o = 0,
                    i = !1,
                    s = 0;
                e.core.$outer.find(".lg-thumb").on("touchstart.lg", function(o) {
                    e.thumbTotalWidth > e.thumbOuterWidth && (o.preventDefault(), t = o.originalEvent.targetTouches[0].pageX, e.thumbClickable = !1)
                }), e.core.$outer.find(".lg-thumb").on("touchmove.lg", function(l) {
                    e.thumbTotalWidth > e.thumbOuterWidth && (l.preventDefault(), o = l.originalEvent.targetTouches[0].pageX, i = !0, e.$thumbOuter.addClass("lg-dragging"), s = e.left, (s -= o - t) > e.thumbTotalWidth - e.thumbOuterWidth && (s = e.thumbTotalWidth - e.thumbOuterWidth), s < 0 && (s = 0), e.setTranslate(s))
                }), e.core.$outer.find(".lg-thumb").on("touchend.lg", function() {
                    e.thumbTotalWidth > e.thumbOuterWidth && i ? (i = !1, e.$thumbOuter.removeClass("lg-dragging"), Math.abs(o - t) < e.core.s.swipeThreshold && (e.thumbClickable = !0), e.left = s) : e.thumbClickable = !0
                })
            }, o.prototype.toogle = function() {
                var e = this;
                e.core.s.toogleThumb && (e.core.$outer.addClass("lg-can-toggle"), e.$thumbOuter.append('<span class="lg-toogle-thumb lg-icon"></span>'), e.core.$outer.find(".lg-toogle-thumb").on("click.lg", function() {
                    e.core.$outer.toggleClass("lg-thumb-open")
                }))
            }, o.prototype.thumbkeyPress = function() {
                var t = this;
                e(window).on("keydown.lg.thumb", function(e) {
                    38 === e.keyCode ? (e.preventDefault(), t.core.$outer.addClass("lg-thumb-open")) : 40 === e.keyCode && (e.preventDefault(), t.core.$outer.removeClass("lg-thumb-open"))
                })
            }, o.prototype.destroy = function() {
                this.core.s.thumbnail && this.core.$items.length > 1 && (e(window).off("resize.lg.thumb orientationchange.lg.thumb keydown.lg.thumb"), this.$thumbOuter.remove(), this.core.$outer.removeClass("lg-has-thumb"))
            }, e.fn.lightGallery.modules.Thumbnail = o
        }()
    }),
    function(e, t) {
        "function" == typeof define && define.amd ? define(["jquery"], function(e) {
            return t(e)
        }) : "object" == typeof module && module.exports ? module.exports = t(require("jquery")) : t(e.jQuery)
    }(this, function(e) {
        ! function() {
            "use strict";
            var t = {
                    videoMaxWidth: "855px",
                    autoplayFirstVideo: !0,
                    youtubePlayerParams: !1,
                    vimeoPlayerParams: !1,
                    dailymotionPlayerParams: !1,
                    vkPlayerParams: !1,
                    videojs: !1,
                    videojsOptions: {}
                },
                o = function(o) {
                    return this.core = e(o).data("lightGallery"), this.$el = e(o), this.core.s = e.extend({}, t, this.core.s), this.videoLoaded = !1, this.init(), this
                };
            o.prototype.init = function() {
                var t = this;
                t.core.$el.on("hasVideo.lg.tm", function(e, t, o, i) {
                    var s = this;
                    if (s.core.$slide.eq(t).find(".lg-video").append(s.loadVideo(o, "lg-object", !0, t, i)), i)
                        if (s.core.s.videojs) try {
                            videojs(s.core.$slide.eq(t).find(".lg-html5").get(0), s.core.s.videojsOptions, function() {
                                !s.videoLoaded && s.core.s.autoplayFirstVideo && this.play()
                            })
                        } catch (e) {
                            console.error("Make sure you have included videojs")
                        } else !s.videoLoaded && s.core.s.autoplayFirstVideo && s.core.$slide.eq(t).find(".lg-html5").get(0).play()
                }.bind(this)), t.core.$el.on("onAferAppendSlide.lg.tm", function(e, t) {
                    var o = this.core.$slide.eq(t).find(".lg-video-cont");
                    o.hasClass("lg-has-iframe") || (o.css("max-width", this.core.s.videoMaxWidth), this.videoLoaded = !0)
                }.bind(this)), t.core.doCss() && t.core.$items.length > 1 && (t.core.s.enableSwipe || t.core.s.enableDrag) ? t.core.$el.on("onSlideClick.lg.tm", function() {
                    var e = t.core.$slide.eq(t.core.index);
                    t.loadVideoOnclick(e)
                }) : t.core.$slide.on("click.lg", function() {
                    t.loadVideoOnclick(e(this))
                }), t.core.$el.on("onBeforeSlide.lg.tm", function(t, o, i) {
                    var s, l = this,
                        n = l.core.$slide.eq(o),
                        a = n.find(".lg-youtube").get(0),
                        r = n.find(".lg-vimeo").get(0),
                        d = n.find(".lg-dailymotion").get(0),
                        c = n.find(".lg-vk").get(0),
                        u = n.find(".lg-html5").get(0);
                    if (a) a.contentWindow.postMessage('{"event":"command","func":"pauseVideo","args":""}', "*");
                    else if (r) try {
                            $f(r).api("pause")
                        } catch (e) {
                            console.error("Make sure you have included froogaloop2 js")
                        } else if (d) d.contentWindow.postMessage("pause", "*");
                        else if (u)
                        if (l.core.s.videojs) try {
                            videojs(u).pause()
                        } catch (e) {
                            console.error("Make sure you have included videojs")
                        } else u.pause();
                    c && e(c).attr("src", e(c).attr("src").replace("&autoplay", "&noplay")), s = l.core.s.dynamic ? l.core.s.dynamicEl[i].src : l.core.$items.eq(i).attr("href") || l.core.$items.eq(i).attr("data-src");
                    var h = l.core.isVideo(s, i) || {};
                    (h.youtube || h.vimeo || h.dailymotion || h.vk) && l.core.$outer.addClass("lg-hide-download")
                }.bind(this)), t.core.$el.on("onAfterSlide.lg.tm", function(e, o) {
                    t.core.$slide.eq(o).removeClass("lg-video-playing")
                }), t.core.s.autoplayFirstVideo && t.core.$el.on("onAferAppendSlide.lg.tm", function(e, o) {
                    if (!t.core.lGalleryOn) {
                        var i = t.core.$slide.eq(o);
                        setTimeout(function() {
                            t.loadVideoOnclick(i)
                        }, 100)
                    }
                })
            }, o.prototype.loadVideo = function(t, o, i, s, l) {
                var n = "",
                    a = 1,
                    r = "",
                    d = this.core.isVideo(t, s) || {};
                if (i && (a = this.videoLoaded ? 0 : this.core.s.autoplayFirstVideo ? 1 : 0), d.youtube) r = "?wmode=opaque&autoplay=" + a + "&enablejsapi=1", this.core.s.youtubePlayerParams && (r = r + "&" + e.param(this.core.s.youtubePlayerParams)), n = '<iframe class="lg-video-object lg-youtube ' + o + '" width="560" height="315" src="//www.youtube.com/embed/' + d.youtube[1] + r + '" frameborder="0" allowfullscreen></iframe>';
                else if (d.vimeo) r = "?autoplay=" + a + "&api=1", this.core.s.vimeoPlayerParams && (r = r + "&" + e.param(this.core.s.vimeoPlayerParams)), n = '<iframe class="lg-video-object lg-vimeo ' + o + '" width="560" height="315"  src="//player.vimeo.com/video/' + d.vimeo[1] + r + '" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>';
                else if (d.dailymotion) r = "?wmode=opaque&autoplay=" + a + "&api=postMessage", this.core.s.dailymotionPlayerParams && (r = r + "&" + e.param(this.core.s.dailymotionPlayerParams)), n = '<iframe class="lg-video-object lg-dailymotion ' + o + '" width="560" height="315" src="//www.dailymotion.com/embed/video/' + d.dailymotion[1] + r + '" frameborder="0" allowfullscreen></iframe>';
                else if (d.html5) {
                    var c = l.substring(0, 1);
                    "." !== c && "#" !== c || (l = e(l).html()), n = l
                } else d.vk && (r = "&autoplay=" + a, this.core.s.vkPlayerParams && (r = r + "&" + e.param(this.core.s.vkPlayerParams)), n = '<iframe class="lg-video-object lg-vk ' + o + '" width="560" height="315" src="//vk.com/video_ext.php?' + d.vk[1] + r + '" frameborder="0" allowfullscreen></iframe>');
                return n
            }, o.prototype.loadVideoOnclick = function(e) {
                var t = this;
                if (e.find(".lg-object").hasClass("lg-has-poster") && e.find(".lg-object").is(":visible"))
                    if (e.hasClass("lg-has-video")) {
                        var o = e.find(".lg-youtube").get(0),
                            i = e.find(".lg-vimeo").get(0),
                            s = e.find(".lg-dailymotion").get(0),
                            l = e.find(".lg-html5").get(0);
                        if (o) o.contentWindow.postMessage('{"event":"command","func":"playVideo","args":""}', "*");
                        else if (i) try {
                                $f(i).api("play")
                            } catch (e) {
                                console.error("Make sure you have included froogaloop2 js")
                            } else if (s) s.contentWindow.postMessage("play", "*");
                            else if (l)
                            if (t.core.s.videojs) try {
                                videojs(l).play()
                            } catch (e) {
                                console.error("Make sure you have included videojs")
                            } else l.play();
                        e.addClass("lg-video-playing")
                    } else {
                        e.addClass("lg-video-playing lg-has-video");
                        var n = function(o, i) {
                            if (e.find(".lg-video").append(t.loadVideo(o, "", !1, t.core.index, i)), i)
                                if (t.core.s.videojs) try {
                                    videojs(t.core.$slide.eq(t.core.index).find(".lg-html5").get(0), t.core.s.videojsOptions, function() {
                                        this.play()
                                    })
                                } catch (e) {
                                    console.error("Make sure you have included videojs")
                                } else t.core.$slide.eq(t.core.index).find(".lg-html5").get(0).play()
                        };
                        t.core.s.dynamic ? n(t.core.s.dynamicEl[t.core.index].src, t.core.s.dynamicEl[t.core.index].html) : n(t.core.$items.eq(t.core.index).attr("href") || t.core.$items.eq(t.core.index).attr("data-src"), t.core.$items.eq(t.core.index).attr("data-html"));
                        var a = e.find(".lg-object");
                        e.find(".lg-video").append(a), e.find(".lg-video-object").hasClass("lg-html5") || (e.removeClass("lg-complete"), e.find(".lg-video-object").on("load.lg error.lg", function() {
                            e.addClass("lg-complete")
                        }))
                    }
            }, o.prototype.destroy = function() {
                this.videoLoaded = !1
            }, e.fn.lightGallery.modules.video = o
        }()
    }),
    function(e, t) {
        "function" == typeof define && define.amd ? define(["jquery"], function(e) {
            return t(e)
        }) : "object" == typeof exports ? module.exports = t(require("jquery")) : t(jQuery)
    }(0, function(e) {
        ! function() {
            "use strict";
            var t = {
                    scale: 1,
                    zoom: !0,
                    actualSize: !0,
                    enableZoomAfter: 300,
                    useLeftForZoom: function() {
                        var e = !1,
                            t = navigator.userAgent.match(/Chrom(e|ium)\/([0-9]+)\./);
                        return t && parseInt(t[2], 10) < 54 && (e = !0), e
                    }()
                },
                o = function(o) {
                    return this.core = e(o).data("lightGallery"), this.core.s = e.extend({}, t, this.core.s), this.core.s.zoom && this.core.doCss() && (this.init(), this.zoomabletimeout = !1, this.pageX = e(window).width() / 2, this.pageY = e(window).height() / 2 + e(window).scrollTop()), this
                };
            o.prototype.init = function() {
                var t = this,
                    o = '<span id="lg-zoom-in" class="lg-icon"></span><span id="lg-zoom-out" class="lg-icon"></span>';
                t.core.s.actualSize && (o += '<span id="lg-actual-size" class="lg-icon"></span>'), t.core.s.useLeftForZoom ? t.core.$outer.addClass("lg-use-left-for-zoom") : t.core.$outer.addClass("lg-use-transition-for-zoom"), this.core.$outer.find(".lg-toolbar").append(o), t.core.$el.on("onSlideItemLoad.lg.tm.zoom", function(o, i, s) {
                    var l = t.core.s.enableZoomAfter + s;
                    e("body").hasClass("lg-from-hash") && s ? l = 0 : e("body").removeClass("lg-from-hash"), t.zoomabletimeout = setTimeout(function() {
                        t.core.$slide.eq(i).addClass("lg-zoomable")
                    }, l + 30)
                });
                var i = 1,
                    s = function(o) {
                        var i = t.core.$outer.find(".lg-current .lg-image"),
                            s = (e(window).width() - i.prop("offsetWidth")) / 2,
                            l = (e(window).height() - i.prop("offsetHeight")) / 2 + e(window).scrollTop(),
                            n = (o - 1) * (t.pageX - s),
                            a = (o - 1) * (t.pageY - l);
                        i.css("transform", "scale3d(" + o + ", " + o + ", 1)").attr("data-scale", o), t.core.s.useLeftForZoom ? i.parent().css({
                            left: -n + "px",
                            top: -a + "px"
                        }).attr("data-x", n).attr("data-y", a) : i.parent().css("transform", "translate3d(-" + n + "px, -" + a + "px, 0)").attr("data-x", n).attr("data-y", a)
                    },
                    l = function() {
                        i > 1 ? t.core.$outer.addClass("lg-zoomed") : t.resetZoom(), i < 1 && (i = 1), s(i)
                    },
                    n = function(o, s, n, a) {
                        var r, d = s.prop("offsetWidth");
                        r = t.core.s.dynamic ? t.core.s.dynamicEl[n].width || s[0].naturalWidth || d : t.core.$items.eq(n).attr("data-width") || s[0].naturalWidth || d, t.core.$outer.hasClass("lg-zoomed") ? i = 1 : r > d && (i = r / d || 2), a ? (t.pageX = e(window).width() / 2, t.pageY = e(window).height() / 2 + e(window).scrollTop()) : (t.pageX = o.pageX || o.originalEvent.targetTouches[0].pageX, t.pageY = o.pageY || o.originalEvent.targetTouches[0].pageY), l(), setTimeout(function() {
                            t.core.$outer.removeClass("lg-grabbing").addClass("lg-grab")
                        }, 10)
                    },
                    a = !1;
                t.core.$el.on("onAferAppendSlide.lg.tm.zoom", function(e, o) {
                    var i = t.core.$slide.eq(o).find(".lg-image");
                    i.on("dblclick", function(e) {
                        n(e, i, o)
                    }), i.on("touchstart", function(e) {
                        a ? (clearTimeout(a), a = null, n(e, i, o)) : a = setTimeout(function() {
                            a = null
                        }, 300), e.preventDefault()
                    })
                }), e(window).on("resize.lg.zoom scroll.lg.zoom orientationchange.lg.zoom", function() {
                    t.pageX = e(window).width() / 2, t.pageY = e(window).height() / 2 + e(window).scrollTop(), s(i)
                }), e("#lg-zoom-out").on("click.lg", function() {
                    t.core.$outer.find(".lg-current .lg-image").length && (i -= t.core.s.scale, l())
                }), e("#lg-zoom-in").on("click.lg", function() {
                    t.core.$outer.find(".lg-current .lg-image").length && (i += t.core.s.scale, l())
                }), e("#lg-actual-size").on("click.lg", function(e) {
                    n(e, t.core.$slide.eq(t.core.index).find(".lg-image"), t.core.index, !0)
                }), t.core.$el.on("onBeforeSlide.lg.tm", function() {
                    i = 1, t.resetZoom()
                }), t.zoomDrag(), t.zoomSwipe()
            }, o.prototype.resetZoom = function() {
                this.core.$outer.removeClass("lg-zoomed"), this.core.$slide.find(".lg-img-wrap").removeAttr("style data-x data-y"), this.core.$slide.find(".lg-image").removeAttr("style data-scale"), this.pageX = e(window).width() / 2, this.pageY = e(window).height() / 2 + e(window).scrollTop()
            }, o.prototype.zoomSwipe = function() {
                var e = this,
                    t = {},
                    o = {},
                    i = !1,
                    s = !1,
                    l = !1;
                e.core.$slide.on("touchstart.lg", function(o) {
                    if (e.core.$outer.hasClass("lg-zoomed")) {
                        var i = e.core.$slide.eq(e.core.index).find(".lg-object");
                        l = i.prop("offsetHeight") * i.attr("data-scale") > e.core.$outer.find(".lg").height(), ((s = i.prop("offsetWidth") * i.attr("data-scale") > e.core.$outer.find(".lg").width()) || l) && (o.preventDefault(), t = {
                            x: o.originalEvent.targetTouches[0].pageX,
                            y: o.originalEvent.targetTouches[0].pageY
                        })
                    }
                }), e.core.$slide.on("touchmove.lg", function(n) {
                    if (e.core.$outer.hasClass("lg-zoomed")) {
                        var a, r, d = e.core.$slide.eq(e.core.index).find(".lg-img-wrap");
                        n.preventDefault(), i = !0, o = {
                            x: n.originalEvent.targetTouches[0].pageX,
                            y: n.originalEvent.targetTouches[0].pageY
                        }, e.core.$outer.addClass("lg-zoom-dragging"), r = l ? -Math.abs(d.attr("data-y")) + (o.y - t.y) : -Math.abs(d.attr("data-y")), a = s ? -Math.abs(d.attr("data-x")) + (o.x - t.x) : -Math.abs(d.attr("data-x")), (Math.abs(o.x - t.x) > 15 || Math.abs(o.y - t.y) > 15) && (e.core.s.useLeftForZoom ? d.css({
                            left: a + "px",
                            top: r + "px"
                        }) : d.css("transform", "translate3d(" + a + "px, " + r + "px, 0)"))
                    }
                }), e.core.$slide.on("touchend.lg", function() {
                    e.core.$outer.hasClass("lg-zoomed") && i && (i = !1, e.core.$outer.removeClass("lg-zoom-dragging"), e.touchendZoom(t, o, s, l))
                })
            }, o.prototype.zoomDrag = function() {
                var t = this,
                    o = {},
                    i = {},
                    s = !1,
                    l = !1,
                    n = !1,
                    a = !1;
                t.core.$slide.on("mousedown.lg.zoom", function(i) {
                    var l = t.core.$slide.eq(t.core.index).find(".lg-object");
                    a = l.prop("offsetHeight") * l.attr("data-scale") > t.core.$outer.find(".lg").height(), n = l.prop("offsetWidth") * l.attr("data-scale") > t.core.$outer.find(".lg").width(), t.core.$outer.hasClass("lg-zoomed") && e(i.target).hasClass("lg-object") && (n || a) && (i.preventDefault(), o = {
                        x: i.pageX,
                        y: i.pageY
                    }, s = !0, t.core.$outer.scrollLeft += 1, t.core.$outer.scrollLeft -= 1, t.core.$outer.removeClass("lg-grab").addClass("lg-grabbing"))
                }), e(window).on("mousemove.lg.zoom", function(e) {
                    if (s) {
                        var r, d, c = t.core.$slide.eq(t.core.index).find(".lg-img-wrap");
                        l = !0, i = {
                            x: e.pageX,
                            y: e.pageY
                        }, t.core.$outer.addClass("lg-zoom-dragging"), d = a ? -Math.abs(c.attr("data-y")) + (i.y - o.y) : -Math.abs(c.attr("data-y")), r = n ? -Math.abs(c.attr("data-x")) + (i.x - o.x) : -Math.abs(c.attr("data-x")), t.core.s.useLeftForZoom ? c.css({
                            left: r + "px",
                            top: d + "px"
                        }) : c.css("transform", "translate3d(" + r + "px, " + d + "px, 0)")
                    }
                }), e(window).on("mouseup.lg.zoom", function(e) {
                    s && (s = !1, t.core.$outer.removeClass("lg-zoom-dragging"), !l || o.x === i.x && o.y === i.y || (i = {
                        x: e.pageX,
                        y: e.pageY
                    }, t.touchendZoom(o, i, n, a)), l = !1), t.core.$outer.removeClass("lg-grabbing").addClass("lg-grab")
                })
            }, o.prototype.touchendZoom = function(e, t, o, i) {
                var s = this,
                    l = s.core.$slide.eq(s.core.index).find(".lg-img-wrap"),
                    n = s.core.$slide.eq(s.core.index).find(".lg-object"),
                    a = -Math.abs(l.attr("data-x")) + (t.x - e.x),
                    r = -Math.abs(l.attr("data-y")) + (t.y - e.y),
                    d = (s.core.$outer.find(".lg").height() - n.prop("offsetHeight")) / 2,
                    c = Math.abs(n.prop("offsetHeight") * Math.abs(n.attr("data-scale")) - s.core.$outer.find(".lg").height() + d),
                    u = (s.core.$outer.find(".lg").width() - n.prop("offsetWidth")) / 2,
                    h = Math.abs(n.prop("offsetWidth") * Math.abs(n.attr("data-scale")) - s.core.$outer.find(".lg").width() + u);
                (Math.abs(t.x - e.x) > 15 || Math.abs(t.y - e.y) > 15) && (i && (r <= -c ? r = -c : r >= -d && (r = -d)), o && (a <= -h ? a = -h : a >= -u && (a = -u)), i ? l.attr("data-y", Math.abs(r)) : r = -Math.abs(l.attr("data-y")), o ? l.attr("data-x", Math.abs(a)) : a = -Math.abs(l.attr("data-x")), s.core.s.useLeftForZoom ? l.css({
                    left: a + "px",
                    top: r + "px"
                }) : l.css("transform", "translate3d(" + a + "px, " + r + "px, 0)"))
            }, o.prototype.destroy = function() {
                var t = this;
                t.core.$el.off(".lg.zoom"), e(window).off(".lg.zoom"), t.core.$slide.off(".lg.zoom"), t.core.$el.off(".lg.tm.zoom"), t.resetZoom(), clearTimeout(t.zoomabletimeout), t.zoomabletimeout = !1
            }, e.fn.lightGallery.modules.zoom = o
        }()
    }),
    function(e, t) {
        "function" == typeof define && define.amd ? define(["jquery"], function(e) {
            return t(e)
        }) : "object" == typeof exports ? module.exports = t(require("jquery")) : t(jQuery)
    }(0, function(e) {
        ! function() {
            "use strict";
            var t = {
                    hash: !0
                },
                o = function(o) {
                    return this.core = e(o).data("lightGallery"), this.core.s = e.extend({}, t, this.core.s), this.core.s.hash && (this.oldHash = window.location.hash, this.init()), this
                };
            o.prototype.init = function() {
                var t, o = this;
                o.core.$el.on("onAfterSlide.lg.tm", function(e, t, i) {
                    history.replaceState ? history.replaceState(null, null, window.location.pathname + window.location.search + "#lg=" + o.core.s.galleryId + "&slide=" + i) : window.location.hash = "lg=" + o.core.s.galleryId + "&slide=" + i
                }), e(window).on("hashchange.lg.hash", function() {
                    t = window.location.hash;
                    var e = parseInt(t.split("&slide=")[1], 10);
                    t.indexOf("lg=" + o.core.s.galleryId) > -1 ? o.core.slide(e, !1, !1) : o.core.lGalleryOn && o.core.destroy()
                })
            }, o.prototype.destroy = function() {
                this.core.s.hash && (this.oldHash && this.oldHash.indexOf("lg=" + this.core.s.galleryId) < 0 ? history.replaceState ? history.replaceState(null, null, this.oldHash) : window.location.hash = this.oldHash : history.replaceState ? history.replaceState(null, document.title, window.location.pathname + window.location.search) : window.location.hash = "", this.core.$el.off(".lg.hash"))
            }, e.fn.lightGallery.modules.hash = o
        }()
    }),
    function(e, t) {
        "function" == typeof define && define.amd ? define(["jquery"], function(e) {
            return t(e)
        }) : "object" == typeof exports ? module.exports = t(require("jquery")) : t(jQuery)
    }(0, function(e) {
        ! function() {
            "use strict";
            var t = {
                    share: !0,
                    facebook: !0,
                    facebookDropdownText: "Facebook",
                    twitter: !0,
                    twitterDropdownText: "Twitter",
                    googlePlus: !0,
                    googlePlusDropdownText: "GooglePlus",
                    pinterest: !0,
                    pinterestDropdownText: "Pinterest"
                },
                o = function(o) {
                    return this.core = e(o).data("lightGallery"), this.core.s = e.extend({}, t, this.core.s), this.core.s.share && this.init(), this
                };
            o.prototype.init = function() {
                var t = this,
                    o = '<span id="lg-share" class="lg-icon"><ul class="lg-dropdown" style="position: absolute;">';
                o += t.core.s.facebook ? '<li><a id="lg-share-facebook" target="_blank"><span class="lg-icon"></span><span class="lg-dropdown-text">' + this.core.s.facebookDropdownText + "</span></a></li>" : "", o += t.core.s.twitter ? '<li><a id="lg-share-twitter" target="_blank"><span class="lg-icon"></span><span class="lg-dropdown-text">' + this.core.s.twitterDropdownText + "</span></a></li>" : "", o += t.core.s.googlePlus ? '<li><a id="lg-share-googleplus" target="_blank"><span class="lg-icon"></span><span class="lg-dropdown-text">' + this.core.s.googlePlusDropdownText + "</span></a></li>" : "", o += t.core.s.pinterest ? '<li><a id="lg-share-pinterest" target="_blank"><span class="lg-icon"></span><span class="lg-dropdown-text">' + this.core.s.pinterestDropdownText + "</span></a></li>" : "", o += "</ul></span>", this.core.$outer.find(".lg-toolbar").append(o), this.core.$outer.find(".lg").append('<div id="lg-dropdown-overlay"></div>'), e("#lg-share").on("click.lg", function() {
                    t.core.$outer.toggleClass("lg-dropdown-active")
                }), e("#lg-dropdown-overlay").on("click.lg", function() {
                    t.core.$outer.removeClass("lg-dropdown-active")
                }), t.core.$el.on("onAfterSlide.lg.tm", function(o, i, s) {
                    setTimeout(function() {
                        e("#lg-share-facebook").attr("href", "https://www.facebook.com/sharer/sharer.php?u=" + encodeURIComponent(t.getSahreProps(s, "facebookShareUrl") || window.location.href)), e("#lg-share-twitter").attr("href", "https://twitter.com/intent/tweet?text=" + t.getSahreProps(s, "tweetText") + "&url=" + encodeURIComponent(t.getSahreProps(s, "twitterShareUrl") || window.location.href)), e("#lg-share-googleplus").attr("href", "https://plus.google.com/share?url=" + encodeURIComponent(t.getSahreProps(s, "googleplusShareUrl") || window.location.href)), e("#lg-share-pinterest").attr("href", "http://www.pinterest.com/pin/create/button/?url=" + encodeURIComponent(t.getSahreProps(s, "pinterestShareUrl") || window.location.href) + "&media=" + encodeURIComponent(t.getSahreProps(s, "src")) + "&description=" + t.getSahreProps(s, "pinterestText"))
                    }, 100)
                })
            }, o.prototype.getSahreProps = function(e, t) {
                var o = "";
                if (this.core.s.dynamic) o = this.core.s.dynamicEl[e][t];
                else {
                    var i = this.core.$items.eq(e).attr("href"),
                        s = this.core.$items.eq(e).data(t);
                    o = "src" === t && i || s
                }
                return o
            }, o.prototype.destroy = function() {}, e.fn.lightGallery.modules.share = o
        }()
    }), $(document).ready(function() {
        $(".lightgallery").lightGallery()
    });