       
@include('frontend.includes.header')
<!-- ****Banner Section**** -->
<section class="innerbanner" style="background: url({{asset('public/frontend/images/aboutbanner.jpg')}}) no-repeat center top;">
    <div class="breadcrmb">
        <div class="wid">
            <h2>Services</h2>
            <div class="brdcrmb">
                <a href="{{route('index')}}">Home</a><span>/</span>Contact Us<span>/</span><small>Locations</small>
            </div>
        </div>
    </div>
</section>


<!-- Inner page content section -->

<section class="locationss">
    <div class="wid">
            <div id="verticalTab">  
                <ul class="resp-tabs-list">
                @foreach($locations as $location)
                    <li>{{$location->name}}</li>
                    
                    @endforeach
                </ul>
                <div class="resp-tabs-container">
                        <?php $count = 1 ;?>
                @foreach($locations as $location)

                    <div>
                        <div class="locdet">
                    <h3>
                        Address
                    </h3>
                    <p>
                        TUV NORD Training Center<br>
                        {{$location->building}},<br>
                        {{$location->addressline1}},<br>
                        {{$location->addressline2}}<br>
                        {{$location->phone}}<br>
                        {{$location->state}},<br>
                        {{$location->country}}
                    </p>
                        </div>
                    <div class="locmap">
                            <h2>
                                    Map
                                </h2>
                              
                            <div id="map_canvas{{$count}}" style="width:100%; height:250px; margin-left:80px;"></div>
                            <?php ++$count ;?> 
                            {{-- <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3632.6140067010747!2d54.43398331446904!3d24.429473868412817!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3e5e684bfe0e8f73%3A0x65c4d88e1ea42b0f!2sTUV+NORD+Middle+East!5e0!3m2!1sen!2sin!4v1564423733196!5m2!1sen!2sin" width="100%" height="250" frameborder="0" style="border:0" allowfullscreen></iframe> --}}
                    </div>
                    </div>
                    @endforeach
                </div>
            </div>
    </div>
</section>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDnUbzrV5PzdJzVHuXrWE715wkq4r0RvjQ&callback=initMap"
    async defer></script>
<script type="text/javascript">
    var map1, map2;
    
    function initMap(condition) {
        // create the maps
        var uluru1 = {lat: 24.429559, lng: 54.436169};
        var uluru2 = {lat: 25.350452, lng: 55.394111};
        var uluru3 = {lat: 29.371765, lng: 47.973603};
        var uluru4 = {lat: 26.231715, lng: 50.677077};
        var uluru5 = {lat: 23.597853, lng: 58.539464};

        var myOptions = {
            zoom: 14,
            center: new google.maps.LatLng(24.429559, 54.436169),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }
        map1 = new google.maps.Map(document.getElementById("map_canvas1"), myOptions);
        var marker = new google.maps.Marker({position: uluru1, map: map1});
    
        var myOptions2 = {
            zoom: 14,
            center: new google.maps.LatLng(25.350452, 55.394111),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }

        map2 = new google.maps.Map(document.getElementById("map_canvas2"), myOptions2);
        var marker2 = new google.maps.Marker({position: uluru2, map: map2});

        var myOptions3 = {
            zoom: 14,
            center: new google.maps.LatLng(29.371765, 47.973603),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }

        map3 = new google.maps.Map(document.getElementById("map_canvas3"), myOptions3);
        var marker3 = new google.maps.Marker({position: uluru3, map: map3});


        var myOptions4 = {
            zoom: 14,
            center: new google.maps.LatLng(26.231715, 50.677077),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }

        map4 = new google.maps.Map(document.getElementById("map_canvas4"), myOptions4);
        var marker4 = new google.maps.Marker({position: uluru4, map: map4});


        var myOptions5 = {
            zoom: 14,
            center: new google.maps.LatLng(23.597853, 58.539464),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }

        map5 = new google.maps.Map(document.getElementById("map_canvas5"), myOptions5);
        var marker5 = new google.maps.Marker({position: uluru5, map: map5});
    }
    </script> 




<script type="text/javascript" src="{{ asset('public/frontend/js/easy-responsive-tabs.js') }}"></script>
<script>
        $(document).ready(function () {
        $('#horizontalTab').easyResponsiveTabs({
        type: 'default', //Types: default, vertical, accordion           
        width: 'auto', //auto or any width like 600px
        fit: true,   // 100% fit in a container
        closed: 'accordion', // Start closed if in accordion view
        activate: function(event) { // Callback function if tab is switched
        var $tab = $(this);
        var $info = $('#tabInfo');
        var $name = $('span', $info);
        $name.text($tab.text());
        $info.show();
        }
        });
        $('#verticalTab').easyResponsiveTabs({
        type: 'vertical',
        width: 'auto',
        fit: true
        });
        });
        </script>
@include('frontend.includes.footer')